//
//  RMTheme.swift
//  Repairman
//
//  Created by Shebin Koshy on 03/03/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMTheme: NSObject {
    
    class func cellCornerRadius(cell:UITableViewCell, borderColor:UIColor, backgroundColor:UIColor) {
        let v = cell.contentView.viewWithTag(3000)
        if v != nil {
//            v!.backgroundColor = backgroundColor
//            v!.layer.borderColor = borderColor.cgColor
//            return;
            v!.removeFromSuperview()
        }
        let view = UIView(frame: CGRect(x:1,y:1,width:cell.frame.size.width - 2,height:cell.frame.size.height - 2))
        view.tag = 3000
        view.backgroundColor = backgroundColor
        view.layer.cornerRadius = 5
        view.layer.borderColor = borderColor.cgColor
        view.layer.borderWidth = 1
        cell.contentView.insertSubview(view, at: 0)
    }

}

private let swizzling: (AnyClass, Selector, Selector) -> () = { forClass, originalSelector, swizzledSelector in
    let originalMethod = class_getInstanceMethod(forClass, originalSelector)
    let swizzledMethod = class_getInstanceMethod(forClass, swizzledSelector)
    method_exchangeImplementations(originalMethod!, swizzledMethod!)
}


//extension NSCalendar {
//    static func swizzleGetter(){
//        guard self === NSCalendar.self else {
//            return
//        }
//        
//        let originalSelector = Selector("timezone")
//        let swizzledSelector = #selector(swizzled_Timezone)
//        swizzling(NSCalendar.self, originalSelector, swizzledSelector)
//    }
//    
//    @objc func swizzled_Timezone() -> TimeZone{
//                swizzled_Timezone()
//                print("swizzled_layoutSubviews")
//        return .none
//        //        view.backgroundColor = UIColor.black
//        //    }
//}

//extension UIViewController {
//    
//    static func swizzleViewDidLoad(){
//        guard self === UIViewController.self else { return }
//        let originalSelector = #selector(viewDidLoad)
//        let swizzledSelector = #selector(swizzled_viewDidLoad)
//        swizzling(UIViewController.self, originalSelector, swizzledSelector)
//    }
//    
//    @objc func swizzled_viewDidLoad() {
//        swizzled_viewDidLoad()
//        print("swizzled_layoutSubviews")
//        view.backgroundColor = UIColor.black
//    }
//    
//}

//extension UIButton {
//}



