//
//  RMTimesheet.swift
//  Repairman
//
//  Created by Shebin Koshy on 04/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMTimesheet: NSObject {
    
//    "id": "5",
//    "project_id": "1",
//    "user_id": "2",
//    "start_time": "2017-12-14 16:53:38",
//    "end_time": "2017-12-14 16:53:46",
//    "status": "logged",
//    "note": "",
//    "task_id": "2",
//    "deleted": "0",
//    "logged_by_user": "Rahul Raj",
//    "logged_by_avatar": "_file5a642d7a4d8bb-avatar.png",
//    "task_title": "Sample task",
//    "project_title": "Test project"
    
    var timesheetUniqueId:String?
    var timesheetStartTime:String?
    var timesheetEndTime:String?
    var timesheetStatus:String?
    var timesheetNote:String?
    var timesheetTaskTitle:String?
    var timesheetUsername:String?
    var timesheetUserAvatarUrl:String?
    var timesheetTotalTime:String?
    
    func timesheetStatusString() -> String? {
        if timesheetStatus == "logged"{
            return "Logged"
        }
        if timesheetStatus == "open" {
            return "Open"
        }
        return timesheetStatus
    }
    
    
    func timesheetStatusColor() -> UIColor {
        if timesheetStatus == nil {
            
            return UIColor.black
        }
        if timesheetStatus == "open" {
            return UIColor(red: 233/255, green: 176/255, blue: 96/255, alpha: 1)
            //            return UIColor.orange
        }
        if timesheetStatus == "logged" {
            return UIColor(red: 76/255, green: 176/255, blue: 147/255, alpha: 1)
            //            return UIColor.green
        }
        return UIColor.black
    }

}
