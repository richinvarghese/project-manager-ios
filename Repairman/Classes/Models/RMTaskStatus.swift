//
//  RMTaskStatus.swift
//  Repairman
//
//  Created by Shebin Koshy on 26/01/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMTaskStatus: NSObject {

    
    var taskStatusUniqueId: String?
    var taskStatusTitle: String?
    var taskStatusKeyName: String?
    var taskStatusColor: String?
    var taskStatusSort: String?
//    var taskStatusDelete: String?
    
}
