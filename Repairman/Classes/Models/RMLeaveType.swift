//
//  RMLeaveType.swift
//  Repairman
//
//  Created by Shebin Koshy on 24/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMLeaveType: NSObject {
    
    var uniqueId: String?
    var leaveTypeTitle: String?
    var leaveTypeStatus: String?
    var leaveTypeColor: String?
    var leaveTypeDescription: String?
    var leaveTypeDeleted: String?

//    "id": "1",
//    "title": "Full Day",
//    "status": "active",
//    "color": "#83c340",
//    "description": "",
//    "deleted": "0"
    
    public class func leaveForDict(dictLeaveType:Dictionary<String,Any?>) -> RMLeaveType {
        let leaveType = RMLeaveType()
        leaveType.uniqueId = dictLeaveType["id"] as? String
        leaveType.leaveTypeTitle = dictLeaveType["title"] as? String
        leaveType.leaveTypeStatus =  dictLeaveType["status"] as? String
        leaveType.leaveTypeColor = dictLeaveType["color"] as? String
        //        leave.leaveTotalHours = dictLeave["total_hours"] as? String
        leaveType.leaveTypeDescription = dictLeaveType["description"] as? String
        leaveType.leaveTypeDeleted = dictLeaveType["deleted"] as? String
        return leaveType
    }
}
