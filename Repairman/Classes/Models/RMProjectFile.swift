//
//  RMProjectFile.swift
//  Repairman
//
//  Created by Shebin Koshy on 26/01/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMProjectFile: NSObject {

    
    var projectFileUniqueId: String?
    var projectFileImageUrl: String?
    var projectFileDescription: String?
    var projectFileCreatedAt: String?
    var projectUniqueId: String?
    var fileUploadedById: String?
    var fileUploadedByUsername: String?
    var fileUploadedByUserImageUrl: String?
    var fileUploadedByUserType: String?
    
}
