//
//  RMAnnouncement.swift
//  Repairman
//
//  Created by Shebin Koshy on 05/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMAnnouncement: NSObject {

    var announcementTitle: String?
    var announcementCreatedByUsername:String?
    var announcementStartDate: String?
    var announcementEndDate: String?
    var announcementDescription: String?
    var accouncementAvatarImageURL:String?
    
}
