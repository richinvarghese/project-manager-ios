//
//  RMTask.swift
//  Repairman
//
//  Created by Shebin Koshy on 06/01/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMTask: NSObject {
    
//    public enum TaskStatusType: Int {
//        case ToDo = 0
//        case InProgress = 1
//        case Done = 2
//    }
//
//    public class func arrayTaskStatusTypes() -> [String] {
//        return ["To Do", "In Progress", "Done"]
//    }
    
    var taskTitle:String?
    var taskDescription:String?
    var taskStatus:String?
    var taskStatusKeyName:String?
    var taskStatusHexColor: String?
    var taskUniqueId:String?
//    var taskStatusType:TaskStatusType?
    var taskAssignedUser: String?
    var taskAssignedUserAvatarImageUrl: String?
    func taskStatusColor() -> UIColor {
        
        return UIColor.colorWithHexString(hex: taskStatusHexColor!)
        
//        return colo
//        return UIColor(rgb: Int(taskStatusHexColor!)!);
        
//        if taskStatusType == .NotStarted {
//            return UIColor.blue
//        }
//        else if taskStatusType == .InProgress {
//            return UIColor.orange
//        }
//        else if taskStatusType == .D {
//            return UIColor.green
//        }
//        return UIColor.black
    }
    
    
//    private func rgbColor(rgb: Int) -> UIColor {
//
//        return rgb(red: (rgb >> 16) & 0xFF, green: (rgb >> 8) & 0xFF, blue: rgb & 0xFF)
//
////        let color = rgb(red: (rgb >> 16) & 0xFF, green: (rgb >> 8) & 0xFF, blue: rgb & 0xFF)
////        return color
//    }
//
//    private func rgb(red: Int, green: Int, blue: Int) -> UIColor {
//    assert(red >= 0 && red <= 255, "Invalid red component")
//    assert(green >= 0 && green <= 255, "Invalid green component")
//    assert(blue >= 0 && blue <= 255, "Invalid blue component")
//
//    let color = UIColor(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
//        return color
//    }
    
    
//    func colorWithHexString (hex:String) -> UIColor {
//
//
//        let hex = hex.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
//        var int = UInt32()
//        Scanner(string: hex).scanHexInt32(&int)
//        let a, r, g, b: UInt32
//        switch hex.characters.count {
//        case 3: // RGB (12-bit)
//            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
//        case 6: // RGB (24-bit)
//            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
//        case 8: // ARGB (32-bit)
//            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
//        default:
//            return .clear
//        }
//        return UIColor(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
//
////        var cString:String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
//////        var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).uppercaseString
////
////        if (cString.hasPrefix("#")) {
////            let index = cString.index(of: "#")!
////            let substr = cString.prefix(upTo: index)
////            cString = String(substr)
//////            let indexStartOfText = cString.index(cString.startIndex, offsetBy: 1)
//////            cString = cString.SUB
//////            cString = cString.substringFromIndex(1)
////        }
////
////        if (countElements(cString) != 6) {
////            return UIColor.gray
////        }
////
////        var rString = cString.substringToIndex(2)
////        var gString = cString.substringFromIndex(2).substringToIndex(2)
////        var bString = cString.substringFromIndex(4).substringToIndex(2)
////
////        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
////        NSScanner.scannerWithString(rString).scanHexInt(&r)
////        NSScanner.scannerWithString(gString).scanHexInt(&g)
////        NSScanner.scannerWithString(bString).scanHexInt(&b)
////
////        return UIColor(red: Float(r) / 255.0, green: Float(g) / 255.0, blue: Float(b) / 255.0, alpha: Float(1))
//    }
    
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    
    class func colorWithHexString (hex:String) -> UIColor {
        
        
        let hex = hex.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            return .clear
        }
        return UIColor(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
