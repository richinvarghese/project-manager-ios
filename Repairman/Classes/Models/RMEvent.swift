
//
//  RMEvent.swift
//  Repairman
//
//  Created by Shebin Koshy on 23/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit

class RMEvent: NSObject {

    public enum EventRepeatType: Int {
        case none = 0
        case day = 1
        case week = 2
        case month = 3
        case year = 4
    }
    
    

    
    var eventUniqueId: String?
    var eventStartDate:Date?
    var eventEndDate:Date?
    var eventTitle:String?
    var eventDescription:String?
    var eventcreateById:String?
    var eventLocation:String?
    var eventClientId:String?
    var eventLabels:String?
    var eventShareWith:String?
    var eventDeleted:String?
    var eventColor:String?
    var eventRecurring:String?
    var eventRepeatEvery:Int?
    var eventRepeatType:String?
    var eventNumberOfCycles:String?
    var eventLastStartDate:String?
    var eventRecurringDates:String?
    var eventCreatedByName:String?
    var eventCreatedOwnerAvatar:String?
    var eventCompanyName:String?
//    var eventSta
    
    
//    func eventStartDateBasedOn(calendarDate:Date) -> Date {
//        let calendar = Calendar.current
//
//        var dateComponents: DateComponents? = calendar.dateComponents([.year], from: calendarDate)
//
//        let date: Date? = eventStartDateBasedOn(year: dateComponents!.year!)
//        print("eventStartDateBasedOnCal = \(RMGlobalManager.webServiceOnlyStringDateFromDate(date: date!))")
//        return date!
//    }
//
//    private func eventStartDateBasedOn(year:Int) -> Date {
//        let calendar = Calendar.current
//
//        var dateComponents: DateComponents? = calendar.dateComponents([.year], from: eventStartDate!)
//        dateComponents?.year = year
//
//        let date: Date? = calendar.date(from: dateComponents!)
//            print("eventStartDateBasedOnYear = \(RMGlobalManager.webServiceOnlyStringDateFromDate(date: date!))")
//        return date!
//    }
    
    func eventStartingDateOnlyString() -> String {
        
        if eventStartDate == nil {
            return ""
        }
        
        return RMGlobalManager.webServiceOnlyStringDateFromDate(date: eventStartDate!)
    }
    
    func repeatType() -> EventRepeatType {
        if eventRepeatType == "days" {
            return EventRepeatType.day
        }
        
        if eventRepeatType == "weeks" {
            return EventRepeatType.week
        }
        
        if eventRepeatType == "months" {
            return EventRepeatType.month
        }
        
        if eventRepeatType == "years" {
            return EventRepeatType.year
        }
        return EventRepeatType.none
    }
    
    func day() -> Int {
        let componenentCurrentDate = Calendar.current.dateComponents([.day], from: eventStartDate!)
        return componenentCurrentDate.day!
    }
    
    func weekDay() -> Int {
        let componenentCurrentDate = Calendar.current.dateComponents([.weekday], from: eventStartDate!)
        return componenentCurrentDate.weekday!
    }
    
    
    
    func month() -> Int {
        let componenentCurrentDate = Calendar.current.dateComponents([.month], from: eventStartDate!)
        return componenentCurrentDate.month!
    }
    
    func year() -> Int {
        let componenentCurrentDate = Calendar.current.dateComponents([.year], from: eventStartDate!)
        return componenentCurrentDate.year!
    }
    
    func startingTimeOfDate(date:Date) -> Date {
        let date = Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: date)!
        return date
    }
    
    func endingTimeOfDate(date:Date) -> Date {
        let date = Calendar.current.date(bySettingHour: 23, minute: 59, second: 59, of: date)!
        return date
    }
    
    func isNeedToConsider(calendarDate:Date) -> Bool{
        
        if eventStartDate == nil || eventEndDate == nil {
            return false
        }
        
//        var calendarDate1 = RMGlobalManager.utcDateRemovingTime(date: calendarDate)
//
//        if eventTitle == "may 7 to may 9" && RMGlobalManager.webServiceOnlyStringDateFromDate(date: calendarDate) == "2018-03-07"{
//            let som = RMGlobalManager.utcDateRemovingTime(date: calendarDate)
//            var calendarDateString = RMGlobalManager.webServiceOnlyStringDateFromDate(date: calendarDate)
//           var calendarDate1 = RMGlobalManager.dateFromWebServiceString(stringOnlyDate: calendarDateString)!
//
//            print("may 7 to may 9")
//        }
//        if 3<=25&&10>=25
//        if 24 >= 25 && 26<=25
        
//        var calendarDateString = RMGlobalManager.webServiceOnlyStringDateFromDate(date: calendarDate)
//        var calendarDate1 = RMGlobalManager.dateFromWebServiceString(stringOnlyDate: calendarDateString)!
//
//
//        var startDateString = RMGlobalManager.webServiceOnlyStringDateFromDate(date: eventStartDate!)
//        var startDate1 = RMGlobalManager.dateFromWebServiceString(stringOnlyDate: startDateString)!
//
//
//        var endDateString = RMGlobalManager.webServiceOnlyStringDateFromDate(date: eventEndDate!)
//        var endDate1 = RMGlobalManager.dateFromWebServiceString(stringOnlyDate: endDateString)!
        let sartingTimeOfCalendarDate = startingTimeOfDate(date: calendarDate)
        let endingTImeOfCalendarDate = endingTimeOfDate(date: calendarDate)
//        if eventStartDate! <= calendarDate && eventEndDate! >= calendarDate {
//            return true
//        }
        
        if eventStartDate! <= endingTImeOfCalendarDate && eventEndDate! >= sartingTimeOfCalendarDate {
            return true
        }
        
//        if startDate1 <= calendarDate1 && endDate1 >= calendarDate1 {
//            return true
//        }
        
//        if eventStartDate! >= calendarDate && eventEndDate! <= calendarDate {
//            return true
//        }
        
        if eventRepeatEvery == 0 {
            /**
             not repeating
             */
            return false
        }
        
        if eventNumberOfCycles != "0" {
             //FIXME: event number of cycle
            return false
        }
        
        let componenentStartDate = Calendar.current.dateComponents([.day,.weekday,.month,.year], from: eventStartDate!)
        
        let componenentEndDate = Calendar.current.dateComponents([.day,.weekday,.month,.year], from: eventEndDate!)
        
        let componenentCalendarDate = Calendar.current.dateComponents([.day,.weekday,.month,.year], from: calendarDate)
        
        
        
        if repeatType() == .day {
            if componenentStartDate.day! <= componenentCalendarDate.day! && componenentEndDate.day! >= componenentCalendarDate.day! {
                return true
            }
            return false
        }
        
        if repeatType() == .week {
            if componenentStartDate.weekday! <= componenentCalendarDate.weekday! && componenentEndDate.weekday! >= componenentCalendarDate.weekday! {
                return true
            }
            return false
        }
        
        if repeatType() == .month {
            
//            if jan <= march && jan >= march && 12<=25&&22>=25 {
//
//            }
            
//            if jan >=
            
            if componenentStartDate.month! <= componenentCalendarDate.month! && componenentEndDate.month! >= componenentCalendarDate.month! && componenentStartDate.day! <= componenentCalendarDate.day! && componenentEndDate.day! >= componenentCalendarDate.day!{
                return true
            }
            return false
        }
        
        
        if repeatType() == .year {
            if componenentStartDate.year! <= componenentCalendarDate.year! && componenentEndDate.year! >= componenentCalendarDate.year! && componenentStartDate.month! <= componenentCalendarDate.month! && componenentEndDate.month! >= componenentCalendarDate.month! && componenentStartDate.day! <= componenentCalendarDate.day! && componenentEndDate.day! >= componenentCalendarDate.day!{
                return true
            }
            return false
        }
        
        return false
    }
    
    
    
    class func eventFromDict(dict:Dictionary<String,Any>) -> RMEvent {
        
        let event = RMEvent()
        event.eventUniqueId = dict["id"] as? String
        event.eventTitle = dict["title"] as? String
        event.eventDescription = dict["description"] as? String
        let startDateString = dict["start_date"] as? String
        let endDateString = dict["end_date"] as? String
        let startTimeString = dict["start_time"] as? String
        let endTimeString = dict["end_time"] as? String
        if startDateString != nil && endDateString != nil && startTimeString != nil && endTimeString != nil {
            let startDateTimeString = "\(startDateString!) \(startTimeString!)"
            let endDateTimeString = "\(endDateString!) \(endTimeString!)"
//            event.eventStartDate = RMGlobalManager.dateFromWebServiceString(stringDate: startDateTimeString)
//            event.eventEndDate = RMGlobalManager.dateFromWebServiceString(stringDate: endDateTimeString)
            
            event.eventStartDate = RMGlobalManager.dateFromWebServiceString(stringDate: startDateTimeString)
            event.eventEndDate = RMGlobalManager.dateFromWebServiceString(stringDate: endDateTimeString)
            
            if event.eventStartDate == nil || event.eventEndDate == nil {
                print("df")
            }
            
        }
        
        
        
        //    "id": "1",
        //    "title": "Republic Day",
        //    "description": "National Holiday",
        //    "start_date": "2018-01-26",
        //    "end_date": "2018-01-26",
        //    "start_time": "00:00:00",
        //    "end_time": "00:00:00",
        
        
        event.eventcreateById = dict["created_by"] as? String
        event.eventLocation = dict["location"] as? String
        event.eventClientId = dict["client_id"] as? String
        event.eventLabels = dict["labels"] as? String
        
        //    "created_by": "1",
        //    "location": "",
        //    "client_id": "0",
        //    "labels": "",
        
        event.eventShareWith = dict["share_with"] as? String
        event.eventDeleted = dict["deleted"] as? String
        event.eventColor = dict["color"] as? String
        event.eventRecurring = dict["recurring"] as? String
        event.eventRepeatEvery = dict["repeat_every"] as? Int
        event.eventRepeatType = dict["repeat_type"] as? String
        //    "share_with": "all",
        //    "deleted": "0",
        //    "color": "#83c340",
        //    "recurring": "0",
        //    "repeat_every": "1",
        //    "repeat_type": "months",
        
        
        event.eventNumberOfCycles = dict["no_of_cycles"] as? String
//        event.eventStartDate = dict["last_start_date"] as? String
        event.eventRecurringDates = dict["recurring_dates"] as? String
        event.eventCreatedByName = dict["created_by_name"] as? String
        event.eventCreatedOwnerAvatar = dict["created_by_avatar"] as? String
        event.eventCompanyName = dict["company_name"] as? String
        //    "no_of_cycles": "0",
        //    "last_start_date": "0000-00-00",
        //    "recurring_dates": "",
        //    "created_by_name": "Vimal Karthik",
        //    "created_by_avatar": "_file5a49dcf35ff89-avatar.png",
        //    "company_name": null
        return event
    }
}
