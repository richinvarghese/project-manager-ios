//
//  RMNote.swift
//  Repairman
//
//  Created by Shebin Koshy on 19/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit

class RMNote: NSObject {

    var notePostedDate:Date?
    var noteDescription:String?
    var noteTitle:String?
    var noteUniqueId:String?
}
