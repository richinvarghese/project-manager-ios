//
//  RMProject.swift
//  Repairman
//
//  Created by Shebin Koshy on 18/01/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMProject: NSObject {
    
    public enum ProjectStatusType: Int {
        case Open = 0
        case Closed = 1
        case Completed = 2
        case None = 3
    }
    
    public class func arrayProjectStatusType() -> [String] {
        return ["open", "closed", "completed"]
    }
    
    
    
    var projectTitle: String?
    var projectDescription: String?
    var projectSartDate: String?
    var projectEndDate: String?
    var projectStatus: String?
    var projectPrice: String?
    var projectCompanyName: String?
    var projectCurrencySymbol: String?
//    var projectTotalPoints: String?
//    var projectCompletedPoints: String?
    var projectProgress: String?
    var projectUniqueId: String?
    
    
    func projectStatusType() -> ProjectStatusType{
        let index = RMProject.arrayProjectStatusType().index(of: projectStatus!)
        if index != nil {
            return ProjectStatusType(rawValue: index!)!
        }
        return ProjectStatusType.None
    }
    
    
    func projectStatusColor() -> UIColor {
        if projectStatusType() == .Closed {
            return UIColor.blue
        }
        else if projectStatusType() == .Open {
            return UIColor.orange
        }
        else if projectStatusType() == .Completed {
            return UIColor.green
        }
        return UIColor.black
    }

//    "id": "2",
//    "title": "AKSTECH Aircon Chemical Wash",
//    "description": "- Chemical Wash\n- Change motor",
//    "start_date": "2017-12-18",
//    "deadline": "2017-12-18",
//    "client_id": "2",
//    "created_date": "2017-12-18",
//    "created_by": "1",
//    "status": "open",
//    "labels": "",
//    "price": "200",
//    "starred_by": "",
//    "deleted": "0",
//    "company_name": "AKSTECH",
//    "currency_symbol": "$",
//    "total_points": "7",
//    "completed_points": null
    
}
