//
//  RMExpenseCategory.swift
//  Repairman
//
//  Created by Shebin Koshy on 24/03/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMExpenseCategory: NSObject {
    
    var uniqueId: String?
    var expenseCategoryTitle: String?
    
    public class func objectForDict(dictExpenseCategory:Dictionary<String,Any?>) -> RMExpenseCategory {
        let expenseCategory = RMExpenseCategory()
        expenseCategory.uniqueId = dictExpenseCategory["id"] as? String
        expenseCategory.expenseCategoryTitle = dictExpenseCategory["title"] as? String
        return expenseCategory
    }

}
