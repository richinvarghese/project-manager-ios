//
//  RMTimeCard.swift
//  Repairman
//
//  Created by Shebin Koshy on 05/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMTimeCard: NSObject {
    
    var timeCardInDate: String?
    var timeCardInTime: String?
    var timeCardOutDate: String?
    var timeCardOutTime: String?
    var timeCardDuration: String?
    var timeCardDateAndTimeIn: String?
    var timeCardDateAndTimeOut: String?
    

}
