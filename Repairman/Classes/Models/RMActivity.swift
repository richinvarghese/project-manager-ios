//
//  RMActivity.swift
//  Repairman
//
//  Created by Shebin Koshy on 21/01/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMActivity: NSObject {
    

//    "name": "Rahul Raj",
//    "created_at": "2017-12-14 16:51:40",
//    "action": "updated",
//    "title": "Task #2 - Sample task",
//    "previous": "To Do ",
//    "current": "Done ",
//    "priority": ""
    
    var activityTitle:String?
    private var activityAction:String?
    var activityPreviousStatus:String?
    var activityCurrentStatus:String?
    var activityPriority: String?
    var activityStatusKeyName:String?
    var activityUserName: String?
    var activityUserAvatarImageUrl: String?
    var activityCreatedDate: String?
    
    func activityActionColor() -> UIColor {
        if activityAction == nil {
            
            return UIColor.black
        }
        if activityAction == "updated" {
            return UIColor(red: 233/255, green: 176/255, blue: 96/255, alpha: 1)
//            return UIColor.orange
        }
        if activityAction == "created" {
            return UIColor(red: 76/255, green: 176/255, blue: 147/255, alpha: 1)
//            return UIColor.green
        }
        return UIColor.black
    }
    
    func currentStatusColor() -> UIColor {
        return UIColor(red: 76/255, green: 176/255, blue: 147/255, alpha: 1)
    }
    
    func isActionTypeUpdated() -> Bool {
        if activityAction == "updated" {
            return true
        }
        else {
            return false
        }
    }
    
    func setActivityAction(action:String) {
        activityAction = action
    }
    
    func getActivityAction() -> String {
        if activityAction == "created" {
            return "Added"
        }
        if isActionTypeUpdated() == true {
            return "Updated"
        }
        if activityAction == nil {
            return ""
        }
        return activityAction!
    }
}
