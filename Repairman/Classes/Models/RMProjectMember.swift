//
//  RMProjectMember.swift
//  Repairman
//
//  Created by Shebin Koshy on 20/01/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMProjectMember: NSObject {
    var memberName: String?
    var memberJobTitle: String?
    var memberAvatarImageUrl: String?
    var uniqueId:String?
}
