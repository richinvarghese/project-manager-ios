//
//  RMExpense.swift
//  Repairman
//
//  Created by Shebin Koshy on 24/03/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMExpense: NSObject {
    
//    var attachmentType:RMTimeLine.TimeLineAttachmentType?
    var uniqueId: String?
    var expenseDate: String?
    var expenseCategoryTitle: String?
    var expenseTitle: String?
    var expenseDescription: String?
    var expenseFilesLink: String?
    var expenseAmount: String?
    
    public class func objectForDict(dictExpense:Dictionary<String,Any?>) -> RMExpense {
        let expense = RMExpense()
        expense.expenseDate = dictExpense["expense_date"] as? String
        expense.expenseFilesLink = dictExpense["files_link"] as? String
        expense.expenseCategoryTitle =  dictExpense["category_title"] as? String
        expense.expenseTitle = dictExpense["title"] as? String
        expense.expenseDescription = dictExpense["description"] as? String
        expense.expenseAmount = dictExpense["amount"] as? String
        if expense.expenseFilesLink?.count == 0 {
            expense.expenseFilesLink = nil
        }
        return expense
    }
}
