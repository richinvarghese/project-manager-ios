//
//  RMMessage.swift
//  Repairman
//
//  Created by Shebin Koshy on 18/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMMessage: NSObject {

    var messageOwnerName:String?
    var messageSubject:String?
    var messageCreatedDate:Date?
    var messageOwnerAvatarImageUrl:String?
    var messageUniqueId:String?
    var messageDescription: String?
    var attachment:URL?
    //    var taskStatusType:TaskStatusType?
    var isMyOwnPost:Bool?
    var recipientUniqueId: String?
    var isReply:Bool?
    var attachmentType:RMTimeLine.TimeLineAttachmentType?
//    var recipientUniqueId:String?
    
    public class func messageForDict(dictMessage:Dictionary<String,Any?>) -> RMMessage {
        let message = RMMessage()
        message.messageUniqueId = dictMessage["id"] as? String
        message.messageOwnerName = dictMessage["user_name"] as? String
        let postedDateString = dictMessage["created_at"] as? String
        if postedDateString != nil {
            let messagePostedDate = RMGlobalManager.dateFromWebServiceString(stringDate: postedDateString!)
            message.messageCreatedDate = messagePostedDate
        }
        let stringOwnerId = dictMessage["to_user_id"] as? String //FIXME: it should be from_user_idddd
//        if Int64(stringOwnerId) == 1 {
//            timeLine.isAdminPost = true
//        } else {
//            timeLine.isAdminPost = false
//        }
        
        
//        if RMUserDefaultManager.getCurrentUserUniqueId() != nil && Int64(stringOwnerId) == Int64(RMUserDefaultManager.getCurrentUserUniqueId()!) {
        if stringOwnerId != nil {
            message.isMyOwnPost = true
        } else {
            message.isMyOwnPost = false
        }
        
        message.messageSubject = dictMessage["reply_subject"] as? String ?? ""
        if message.messageSubject!.count > 0 {
            message.isReply = true
        } else {
        
            message.isReply = false
            message.messageSubject = dictMessage["subject"] as? String
        }
        //        timeLine.timeLineOwnerImageURL = dictTimeLine["imageurl123"] as? String
        message.messageOwnerAvatarImageUrl = dictMessage["profile_pic"] as? String
        message.messageDescription = dictMessage["message"] as? String
        
//        var imageURL:URL?
//        if dictMessage["imageurl"] is String {
//            imageURL = URL(string:dictMessage["imageurl"]! as! String)
//        }
//        if imageURL != nil {
//            message.attachmentType = TimeLineAttachmentType.Photo
//            timeLine.attachment = imageURL
//        }
        //        timeLine.attachmentType = dictTimeLine["attachmentType"] as? TimeLineAttachmentType
        let stringURL = dictMessage["files"] as? String
        if stringURL != nil {
            message.attachment = URL(string:stringURL!)
        }
        
        let imageURL = dictMessage["imageurl"] as? String
        if message.attachment == nil && imageURL != nil {
            message.attachment = URL(string:imageURL!)
        }
        
        return message
    }
    
}
