//
//  RMMilestone.swift
//  Repairman
//
//  Created by Shebin Koshy on 21/01/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMMilestone: NSObject {
    var milestoneUniqueId:String?
    var milestoneTitle:String?
    var projectUniqueId:String?
    var milestoneDueDate:String?
    var milestoneDescription:String?
    var milestoneProgress:String?
}
