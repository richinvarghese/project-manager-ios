//
//  RMTimeLine.swift
//  Repairman
//
//  Created by Shebin Koshy on 16/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit

class RMTimeLine: NSObject {
    
    public enum TimeLineAttachmentType: Int {
        case Document = 1
        case Photo = 2
        case Video = 3
    }
    

    var timeLineOwnerName:String?
    var timeLinePostedDate:Date?
    var isAdminPost:Bool?
    var timeLineDescription:String?
    var timeLineOwnerImageURL:String?
    var isMyOwnPost:Bool?
//    var attachment:Data?
    var attachment:URL?
    var attachmentType:TimeLineAttachmentType?
    var isReply:Bool?
    
    public class func dictForPostingTimeLine(timeLine:RMTimeLine) -> Dictionary<String,Any> {
        var dict = Dictionary<String,Any>()
        dict["timeLineOwnerName"] = timeLine.timeLineOwnerName
        dict["timeLinePostedDate"] = timeLine.timeLinePostedDate
        dict["isAdminPost"] = timeLine.isAdminPost
        dict["timeLineDescription"] = timeLine.timeLineDescription
        dict["timeLineOwnerImageURL"] = timeLine.timeLineOwnerImageURL
        dict["isMyOwnPost"] = timeLine.isMyOwnPost
        dict["attachment"] = timeLine.attachment
        dict["attachmentType"] = timeLine.attachmentType
        return dict
    }
    
    public class func timeLineForDict(dictTimeLine:Dictionary<String,Any>) -> RMTimeLine {
        let timeLine = RMTimeLine()
        timeLine.timeLineOwnerName = "\(dictTimeLine["first_name"] as! String) \(dictTimeLine["last_name"] as! String)"
        let postedDateString = dictTimeLine["created_at"] as? String
        if postedDateString != nil {
            let timeLinePostedDate = RMGlobalManager.dateFromWebServiceString(stringDate: postedDateString!)
            timeLine.timeLinePostedDate = timeLinePostedDate
        }
        let stringOwnerId = dictTimeLine["created_by_id"] as! String
        if Int64(stringOwnerId) == 1 {
            timeLine.isAdminPost = true
        } else {
            timeLine.isAdminPost = false
        }
        
        if RMUserDefaultManager.getCurrentUserUniqueId() != nil && Int64(stringOwnerId) == Int64(RMUserDefaultManager.getCurrentUserUniqueId()!) {
            timeLine.isMyOwnPost = true
        } else {
            timeLine.isMyOwnPost = false
        }
        
        timeLine.timeLineDescription = dictTimeLine["description"] as? String
//        timeLine.timeLineOwnerImageURL = dictTimeLine["imageurl123"] as? String
        timeLine.timeLineOwnerImageURL = dictTimeLine["profile_pic"] as? String

        
        var imageURL:URL?
        if dictTimeLine["imageurl"] is String {
            imageURL = URL(string:dictTimeLine["imageurl"]! as! String)
        }
        if imageURL != nil {
            timeLine.attachmentType = TimeLineAttachmentType.Photo
            timeLine.attachment = imageURL
        }
//        timeLine.attachmentType = dictTimeLine["attachmentType"] as? TimeLineAttachmentType
        
        
        return timeLine
    }
    
}
