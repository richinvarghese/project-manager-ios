//
//  RMLeave.swift
//  Repairman
//
//  Created by Shebin Koshy on 24/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMLeave: NSObject {

//    "id": "4",
//    "start_date": "2018-01-12",
//    "end_date": "2018-01-12",
//    "total_hours": "8.00",
//    "total_days": "1.00",
//    "applicant_id": "8",
//    "status": "approved",
//    "reason": "Severe headache and throat pain",
//    "applicant_name": "Sreelakshmi U",
//    "leave_type_title": "Full Day",
//    "leave_type_color": "#83c340"
    
    var uniqueId: String?
    var leaveStartDate: String?
    var leaveEndDate: String?
    var leaveTotalHours: String?
    var leaveTotalDays: String?
    var leaveApplicantId: String?
    var leaveTypeTitle: String?
    var leaveTypeColor: String?
    var leaveStatus: String?
    var leaveApplicantName: String?
    var leaveReason: String?
//    var leaveIsCanceled: Bool?
    
    
    public class func leaveForDict(dictLeave:Dictionary<String,Any?>) -> RMLeave {
        let leave = RMLeave()
        leave.uniqueId = dictLeave["id"] as? String
        leave.leaveTotalHours = dictLeave["total_hours"] as? String
        leave.leaveStartDate =  dictLeave["start_date"] as? String
        leave.leaveEndDate = dictLeave["end_date"] as? String
//        leave.leaveTotalHours = dictLeave["total_hours"] as? String
        leave.leaveTotalDays = dictLeave["total_days"] as? String
        leave.leaveTypeTitle = dictLeave["leave_type_title"] as? String
        leave.leaveTypeColor = dictLeave["leave_type_color"] as? String
        leave.leaveApplicantId = dictLeave["applicant_id"] as? String
        leave.leaveStatus = dictLeave["status"] as? String
        
        if leave.leaveStatus != nil {
            leave.leaveStatus = leave.leaveStatus!.uppercased()
        }
        
        leave.leaveApplicantName = dictLeave["leaveApplicantName"] as? String
        leave.leaveReason = dictLeave["reason"] as? String
        return leave
    }
    
    
    func colorLeaveType() -> UIColor {
        if leaveTypeColor == nil {
            return UIColor.black
        }
        return UIColor.colorWithHexString(hex: leaveTypeColor!)
    }
    
    func colorLeaveStatus() -> UIColor {
        if leaveStatus?.lowercased() == "canceled" {
            return UIColor.brown
        }
        if leaveStatus?.lowercased() == "pending" {
            return UIColor(red: 233/255, green: 176/255, blue: 96/255, alpha: 1)
//            return UIColor.orange
        }
        if leaveStatus?.lowercased() == "approved" {
            return UIColor(red: 76/255, green: 176/255, blue: 147/255, alpha: 1)
//            return UIColor.green
        }
        if leaveStatus?.lowercased() == "rejected" {
            return UIColor.red
        }
        return UIColor.gray
    }
}
