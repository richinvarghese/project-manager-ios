//
//  RMTimesheetListTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 04/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMTimesheetListTableViewController: UITableViewController {

    var arrayToDisplay: Array<RMTimesheet>?//FIXME: temporary code
//    var delegate: RMFileListTableViewControllerDelegate?
//    var projectUniqueId:String?
    //    static let reuseIdentifierNoteCell = "RMNoteCell"
    //
    //    var selectedProjectUniqueId: String?
    var arrayTaskList: [RMTask]?
    var arrayFilter: [RMTimesheet]?
    
    weak var delegate : RMProjectOverviewTableViewControllerDelegate?
    var projectUniqueId:String?
    
    var buttonSelectTask : UIButton?
    var  buttonTimer : UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.title = RMStrings.ACTIVITIES_STRING()
        //        self.tableView.register(UITableViewCell.self)
        //        self.tableView.register(RMNoteListTableViewCell.self)
        //        let nib = UINib(nibName: "RMSubtitleTableViewCell", bundle: nil)
        //        self.tableView.register(nib, forCellReuseIdentifier: RMNoteListTableViewController.reuseIdentifierNoteCell)
        self.tableView.register(RMTimeSheetTableViewCell.self)
        self.tableView.tableFooterView = UIView()
        //        fetchNote()
//        self.tableView.estimatedRowHeight = 200
//        self.tableView.rowHeight = UITableViewAutomaticDimension
//        let viewHeader:UIView = Bundle.main.loadNibNamed("TimesheetHeader", owner: nil, options: nil)![0] as! UIView
//        viewHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 50)
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44))
         buttonSelectTask = UIButton(frame: CGRect(x: 0, y: 0, width: 140, height: 30))
         buttonTimer = UIButton(frame: CGRect(x: UIScreen.main.bounds.size.width - 170, y: 0, width: 140, height: 30))
        viewHeader.addSubview(buttonSelectTask!)
        viewHeader.addSubview(buttonTimer!)
        buttonSelectTask?.addTarget(self, action: #selector(buttonActionForSelectTask), for: .touchUpInside)
        buttonTimer?.addTarget(self, action: #selector(buttonActionForTimer), for: .touchUpInside)
        buttonSelectTask?.setTitle(selectTaskString(), for: .normal)
        
        setupButton(button: buttonSelectTask!)
        setupButton(button: buttonTimer!)
        buttonTimer!.backgroundColor = UIColor.colorWithHexString(hex: "#91b53d")
//        let v = viewHeader.viewWithTag(347)
        tableView.tableHeaderView = viewHeader
        buttonTimer?.setTitle("Timer", for: .normal)
//        tableView.tableHeaderView?.translatesAutoresizingMaskIntoConstraints = false
//        tableView.tableHeaderView.tra
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let prjectVC = self.delegate as! RMProjectViewController
        if prjectVC.isTimerStarted == true {
            buttonTimer?.setTitle("Stop Timer", for: .normal)
        } else {
            buttonTimer?.setTitle("Start Timer", for: .normal)
        }
    }
    
    func selectTaskString() -> String {
        return "Select Task"
    }
    
    
    func setupButton(button:UIButton) {
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 2
        button.clipsToBounds = true
        button.layer.borderColor =  UIColor.blue.cgColor
        button.backgroundColor = RMGlobalManager.appBlueColor()
    }
    
    //    func fetchActivi() {
    //
    //        //        RMWebServiceManager.saveNote(title: "test123", description: "test4546")
    //        //        return
    //        RMNoteListTableViewController.arrayToDisplay = nil
    //        self.tableView.reloadData()
    //
    //        RMWebServiceManager.fetchNote(projectUniqueId:selectedProjectUniqueId) {[weak self] (result:Any?, isSuccess:Bool) in
    //            if isSuccess == false {
    //                return
    //            }
    //            let notesArray = result as! [RMNote]
    //            RMNoteListTableViewController.arrayToDisplay = notesArray
    //            self?.tableView.reloadData()
    //        }
    //    }
    
    
    func updateTimerInServer(projectUniqueId:String,note:String?,taskId:String?,completion:@escaping TimerStatusCompletion) {
        let currentUserUniqueId = RMUserDefaultManager.getCurrentUserUniqueId()!
        RMWebServiceManager.timerStatus(projectUniqueId: projectUniqueId, currentUserUniqueId: currentUserUniqueId, note: note, taskId: taskId) {[weak self] (result:Any?, isSuccess:Bool) in
            if isSuccess == true && result is Dictionary<String,Any?> {
                let dict = result as! Dictionary<String,Any?>
                let stringTimerStatus = dict["timer_status"] as? String
                if stringTimerStatus?.lowercased() == "start" {
//                    self?.buttonTimer?.setTitle("Start Timer", for: .normal)
                    self?.delegate?.timerUpdated(timerIsStarted: false)
                    completion(true,false)
                } else if stringTimerStatus?.lowercased() == "stop"{
//                    self?.buttonTimer?.setTitle("Stop Timer", for: .normal)
                    self?.delegate?.timerUpdated(timerIsStarted: true)
                    completion(true,true)
                } else {
                    completion(false,nil)
                }
                
                
                
            } else {
                completion(false,nil)
            }
            
        }
    }
    
    @objc func buttonActionForTimer() {
        let prjectVC = self.delegate as! RMProjectViewController
        if prjectVC.isTimerStarted == true {
            let timernoteTableVC = RMTimerNoteTableViewController.instance()
            timernoteTableVC.arrayTaskList = prjectVC.taskList.arrayTask
            timernoteTableVC.delegate = self
            let nav = RMNavigationController(rootViewController: timernoteTableVC)
            prjectVC.navigationController?.present(nav, animated: true, completion: nil)
            return
        }
        updateTimerInServer(projectUniqueId: projectUniqueId!, note: nil, taskId: nil) { (isWebserviceSuccess, isTimerStarted) in
            
            
            
            if isTimerStarted == true {
                self.buttonTimer?.setTitle("Stop Timer", for: .normal)
//                buttonTimer!.setTitle("\u{023F1}\(RMStrings.STOP_STRING())", for: .normal)
//                buttonTimer!.setTitleColor(UIColor.red, for: .normal)
//                buttonTimer!.layer.borderColor = UIColor.red.cgColor
            } else{
                self.buttonTimer?.setTitle("Start Timer", for: .normal)
//                buttonTimer!.setTitle("\u{023F1}\(RMStrings.START_STRING())", for: .normal)
//                //            buttonTimer!.setTitleColor(UIColor(red: 0, green: 179, blue: 147), for: .normal)
//                buttonTimer!.setTitleColor(UIColor.blue, for: .normal)
//                //            buttonTimer!.layer.borderColor = UIColor(red: 0, green: 179, blue: 147).cgColor
//                buttonTimer!.layer.borderColor = UIColor.blue.cgColor
                
            }
            
        }
    }
    
    @objc func buttonActionForSelectTask() {
        let ar = arrayTaskList?.map {
            $0.taskTitle
        }
        if ar == nil {
            return;
        }
        let cancelString = "Cancel"
        RMAlert.showActionSheet(title: nil, showInViewController: self, defaultButtonNames: (ar as! [String]), destructiveButtonNames: nil, cancelButtonNames: [cancelString]) { (selectedButtonTitle) in
            if selectedButtonTitle == cancelString {
                self.buttonSelectTask?.setTitle(self.selectTaskString(), for: .normal)
                self.arrayFilter = nil
            }else {
                self.buttonSelectTask?.setTitle(selectedButtonTitle, for: .normal)
                self.arrayFilter = self.arrayToDisplay!.filter{
                    $0.timesheetTaskTitle == selectedButtonTitle
                }
            }
            
            
            
            self.tableView.reloadData()
        }
    }
    
    
    //    @objc func buttonActionForAdd() {
    //        let newNoteTableVC = RMAddNoteTableViewController.instance()
    //        newNoteTableVC.delegate = self
    //        newNoteTableVC.selectedProjectUniqueId = selectedProjectUniqueId
    //        self.navigationController?.pushViewController(newNoteTableVC, animated: true)
    //    }
    
    //    func displayingArray() -> [RMNote] {
    //        if RMNoteListTableViewController.arrayToDisplay == nil {
    //            return []
    //        }
    //
    //        return RMNoteListTableViewController.arrayToDisplay!
    //
    //    }
    
    func showingProjectFile(atIndexPath:IndexPath) -> RMTimesheet {
        //        let array = displayingArray()
        if self.arrayFilter != nil {
            return arrayFilter![atIndexPath.row]
        }
        return arrayToDisplay![atIndexPath.row]
    }
    
//    func barButtonItemAdd(navigationItem:UINavigationItem) {
//        let barButtonItemAdd = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(buttonActionForAdd))
//        navigationItem.rightBarButtonItem = barButtonItemAdd
//    }
//
//    @objc func buttonActionForAdd() {
//        let addTimeLineTableVC = RMAddTimeLineTableViewController.instance(selectedTimeLine: nil, replyToTimeLine: nil)
//        addTimeLineTableVC.delegate = self
//        addTimeLineTableVC.isForFileUpload = true
//        self.navigationController?.pushViewController(addTimeLineTableVC, animated: true)
//    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.arrayFilter != nil {
            return self.arrayFilter!.count
        }
        
        if arrayToDisplay == nil {
            return 0
        }
        return arrayToDisplay!.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let cell: RMTimeSheetTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        //        if cell == nil {
        //            cell = UITableViewCell(style: UITableViewCellStyle.value2, reuseIdentifier: "UITableViewCell")
        //        }
        
        let timesheet = showingProjectFile(atIndexPath: indexPath)
        cell.setTimesheet(timesheet: timesheet)
        //        cell!.textLabel?.text = note.noteTitle
        //        cell!.detailTextLabel?.text = RMGlobalManager.stringFromDate(date:note.notePostedDate!)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let timesheet = showingProjectFile(atIndexPath: indexPath)
        
//        if projectFile.projectFileImageUrl == nil {
//            return
//        }
//
//        let documentVC = RMDocumentViewController(url:projectFile.projectFileImageUrl!, isGoingToPresent:true)
//        //        documentVC.url
//        //        let imageViewerVC = RMImageViewerViewController.instance(imageURLToShow: URL(string:projectFile.projectFileImageUrl!)!, isGoingToPresent: true)
//        let navController = UINavigationController(rootViewController: documentVC)
//        self.present(navController, animated: true, completion: nil)
        //        let editNoteTableVC = RMAddNoteTableViewController.instance()
        //        editNoteTableVC.selectedNote = note
        //        editNoteTableVC.delegate = self
        //        editNoteTableVC.selectedProjectUniqueId = selectedProjectUniqueId
        //        self.navigationController?.pushViewController(editNoteTableVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        let projectFile = showingProjectFile(atIndexPath: indexPath)
        return 98
//        return 152
//        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
        
        RMTheme.cellCornerRadius(cell: cell, borderColor: UIColor.red, backgroundColor: UIColor.clear)
    }
    
    //    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    //        let note = showingNote(atIndexPath: indexPath)
    //        if editingStyle == .delete {
    //
    //            RMWebServiceManager.deleteNote(noteUniqueId: note.noteUniqueId!, responseHandler: {[weak self] (result:Any?, isSuccess:Bool) in
    //                RMNoteListTableViewController.arrayToDisplay?.remove(at: indexPath.row)
    //                self?.tableView.reloadData()
    //            })
    //
    //
    //        }
    //    }
    
    deinit {
        print("dealloc=RMActivityListTableViewController")
    }
    
}


extension RMTimesheetListTableViewController:RMTimerNoteTableViewControllerDelegate {
    func updateTimerInServer(timerNoteTableViewController: RMTimerNoteTableViewController, note: String?, taskUniqueId: String?, completionHandler: @escaping (Bool, Bool?) -> Void) {
        updateTimerInServer(projectUniqueId: projectUniqueId!, note: note, taskId: taskUniqueId, completion: completionHandler)
    }
}

