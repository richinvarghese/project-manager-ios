//
//  RMAddTimeLineTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 16/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit

@objc protocol RMAddTimeLineTableViewControllerDelegate: class {
    func modifiedTimeLine(addTimeLineTableVC:RMAddTimeLineTableViewController)
    
    @objc optional func timeLinePostbuttonAction(addTimeLineTableVC:RMAddTimeLineTableViewController,fileData:Data?,timeLineDescription:String, mimeType:String?,fileExtension:String?,completion:(Bool) -> Swift.Void)
}

class RMAddTimeLineTableViewController: UITableViewController {

    var isForFileUpload:Bool?
    @IBOutlet weak var textViewDesciption: UITextView!
    var labelPlaceholderDescription : UILabel?
    let lengthLimitDescription = 250
    internal var selectedTimeLine : RMTimeLine?
    private var replyToTimeLine : RMTimeLine?
    var currentAttachmentType: RMTimeLine.TimeLineAttachmentType?
    var currentAttachmentData: Data?
    var currentAttachmentURL: URL?
    weak var delegate:RMAddTimeLineTableViewControllerDelegate?
    
    @IBOutlet weak var imageViewAttachment: UIImageView!
    @IBOutlet weak var attachementCell: UITableViewCell!
    @IBOutlet weak var attachementButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    
    public class func instance(selectedTimeLine:RMTimeLine?, replyToTimeLine:RMTimeLine?) -> RMAddTimeLineTableViewController {
        let rootStoryBoard = UIStoryboard(name: "RMRoot", bundle: nil)
        let newTimeLineTableVC = rootStoryBoard.instantiateViewController(withIdentifier: "RMAddTimeLineTableViewController") as! RMAddTimeLineTableViewController
        newTimeLineTableVC.selectedTimeLine = selectedTimeLine
        newTimeLineTableVC.replyToTimeLine = replyToTimeLine
        return newTimeLineTableVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if selectedTimeLine != nil {
            textViewDesciption.text = selectedTimeLine?.timeLineDescription
            if selectedTimeLine!.attachment != nil {
                setAttachment(attachment: selectedTimeLine!.attachment!, attachmentType: selectedTimeLine!.attachmentType!)
            }
        }
        
        textViewDesciption.layer.borderWidth = 1
        textViewDesciption.layer.borderColor = UIColor.lightGray.cgColor
        textViewDesciption.delegate = self
//        let barButtonItemAttachment = UIBarButtonItem(title: RMStrings.ATTACHMENT_STRING(), style: .done, target: self, action: #selector(buttonActionForAttachment))
        if replyToTimeLine == nil {
//            let barButtonItemAttachment = UIBarButtonItem(image: UIImage(named:"AttachmentButton"), style: .done, target: self, action: #selector(buttonActionForAttachment))
//            self.navigationItem.rightBarButtonItem = barButtonItemAttachment
            
        } else {
            attachementButton.isHidden = true
        }
        
        if selectedTimeLine?.isAdminPost == false {
            self.navigationItem.rightBarButtonItem = nil
        }
        
        self.tableView.tableFooterView = UIView()
        labelPlaceholderDescription = RMGlobalManager.addPlaceholderForView(view: textViewDesciption, text: RMStrings.DESCRIPTION_STRING(), font: textViewDesciption.font!)
        if textViewDesciption.text.count > 0 {
            labelPlaceholderDescription?.isHidden = true
        }
        
        self.tableView.separatorStyle = .none
//        self.tableView.allowsSelection = false
        let barButtonItem = UIBarButtonItem(title: RMStrings.DONE_STRING(), style: .done, target: self, action: #selector(barButtonDoneAction))
        let toolbarWithBarButton = RMGlobalManager.toolbar(barButtonItem: barButtonItem)
        textViewDesciption.inputAccessoryView = toolbarWithBarButton
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction))
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        
        
        imageViewAttachment.layer.borderColor = UIColor.lightGray.cgColor
        imageViewAttachment.layer.borderWidth = 1
        
        setupUploadButton(uploadButton:attachementButton)
        setupButton(button: submitButton)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupUploadButton(uploadButton:UIButton) {
        uploadButton.layer.cornerRadius = 2.5
        uploadButton.layer.borderWidth = 1
        uploadButton.layer.borderColor = RMGlobalManager.appBlueColor().cgColor
    }
    
    func setupButton(button:UIButton) {
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 2
        button.clipsToBounds = true
        button.layer.borderColor =  UIColor.blue.cgColor
        button.backgroundColor = RMGlobalManager.appBlueColor()
    }
    
    func isChangedCurrentAttachment(attachment:AnyObject?, attachmentType:RMTimeLine.TimeLineAttachmentType?) -> Bool {
        guard let attachment = attachment else {
            currentAttachmentURL = nil
            currentAttachmentData = nil
            currentAttachmentType = nil
            return false
        }
        
        guard let attachmentType = attachmentType else {
            currentAttachmentURL = nil
            currentAttachmentData = nil
            currentAttachmentType = nil
            return false
        }
        
        currentAttachmentType = attachmentType
        return true
    }
    
    func setAttachment(attachment:URL?, attachmentType:RMTimeLine.TimeLineAttachmentType?) {
        
        if isChangedCurrentAttachment(attachment: attachment as AnyObject, attachmentType: attachmentType) == false {
            return
        }
//        currentAttachmentData = nil
        currentAttachmentURL = attachment
        
        if attachmentType == RMTimeLine.TimeLineAttachmentType.Photo {
//            let image = UIImage(data:attachment,scale:1.0)
//            imageViewAttachment.image = image
            imageViewAttachment.sd_setImage(with: attachment)
            
        }
        
        if attachmentType == RMTimeLine.TimeLineAttachmentType.Document {
            //            let image = UIImage(data:attachment,scale:1.0)
            //            imageViewAttachment.image = image
//            imageViewAttachment.sd_setImage(with: attachment)
            imageViewAttachment.image = UIImage(named:"FileIcon")
        }
        tableView.reloadData()
    }
    
    
    func setAttachment(attachment:Data?, attachmentType:RMTimeLine.TimeLineAttachmentType?) {
        
        if isChangedCurrentAttachment(attachment: attachment as AnyObject, attachmentType: attachmentType) == false {
            return
        }
        currentAttachmentData = attachment
        currentAttachmentURL = nil
        
        if attachmentType == RMTimeLine.TimeLineAttachmentType.Photo {
            let image = UIImage(data:attachment!,scale:1.0)
            imageViewAttachment.image = image
            
        }
        if attachmentType == RMTimeLine.TimeLineAttachmentType.Document {
            imageViewAttachment.image = UIImage(named:"FileIcon")
            
        }
        tableView.reloadData()
    }
    
//    public class func instance() -> RMAddNotesTableViewController {
//        let rootStoryBoard = UIStoryboard(name: "RMRoot", bundle: nil)
//        let newNoteTableVC = rootStoryBoard.instantiateViewController(withIdentifier: "RMAddNotesTableViewController") as! RMAddNotesTableViewController
//        return newNoteTableVC
//    }
    
    
    @IBAction func buttonActionForEditAttachment(_ sender: Any) {
        buttonActionForAttachment(sender)
    }
    

    @IBAction func buttonActionForDeleteAttachment(_ sender: Any) {
        RMAlert.showAlert(title: nil, message: RMStrings.CONFIRM_ATTACHMENT_DELETION_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.YES_STRING()], destructiveButtonNames: nil, cancelButtonNames: [RMStrings.NO_STRING()]) { [weak self](selectedButtonTitle:String) in
            guard let strongSelf = self else {
                return
            }
            if selectedButtonTitle == RMStrings.YES_STRING() {
                if strongSelf.currentAttachmentType == RMTimeLine.TimeLineAttachmentType.Photo {
                    strongSelf.imageViewAttachment.image = nil
                    strongSelf.tableView.reloadData()
                }
                if strongSelf.currentAttachmentType == RMTimeLine.TimeLineAttachmentType.Document {
                    strongSelf.imageViewAttachment.image = nil
                    strongSelf.tableView.reloadData()
                }
                strongSelf.currentAttachmentType = nil
                strongSelf.currentAttachmentURL = nil
            }
        }
        
    }

    
    
    
    @objc func tapGestureAction() {
        
        self.view.endEditing(true)
        
    }
    
    
    @IBAction func buttonActionPost(_ sender: Any) {
        textViewDesciption.resignFirstResponder()
        
        
        textViewDesciption.text = textViewDesciption.text.trimmingCharacters(in: .whitespacesAndNewlines)
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController:self)
            return
        }
        if textViewDesciption.text.count == 0 {
            RMAlert.showAlert(title: nil, message: RMStrings.ENTER_DESCRIPTION_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textViewDesciption.becomeFirstResponder()
            })
            return
        }

        /***
         Success
         */
        
        if selectedTimeLine != nil {
            selectedTimeLine!.timeLineDescription = textViewDesciption.text
            selectedTimeLine!.attachment = currentAttachmentURL
            selectedTimeLine!.attachmentType = currentAttachmentType
//            if currentAttachmentType == RMNote.NoteAttachmentType.Photo {
//                var documentData : Data?
////                if currentAttachmentData != nil {
////                    documentData = UIImagePNGRepresentation(imageViewAttachment.image!)!
////                }
//               selectedNote!.attachment = documentData
//            }
            
        } else {
        
//            //FIXME: Dummy data
//            let newTimeLine = RMTimeLine()
//            newTimeLine.isMyOwnPost = true
//            newTimeLine.timeLineDescription = textViewDesciption.text
//            newTimeLine.timeLinePostedDate = Date()
//            newTimeLine.timeLineOwnerName = "My Name"
//            newTimeLine.timeLineOwnerImageURL = "https://i0.wp.com/www.americanbazaaronline.com/wp-content/uploads/2014/02/SundarPichai-300x300.jpg"
//            newTimeLine.attachment = currentAttachmentURL
//            newTimeLine.attachmentType = currentAttachmentType
//
//            if replyToTimeLine == nil {
//                //FIXME:dd
////                RMTimeLineListTableViewController.arrayToDisplay?.insert(newTimeLine, at: 0)
//                newTimeLine.isAdminPost = true
//            } else {
//                //FIXME:dd
//
////                let indexOfReply = RMTimeLineListTableViewController.arrayToDisplay!.index(of: replyToTimeLine!)!
////                RMTimeLineListTableViewController.arrayToDisplay?.insert(newTimeLine, at: indexOfReply+1)
//                newTimeLine.isAdminPost = false
//            }
        }
        
        var compressedData: Data?
        var mimeType:String = ""
        var fileExtensiom:String = ""
        if imageViewAttachment.image != nil {
            let imageCompressed = RMGlobalManager.compressImage(image: imageViewAttachment.image!)
            let image = UIImage(data:imageCompressed!)
            compressedData = UIImagePNGRepresentation(image!)
            mimeType = "image/jpeg"
            fileExtensiom = "jpeg"
//            compressedData = imageCompressed//?.base64EncodedData()
//            compressedData.
//            let strBase64:String = (imageCompressed?.base64EncodedString())!
//            compressedData = strBase64.data(using: .utf8)
//            print(compressedData)
        }
        if currentAttachmentType == RMTimeLine.TimeLineAttachmentType.Document {
            compressedData = currentAttachmentData
            mimeType = "application/pdf"
            let url = currentAttachmentURL
            fileExtensiom = (url?.deletingPathExtension().lastPathComponent)!
            fileExtensiom = "pdf"
        }
        
        
        if isForFileUpload == true {
            if compressedData == nil {
                
                RMAlert.showAlert(title: nil, message: RMStrings.ATTACH_FILE_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: nil)
                return
            }
            
            
            
            self.delegate?.timeLinePostbuttonAction!(addTimeLineTableVC: self, fileData: compressedData, timeLineDescription: textViewDesciption.text, mimeType:mimeType,fileExtension: fileExtensiom,completion: {[weak self] (isSuccess:Bool) in
                guard let strongSelf = self else {
                    return
                }
                if isSuccess == true {
                    strongSelf.clearAllAndPopNavigationController()
                }
            })
            return
        }
        
        
        RMWebServiceManager.postTimeLine(fileData: compressedData,userUniqueId:RMUserDefaultManager.getCurrentUserUniqueId()!,timeLineDescription:textViewDesciption.text!, mimeType: mimeType, fileExtension: fileExtensiom){[weak self] (result: Any?,isSuccess:Bool) in
            
            guard let strongSelf = self else {
                return
            }
            
            if isSuccess == true {
                strongSelf.clearAllAndPopNavigationController()
            }
        }
        
        
        
    }
    
    func clearAllAndPopNavigationController() {
        textViewDesciption.text = ""
        labelPlaceholderDescription?.isHidden = false
        imageViewAttachment.image = nil
        currentAttachmentType = nil
        currentAttachmentURL = nil
        selectedTimeLine = nil
        replyToTimeLine = nil
        tableView.reloadData()
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
            self.delegate?.modifiedTimeLine(addTimeLineTableVC: self)
        }
    }
    
    @objc func barButtonDoneAction() {
        textViewDesciption.resignFirstResponder()
    }
    
    
    @IBAction func buttonActionForAttachment(_ sender: Any) {
        
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController:self)
            return
        }
        
        RMAttachment.sharedInstance.showPickerActionSheet(showInViewController: self) { [weak self] (attachmentType:RMAttachment.AttachmentType, url:URL?,data:Data?) in
            
            guard let strongSelf = self else {
                return
            }
            
            if attachmentType == .Cancel {
                return
            }
            
//            strongSelf.setAttachment(attachment: object as! Data, attachmentType: attachmentType)
            if attachmentType == .Document {
//                strongSelf.currentAttachmentURL = url
                strongSelf.currentAttachmentData = data
                strongSelf.setAttachment(attachment: url, attachmentType: RMTimeLine.TimeLineAttachmentType.Document)
//                strongSelf.setAttachment(attachment: data!, attachmentType: RMTimeLine.TimeLineAttachmentType.Document)
            }
            
            if attachmentType == .Photo {
                strongSelf.setAttachment(attachment: data!, attachmentType: RMTimeLine.TimeLineAttachmentType.Photo)
            }
        }
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        if imageViewAttachment.image == nil && cell == attachementCell {
            return 0
        }
        //        if selectedNote != nil && selectedNote!.attachment == nil && cell == attachementCell {
        //            return 0
        //        }
        return super.tableView(tableView, heightForRowAt: indexPath)
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        if imageViewAttachment.image != nil && cell == attachementCell && currentAttachmentType == RMTimeLine.TimeLineAttachmentType.Photo {
            let imageViewerVC = RMImageViewerViewController.instance(imageToShow: imageViewAttachment.image!,isGoingToPresent: true)
            let navController = UINavigationController(rootViewController: imageViewerVC)
            self.present(navController, animated: true, completion: nil)
        }
        
        if imageViewAttachment.image != nil && cell == attachementCell && currentAttachmentType == RMTimeLine.TimeLineAttachmentType.Document {
//            let imageViewerVC = RMImageViewerViewController.instance(imageToShow: imageViewAttachment.image!,isGoingToPresent: true)
            let documentVC = RMDocumentViewController(url: currentAttachmentURL!, isGoingToPresent: true)
//            let documentVC = RMDocumentViewController(dataToLoad: currentAttachmentData!, isGoingToPresent: true)
            let navController = UINavigationController(rootViewController: documentVC)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if imageViewAttachment.image != nil && cell == attachementCell {
            cell.selectionStyle = .default
        } else {
            cell.selectionStyle = .none
        }
    }
    
}
extension RMAddTimeLineTableViewController : UITextViewDelegate {
    // MARK: - Text view delegate
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText string: String) -> Bool {
        guard let text = textView.text else { return true }
        let newLength = text.count + string.count - range.length
        if(newLength == 0)
        {
            labelPlaceholderDescription?.isHidden = false
        }
        else
        {
            labelPlaceholderDescription?.isHidden = true
        }
        return newLength <= lengthLimitDescription
    }

    
    
}


extension RMAddTimeLineTableViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if gestureRecognizer is UITapGestureRecognizer {
            let location = touch.location(in: touch.view)
            if view.frame.contains(location) && imageViewAttachment.image != nil {
                return false
            }
        }
        return true
    }
}
