//
//  RMAddNoteTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 19/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit

protocol RMAddNoteTableViewControllerDelegate: class {
    func modifiedNote(addNoteTableVC:RMAddNoteTableViewController)
}

class RMAddNoteTableViewController: UITableViewController {

    public class func instance() -> RMAddNoteTableViewController {
        let rootStoryBoard = UIStoryboard(name: "RMNoteAndEvent", bundle: nil)
        let newNoteTableVC = rootStoryBoard.instantiateViewController(withIdentifier: "RMAddNoteTableViewController") as! RMAddNoteTableViewController
//        newTimeLineTableVC.selectedTimeLine = selectedTimeLine
//        newTimeLineTableVC.replyToTimeLine = replyToTimeLine
        return newNoteTableVC
    }

    @IBOutlet weak var textViewNoteDesciption: UITextView!
    var labelPlaceholderDescription : UILabel?
    let lengthLimitDescription = 250
    @IBOutlet weak var textFieldNoteTitle: UITextField!
    let lengthLimitNoteTitle = 30
    var selectedNote:RMNote?
    weak var delegate:RMAddNoteTableViewControllerDelegate?
    var selectedProjectUniqueId: String?
    @IBOutlet weak var submitButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.tableFooterView = UIView()
        self.tableView.allowsSelection = false
        textFieldNoteTitle.layer.borderWidth = 1
        textFieldNoteTitle.layer.borderColor = UIColor.lightGray.cgColor
        textFieldNoteTitle.borderStyle = .none
        let leftPaddingView = UIView(frame: CGRect(x:0, y:0,width:10,height:textFieldNoteTitle.frame.size.height))
        textFieldNoteTitle.leftView = leftPaddingView
        textFieldNoteTitle.leftViewMode = .always
        textFieldNoteTitle.returnKeyType = .next
        textViewNoteDesciption.layer.borderWidth = 1
        textViewNoteDesciption.layer.borderColor = UIColor.lightGray.cgColor
        textViewNoteDesciption.delegate = self as UITextViewDelegate
        textFieldNoteTitle.delegate = self as UITextFieldDelegate
        
        labelPlaceholderDescription = RMGlobalManager.addPlaceholderForView(view: textViewNoteDesciption, text: RMStrings.DESCRIPTION_STRING(), font: textViewNoteDesciption.font!)
        if textViewNoteDesciption.text.count > 0 {
            labelPlaceholderDescription?.isHidden = true
        }
        
        self.tableView.separatorStyle = .none
        //        self.tableView.allowsSelection = false
        let barButtonItem = UIBarButtonItem(title: RMStrings.DONE_STRING(), style: .done, target: self, action: #selector(barButtonDoneAction))
        let toolbarWithBarButtonDone = RMGlobalManager.toolbar(barButtonItem: barButtonItem)
        textViewNoteDesciption.inputAccessoryView = toolbarWithBarButtonDone
        
        
        let barButtonItemNext = UIBarButtonItem(title: RMStrings.NEXT_STRING(), style: .done, target: self, action: #selector(barButtonNextAction))
        let toolbarWithBarButtonNext = RMGlobalManager.toolbar(barButtonItem: barButtonItemNext)
        textFieldNoteTitle.inputAccessoryView = toolbarWithBarButtonNext
        textFieldNoteTitle.placeholder = "Title"
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction))
//        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)

        
        if selectedNote != nil {
            textFieldNoteTitle.text = selectedNote!.noteTitle
            textViewNoteDesciption.text = selectedNote!.noteDescription
            labelPlaceholderDescription?.isHidden = true
        }
        
        setupButton(button: submitButton)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setupButton(button:UIButton) {
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 2
        button.clipsToBounds = true
        button.layer.borderColor =  UIColor.blue.cgColor
        button.backgroundColor = RMGlobalManager.appBlueColor()
    }
    
    func setupUploadButton(uploadButton:UIButton) {
        uploadButton.layer.cornerRadius = 2.5
        uploadButton.layer.borderWidth = 1
        uploadButton.layer.borderColor = RMGlobalManager.appBlueColor().cgColor
    }
    
    
    @objc func tapGestureAction() {
        
        self.view.endEditing(true)
        
    }
    
    @IBAction func buttonActionPost(_ sender: Any) {
        textViewNoteDesciption.resignFirstResponder()
        
        
        textViewNoteDesciption.text = textViewNoteDesciption.text.trimmingCharacters(in: .whitespacesAndNewlines)
        
        textFieldNoteTitle.resignFirstResponder()
        
        
        textFieldNoteTitle.text = textFieldNoteTitle.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController:self)
            return
        }
        
        if textFieldNoteTitle.text?.count == 0 {
            RMAlert.showAlert(title: nil, message: RMStrings.ENTER_TITLE_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textFieldNoteTitle.becomeFirstResponder()
            })
            return
        }
        
        if textViewNoteDesciption.text.count == 0 {
            RMAlert.showAlert(title: nil, message: RMStrings.ENTER_DESCRIPTION_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textViewNoteDesciption.becomeFirstResponder()
            })
            return
        }
        
        
        
        
        
        
        /***
         Success
         */
        
        if selectedNote != nil {
//            selectedTimeLine!.timeLineDescription = textViewNoteDesciption.text
//            selectedTimeLine!.attachment = currentAttachmentData
//            selectedTimeLine!.attachmentType = currentAttachmentType
            //            if currentAttachmentType == RMNote.NoteAttachmentType.Photo {
            //                var documentData : Data?
            ////                if currentAttachmentData != nil {
            ////                    documentData = UIImagePNGRepresentation(imageViewAttachment.image!)!
            ////                }
            //               selectedNote!.attachment = documentData
            //            }
//            selectedNote!.noteTitle = textFieldNoteTitle.text
//            selectedNote!.noteDescription = textViewNoteDesciption.text
//            selectedNote!.notePostedDate = Date()

            RMWebServiceManager.updateNote(projectUniqueId:selectedProjectUniqueId, noteUniqueId: selectedNote!.noteUniqueId!,title: textFieldNoteTitle.text!, description: textViewNoteDesciption.text!)  {[weak self] (result:Any?, isSuccess:Bool) in
                if isSuccess == true {
                    self?.textFieldNoteTitle.text = ""
                    self?.textViewNoteDesciption.text = ""
                    self?.labelPlaceholderDescription?.isHidden = false
                    //        imageViewAttachment.image = nil
                    //        currentAttachmentType = nil
                    //        currentAttachmentData = nil
                    //        selectedTimeLine = nil
                    //        replyToTimeLine = nil
                    self?.tableView.reloadData()
                    self?.delegate?.modifiedNote(addNoteTableVC: self!)
                    DispatchQueue.main.async {
                        self?.navigationController?.popViewController(animated: true)
                    }
                }
            }
            
            return
            
        } else {
    
            //FIXME: Dummy data
//            let newNote = RMNote()
////            newTimeLine.isMyOwnPost = true
//        newNote.noteTitle = textFieldNoteTitle.text
//            newNote.noteDescription = textViewNoteDesciption.text
//            newNote.notePostedDate = Date()
//            newTimeLine.timeLineOwnerName = "My Name"
//            newTimeLine.timeLineOwnerImageURL = "https://i0.wp.com/www.americanbazaaronline.com/wp-content/uploads/2014/02/SundarPichai-300x300.jpg"
//            newTimeLine.attachment = currentAttachmentData
//            newTimeLine.attachmentType = currentAttachmentType
        
//            if replyToTimeLine == nil {
//                RMNoteListTableViewController.arrayToDisplay?.insert(newNote, at: 0)
//                newTimeLine.isAdminPost = true
//            } else {
//                let indexOfReply = RMTimeLineListTableViewController.arrayToDisplay!.index(of: replyToTimeLine!)!
//                RMTimeLineListTableViewController.arrayToDisplay?.insert(newTimeLine, at: indexOfReply+1)
//                newTimeLine.isAdminPost = false
//            }
        }
        
        RMWebServiceManager.saveNote(projectUniqueId:selectedProjectUniqueId,title: textFieldNoteTitle.text!, description: textViewNoteDesciption.text!)  {[weak self] (result:Any?, isSuccess:Bool) in
            if isSuccess == true {
                self?.textFieldNoteTitle.text = ""
                self?.textViewNoteDesciption.text = ""
                self?.labelPlaceholderDescription?.isHidden = false
                //        imageViewAttachment.image = nil
                //        currentAttachmentType = nil
                //        currentAttachmentData = nil
                //        selectedTimeLine = nil
                //        replyToTimeLine = nil
                self?.tableView.reloadData()
                self?.delegate?.modifiedNote(addNoteTableVC: self!)
                DispatchQueue.main.async {
                    self?.navigationController?.popViewController(animated: true)
                }
            }
        }
    
        
    }
    
    @objc func barButtonDoneAction() {
        textViewNoteDesciption.resignFirstResponder()
    }


    @objc func barButtonNextAction() {
        textFieldNoteTitle.resignFirstResponder()
        textViewNoteDesciption.becomeFirstResponder()
    }
    // MARK: - Table view delegate
    
    
    

}

extension RMAddNoteTableViewController : UITextViewDelegate {
    // MARK: - Text view delegate
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText string: String) -> Bool {
        guard let text = textView.text else { return true }
        let newLength = text.count + string.count - range.length
        if(newLength == 0)
        {
            labelPlaceholderDescription?.isHidden = false
        }
        else
        {
            labelPlaceholderDescription?.isHidden = true
        }
        return newLength <= lengthLimitDescription
    }
    
    
    
}

extension RMAddNoteTableViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        if(newLength == 0)
        {
//            labelPlaceholderDescription?.isHidden = false
        }
        else
        {
//            labelPlaceholderDescription?.isHidden = true
        }
        return newLength <= lengthLimitNoteTitle
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldNoteTitle {
            textViewNoteDesciption.becomeFirstResponder()
            return false
        }
        return true
    }
}

