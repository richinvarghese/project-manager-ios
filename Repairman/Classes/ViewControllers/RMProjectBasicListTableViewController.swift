//
//  RMProjectListTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 20/01/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMProjectBasicListTableViewController: UITableViewController {
    
    var arrayToDisplay: [RMProject]?
    var arraySearch: [RMProject]?
    var searchBar: UISearchBar?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = RMStrings.PROJECT_STRING()
        tableView.register(RMProjectBasicListTableViewCell.self)
        tableView.tableFooterView = UIView()
        fetchBasicProject()
        searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 50))
        searchBar!.delegate = self
        searchBar?.placeholder = "Search"
        tableView.tableHeaderView = searchBar
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func fetchBasicProject() {
        
        self.arrayToDisplay = nil
        self.tableView.reloadData()
        
        RMWebServiceManager.fetchBasicProject {[weak self] (result:Any?, isSuccess:Bool) in
            if isSuccess == false {
                return
            }
            let projectsArray = result as! [RMProject]
            
//            let pro = RMProject()
//            pro.projectTitle = "Project title"
//            pro.projectCompanyName = "Apstrix"
//            pro.projectStatus = "open"
//            let ar = [pro]
            
//            self?.arrayToDisplay = ar
            self?.arrayToDisplay = projectsArray
            self?.tableView.reloadData()
        }
    }
    

    func showingBasicProject(atIndexPath:IndexPath) -> RMProject {
        if searchBar?.text?.count == 0 {
            return arrayToDisplay![atIndexPath.row]
        }
        //        let array = displayingArray()
        return arraySearch![atIndexPath.row]
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayToDisplay == nil {
            return 0
        }
        if searchBar?.text?.count == 0 {
            return arrayToDisplay!.count
        }
        if arraySearch == nil {
            return 0
        }
        return arraySearch!.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:RMProjectBasicListTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let basicProject = showingBasicProject(atIndexPath: indexPath)
        cell.setBasicProject(project:basicProject)
        cell.delegate = self
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let basicProject = showingBasicProject(atIndexPath: indexPath)
        let projectVC = RMProjectViewController.instance()
        projectVC.title = basicProject.projectTitle
        projectVC.selectedProjectUniqueId = basicProject.projectUniqueId
        self.navigationController?.pushViewController(projectVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }

    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        var color : UIColor!
        if (indexPath.row%2 == 0) {
            color = UIColor.red
        } else {
            color = RMGlobalManager.appBlueColor()
        }
        
        let cell1:RMProjectBasicListTableViewCell = cell as! RMProjectBasicListTableViewCell
        cell1.labelSatus.backgroundColor = color
        cell1.labelSatus.textColor = UIColor.white
        cell1.labelProjectName.textColor = color
        cell1.labelCompanyName.textColor = color
        RMTheme.cellCornerRadius(cell: cell, borderColor: color, backgroundColor: UIColor.clear)
    }
}

extension RMProjectBasicListTableViewController: RMProjectBasicListTableViewCellDelegate {
    
    func buttonActionLeaveProject(cell:RMProjectBasicListTableViewCell) {
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController: self)
            return
        }
        
        
        let indexPath =  tableView.indexPath(for: cell)
        if indexPath == nil {
            return
        }
        let stringYes = "YES"
        RMAlert.showAlert(title: nil, message: "Are you sure? Do you want to leave from project?", showInViewController: self, defaultButtonNames: [stringYes], destructiveButtonNames: nil, cancelButtonNames: ["NO"]) { [weak self] (selectedTitle) in
            if selectedTitle == stringYes {
                let project = self?.showingBasicProject(atIndexPath: indexPath!)
                if project == nil {
                    return;
                }
                RMWebServiceManager.projectMemeberLeave(userUniqueId: RMUserDefaultManager.getCurrentUserUniqueId()!, projectUniqueId: project!.projectUniqueId!) { (result, isSuccess) in
                    if (!isSuccess) {
                        return;
                    }
                }
            }
        }
        
        
    }
}


extension RMProjectBasicListTableViewController: RMNavigationBarButtonItems {
    func neededNavigationBarButtonItems() -> RMNeededNavigationButtonItem {
        if navigationController?.viewControllers.count == 1 {
            return .leftMenu
        }
        
        return .none
    }
}

extension RMProjectBasicListTableViewController: RMNavigationBarButtonActions {
    func barButtonLeftAction() {
        RMSideMenuAndRoot.showLeftMenu()
    }
}

extension RMProjectBasicListTableViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) // called when text changes (including clear)
    {
        if arrayToDisplay == nil {
            return;
        }
        arraySearch = arrayToDisplay?.filter {
            ($0.projectTitle?.contains(searchText))!
        }
        tableView.reloadData()
    }
    
}
