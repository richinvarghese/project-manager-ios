//
//  RMLeftSideTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 19/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit

class RMLeftSideTableViewController: UITableViewController {
    @IBOutlet weak var cellDashboard: UITableViewCell!
    @IBOutlet weak var cellProject: UITableViewCell!
    @IBOutlet weak var cellTimeLine: UITableViewCell!

    @IBOutlet weak var cellEvent: UITableViewCell!
    @IBOutlet weak var cellNote: UITableViewCell!
    @IBOutlet weak var cellLeave: UITableViewCell!
    @IBOutlet weak var cellTimecard: UITableViewCell!
    @IBOutlet weak var cellMessages: UITableViewCell!
    @IBOutlet weak var cellAnnouncement: UITableViewCell!
    @IBOutlet weak var cellUpdateLocation: UITableViewCell!
    @IBOutlet weak var cellLogout: UITableViewCell!
    
    public class func instance() -> RMLeftSideTableViewController {
        let sideMenusStoryBoard = UIStoryboard(name: "RMSideMenus", bundle: nil)
        let leftSideTableVC = sideMenusStoryBoard.instantiateViewController(withIdentifier: "RMLeftSideTableViewController") as! RMLeftSideTableViewController
        return leftSideTableVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.navigationController?.navigationBar.isHidden = true
        self.tableView.tableFooterView = UIView()
        self.tableView.backgroundColor = RMGlobalManager.appBlueColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        if cell == cellDashboard {
            self.dismiss(animated: true, completion: {
                RMSideMenuAndRoot.showDashboard()
            })
        }
        if cell == cellProject {
            self.dismiss(animated: true, completion: {
                RMSideMenuAndRoot.showBasicProject()
            })
        }
        else if cell == cellNote {
            self.dismiss(animated: true, completion: { 
                RMSideMenuAndRoot.showNote()
            })
        }
        else if cell == cellTimeLine {
            self.dismiss(animated: true, completion: {
                RMSideMenuAndRoot.showTimeLine()
            })
        }
        else if cell == cellEvent {
            self.dismiss(animated: true, completion: {
                RMSideMenuAndRoot.showEvent()
            })
        }
        else if cell == cellLeave {
            self.dismiss(animated: true, completion: {
                RMSideMenuAndRoot.showLeave()
            })
        }
        else if cell == cellTimecard {
            self.dismiss(animated: true, completion: {
                RMSideMenuAndRoot.showTimeCard()
            })
        }
        else if cell == cellMessages {
            self.dismiss(animated: true, completion: {
                RMSideMenuAndRoot.showMessages()
            })
        }
        else if cell == cellUpdateLocation {
            
            
            let dash = RMAppDelegate.dashboardInstance()
            if dash.isLocationAndNetworkWorking() == false {
                return;
            }
            if dash.currentLatString == nil || dash.currentLongString == nil {
                return;
            }
            self.dismiss(animated: true, completion: {
//                RMSideMenuAndRoot.showMessages()
//                [
                
               
                RMWebServiceManager.saveLocation(userUniqueId: RMUserDefaultManager.getCurrentUserUniqueId()!, latitiude: dash.currentLatString!, longitude: dash.currentLongString!) { [weak self] (result,isSuccess) in
//                    self?.isSaveLocationWebAPIStarted = true
                }
            })
        }
        else if cell == cellAnnouncement {
            self.dismiss(animated: true, completion: {
                RMSideMenuAndRoot.showAnnouncements()
            })
        }
        else if cell == cellLogout {
            let userId = RMUserDefaultManager.getCurrentUserUniqueId()
            if userId != nil {
                RMAppDelegate.firebaseUnSubscribe(topic: userId!)
            }
            self.dismiss(animated: true, completion: {
                RMUserDefaultManager.setCurrentUserUniqueId(appUserUniqueId: nil)
                RMSideMenuAndRoot.showLogin()
            })
        }
        
//        RMEventCalendarTableViewController.arrayEvents = nil
//        RMSideMenuAndRoot.eventVC?.calendarManager = nil
//        RMSideMenuAndRoot.eventVC = nil
//        RMEventListViewController.a
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.backgroundColor = UIColor.clear
        cell.backgroundColor = UIColor.clear
    }
    
    

}
