//
//  RMTimerNoteTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 04/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

public typealias TimerStatusCompletion = (_ isWebserviceSuccess:Bool,_ isTimerStarted:Bool?) -> Swift.Void

protocol RMTimerNoteTableViewControllerDelegate: class {
    func updateTimerInServer(timerNoteTableViewController:RMTimerNoteTableViewController,note:String?,taskUniqueId:String?,completionHandler:@escaping TimerStatusCompletion)
}

class RMTimerNoteTableViewController: UITableViewController {

    @IBOutlet weak var textFieldTaskList: UITextField!
    @IBOutlet weak var textViewNote: UITextView!
    
    var labelPlaceholderNote : UILabel?
    let lengthLimitNote = 250
    
    var arrayTaskList:[RMTask]?
    var selectedTaskUniqueId: String?
    
    var delegate: RMTimerNoteTableViewControllerDelegate?
    
    public class func instance() -> RMTimerNoteTableViewController {
        let timerNoteStoryBoard = UIStoryboard(name: "RMTimerNote", bundle: nil)
        let timerNoteTableVC = timerNoteStoryBoard.instantiateViewController(withIdentifier: "RMTimerNoteTableViewController") as! RMTimerNoteTableViewController
        return timerNoteTableVC
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = RMStrings.STOP_TIMER_STRING()
        
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
        textViewNote.text = ""
        textViewNote.layer.borderWidth = 1
        textViewNote.layer.borderColor = UIColor.lightGray.cgColor
        textViewNote.delegate = self as UITextViewDelegate
        labelPlaceholderNote = RMGlobalManager.addPlaceholderForView(view: textViewNote, text: RMStrings.NOTE_STRING(), font: textViewNote.font!)
        if textViewNote.text.count > 0 {
            labelPlaceholderNote?.isHidden = true
        }
        let barButtonItem = UIBarButtonItem(title: RMStrings.DONE_STRING(), style: .done, target: self, action: #selector(barButtonDoneAction))
        let toolbarWithBarButtonDone = RMGlobalManager.toolbar(barButtonItem: barButtonItem)
        textViewNote.inputAccessoryView = toolbarWithBarButtonDone
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction))
        //        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        
        
        textFieldTaskList.layer.borderWidth = 1
        textFieldTaskList.layer.borderColor = UIColor.lightGray.cgColor
        textFieldTaskList.borderStyle = .none
        let leftPaddingView = UIView(frame: CGRect(x:0, y:0,width:10,height:textFieldTaskList.frame.size.height))
        textFieldTaskList.leftView = leftPaddingView
        textFieldTaskList.leftViewMode = .always
        textFieldTaskList.returnKeyType = .next
        
        textFieldTaskList.placeholder = RMStrings.TASKS_STRING()
        textFieldTaskList.delegate = self
        let _ = RMGlobalManager.pickerView(textField: textFieldTaskList, toolBarButtonTitle: RMStrings.DONE_STRING(), pickerViewDelegateAndDataSource: self, barButtonTarget: self, barButtonAction: #selector(barButtonDoneAction))
        
        let barButtonCancel = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(barButtonCancelAction))
        self.navigationItem.leftBarButtonItem = barButtonCancel
        
        let barButtonSubmit = UIBarButtonItem(title: RMStrings.POST_STRING(), style: .plain, target: self
            , action: #selector(buttonActionPost))
        self.navigationItem.rightBarButtonItem = barButtonSubmit
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func tapGestureAction() {
        
        self.view.endEditing(true)
        
    }
    @objc func buttonActionPost() {
        textViewNote.resignFirstResponder()
        
        
        textViewNote.text = textViewNote.text.trimmingCharacters(in: .whitespacesAndNewlines)
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController:self)
            return
        }
//        if textViewNote.text.count == 0 {
//            RMAlert.showAlert(title: nil, message: RMStrings.ENTER_NOTE_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
//                self?.textViewNote.becomeFirstResponder()
//            })
//            return
//        }
        
        delegate?.updateTimerInServer(timerNoteTableViewController: self,  note: textViewNote.text, taskUniqueId: selectedTaskUniqueId, completionHandler: { (isWebserviceSucced:Bool, isTimerStarted:Bool?) in
            if isWebserviceSucced == false || isTimerStarted == true {
                RMAlert.showAlert(title: nil, message: RMStrings.SOMETHING_WENT_WRONG_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction:nil)
                return
            }
            self.dismiss(animated: true, completion: nil)
        })
        
    }
    
    @objc func barButtonDoneAction() {
        textViewNote.resignFirstResponder()
        textFieldTaskList.resignFirstResponder()
    }
    
    @objc func barButtonCancelAction() {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    
//    func updateTimerInServer(projectUniqueId:String,note:String?,taskId:String?) {
//        let currentUserUniqueId = RMUserDefaultManager.getCurrentUserUniqueId()!
//        RMWebServiceManager.timerStatus(projectUniqueId: projectUniqueId, currentUserUniqueId: currentUserUniqueId, note: nil, taskId: nil) {[weak self] (result:Any?, isSuccess:Bool) in
//            if isSuccess == true && result is Dictionary<String,Any?> {
//                let dict = result as! Dictionary<String,Any?>
//                let stringTimerStatus = dict["timer_status"] as? String
//                if stringTimerStatus?.lowercased() == "start" {
//
//                    self?.prjectOverviewTableVCDelegate?.timerUpdated(timerIsStarted: false)
//                } else if stringTimerStatus?.lowercased() == "stop"{
//
//                    self?.prjectOverviewTableVCDelegate?.timerUpdated(timerIsStarted: true)
//                }
//
//            }
//        }
//    }

    // MARK: - Table view delegate
    
}

extension RMTimerNoteTableViewController : UITextViewDelegate {
    // MARK: - Text view delegate
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText string: String) -> Bool {
        guard let text = textView.text else { return true }
        let newLength = text.count + string.count - range.length
        if(newLength == 0)
        {
            labelPlaceholderNote?.isHidden = false
        }
        else
        {
            labelPlaceholderNote?.isHidden = true
        }
        return newLength <= lengthLimitNote
    }
    
    
    
}

extension RMTimerNoteTableViewController : UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textFieldTaskList != textField {
            return true
        }
        if textField.text == nil || textField.text?.count == 0 {
            if arrayTaskList == nil {
                return true
            }
            if arrayTaskList!.count > 0 {
                textFieldTaskList.text = arrayTaskList![0].taskTitle
                selectedTaskUniqueId = arrayTaskList![0].taskUniqueId
            }
        }
        return true
            
    }
}

extension RMTimerNoteTableViewController:UIPickerViewDataSource,UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if arrayTaskList == nil {
            return 0
        }
        return arrayTaskList!.count
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let task = arrayTaskList![row]
        return task.taskTitle
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if arrayTaskList == nil {
            return
        }
        if arrayTaskList!.count == 0 {
            return
        }
        let task = arrayTaskList![row]
        selectedTaskUniqueId = task.taskUniqueId
        textFieldTaskList.text = task.taskTitle
        
    }
}
