//
//  RMProjectViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 25/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit

class RMProjectViewController: UIViewController {

    
    public class func instance() -> RMProjectViewController {
        let storyBoard = UIStoryboard(name: "RMProjectStoryboard", bundle: nil)
        let projectVC = storyBoard.instantiateViewController(withIdentifier: "RMProjectViewController") as! RMProjectViewController
        //        newTimeLineTableVC.selectedTimeLine = selectedTimeLine
        //        newTimeLineTableVC.replyToTimeLine = replyToTimeLine
        return projectVC
    }
    
//    public class func keyOverview -
    
    var isFetchingProject:Bool?
    
    @IBOutlet weak var segment: UISegmentedControl!
    
    @IBOutlet weak var viewBelowSegment: UIView!
    let projectOverview = RMProjectOverviewTableViewController(style:.plain)
    let taskList = RMTaskListTableViewController()
    let noteList = RMNoteListTableViewController()
    let activityList = RMActivityListTableViewController()
    let milestoneList = RMMilestoneListTableViewController()
    let fileList = RMFileListTableViewController()
    let timesheetList = RMTimesheetListTableViewController()
    let expenseList = RMExpenseListTableViewController()
    var selectedProjectUniqueId: String? {
        didSet {
            noteList.selectedProjectUniqueId = selectedProjectUniqueId
            fileList.projectUniqueId = selectedProjectUniqueId
            fetchProjectDetails(segmentSelectionOnCompletion:nil)
            projectOverview.projectUniqueId = selectedProjectUniqueId
            expenseList.projectUniqueId = selectedProjectUniqueId
            timesheetList.projectUniqueId = selectedProjectUniqueId
        }
    }
    var isTimerStarted : Bool = false {
        didSet {
            if segment.selectedSegmentIndex < 0 {
                return
            }
            let title = segment.titleForSegment(at: segment.selectedSegmentIndex)
            if title == nil {
                return
            }
            if title != RMStrings.OVERVIEW_STRING() {
                return
            }
            projectOverview.barButtonItemTimer(navigationItem: self.navigationItem,isStarted: isTimerStarted)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.title = RMStrings.PROJECT_STRING()
        // Do any additional setup after loading the view.
        noteList.delegate = self
        noteList.fetchNote()
        segment.removeAllSegments()
        taskList.delegate = self
        fileList.delegate = self
        expenseList.delegate = self
        projectOverview.delegate = self
        timesheetList.delegate = self
//        segment.setTitle(RMStrings.OVERVIEW_STRING(), forSegmentAt: 0)
//
//        segment.setTitle(RMStrings.TASKS_STRING(), forSegmentAt: 1)
//        segment.insertSegment(withTitle: RMStrings.ACTIVITY_STRING(), at: 2, animated: false)
//        segment.insertSegment(withTitle: RMStrings.MILESTONES_STRING(), at: 3, animated: false)
//        segment.insertSegment(withTitle: RMStrings.NOTES_STRING(), at: 4, animated: false)
//        segment.insertSegment(withTitle: RMStrings.FILES_STRING(), at: 5, animated: false)
//        segment.removeSegment(at: 5, animated: false)
//        segment.removeSegment(at: 4, animated: false)
//        segment.removeSegment(at: 3, animated: false)
//        segment.removeSegment(at: 2, animated: false)
//        segment.removeSegment(at: 1, animated: false)
//        segment.setTitle(RMStrings.MILESTONES_STRING(), forSegmentAt: 2)
//        segment.setTitle(RMStrings.NOTES_STRING(), forSegmentAt: 3)
//        segment.setTitle(RMStrings.FILES_STRING(), forSegmentAt: 4)
//        segment.titleForSegment(at: 0) = RMStrings.OVERVIEW_STRING()
//        segment.titleForSegment(at: 1) = RMStrings.TASK_STRING()
        segment.addTarget(self, action: #selector(changeSegment), for: .valueChanged)
//        changeSegment(segment)
        self.addChildViewController(taskList)
        self.addChildViewController(projectOverview)
        self.addChildViewController(noteList)
        self.addChildViewController(activityList)
        self.addChildViewController(fileList)
        self.addChildViewController(expenseList)
//        segment.addTarget(self, action: "changeSegment:", forSelector("changeSegment:"): .valueChanged)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
//    @objc func buttonActionForAdd() {
////        addOrEditTimeLine(timeLine: nil, replyToTimeLine: nil)
//    }
//
//
//    func addOrEditTaskList(timeLine:RMTimeLine?,replyToTimeLine:RMTimeLine?) {
//        let addTimeLineTableVC = RMAddTimeLineTableViewController.instance(selectedTimeLine: timeLine, replyToTimeLine: replyToTimeLine)
//        self.navigationController?.pushViewController(addTimeLineTableVC, animated: true)
//    }
    
    func indexOfNotesSegment() -> Int? {
        return indexOfSegment(title: RMStrings.NOTES_STRING())
//        if self.segment.numberOfSegments <= 1 {
//            return nil;
//        }
//        var indexNotes:Int?
//        for i in 0...(self.segment.numberOfSegments - 1) {
//            let title = self.segment.titleForSegment(at: i)
//            if title == RMStrings.NOTES_STRING() {
//                indexNotes = i
//                break;
//            }
//        }
//        return indexNotes
    }
    
    func indexOfSegment(title:String) -> Int? {
        if self.segment.numberOfSegments <= 1 {
            return nil;
        }
        var indexNotes:Int?
        for i in 0...(self.segment.numberOfSegments - 1) {
            let title1 = self.segment.titleForSegment(at: i)
            if title1 == title {
                indexNotes = i
                break;
            }
        }
        return indexNotes
    }
    
    func clearView() {
        taskList.tableView.removeFromSuperview()
        projectOverview.tableView.removeFromSuperview()
        noteList.tableView.removeFromSuperview()
        milestoneList.tableView.removeFromSuperview()
        fileList.tableView.removeFromSuperview()
        timesheetList.tableView.removeFromSuperview()
        expenseList.tableView.removeFromSuperview()
        self.navigationItem.rightBarButtonItem = nil
    }
    
    @objc func changeSegment(_ sender: UISegmentedControl) {
        print("Change color handler is called.")
        print("Changing Color to ")
        clearView()
        let title = sender.titleForSegment(at: sender.selectedSegmentIndex)
        if title == nil {
            return
        }
        var tableView: UITableView?
        switch title! {
        case RMStrings.OVERVIEW_STRING():
//            let tableView =
            
            self.viewBelowSegment.addSubview(projectOverview.tableView)
            projectOverview.barButtonItemTimer(navigationItem: self.navigationItem,isStarted: isTimerStarted)
            tableView = projectOverview.tableView
            projectOverview.tableView.translatesAutoresizingMaskIntoConstraints = false
            RMProjectViewController.addConstraintWithSuperView(view:projectOverview.tableView)
            
//            self.navigationItem.rightBarButtonItem = nil
        case RMStrings.TASKS_STRING():
//            self.view.backgroundColor = UIColor.blue
//            print("Blue")
//            taskList.tableView.removeFromSuperview()
//            projectOverview.tableView.removeFromSuperview()
//            noteList.tableView.removeFromSuperview()
            self.viewBelowSegment.addSubview(taskList.tableView)
            RMProjectViewController.addConstraintWithSuperView(view:taskList.tableView)
           tableView = taskList.tableView
            taskList.tableView.translatesAutoresizingMaskIntoConstraints = false
//            self.navigationItem.rightBarButtonItem = nil
        
        case RMStrings.NOTES_STRING():
//            noteList.tableView.removeFromSuperview()
//            projectOverview.tableView.removeFromSuperview()
//            noteList.tableView.removeFromSuperview()
            self.viewBelowSegment.addSubview(noteList.tableView)
            RMProjectViewController.addConstraintWithSuperView(view: noteList.tableView)
           tableView = noteList.tableView
        noteList.tableView.translatesAutoresizingMaskIntoConstraints = false
            noteList.barButtonItemAdd(navigationItem: self.navigationItem)
//            let barButtonItemAdd = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(buttonActionForAdd))
//            self.navigationItem.rightBarButtonItem = barButtonItemAdd
        case RMStrings.ACTIVITIES_STRING():
//            noteList.tableView.removeFromSuperview()
//            projectOverview.tableView.removeFromSuperview()
//            noteList.tableView.removeFromSuperview()
//            activityList.tableView.removeFromSuperview()
            self.viewBelowSegment.addSubview(activityList.tableView)
            RMProjectViewController.addConstraintWithSuperView(view: activityList.tableView)
           tableView = activityList.tableView
        activityList.tableView.translatesAutoresizingMaskIntoConstraints = false
//            noteList.barButtonItemAdd(navigationItem: self.navigationItem)
        case RMStrings.MILESTONES_STRING():
            self.viewBelowSegment.addSubview(milestoneList.tableView)
            RMProjectViewController.addConstraintWithSuperView(view: milestoneList.tableView)
           tableView = milestoneList.tableView
        milestoneList.tableView.translatesAutoresizingMaskIntoConstraints = false
            
        case RMStrings.FILES_STRING():
            self.viewBelowSegment.addSubview(fileList.tableView)
            RMProjectViewController.addConstraintWithSuperView(view: fileList.tableView)
           tableView = fileList.tableView
        fileList.tableView.translatesAutoresizingMaskIntoConstraints = false
            fileList.barButtonItemAdd(navigationItem: self.navigationItem)
        case RMStrings.TIMER_STRING():
            //            noteList.tableView.removeFromSuperview()
            //            projectOverview.tableView.removeFromSuperview()
            //            noteList.tableView.removeFromSuperview()
            //            activityList.tableView.removeFromSuperview()
            timesheetList.arrayTaskList = taskList.arrayTask
            self.viewBelowSegment.addSubview(timesheetList.tableView)
            RMProjectViewController.addConstraintWithSuperView(view: timesheetList.tableView)
           tableView = timesheetList.tableView
        timesheetList.tableView.translatesAutoresizingMaskIntoConstraints = false
            
        case RMStrings.EXPENSE_STRING():
            //            noteList.tableView.removeFromSuperview()
            //            projectOverview.tableView.removeFromSuperview()
            //            noteList.tableView.removeFromSuperview()
            //            activityList.tableView.removeFromSuperview()
            self.viewBelowSegment.addSubview(expenseList.tableView)
            RMProjectViewController.addConstraintWithSuperView(view: expenseList.tableView)
           tableView = expenseList.tableView
            expenseList.tableView.translatesAutoresizingMaskIntoConstraints = false
           expenseList.barButtonItemAdd(navigationItem: self.navigationItem)
            
        default:
            self.viewBelowSegment.backgroundColor = UIColor.purple
//            print("Purple")
            self.navigationItem.rightBarButtonItem = nil
        }
        viewBelowSegment.layoutSubviews()
        viewBelowSegment.layoutIfNeeded()
        let dispatchTime = DispatchTime.now() + 0.01
        DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
            tableView?.reloadData()
            tableView?.layoutIfNeeded()
            tableView?.layoutSubviews()
            self.view.layoutIfNeeded()
            self.view.layoutSubviews()
            self.viewBelowSegment.layoutSubviews()
            self.viewBelowSegment.layoutIfNeeded()
        }
        DispatchQueue.main.async {
            tableView?.reloadData()
        }
        
    }
    
    
    
    func fetchProjectDetails(segmentSelectionOnCompletion:String?) {
        
        if isFetchingProject == true {
            return
        }
        //FIXME: Dummy data
        //        dummyData()
        
        //        return;
        if segment != nil {/**Otherwise crash will occure at first time*/
            segment.removeAllSegments()
        }
        projectOverview.arrayToDisplay = nil
        taskList.arrayTask = nil
        expenseList.arrayToDisplay = nil
        projectOverview.tableView.reloadData()
        taskList.tableView.reloadData()
        expenseList.tableView.removeFromSuperview()
        
        guard let projectUniqueId = selectedProjectUniqueId else {
            return
        }
        
        isFetchingProject = true
        
        RMWebServiceManager.fetchProjectDetails(projectUniqueId: projectUniqueId, currentUserUniqueId: RMUserDefaultManager.getCurrentUserUniqueId()!) {[weak self] (result:Any?, isSuccess:Bool) in
            
            self?.isFetchingProject = nil
            
            if isSuccess == false {
                return
            }
            let projectsDict = result as! Dictionary<String,Any>
//            for key in projectsDict.keys {
                if let _ = projectsDict[RMStrings.OVERVIEW_STRING()] {
                    
                    let key = RMStrings.OVERVIEW_STRING()
                    
                    self?.projectOverview.arrayToDisplay = projectsDict[key] as? Array<Any>
                    let index = self?.segment.numberOfSegments ?? 0
                   self?.segment?.insertSegment(withTitle: RMStrings.OVERVIEW_STRING(), at: index, animated: false)
//                    self?.segment.setTitle(RMStrings.OVERVIEW_STRING(), forSegmentAt: 0)
                    
                }
            
            if let _ = projectsDict[RMStrings.TASKS_STRING()] {
                    let key = RMStrings.TASKS_STRING()
                    self?.taskList.arrayTask = projectsDict[key] as? [RMTask]
                    let index = self?.segment.numberOfSegments ?? 0
                    self?.segment?.insertSegment(withTitle: RMStrings.TASKS_STRING(), at: index, animated: false)
                }
            
            if let _ = projectsDict[RMStrings.ACTIVITIES_STRING()] {
                    let key = RMStrings.ACTIVITIES_STRING()
                    self?.activityList.arrayToDisplay = projectsDict[key] as? [RMActivity]
                    let index = self?.segment.numberOfSegments ?? 0
                    self?.segment.insertSegment(withTitle: RMStrings.ACTIVITIES_STRING(), at: index, animated: false)
                }
            
            if let _ = projectsDict[RMStrings.MILESTONES_STRING()] {
                let key = RMStrings.MILESTONES_STRING()
                self?.milestoneList.arrayToDisplay = projectsDict[key] as? [RMMilestone]
                let index = self?.segment.numberOfSegments ?? 0
                self?.segment.insertSegment(withTitle: RMStrings.MILESTONES_STRING(), at: index, animated: false)
            }
            
            if let _ = projectsDict[RMStrings.FILES_STRING()] {
                let key = RMStrings.FILES_STRING()
                self?.fileList.arrayToDisplay = projectsDict[key] as? [RMProjectFile]
                let index = self?.segment.numberOfSegments ?? 0
                self?.segment.insertSegment(withTitle: RMStrings.FILES_STRING(), at: index, animated: false)
            }
            
            if let _ = projectsDict["taskStatus"] {
                self?.taskList.arrayTaskStatus = projectsDict["taskStatus"] as? [RMTaskStatus]
            }
            
            if let _ = projectsDict[RMStrings.TIMER_STRING()] {
                let key = RMStrings.TIMER_STRING()
                self?.timesheetList.arrayToDisplay = projectsDict[key] as? [RMTimesheet]
                let index = self?.segment.numberOfSegments ?? 0
                self?.segment.insertSegment(withTitle: key, at: index, animated: false)
            }
            
            if let _ = projectsDict[RMStrings.EXPENSE_STRING()] {
                let key = RMStrings.EXPENSE_STRING()
                self?.expenseList.arrayToDisplay = projectsDict[key] as? [RMExpense]
                let index = self?.segment.numberOfSegments ?? 0
                self?.segment.insertSegment(withTitle: key, at: index, animated: false)
            }
            
            
            if let _ = projectsDict[RMStrings.EXPENSE_CATEGORY_STRING()] {
                self?.expenseList.arrayExpenseCategory = projectsDict[RMStrings.EXPENSE_CATEGORY_STRING()] as? [RMExpenseCategory]
            }
            
            
            if let _ = projectsDict["timerStatus"] {
                let arrayTimerStatus = projectsDict["timerStatus"] as? [String]
                if arrayTimerStatus != nil &&  arrayTimerStatus!.count > 0 {
                        let stringTimerStatus = arrayTimerStatus!.first
                    if stringTimerStatus == "start" {
                        self?.isTimerStarted = false
                    } else {
                        self?.isTimerStarted = true
                    }
                }
            }
            
//                        self?.segment.insertSegment(withTitle: RMStrings.MILESTONES_STRING(), at: 3, animated: false)
//            let index = self?.segment.numberOfSegments ?? 0
//                        self?.segment.insertSegment(withTitle: RMStrings.NOTES_STRING(), at: index, animated: false)
//                        self?.segment.insertSegment(withTitle: RMStrings.FILES_STRING(), at: 5, animated: false)
                
//                }
//                else {
//                    self?.projectOverview.arrayToDisplay = subArray as? Array<Any>
//                }
            if self != nil {
                self!.segment.selectedSegmentIndex = 0
                self!.changeSegment(self!.segment)
            }
            
            self?.projectOverview.tableView.reloadData()
            self?.taskList.tableView.reloadData()
            self?.activityList.tableView.reloadData()
            self?.noteList.tableView.reloadData()
            self?.milestoneList.tableView.reloadData()
            self?.fileList.tableView.reloadData()
            self?.timesheetList.tableView.reloadData()
            
            if segmentSelectionOnCompletion != nil {
                let indexNeedToSelect = self?.indexOfSegment(title: segmentSelectionOnCompletion!)
                if indexNeedToSelect != nil {
                    self?.segment.selectedSegmentIndex = indexNeedToSelect!
                    self?.changeSegment((self?.segment)!)
                }
            }
            
            }
            
        
            
        
    }
    
    
    class func addConstraintWithSuperView(view:UIView){
        NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: view.superview, attribute: .top, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: view.superview, attribute: .bottom, multiplier: 1, constant: 10).isActive = true
        NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: view.superview, attribute: .leading, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: view.superview, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
    }

}

extension RMProjectViewController: RMNoteListTableViewControllerDelegate {
    @objc func notesFetched(noteListTableVC: RMNoteListTableViewController) {
        
        if projectOverview.arrayToDisplay == nil {
            let selector = #selector(notesFetched(noteListTableVC:))
            self.perform(selector, with: nil, afterDelay: 0.5)
            return;
        }
        
        
        let indexNotes = indexOfNotesSegment()
        if indexNotes != nil {
            return
        }
//        if projectOverview.arrayToDisplay?.count
        
        if noteList.displayingArray().count > 0 {
            let index = self.segment.numberOfSegments
            self.segment.insertSegment(withTitle: RMStrings.NOTES_STRING(), at: index, animated: false)
        }
//        else {
//            if self.segment.numberOfSegments <= 1 {
//                return
//            }
//
//            if indexNotes != nil {
//                self.segment.removeSegment(at: indexNotes!, animated: false)
//            }
//        }
    }
}

extension RMProjectViewController: RMTaskListTableViewControllerDelegate {
    func taskChanged(taskListTableVC:RMTaskListTableViewController) {
        fetchProjectDetails(segmentSelectionOnCompletion:RMStrings.TASKS_STRING())
    }
}

extension RMProjectViewController: RMFileListTableViewControllerDelegate {
    func fileChanged(fileListTableVC:RMFileListTableViewController) {
        fetchProjectDetails(segmentSelectionOnCompletion:RMStrings.FILES_STRING())
    }
}

extension RMProjectViewController: RMExpenseListTableViewControllerDelegate {
    func expenseChanged(fileListTableVC:RMExpenseListTableViewController) {
        fetchProjectDetails(segmentSelectionOnCompletion:RMStrings.EXPENSE_STRING())
    }
}

extension RMProjectViewController:RMProjectOverviewTableViewControllerDelegate {
    func timerUpdated(timerIsStarted: Bool) {
        isTimerStarted = timerIsStarted
    }
}
//extension RMProjectViewController: RMNavigationBarButtonItems {
//    func neededNavigationBarButtonItems() -> RMNeededNavigationButtonItem {
//        return .leftMenu
//    }
//}
//
//extension RMProjectViewController: RMNavigationBarButtonActions {
//    func barButtonLeftAction() {
//        RMSideMenuAndRoot.showLeftMenu()
//    }
//}

