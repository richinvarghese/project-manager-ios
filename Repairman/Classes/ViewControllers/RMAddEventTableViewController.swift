//
//  RMAddEventTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 23/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit

protocol RMAddEventDelegate:class {
    func addedOrEditedEvent(addEventTableVC:RMAddEventTableViewController)
}

class RMAddEventTableViewController: UITableViewController {

    public class func instance() -> RMAddEventTableViewController {
        let storyBoard = UIStoryboard(name: "RMNoteAndEvent", bundle: nil)
        let newEventTableVC = storyBoard.instantiateViewController(withIdentifier: "RMAddEventTableViewController") as! RMAddEventTableViewController
        //        newTimeLineTableVC.selectedTimeLine = selectedTimeLine
        //        newTimeLineTableVC.replyToTimeLine = replyToTimeLine
        return newEventTableVC
    }
    @IBOutlet weak var textFieldEventStartDate: UITextField!
    @IBOutlet weak var textFieldEventEndDate: UITextField!
    @IBOutlet weak var textFieldEventLocation: UITextField!
    @IBOutlet weak var textViewEventDesciption: UITextView!
    var labelPlaceholderDescription : UILabel?
    let lengthLimitDescription = 250
    @IBOutlet weak var textFieldEventTitle: UITextField!
    let lengthLimitEventTitle = 30
    var selectedEvent:RMEvent?
    weak var delegate:RMAddEventDelegate?
    
    var datePickerStartDate: UIDatePicker?
    var datePickerEndDate: UIDatePicker?
    
    @IBOutlet weak var labelVisibility: UILabel!
    @IBOutlet weak var labelOnlyMe: UILabel!
    @IBOutlet weak var labelAllTeamMembers: UILabel!
    @IBOutlet weak var buttonRadioOnlyMeImageOnly: UIButton!
    @IBOutlet weak var buttonRadioAllTeamMembersImageOnly: UIButton!
     @IBOutlet weak var submitButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView()
        self.tableView.allowsSelection = false
        labelVisibility.textColor = RMGlobalManager.appBlueColor()
        labelOnlyMe.textColor = RMGlobalManager.appBlueColor()
        labelAllTeamMembers.textColor = RMGlobalManager.appBlueColor()
        labelPlaceholderDescription = RMGlobalManager.addPlaceholderForView(view: textViewEventDesciption, text: RMStrings.DESCRIPTION_STRING(), font: textViewEventDesciption.font!)
        if textViewEventDesciption.text.count > 0 {
            labelPlaceholderDescription?.isHidden = true
        }
        
        self.tableView.separatorStyle = .none
        //        self.tableView.allowsSelection = false
        let barButtonItem = UIBarButtonItem(title: RMStrings.DONE_STRING(), style: .done, target: self, action: #selector(barButtonDoneAction))
        let toolbarWithBarButtonDone = RMGlobalManager.toolbar(barButtonItem: barButtonItem)
        textViewEventDesciption.inputAccessoryView = toolbarWithBarButtonDone
        
        textViewEventDesciption.layer.borderWidth = 1
        textViewEventDesciption.layer.borderColor = UIColor.lightGray.cgColor
        textViewEventDesciption.delegate = self as UITextViewDelegate
        
        setupTextField(textField: textFieldEventStartDate)
        setupTextField(textField: textFieldEventEndDate)
        setupTextField(textField: textFieldEventTitle)
        setupTextField(textField: textFieldEventLocation)
        textFieldEventStartDate.placeholder = RMStrings.START_DATE_STRING()
        textFieldEventEndDate.placeholder = RMStrings.END_DATE_STRING()
        
        textFieldEventLocation.placeholder = "Location"
        
        datePickerStartDate = UIDatePicker(frame: CGRect(x:0,y:0,width:UIScreen.main.bounds.size.width,height:200))
        datePickerStartDate?.minimumDate = nil;
        datePickerStartDate?.maximumDate = nil;
        datePickerStartDate?.translatesAutoresizingMaskIntoConstraints = false
        //datePickerStartDate?.datePickerMode = .date
        datePickerStartDate?.datePickerMode = .dateAndTime
        datePickerStartDate?.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
        
        textFieldEventStartDate?.inputView = datePickerStartDate
        textFieldEventStartDate.delegate = self
        textFieldEventStartDate?.inputAccessoryView = RMGlobalManager.toolBarWithNext(target: self, action: #selector(barButtonNextAction))
        
        
        datePickerEndDate = UIDatePicker(frame: CGRect(x:0,y:0,width:UIScreen.main.bounds.size.width,height:200))
        datePickerEndDate?.minimumDate = nil;
        datePickerEndDate?.maximumDate = nil;
        datePickerEndDate?.translatesAutoresizingMaskIntoConstraints = false
        datePickerEndDate?.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
        //datePickerEndDate?.datePickerMode = .date
        datePickerEndDate?.datePickerMode = .dateAndTime
        textFieldEventEndDate?.inputView = datePickerEndDate
        textFieldEventEndDate.delegate = self
        textFieldEventEndDate?.inputAccessoryView = RMGlobalManager.toolBarWithNext(target: self, action: #selector(barButtonNextAction))
        
        
//        startDatePicker = RMGlobalManager.
//        let datePicker = UIDatePicker()
//        datePicker.datePickerMode = .date
//        if selectedEvent != nil {
//            datePicker.date = selectedEvent!.eventStartDate!//FIXME: no idaer
//        } else {
//            datePicker.date = Date()
//        }
//        dateChanged(datePicker)
//        datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
//        textFieldEventDate.inputView = datePicker
        
        textFieldEventTitle.placeholder = RMStrings.TITLE_STRING()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction))
        //        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        
        
        if selectedEvent != nil {
            textFieldEventTitle.text = selectedEvent!.eventTitle
            textViewEventDesciption.text = selectedEvent!.eventDescription
            labelPlaceholderDescription?.isHidden = true
            
//            textFieldEventStartDate.text = RMGlobalManager.webServiceOnlyStringDateFromDate(date: selectedEvent!.eventStartDate!)
//            textFieldEventEndDate.text = RMGlobalManager.webServiceOnlyStringDateFromDate(date: selectedEvent!.eventEndDate!)
            textFieldEventStartDate.text = RMGlobalManager.shortDateAndTimeString(date: selectedEvent!.eventStartDate!)
            textFieldEventEndDate.text = RMGlobalManager.shortDateAndTimeString(date: selectedEvent!.eventEndDate!)
            textFieldEventLocation.text = selectedEvent!.eventLocation
        }
        
        buttonRadioOnlyMeImageOnly.isSelected = true
        setupButton(button: submitButton)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupButton(button:UIButton) {
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 2
        button.clipsToBounds = true
        button.layer.borderColor =  UIColor.blue.cgColor
        button.backgroundColor = RMGlobalManager.appBlueColor()
    }
    
    func setupTextField(textField:UITextField) {
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.borderStyle = .none
        let leftPaddingView = UIView(frame: CGRect(x:0, y:0,width:10,height:textField.frame.size.height))
        textField.leftView = leftPaddingView
        textField.leftViewMode = .always
        textField.returnKeyType = .next
        
        textField.delegate = self as UITextFieldDelegate
        
        let barButtonItemNext = UIBarButtonItem(title: RMStrings.NEXT_STRING(), style: .done, target: self, action: #selector(barButtonNextAction))
        let toolbarWithBarButtonNext = RMGlobalManager.toolbar(barButtonItem: barButtonItemNext)
        textField.inputAccessoryView = toolbarWithBarButtonNext
    }
    
    
    @objc func tapGestureAction() {
        
        self.view.endEditing(true)
        
    }
    
    @objc func datePickerValueChanged(_ sender: UIDatePicker) {
        
        if sender == datePickerStartDate {
        textFieldEventStartDate.text = RMGlobalManager.shortDateAndTimeString(date: sender.date)
//        textFieldEventStartDate.text = RMGlobalManager.webServiceOnlyStringDateFromDate(date: sender.date)
        } else {
//            textFieldEventEndDate.text = RMGlobalManager.webServiceOnlyStringDateFromDate(date: sender.date)
            textFieldEventEndDate.text = RMGlobalManager.shortDateAndTimeString(date: sender.date)
        }
//        let componenets = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
//        if let day = componenets.day, let month = componenets.month, let year = componenets.year {
//            print("\(day) \(month) \(year)")
//        }
    }
    
    @IBAction func buttonActionPost(_ sender: Any) {
        textViewEventDesciption.resignFirstResponder()
        
        
        textViewEventDesciption.text = textViewEventDesciption.text.trimmingCharacters(in: .whitespacesAndNewlines)
        
        textFieldEventTitle.resignFirstResponder()
        
        
        textFieldEventTitle.text = textFieldEventTitle.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController:self)
            return
        }
        
        if textFieldEventTitle.text?.count == 0 {
            RMAlert.showAlert(title: nil, message: RMStrings.ENTER_TITLE_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textFieldEventTitle.becomeFirstResponder()
            })
            return
        }
        
        if textFieldEventStartDate.text?.count == 0 {
            RMAlert.showAlert(title: nil, message: RMStrings.ENTER_START_DATE_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textFieldEventStartDate.becomeFirstResponder()
            })
            return
        }
        
        
        if textFieldEventEndDate.text?.count == 0 {
            RMAlert.showAlert(title: nil, message: RMStrings.ENTER_END_DATE_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textFieldEventEndDate.becomeFirstResponder()
            })
            return
        }
        
        
        if  datePickerStartDate!.date > datePickerEndDate!.date {
            RMAlert.showAlert(title: nil, message: "Enter valid end date", showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: { [weak self] (slectedButtonTitle) in
                self?.textFieldEventEndDate.becomeFirstResponder()
            })
            return;
        }
        
        
        if textViewEventDesciption.text.count == 0 {
            RMAlert.showAlert(title: nil, message: RMStrings.ENTER_DESCRIPTION_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textViewEventDesciption.becomeFirstResponder()
            })
            return
        }
        
        
        
        
        
        
        /***
         Success
         */
        
//        let startDateStringUTC = RMGlobalManager.webServiceOnlyStringDateFromDate(date: datePickerStartDate!.date)
//        let endDateStringUTC = RMGlobalManager.webServiceOnlyStringDateFromDate(date: datePickerEndDate!.date)
////        RMGlobalManager.webservice
        
        let startDateStringUTC = RMGlobalManager.webServiceStringDateAndTimeFromDate(date: datePickerStartDate!.date)
        let endDateStringUTC = RMGlobalManager.webServiceStringDateAndTimeFromDate(date: datePickerEndDate!.date)
        
        
        let selectedEventUniqueId = selectedEvent?.eventUniqueId
        
        RMWebServiceManager.saveEvent(eventUniqueId: selectedEventUniqueId, userUniqueId: RMUserDefaultManager.getCurrentUserUniqueId()!, eventStartDate: startDateStringUTC, eventEndDate: endDateStringUTC, eventTitle: textFieldEventTitle.text, eventDescription: textViewEventDesciption.text,eventLocation:textFieldEventLocation.text,isOnlyMe:buttonRadioOnlyMeImageOnly.isSelected) {[weak self] (result, isSuccess) in
            self?.onSuccess()
        }
        
        
//        if selectedEvent != nil {
//            //            selectedTimeLine!.timeLineDescription = textViewNoteDesciption.text
//            //            selectedTimeLine!.attachment = currentAttachmentData
//            //            selectedTimeLine!.attachmentType = currentAttachmentType
//            //            if currentAttachmentType == RMNote.NoteAttachmentType.Photo {
//            //                var documentData : Data?
//            ////                if currentAttachmentData != nil {
//            ////                    documentData = UIImagePNGRepresentation(imageViewAttachment.image!)!
//            ////                }
//            //               selectedNote!.attachment = documentData
//            //            }
//            selectedEvent!.eventTitle = textFieldEventTitle.text
//            selectedEvent!.eventDescription = textViewEventDesciption.text
////            selectedEvent.
//
////            eventStartDate!//FIXME: no idaer
//            selectedEvent!.eventStartDate = RMGlobalManager.dateFromString(stringOnlyDate:textFieldEventStartDate.text!)
////            selectedEvent!.eventStringDate = RMGlobalManager.onlyDateStringFromDate(date: selectedEvent!.eventDate!)
//
//        } else {
//
//            //FIXME: Dummy data
//            let newEvent = RMEvent()
//            //            newTimeLine.isMyOwnPost = true
//            newEvent.eventTitle = textFieldEventTitle.text
//            newEvent.eventDescription = textViewEventDesciption.text
////            newEvent.eventDate = RMGlobalManager.dateFromString(stringOnlyDate: textFieldEventDate.text!)
//            newEvent.eventStartDate = RMGlobalManager.dateFromString(stringOnlyDate: textFieldEventDate.text!)
//
//            //FIXME: no idear
////            newEvent.eventStringDate = RMGlobalManager.onlyDateStringFromDate(date: newEvent.eventDate!)
//            //            newTimeLine.timeLineOwnerName = "My Name"
//            //            newTimeLine.timeLineOwnerImageURL = "https://i0.wp.com/www.americanbazaaronline.com/wp-content/uploads/2014/02/SundarPichai-300x300.jpg"
//            //            newTimeLine.attachment = currentAttachmentData
//            //            newTimeLine.attachmentType = currentAttachmentType
//
//            //            if replyToTimeLine == nil {
//            RMEventCalendarTableViewController.arrayEvents?.insert(newEvent, at: 0)
//            //                newTimeLine.isAdminPost = true
//            //            } else {
//            //                let indexOfReply = RMTimeLineListTableViewController.arrayToDisplay!.index(of: replyToTimeLine!)!
//            //                RMTimeLineListTableViewController.arrayToDisplay?.insert(newTimeLine, at: indexOfReply+1)
//            //                newTimeLine.isAdminPost = false
//            //            }
//        }
        
        
    }
    
    func onSuccess() {
        delegate?.addedOrEditedEvent(addEventTableVC:self)
        
        textFieldEventTitle.text = ""
        textViewEventDesciption.text = ""
        textFieldEventStartDate.text = ""
        textFieldEventEndDate.text = ""
        textFieldEventLocation.text = ""
        labelPlaceholderDescription?.isHidden = false
        //        imageViewAttachment.image = nil
        //        currentAttachmentType = nil
        //        currentAttachmentData = nil
        //        selectedTimeLine = nil
        //        replyToTimeLine = nil
        tableView.reloadData()
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func barButtonDoneAction() {
        textViewEventDesciption.resignFirstResponder()
    }
    
    
    @objc func barButtonNextAction() {
        if textFieldEventTitle.isFirstResponder == true {
            textFieldEventTitle.resignFirstResponder()
            textFieldEventStartDate.becomeFirstResponder()
        } else if textFieldEventStartDate.isFirstResponder == true {
            textFieldEventStartDate.resignFirstResponder()
            textFieldEventEndDate.becomeFirstResponder()
        } else if textFieldEventEndDate.isFirstResponder == true {
            textFieldEventEndDate.resignFirstResponder()
            textFieldEventLocation.becomeFirstResponder()
        } else if textFieldEventLocation.isFirstResponder == true {
            textFieldEventLocation.resignFirstResponder()
            textViewEventDesciption.becomeFirstResponder()
        }
    }
    
    @IBAction func buttonActionForRadioButtonOnlyMe() {
        buttonRadioOnlyMeImageOnly.isSelected = true
        buttonRadioAllTeamMembersImageOnly.isSelected = false
    }
    
    @IBAction func buttonActionForRadioButtonAllTeamMembers() {
        buttonRadioOnlyMeImageOnly.isSelected = false
        buttonRadioAllTeamMembersImageOnly.isSelected = true
    }
    // MARK: - Table view delegate
    
    
    
    
}

extension RMAddEventTableViewController : UITextViewDelegate {
    // MARK: - Text view delegate
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText string: String) -> Bool {
        guard let text = textView.text else { return true }
        let newLength = text.count + string.count - range.length
        if(newLength == 0)
        {
            labelPlaceholderDescription?.isHidden = false
        }
        else
        {
            labelPlaceholderDescription?.isHidden = true
        }
        return newLength <= lengthLimitDescription
    }
    
    
    
    
}

extension RMAddEventTableViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == textFieldEventStartDate && textField.text?.count == 0{
            datePickerValueChanged(datePickerStartDate!)
        } else if textField == textFieldEventEndDate && textField.text?.count == 0{
            datePickerValueChanged(datePickerEndDate!)
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        if(newLength == 0)
        {
            //            labelPlaceholderDescription?.isHidden = false
        }
        else
        {
            //            labelPlaceholderDescription?.isHidden = true
        }
        return newLength <= lengthLimitEventTitle
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldEventTitle {
            textFieldEventStartDate.becomeFirstResponder()
            return false
        }
        else if textField == textFieldEventStartDate {
            textFieldEventEndDate.becomeFirstResponder()
            return false
        }
        else if textField == textFieldEventEndDate {
            textViewEventDesciption.becomeFirstResponder()
            return false
        }
        return true
    }

}
