//
//  RMAnnouncementListTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 05/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMAnnouncementListTableViewController: UITableViewController {

    var arrayToDisplay: Array<RMAnnouncement>?//FIXME: temporary code
    //    var delegate: RMFileListTableViewControllerDelegate?
    //    var projectUniqueId:String?
    //    static let reuseIdentifierNoteCell = "RMNoteCell"
    //
    //    var selectedProjectUniqueId: String?
//    var labelTotalDuration: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = RMStrings.ANNOUNCEMENT_STRING()
//        labelTotalDuration = UILabel(frame: CGRect(x:160,y:0,width:UIScreen.main.bounds.size.width,height:40))
        //        labelTotalDuration?.text = "4:04:33"
        
//        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\(RMStrings.TOTAL_STRING()) 06:38:23")
//        attributeString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.lightGray, range: NSMakeRange(0, RMStrings.TOTAL_STRING().count))
//        labelTotalDuration?.textAlignment = .center
//        labelTotalDuration?.attributedText =  attributeString
//        self.tableView.tableHeaderView = labelTotalDuration
        //        self.tableView.register(UITableViewCell.self)
        //        self.tableView.register(RMNoteListTableViewCell.self)
        //        let nib = UINib(nibName: "RMSubtitleTableViewCell", bundle: nil)
        //        self.tableView.register(nib, forCellReuseIdentifier: RMNoteListTableViewController.reuseIdentifierNoteCell)
        self.tableView.register(RMAnnouncementListTableViewCell.self)
        self.tableView.tableFooterView = UIView()
        self.tableView.allowsSelection = false
        //        fetchNote()
        //        self.tableView.estimatedRowHeight = 200
        //        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        
        //        var timeCardInDate: String?
        //        var timeCardInTime: String?
        //        var timeCardOutDate: String?
        //        var timeCardOutTime: String?
        //        var timeCardDuration: String?
        
//        arrayToDisplay = [RMAnnouncement]()
//        let timeCard = RMAnnouncement()
//        timeCard.announcementTitle = "Abfd"
//        timeCard.announcementStartDate = "2018-09-23"
//        timeCard.announcementEndDate = "2019-10-12"
//        timeCard.announcementCreatedByUsername = "John Doe"
//
//        arrayToDisplay?.append(timeCard)//FIXME:temp code
//
//        let timeCard2 = RMAnnouncement()
//        timeCard2.announcementTitle = "rtsgfg"
//        timeCard2.announcementStartDate = "2013-12-26"
//        timeCard2.announcementEndDate = "2014-12-14"
//        timeCard2.announcementCreatedByUsername = "James"
//
//        arrayToDisplay?.append(timeCard2)//FIXME:temp code
        
        fetchAnnouncement()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func fetchAnnouncement() {
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController:self)
            return
        }
        RMWebServiceManager.fetchAnnouncements {[weak self] (result, isSuccess) in
            if isSuccess == true {
                self?.arrayToDisplay = result as! [RMAnnouncement
                ]
                self?.tableView.reloadData()
            }
        }
    }
    //    func fetchActivi() {
    //
    //        //        RMWebServiceManager.saveNote(title: "test123", description: "test4546")
    //        //        return
    //        RMNoteListTableViewController.arrayToDisplay = nil
    //        self.tableView.reloadData()
    //
    //        RMWebServiceManager.fetchNote(projectUniqueId:selectedProjectUniqueId) {[weak self] (result:Any?, isSuccess:Bool) in
    //            if isSuccess == false {
    //                return
    //            }
    //            let notesArray = result as! [RMNote]
    //            RMNoteListTableViewController.arrayToDisplay = notesArray
    //            self?.tableView.reloadData()
    //        }
    //    }
    
    
    //    @objc func buttonActionForAdd() {
    //        let newNoteTableVC = RMAddNoteTableViewController.instance()
    //        newNoteTableVC.delegate = self
    //        newNoteTableVC.selectedProjectUniqueId = selectedProjectUniqueId
    //        self.navigationController?.pushViewController(newNoteTableVC, animated: true)
    //    }
    
    //    func displayingArray() -> [RMNote] {
    //        if RMNoteListTableViewController.arrayToDisplay == nil {
    //            return []
    //        }
    //
    //        return RMNoteListTableViewController.arrayToDisplay!
    //
    //    }
    
    func showingAnnouncement(atIndexPath:IndexPath) -> RMAnnouncement {
        //        let array = displayingArray()
        return arrayToDisplay![atIndexPath.row]
    }
    
    //    func barButtonItemAdd(navigationItem:UINavigationItem) {
    //        let barButtonItemAdd = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(buttonActionForAdd))
    //        navigationItem.rightBarButtonItem = barButtonItemAdd
    //    }
    //
    //    @objc func buttonActionForAdd() {
    //        let addTimeLineTableVC = RMAddTimeLineTableViewController.instance(selectedTimeLine: nil, replyToTimeLine: nil)
    //        addTimeLineTableVC.delegate = self
    //        addTimeLineTableVC.isForFileUpload = true
    //        self.navigationController?.pushViewController(addTimeLineTableVC, animated: true)
    //    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayToDisplay == nil {
            return 0
        }
        return arrayToDisplay!.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let cell: RMAnnouncementListTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        //        if cell == nil {
        //            cell = UITableViewCell(style: UITableViewCellStyle.value2, reuseIdentifier: "UITableViewCell")
        //        }
        
        let announcement = showingAnnouncement(atIndexPath: indexPath)
        cell.setAnnouncement(announcement: announcement)
        //        cell!.textLabel?.text = note.noteTitle
        //        cell!.detailTextLabel?.text = RMGlobalManager.stringFromDate(date:note.notePostedDate!)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let announcement = showingAnnouncement(atIndexPath: indexPath)
        
        //        if projectFile.projectFileImageUrl == nil {
        //            return
        //        }
        //
        //        let documentVC = RMDocumentViewController(url:projectFile.projectFileImageUrl!, isGoingToPresent:true)
        //        //        documentVC.url
        //        //        let imageViewerVC = RMImageViewerViewController.instance(imageURLToShow: URL(string:projectFile.projectFileImageUrl!)!, isGoingToPresent: true)
        //        let navController = UINavigationController(rootViewController: documentVC)
        //        self.present(navController, animated: true, completion: nil)
        //        let editNoteTableVC = RMAddNoteTableViewController.instance()
        //        editNoteTableVC.selectedNote = note
        //        editNoteTableVC.delegate = self
        //        editNoteTableVC.selectedProjectUniqueId = selectedProjectUniqueId
        //        self.navigationController?.pushViewController(editNoteTableVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        let projectFile = showingProjectFile(atIndexPath: indexPath)
        return 164
        //        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
        RMTheme.cellCornerRadius(cell: cell, borderColor: RMGlobalManager.appBlueColor(), backgroundColor: UIColor.clear)
    }
    //    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    //        let note = showingNote(atIndexPath: indexPath)
    //        if editingStyle == .delete {
    //
    //            RMWebServiceManager.deleteNote(noteUniqueId: note.noteUniqueId!, responseHandler: {[weak self] (result:Any?, isSuccess:Bool) in
    //                RMNoteListTableViewController.arrayToDisplay?.remove(at: indexPath.row)
    //                self?.tableView.reloadData()
    //            })
    //
    //
    //        }
    //    }
    
    deinit {
        print("dealloc=RMAnnouncementListTableViewController")
    }
    
}

extension RMAnnouncementListTableViewController: RMNavigationBarButtonItems {
    func neededNavigationBarButtonItems() -> RMNeededNavigationButtonItem {
        
        if navigationController?.viewControllers.count == 1 {
            return .leftMenu
        }
        
        return .none
    }
}

extension RMAnnouncementListTableViewController: RMNavigationBarButtonActions {
    func barButtonLeftAction() {
        RMSideMenuAndRoot.showLeftMenu()
    }
}

