//
//  RMProjectOverviewTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 25/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit

protocol RMProjectOverviewTableViewControllerDelegate: class {
    func timerUpdated(timerIsStarted:Bool)
}

class RMProjectOverviewTableViewController: UITableViewController {

//    public class func instance() -> RMProjectOverviewTableViewController {
//        let storyBoard = UIStoryboard(name: "RMProjectStoryboard", bundle: nil)
//        let projectOverviewTableVC = storyBoard.instantiateViewController(withIdentifier: "RMProjectOverviewTableViewController") as! RMProjectOverviewTableViewController
//        //        newTimeLineTableVC.selectedTimeLine = selectedTimeLine
//        //        newTimeLineTableVC.replyToTimeLine = replyToTimeLine
//        return projectOverviewTableVC
//    }
//    var selectedProjectUniqueId: String? {
//        didSet {
//            fetchProjects()
//        }
//    }
    weak var delegate : RMProjectOverviewTableViewControllerDelegate?
    var buttonTimer : UIButton?
    var projectUniqueId:String?
    
    var arrayToDisplay:Array<Any>? /*{
        didSet{
            if arrayToDisplay == nil {
                return
            }
            let some = arrayToDisplay![1] as! [RMProjectMember]
            var arrayMembers = Array<RMProjectMember>()
            for i in 0...10 {
                let member = RMProjectMember()
                member.memberName = "fads"
                member.memberJobTitle = "dfs"
                member.memberAvatarImageUrl = ""
                arrayMembers.append(member)
            }
            arrayToDisplay![1] = arrayMembers
        }
    }*/
    
    var array2ToDisplay:Any?
    
//    @IBOutlet weak var viewEncloseProgress: UIView!
//    @IBOutlet weak var labelProgress: UILabel!
//    @IBOutlet weak var collectionView: UICollectionView!
//    
//    @IBOutlet weak var labelProgress2: UILabel!
//    @IBOutlet weak var viewEncloseProgressTwo: UIView!
//    @IBOutlet weak var collectionView2: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
//        let circleProgress = CircleProgressBar(frame: CGRect(x:0, y:0, width:viewEncloseProgress.frame.size.width,height:viewEncloseProgress.frame.size.height))
//        viewEncloseProgress.addSubview(circleProgress)
//        circleProgress.progressBarWidth = 15
//        circleProgress.progressBarProgressColor = UIColor.green
//        circleProgress.progressBarTrackColor = UIColor.lightGray
////        circleProgress.layer.borderColor = UIColor.red.cgColor
////        circleProgress.layer.borderWidth = 2
//        circleProgress.setProgress(0.3, animated: true)
//        circleProgress.hintHidden = true
//        circleProgress.hintTextFont = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
//        circleProgress.startAngle = 180
//        viewEncloseProgress.backgroundColor = UIColor.clear
//        circleProgress.backgroundColor = UIColor.clear
//        labelProgress.text = "30%"
//        labelProgress.textColor = UIColor.green
//        labelProgress.font = UIFont.systemFont(ofSize: 25)
//        
//    collectionView.register(RMMembersCollectionViewCell.self)
//        collectionView.dataSource = self
//        collectionView.delegate = self
//        
//        collectionView2.register(RMMembersCollectionViewCell.self)
//        collectionView2.dataSource = self
//        collectionView2.delegate = self
////        collectionView.flow
////        let collectionViewFlowLayout = UICollectionViewFlowLayout()
////        collectionViewFlowLayout.
////        self.collectionView.collectionViewLayout = collectionViewFlowLayout
//        some()
        self.tableView.register(RMProjectTableViewCell.self)
        self.tableView.register(RMProjectMemebersTableViewCell.self)
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.allowsSelection = false
        self.tableView.separatorColor = RMGlobalManager.appBlueColor()
        self.tableView.separatorStyle = .none
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
//    func fetchProjects() {
//
//        //FIXME: Dummy data
////        dummyData()
//
////        return;
//
//        self.arrayToDisplay = nil
//        self.tableView.reloadData()
//
//        guard let projectUniqueId = selectedProjectUniqueId else {
//            return
//        }
//
//        RMWebServiceManager.fetchProjectDetails(projectUniqueId: projectUniqueId, currentUserUniqueId: RMUserDefaultManager.getCurrentUserUniqueId()!) {[weak self] (result:Any?, isSuccess:Bool) in
//            if isSuccess == false {
//                return
//            }
//            let projectsArray = result as! [RMProject]
//            self?.arrayToDisplay = projectsArray
//            self?.tableView.reloadData()
//        }
//
//    }
    
    //FIXME: Dummy data
    func dummyData() {
        self.arrayToDisplay = Array<Any>.init()
        var array = Array<Any>()
        let project = RMProject()
        project.projectTitle = "d"
       project.projectDescription = "df"
        project.projectSartDate = "12-1-2018"
       project.projectEndDate = "14-1-2018"
       project.projectStatus = "Close"
       project.projectPrice = "18"
       project.projectCompanyName = "Manga"
       project.projectCurrencySymbol = "$"
//       project.projectTotalPoints = "5"
//       project.projectCompletedPoints = "2"
       project.projectProgress = "12"
       project.projectUniqueId = "3"
        array.append(project)
        
        
        var arrayMembers = Array<RMProjectMember>()
        for i in 0...1411 {
            let member = RMProjectMember()
            member.memberName = "fads"
            member.memberJobTitle = "dfs"
            member.memberAvatarImageUrl = ""
            arrayMembers.append(member)
        }
        
//        let member = RMProjectMember()
//        member.memberName = "fads"
//        member.memberJobTitle = "dfs"
//        member.memberAvatarImageUrl = ""
//
//        let member2 = RMProjectMember()
//        member2.memberName = "fads"
//        member2.memberJobTitle = "dfs"
//        member2.memberAvatarImageUrl = ""
//
//        let member3 = RMProjectMember()
//        member3.memberName = "fads"
//        member3.memberJobTitle = "dfs"
//        member3.memberAvatarImageUrl = ""
        array.append(arrayMembers)
        arrayToDisplay = array
        array2ToDisplay = array
        self.tableView.reloadData()
    }
    
    
    func barButtonItemTimer(navigationItem:UINavigationItem, isStarted:Bool) {
        
        buttonTimer = UIButton()
        buttonTimer?.addTarget(self, action: #selector(buttonActionForTimer), for: .touchUpInside)
        buttonTimer!.frame = CGRect(x:0,y:0,width:80,height:30)
        buttonTimer!.layer.borderWidth = 1
        if isStarted == true {
            buttonTimer!.setTitle("\u{023F1}\(RMStrings.STOP_STRING())", for: .normal)
            buttonTimer!.setTitleColor(UIColor.red, for: .normal)
            buttonTimer!.layer.borderColor = UIColor.red.cgColor
        } else{            
            buttonTimer!.setTitle("\u{023F1}\(RMStrings.START_STRING())", for: .normal)
//            buttonTimer!.setTitleColor(UIColor(red: 0, green: 179, blue: 147), for: .normal)
            buttonTimer!.setTitleColor(UIColor.blue, for: .normal)
//            buttonTimer!.layer.borderColor = UIColor(red: 0, green: 179, blue: 147).cgColor
            buttonTimer!.layer.borderColor = UIColor.blue.cgColor

        }
        
//        buttonTimer.titleLabel?.font = UIFont(name: "FontAwesome", size: 35)
//        let string = NSString.fontAwesomeIconString(forEnum: FAIcon.FAClockO)
        
        
//        rgb(0,179,147)
        buttonTimer!.layer.cornerRadius = buttonTimer!.frame.size.height/2
        buttonTimer!.clipsToBounds = true
        
        let barButtonItemTimer = UIBarButtonItem(customView: buttonTimer!)
//        let barButtonItemTimer = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(buttonActionForAdd))
        navigationItem.rightBarButtonItem = barButtonItemTimer
        navigationItem.rightBarButtonItem = nil
    }
    
    @objc func buttonActionForTimer() {
        
        let prjectVC = self.delegate as! RMProjectViewController
        if prjectVC.isTimerStarted == true {
            let timernoteTableVC = RMTimerNoteTableViewController.instance()
            timernoteTableVC.arrayTaskList = prjectVC.taskList.arrayTask
            timernoteTableVC.delegate = self
            let nav = RMNavigationController(rootViewController: timernoteTableVC)
            prjectVC.navigationController?.present(nav, animated: true, completion: nil)
            return
        }
        updateTimerInServer(projectUniqueId: projectUniqueId!, note: nil, taskId: nil) { (isWebserviceSuccess, isTimerStarted) in
            
        }
//        let currentUserUniqueId = RMUserDefaultManager.getCurrentUserUniqueId()!
//        RMWebServiceManager.timerStatus(projectUniqueId: projectUniqueId!, currentUserUniqueId: currentUserUniqueId, note: nil, taskId: nil) {[weak self] (result:Any?, isSuccess:Bool) in
//            if isSuccess == true && result is Dictionary<String,Any?> {
//                let dict = result as! Dictionary<String,Any?>
//                let stringTimerStatus = dict["timer_status"] as? String
//                if stringTimerStatus?.lowercased() == "start" {
//                    self?.delegate?.timerUpdated(timerIsStarted: false)
//                } else if stringTimerStatus?.lowercased() == "stop"{
//                    self?.delegate?.timerUpdated(timerIsStarted: true)
//                }
//
//            }
//        }
    }
    
    
    func updateTimerInServer(projectUniqueId:String,note:String?,taskId:String?,completion:@escaping TimerStatusCompletion) {
        let currentUserUniqueId = RMUserDefaultManager.getCurrentUserUniqueId()!
        RMWebServiceManager.timerStatus(projectUniqueId: projectUniqueId, currentUserUniqueId: currentUserUniqueId, note: nil, taskId: nil) {[weak self] (result:Any?, isSuccess:Bool) in
            if isSuccess == true && result is Dictionary<String,Any?> {
                let dict = result as! Dictionary<String,Any?>
                let stringTimerStatus = dict["timer_status"] as? String
                if stringTimerStatus?.lowercased() == "start" {
                    
                    self?.delegate?.timerUpdated(timerIsStarted: false)
                    completion(true,false)
                } else if stringTimerStatus?.lowercased() == "stop"{
                    
                    self?.delegate?.timerUpdated(timerIsStarted: true)
                    completion(true,true)
                } else {
                    completion(false,nil)
                }
                
                
                
            } else {
            completion(false,nil)
            }
        }
    }
//
//    
//    func some() {
//        let circleProgress = CircleProgressBar(frame: CGRect(x:0, y:0, width:viewEncloseProgress.frame.size.width,height:viewEncloseProgress.frame.size.height))
//        viewEncloseProgressTwo.addSubview(circleProgress)
//        circleProgress.progressBarWidth = 15
//        circleProgress.progressBarProgressColor = UIColor.green
//        circleProgress.progressBarTrackColor = UIColor.lightGray
//        //        circleProgress.layer.borderColor = UIColor.red.cgColor
//        //        circleProgress.layer.borderWidth = 2
//        circleProgress.setProgress(0.1, animated: true)
//        circleProgress.hintHidden = true
//        circleProgress.hintTextFont = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
//        circleProgress.startAngle = 180
//        viewEncloseProgressTwo.backgroundColor = UIColor.clear
//        circleProgress.backgroundColor = UIColor.clear
//        
//        labelProgress2.text = "10%"
//        labelProgress2.textColor = UIColor.green
//        labelProgress2.font = UIFont.systemFont(ofSize: 25)
//    }
    
    func showingProject(atIndexPath:IndexPath) -> Any {
        //        let array = displayingArray()
        return arrayToDisplay![atIndexPath.row]
    }
    
    

    // MARK: - Table view data source

//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 0
//    }
//
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = arrayToDisplay?.count
        if count == nil {
            return 0
        }
        return count!
//        return arrayToDisplay
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let object = showingProject(atIndexPath: indexPath)
        var cell : UITableViewCell?
        if object is RMProject {
            let project = object as! RMProject
            let cellProject:RMProjectTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell = cellProject
            cellProject.setProject(project: project)
        } else {
            let projectMemebers = object as! [RMProjectMember]
            
            let cellProjectMember: RMProjectMemebersTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell = cellProjectMember
            cellProjectMember.setProjectMembers(projectMembers: projectMemebers)
        }
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt
        indexPath: IndexPath) -> CGFloat {
        let object = showingProject(atIndexPath: indexPath)
        if object is RMProject {
            return UITableViewAutomaticDimension
        } else {
            let array = object as! Array<RMProjectMember>
            var count = array.count/3
            let width = RMProjectMemebersTableViewCell.collectionViewCellHieght()
            if count == 0 {
                return width + 20
            }
            count = count + 1
            return CGFloat(CGFloat(count) * width) + 20
        }
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let object = showingProject(atIndexPath: indexPath)
        if object is RMProject {
            return UITableViewAutomaticDimension
        } else {
            let array = object as! Array<RMProjectMember>
            var count = array.count/3
            let width = RMProjectMemebersTableViewCell.collectionViewCellHieght()
            if count == 0 {
                return width + 20
            }
            count = count + 1
            return CGFloat(CGFloat(count) * width) + 20
        }
    }

}

extension RMProjectOverviewTableViewController:RMTimerNoteTableViewControllerDelegate {
    func updateTimerInServer(timerNoteTableViewController: RMTimerNoteTableViewController, note: String?, taskUniqueId: String?, completionHandler: @escaping (Bool, Bool?) -> Void) {
        updateTimerInServer(projectUniqueId: projectUniqueId!, note: note, taskId: taskUniqueId, completion: completionHandler)
    }
}

//extension RMProjectOverviewTableViewController: UICollectionViewDelegate, UICollectionViewDataSource {
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return 5
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell:RMMembersCollectionViewCell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as RMMembersCollectionViewCell
//        if collectionView == collectionView2 {
//            cell.managa = 2
//        }
////        cell.contentView.backgroundColor = UIColor.red
//        return cell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        let cell = cell as! RMMembersCollectionViewCell
//        print("\(cell.memberAvatarImageView.frame)")
////        cell.memberAvatarImageView.layer.cornerRadius = CGFloat(roundf(Float(cell.memberAvatarImageView
////            .frame.size.width/2.0)))
////        cell.memberAvatarImageView.layer.masksToBounds = true
////        cell.memberAvatarImageView.clipsToBounds = true
//    }
////    size
//}
//
//extension RMProjectOverviewTableViewController:UICollectionViewDelegateFlowLayout {
//    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let width = UIScreen.main.bounds.size.width - 16 - 16 - 16 - 16 //16 for viewEncloseTableView, 16 for collectionView
//        return CGSize(width: width/3, height: width/3)
//    }
//
////    @available(iOS 6.0, *)
//    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return .zero
//    }
////
////    @available(iOS 6.0, *)
//    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
////
////    @available(iOS 6.0, *)
//    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
////
////    @available(iOS 6.0, *)
////    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
////
////    }
////
////    @available(iOS 6.0, *)
////     public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize
//}

