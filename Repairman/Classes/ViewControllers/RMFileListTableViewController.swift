//
//  RMFileListTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 27/01/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

protocol RMFileListTableViewControllerDelegate: class {
    func fileChanged(fileListTableVC:RMFileListTableViewController)
}

class RMFileListTableViewController: UITableViewController {

    var arrayToDisplay: Array<RMProjectFile>?//FIXME: temporary code
    var delegate: RMFileListTableViewControllerDelegate?
    var projectUniqueId:String?
    //    static let reuseIdentifierNoteCell = "RMNoteCell"
    //
    //    var selectedProjectUniqueId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.title = RMStrings.ACTIVITIES_STRING()
        //        self.tableView.register(UITableViewCell.self)
        //        self.tableView.register(RMNoteListTableViewCell.self)
        //        let nib = UINib(nibName: "RMSubtitleTableViewCell", bundle: nil)
        //        self.tableView.register(nib, forCellReuseIdentifier: RMNoteListTableViewController.reuseIdentifierNoteCell)
        self.tableView.register(RMFileListTableViewCell.self)
        self.tableView.tableFooterView = UIView()
        //        fetchNote()
        self.tableView.estimatedRowHeight = 200
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    //    func fetchActivi() {
    //
    //        //        RMWebServiceManager.saveNote(title: "test123", description: "test4546")
    //        //        return
    //        RMNoteListTableViewController.arrayToDisplay = nil
    //        self.tableView.reloadData()
    //
    //        RMWebServiceManager.fetchNote(projectUniqueId:selectedProjectUniqueId) {[weak self] (result:Any?, isSuccess:Bool) in
    //            if isSuccess == false {
    //                return
    //            }
    //            let notesArray = result as! [RMNote]
    //            RMNoteListTableViewController.arrayToDisplay = notesArray
    //            self?.tableView.reloadData()
    //        }
    //    }
    
    
    //    @objc func buttonActionForAdd() {
    //        let newNoteTableVC = RMAddNoteTableViewController.instance()
    //        newNoteTableVC.delegate = self
    //        newNoteTableVC.selectedProjectUniqueId = selectedProjectUniqueId
    //        self.navigationController?.pushViewController(newNoteTableVC, animated: true)
    //    }
    
    //    func displayingArray() -> [RMNote] {
    //        if RMNoteListTableViewController.arrayToDisplay == nil {
    //            return []
    //        }
    //
    //        return RMNoteListTableViewController.arrayToDisplay!
    //
    //    }
    
    func showingProjectFile(atIndexPath:IndexPath) -> RMProjectFile {
        //        let array = displayingArray()
        return arrayToDisplay![atIndexPath.row]
    }
    
    func barButtonItemAdd(navigationItem:UINavigationItem) {
        let barButtonItemAdd = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(buttonActionForAdd))
        navigationItem.rightBarButtonItem = barButtonItemAdd
    }
    
    @objc func buttonActionForAdd() {
        let addTimeLineTableVC = RMAddTimeLineTableViewController.instance(selectedTimeLine: nil, replyToTimeLine: nil)
        addTimeLineTableVC.delegate = self
       addTimeLineTableVC.isForFileUpload = true
        self.navigationController?.pushViewController(addTimeLineTableVC, animated: true)
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayToDisplay == nil {
            return 0
        }
        return arrayToDisplay!.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let cell: RMFileListTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        //        if cell == nil {
        //            cell = UITableViewCell(style: UITableViewCellStyle.value2, reuseIdentifier: "UITableViewCell")
        //        }
        
        let projectFile = showingProjectFile(atIndexPath: indexPath)
        cell.setProjectFile(projectFile: projectFile)
        //        cell!.textLabel?.text = note.noteTitle
        //        cell!.detailTextLabel?.text = RMGlobalManager.stringFromDate(date:note.notePostedDate!)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let projectFile = showingProjectFile(atIndexPath: indexPath)
        
        if projectFile.projectFileImageUrl == nil {
            return
        }
        
        let documentVC = RMDocumentViewController(url:projectFile.projectFileImageUrl!, isGoingToPresent:true)
//        documentVC.url
//        let imageViewerVC = RMImageViewerViewController.instance(imageURLToShow: URL(string:projectFile.projectFileImageUrl!)!, isGoingToPresent: true)
        let navController = UINavigationController(rootViewController: documentVC)
        self.present(navController, animated: true, completion: nil)
        //        let editNoteTableVC = RMAddNoteTableViewController.instance()
        //        editNoteTableVC.selectedNote = note
        //        editNoteTableVC.delegate = self
        //        editNoteTableVC.selectedProjectUniqueId = selectedProjectUniqueId
        //        self.navigationController?.pushViewController(editNoteTableVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        let projectFile = showingProjectFile(atIndexPath: indexPath)
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        RMTheme.cellCornerRadius(cell: cell, borderColor: UIColor.red, backgroundColor: UIColor.white)
    }
    //    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    //        let note = showingNote(atIndexPath: indexPath)
    //        if editingStyle == .delete {
    //
    //            RMWebServiceManager.deleteNote(noteUniqueId: note.noteUniqueId!, responseHandler: {[weak self] (result:Any?, isSuccess:Bool) in
    //                RMNoteListTableViewController.arrayToDisplay?.remove(at: indexPath.row)
    //                self?.tableView.reloadData()
    //            })
    //
    //
    //        }
    //    }
    
    deinit {
        print("dealloc=RMActivityListTableViewController")
    }

}

extension RMFileListTableViewController:RMAddTimeLineTableViewControllerDelegate{
    
    func modifiedTimeLine(addTimeLineTableVC:RMAddTimeLineTableViewController) {
//        fetchTimeLine()
        self.delegate?.fileChanged(fileListTableVC: self)
        
        
    }
    
    @objc func timeLinePostbuttonAction(addTimeLineTableVC:RMAddTimeLineTableViewController,fileData:Data?,timeLineDescription:String, mimeType:String?,fileExtension:String?,completion:@escaping (Bool) -> Swift.Void) {
        
        
        
        RMWebServiceManager.fileUpload(projectUniqueId: projectUniqueId!, fileDescription: timeLineDescription, currentUserUniqueId: RMUserDefaultManager.getCurrentUserUniqueId()!, fileData: fileData!, mimeType: mimeType!, fileExtension: fileExtension!) { (result:Any?, isSuccess:Bool) in
            completion(isSuccess)
            
            if isSuccess == true {
                self.delegate?.fileChanged(fileListTableVC: self)
            }
        }
    }
}
