//
//  RMAddLeaveTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 26/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

public enum LeaveDuration: Int {
    case singleDate = 1
    case multipleDate = 2
    case hour = 3
}

@objc protocol RMAddLeaveTableViewControllerDelegate: class {
    func modifiedLeave(addLeaveTableViewController:RMAddLeaveTableViewController)
}

class RMAddLeaveTableViewController: UITableViewController {
    
    //        var isForFileUpload:Bool?
    @IBOutlet weak var textFieldLeaveType: UITextField!
    @IBOutlet weak var textFieldStartDate: UITextField!
    @IBOutlet weak var textFieldEndDate: UITextField!
    @IBOutlet weak var textFieldSingleDate: UITextField!
    @IBOutlet weak var textFieldHour: UITextField!
//    @IBOutlet weak var textFieldStartDate: UITextField!
    @IBOutlet weak var textViewDesciption: UITextView!
    @IBOutlet weak var segmentLeaveDuration: UISegmentedControl!
    var labelPlaceholderDescription : UILabel?
    let lengthLimitDescription = 250
    //        var labelPlaceholderDescription : UILabel?
    var selectedLeaveType:RMLeaveType?
//    let lengthLimitTitle = 100
//    internal var selectedMessage : RMMessage?
//    private var replyToMessage : RMMessage?
//    var currentAttachmentType: RMTimeLine.TimeLineAttachmentType?
//    var currentAttachmentData: Data?
//    var currentAttachmentURL: URL?
    weak var delegate:RMAddLeaveTableViewControllerDelegate?
    var arrayLeaveType: [RMLeaveType]?
    lazy var arrayHour = { () -> [String] in
        let arrayHour = ["1","2","3","4","5","6","7","8"]
        return arrayHour
    }()
    
    var datePickerStartDate: UIDatePicker?
    var datePickerEndDate: UIDatePicker?
    var datePickerSingleDate: UIDatePicker?
    //        var replyToMessageUniqueId:String?
    
//    @IBOutlet weak var imageViewAttachment: UIImageView!
    @IBOutlet weak var startDateCell: UITableViewCell!
    @IBOutlet weak var endDateCell: UITableViewCell!
    @IBOutlet weak var singleDateCell: UITableViewCell!
    @IBOutlet weak var hourCell: UITableViewCell!
    @IBOutlet weak var submitButton: UIButton!
    
    public class func instance() -> RMAddLeaveTableViewController {
        let leaveStoryBoard = UIStoryboard(name: "RMLeave", bundle: nil)
        let newLeaveTableVC = leaveStoryBoard.instantiateViewController(withIdentifier: "RMAddLeaveTableViewController") as! RMAddLeaveTableViewController
        return newLeaveTableVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textViewDesciption.text = ""
        textViewDesciption.layer.borderWidth = 1
        textViewDesciption.layer.borderColor = UIColor.lightGray.cgColor
        textViewDesciption.delegate = self
        //        let barButtonItemAttachment = UIBarButtonItem(title: RMStrings.ATTACHMENT_STRING(), style: .done, target: self, action: #selector(buttonActionForAttachment))
        //            if replyToMessage == nil {
//        let barButtonItemAttachment = UIBarButtonItem(image: UIImage(named:"AttachmentButton"), style: .done, target: self, action: #selector(buttonActionForAttachment))
//        self.navigationItem.rightBarButtonItem = barButtonItemAttachment
        //            }
        
        //            if selectedMessage?.isAdminPost == false {
        //                self.navigationItem.rightBarButtonItem = nil
        //            }
        
        self.tableView.tableFooterView = UIView()
        labelPlaceholderDescription = RMGlobalManager.addPlaceholderForView(view: textViewDesciption, text: RMStrings.REASON_STRING(), font: textViewDesciption.font!)
        if textViewDesciption.text.count > 0 {
            labelPlaceholderDescription?.isHidden = true
        }
        
        self.tableView.separatorStyle = .none
        //        self.tableView.allowsSelection = false
        let barButtonItem = UIBarButtonItem(title: RMStrings.DONE_STRING(), style: .done, target: self, action: #selector(barButtonDoneAction))
        let toolbarWithBarButton = RMGlobalManager.toolbar(barButtonItem: barButtonItem)
        textViewDesciption.inputAccessoryView = toolbarWithBarButton
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction))
//        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        
        
//        imageViewAttachment.layer.borderColor = UIColor.lightGray.cgColor
//        imageViewAttachment.layer.borderWidth = 1
        
        let _ = RMGlobalManager.pickerView(textField: textFieldLeaveType, toolBarButtonTitle: RMStrings.NEXT_STRING(), pickerViewDelegateAndDataSource: self, barButtonTarget: self, barButtonAction: #selector(barButtonNextAction))
        
        let _ = RMGlobalManager.pickerView(textField: textFieldHour, toolBarButtonTitle: RMStrings.NEXT_STRING(), pickerViewDelegateAndDataSource: self, barButtonTarget: self, barButtonAction: #selector(barButtonNextAction))
        
//        textFieldLeaveType.placeholder = RMStrings.RECIPIENT_STRING()
//        textFieldTitle.placeholder = RMStrings.TITLE_STRING()
        setupTextField(textField: textFieldLeaveType)
        setupTextField(textField: textFieldStartDate)
        setupTextField(textField: textFieldEndDate)
        setupTextField(textField: textFieldSingleDate)
        setupTextField(textField: textFieldHour)
        fetchLeaveType()
        
        
        segmentLeaveDuration.addTarget(self, action: #selector(changeSegment), for: .valueChanged)
        
        
        datePickerStartDate = UIDatePicker(frame: CGRect(x:0,y:0,width:UIScreen.main.bounds.size.width,height:200))
        datePickerStartDate?.minimumDate = Date();
        datePickerStartDate?.maximumDate = nil;
        datePickerStartDate?.translatesAutoresizingMaskIntoConstraints = false
        datePickerStartDate?.datePickerMode = .date
        
        datePickerStartDate?.addTarget(self, action: #selector(datePickerValueChanged(datePicker:)), for: .valueChanged)
        
        textFieldStartDate?.inputView = datePickerStartDate
        textFieldStartDate.delegate = self
        textFieldStartDate?.inputAccessoryView = RMGlobalManager.toolBarWithNext(target: self, action: #selector(barButtonNextAction))
        
        
        datePickerEndDate = UIDatePicker(frame: CGRect(x:0,y:0,width:UIScreen.main.bounds.size.width,height:200))
        datePickerEndDate?.minimumDate = Date();
        datePickerEndDate?.maximumDate = nil;
        datePickerEndDate?.translatesAutoresizingMaskIntoConstraints = false
        datePickerEndDate?.addTarget(self, action: #selector(datePickerValueChanged(datePicker:)), for: .valueChanged)
        datePickerEndDate?.datePickerMode = .date
        textFieldEndDate?.inputView = datePickerEndDate
        textFieldEndDate.delegate = self
        textFieldEndDate?.inputAccessoryView = RMGlobalManager.toolBarWithNext(target: self, action: #selector(barButtonNextAction))
        
        
        
        
        datePickerSingleDate = UIDatePicker(frame: CGRect(x:0,y:0,width:UIScreen.main.bounds.size.width,height:200))
        datePickerSingleDate?.minimumDate = Date();
        datePickerSingleDate?.maximumDate = nil;
        datePickerSingleDate?.translatesAutoresizingMaskIntoConstraints = false
        datePickerSingleDate?.datePickerMode = .date
        
        datePickerSingleDate?.addTarget(self, action: #selector(datePickerValueChanged(datePicker:)), for: .valueChanged)
        
        textFieldSingleDate?.inputView = datePickerSingleDate
        textFieldSingleDate.delegate = self
        textFieldSingleDate?.inputAccessoryView = RMGlobalManager.toolBarWithNext(target: self, action: #selector(barButtonNextAction))
        setupButton(button: submitButton)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setupButton(button:UIButton) {
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 2
        button.clipsToBounds = true
        button.layer.borderColor =  UIColor.blue.cgColor
        button.backgroundColor = RMGlobalManager.appBlueColor()
    }
    
    
    
    func setupTextField(textField:UITextField) {
        textField.leftView = UIView(frame: CGRect(x:0,y:0,width:8,height:30))
        textField.rightView = UIView(frame: CGRect(x:0,y:0,width:8,height:30))
        textField.rightViewMode = .always
        textField.leftViewMode = .always
        textField.delegate = self as! UITextFieldDelegate
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.lightGray.cgColor
        let barButtonItemNext = UIBarButtonItem(title: RMStrings.NEXT_STRING(), style: .done, target: self, action: #selector(barButtonNextAction))
        let toolbarWithBarButtonWithNext = RMGlobalManager.toolbar(barButtonItem: barButtonItemNext)
        textField.inputAccessoryView = toolbarWithBarButtonWithNext
    }
    
    func assignLeaveTypeAtIndex(index:Int) {
        if arrayLeaveType == nil {
            return;
        }
        if arrayLeaveType!.count == 0 {
            return;
        }
        selectedLeaveType = arrayLeaveType![index]
        textFieldLeaveType.text = selectedLeaveType?.leaveTypeTitle
    }
    
    func selectedLeaveDuration() -> LeaveDuration {
        if segmentLeaveDuration.selectedSegmentIndex == 0 {
            return LeaveDuration.singleDate
        } else if segmentLeaveDuration.selectedSegmentIndex == 1 {
            return LeaveDuration.multipleDate
        } else if segmentLeaveDuration.selectedSegmentIndex == 2 {
            return LeaveDuration.hour
        }
        return LeaveDuration.singleDate
    }
    
    @objc func changeSegment(segment:UISegmentedControl) {
        self.view.endEditing(true)
        self.tableView.reloadData()
    }
    
    func selectedLeaveDurationStringWebService() -> String? {
        if selectedLeaveDuration() == .singleDate {
            return "single_date"
        }
        
        if selectedLeaveDuration() == .multipleDate {
            return "multiple_days"
        }
        
        if selectedLeaveDuration() == .hour {
            return "hours"
        }
        return nil
    }
    
    func fetchLeaveType() {
        
        
        
        arrayLeaveType = nil
        tableView.reloadData()
        
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController:self)
            return
        }
        
        
        RMWebServiceManager.fetchLeaveType(currentUserUniqueId: RMUserDefaultManager.getCurrentUserUniqueId()!) { [weak self] (result, isSuccess) in
            if isSuccess == false || result == nil {
                return;
            }
            
            self?.arrayLeaveType = result as? [RMLeaveType]
            self?.tableView.reloadData()
            
        }
    }
    
    @objc func datePickerValueChanged(datePicker:UIDatePicker) {
        if datePicker == datePickerStartDate {
            textFieldStartDate?.text = RMGlobalManager.webServiceOnlyStringDateFromDate(date: datePickerStartDate!.date)
        } else if datePicker == datePickerEndDate {
            textFieldEndDate?.text = RMGlobalManager.webServiceOnlyStringDateFromDate(date: datePickerEndDate!.date)
        } else if datePicker == datePickerSingleDate {
            textFieldSingleDate?.text = RMGlobalManager.webServiceOnlyStringDateFromDate(date: datePickerSingleDate!.date)
        }
    }
    
    
    @objc func tapGestureAction() {
        
        self.view.endEditing(true)
        
    }
    
    @IBAction func buttonActionPost(_ sender: Any) {
        
        textFieldLeaveType.resignFirstResponder()
        
        textFieldSingleDate.resignFirstResponder()
        
        textFieldStartDate.resignFirstResponder()
        
//        textFieldEndDate.text = textFieldRecipient.text?.trimmingCharacters(in: .whitespaces)
        
//        textFieldTitle.text = textFieldTitle.text?.trimmingCharacters(in: .whitespaces)
        
        textViewDesciption.text = textViewDesciption.text.trimmingCharacters(in: .whitespacesAndNewlines)
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController:self)
            return
        }
        
        if textFieldLeaveType.text?.count == 0 {
            RMAlert.showAlert(title: nil, message: RMStrings.ENTER_LEAVE_TYPE_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textFieldLeaveType.becomeFirstResponder()
            })
            return
        }
        
        if textFieldSingleDate.text?.count == 0 && (selectedLeaveDuration() == .singleDate || selectedLeaveDuration() == .hour
            ) {
            RMAlert.showAlert(title: nil, message: RMStrings.ENTER_DATE_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textFieldSingleDate.becomeFirstResponder()
            })
            return
        }
        
        if textFieldHour.text?.count == 0 && (selectedLeaveDuration() == .hour) {
            RMAlert.showAlert(title: nil, message: RMStrings.ENTER_HOUR_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textFieldHour.becomeFirstResponder()
            })
            return
        }
        
        if textFieldStartDate.text?.count == 0 && selectedLeaveDuration() == .multipleDate {
            RMAlert.showAlert(title: nil, message: RMStrings.ENTER_START_DATE_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textFieldStartDate.becomeFirstResponder()
            })
            return
        }
        
        if textFieldEndDate.text?.count == 0 && selectedLeaveDuration() == .multipleDate {
            RMAlert.showAlert(title: nil, message: RMStrings.ENTER_END_DATE_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textFieldEndDate.becomeFirstResponder()
            })
            return
        }
        
        
        
        
        
        
        if selectedLeaveDuration() == .multipleDate && datePickerStartDate!.date > datePickerEndDate!.date {
            RMAlert.showAlert(title: nil, message: "Enter valid end date", showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: { [weak self] (slectedButtonTitle) in
                self?.textFieldEndDate.becomeFirstResponder()
            })
            return;
        }
//        let startDateOnlyString = RMGlobalManager.webServiceOnlyStringDateFromDate(date:datePickerStartDate!.date)
//        let endDateOnlyString = RMGlobalManager.webServiceOnlyStringDateFromDate(date:datePickerEndDate!.date)
        
        
        
        
        
        if textViewDesciption.text.count == 0 {
            RMAlert.showAlert(title: nil, message: RMStrings.ENTER_REASON_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textViewDesciption.becomeFirstResponder()
            })
            return
        }
        
        /***
         Success
         */
        
        
        RMWebServiceManager.postLeave(leaveTypeUniqueId: (selectedLeaveType?.uniqueId)!, leaveReason: textViewDesciption.text, userUniqueId: RMUserDefaultManager.getCurrentUserUniqueId()!, leaveDuration: selectedLeaveDurationStringWebService()!, leaveStartDate: textFieldStartDate.text, leaveEndDate: textFieldEndDate.text, leaveHourDate: textFieldSingleDate.text, leaveHours: textFieldHour.text, leaveSingleDate: textFieldSingleDate.text) {[weak self] (result: Any?,isSuccess:Bool) in
            
            guard let strongSelf = self else {
                return
            }
            
            if isSuccess == true {
                strongSelf.clearAllAndPopNavigationController()
            }
        }
        
        
        
    }
    
    func clearAllAndPopNavigationController() {
        textFieldLeaveType.text = ""
        textFieldSingleDate.text = ""
        textFieldStartDate.text = ""
        textFieldEndDate.text = ""
        textFieldHour.text = ""
        textViewDesciption.text = ""
        labelPlaceholderDescription?.isHidden = false
        
        tableView.reloadData()
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
            self.delegate?.modifiedLeave(addLeaveTableViewController: self)
        }
    }
    
    
    @objc func barButtonNextAction() {
        if textFieldLeaveType.isFirstResponder == true && (selectedLeaveDuration() == .singleDate || selectedLeaveDuration() == .hour) {
            textFieldLeaveType.resignFirstResponder()
            textFieldSingleDate.becomeFirstResponder()
        } else if textFieldLeaveType.isFirstResponder == true && selectedLeaveDuration() == .multipleDate {
            textFieldLeaveType.resignFirstResponder()
            textFieldStartDate.becomeFirstResponder()
        }
        else if textFieldSingleDate.isFirstResponder == true && selectedLeaveDuration() == .hour {
            textFieldSingleDate.resignFirstResponder()
            textFieldHour.becomeFirstResponder()
        } else if textFieldHour.isFirstResponder == true && selectedLeaveDuration() == .hour {
            textFieldHour.resignFirstResponder()
            textViewDesciption.becomeFirstResponder()
        } else if textFieldStartDate.isFirstResponder == true {
            textFieldStartDate.resignFirstResponder()
            textFieldEndDate.becomeFirstResponder()
        } else if textFieldEndDate.isFirstResponder == true {
            textFieldEndDate.resignFirstResponder()
            textViewDesciption.becomeFirstResponder()
        }
        else if textFieldSingleDate.isFirstResponder == true && selectedLeaveDuration() == .singleDate {
            textFieldSingleDate.resignFirstResponder()
            textViewDesciption.becomeFirstResponder()
        }
        
    }
    
    @objc func barButtonDoneAction() {
        textViewDesciption.resignFirstResponder()
    }
    
    
    
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        if selectedLeaveDuration() == .singleDate {
            if cell == startDateCell {
                return 0;
            }
            if cell == endDateCell {
                return 0;
            }
            if cell == hourCell {
                return 0;
            }
        }
        else if selectedLeaveDuration() == .multipleDate {
            if cell == singleDateCell {
                return 0;
            }
            if cell == hourCell {
                return 0;
            }
        }
        else if selectedLeaveDuration() == .hour {
            if cell == startDateCell {
                return 0;
            }
            if cell == endDateCell {
                return 0;
            }
        }
        
        return super.tableView(tableView, heightForRowAt: indexPath)
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if imageViewAttachment.image != nil && cell == attachementCell {
//            cell.selectionStyle = .default
//        } else {
//            cell.selectionStyle = .none
//        }
    }
    
}

extension RMAddLeaveTableViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldEndDate || textField == textFieldHour || (selectedLeaveDuration() == .singleDate && textField == textFieldSingleDate) {
            textField.resignFirstResponder()
            textViewDesciption.becomeFirstResponder()
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textFieldLeaveType == textField && textField.text?.count == 0 {
            
            assignLeaveTypeAtIndex(index: 0)
        }
        
        if textFieldHour == textField && textField.text?.count == 0 {
            
            textFieldHour.text = arrayHour[0]
        }
        
        if (textFieldSingleDate == textField || textFieldStartDate == textField || textFieldEndDate == textField) && textField.text?.count == 0 {
            textField.text = RMGlobalManager.webServiceOnlyStringDateFromDate(date:Date())
        }
        
        return true
    }
}

extension RMAddLeaveTableViewController : UITextViewDelegate {
    // MARK: - Text view delegate
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText string: String) -> Bool {
        guard let text = textView.text else { return true }
        let newLength = text.count + string.count - range.length
        if(newLength == 0)
        {
            labelPlaceholderDescription?.isHidden = false
        }
        else
        {
            labelPlaceholderDescription?.isHidden = true
        }
        return newLength <= lengthLimitDescription
    }
    
    
    
}


//extension RMAddLeaveTableViewController: UIGestureRecognizerDelegate {
//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
//        if gestureRecognizer is UITapGestureRecognizer {
//            let location = touch.location(in: touch.view)
//            if view.frame.contains(location) && imageViewAttachment.image != nil {
//                return false
//            }
//        }
//        return true
//    }
//}

extension RMAddLeaveTableViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
    //
    //    }
    
    
    // returns the # of rows in each component..
    @available(iOS 2.0, *)
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == textFieldHour.inputView {
            return arrayHour.count
        }
        if arrayLeaveType == nil {
            return 0
        }
        return arrayLeaveType!.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == textFieldHour.inputView {
            return arrayHour[row]
        }
        let title = arrayLeaveType![row].leaveTypeTitle
        return title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == textFieldHour.inputView {
            textFieldHour.text = arrayHour[row]
            return;
        }
        assignLeaveTypeAtIndex(index: row)
    }
    
    
}

