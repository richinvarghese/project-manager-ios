//
//  RMDashboardTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 04/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit
import Crashlytics

class RMDashboardTableViewController: UITableViewController {
    @IBOutlet weak var viewSeperatorInBetweenWorkedAndProjectTimes: UIView!
    @IBOutlet weak var labelProjectSeconds: UILabel!
    @IBOutlet weak var labelProjectMinutes: UILabel!
    @IBOutlet weak var labelProjectHours: UILabel!
    @IBOutlet weak var labelWorkHours: UILabel!
    @IBOutlet weak var labelWorkMinutes: UILabel!
    @IBOutlet weak var labelWorkSeconds: UILabel!
    @IBOutlet weak var labelClockedIn: UILabel!
    
    @IBOutlet weak var labelProjectHoursFullCoulmn: UILabel!
    @IBOutlet weak var labelProjectMinFullCoulmn: UILabel!
    
    @IBOutlet weak var buttonClockInorOut: UIButton!
    
    @IBOutlet weak var buttonEncloseClockIn: UIButton!
    @IBOutlet weak var viewEncloseClockIn: UIView!
    @IBOutlet weak var viewEncloseProject: UIView!
    @IBOutlet weak var buttonEncloseProject: UIButton!
    
    
    @IBOutlet weak var buttonEncloseTimecards: UIButton!
    @IBOutlet weak var viewEncloseTimecards: UIView!
    @IBOutlet weak var viewEncloseLeave: UIView!
    @IBOutlet weak var buttonEncloseLeave: UIButton!
    
    
    @IBOutlet weak var buttonEncloseAnnouncements: UIButton!
    @IBOutlet weak var viewEncloseAnnouncements: UIView!
    @IBOutlet weak var viewEncloseMessages: UIView!
    @IBOutlet weak var buttonEncloseMessages: UIButton!
    
    @IBOutlet weak var buttonEncloseCurrentLocation: UIButton!
    @IBOutlet weak var viewEncloseCurrentLocation: UIView!
    @IBOutlet weak var viewEncloseEvents: UIView!
    @IBOutlet weak var buttonEncloseEvents: UIButton!
    
    
    @IBOutlet weak var buttonEncloseTimeline: UIButton!
    @IBOutlet weak var viewEncloseTimeline: UIView!
    @IBOutlet weak var viewEncloseNotes: UIView!
    @IBOutlet weak var buttonEncloseNotes: UIButton!
    
    @IBOutlet weak var viewEncloseTimings: UIView!
    @IBOutlet weak var viewEncloseProjectHours: UIView!
    @IBOutlet weak var viewEncloseWorkHours: UIView!
    
    @IBOutlet weak var labelTitleTotalProjectHours: UILabel!
    @IBOutlet var arrayViewsWorkTime: [UILabel]!
    @IBOutlet var arrayViewsProjectTime: [UILabel]!
    
    var lastLocationUpdatedDate:Date?
    var currentLatString:String?
    var currentLongString:String?
    var isClockInWebAPIStarted:Bool = false
    var isSaveLocationWebAPIStarted:Bool = false
//    var isClockIN: Bool? {
//        didSet{
//            if isClockIN == true {
//                buttonClockInorOut.setTitle("CLOCK OUT", for: .normal)
////                labelClockedIn.isHidden = false
//            } else if isClockIN == false {
//                buttonClockInorOut.setTitle("CLOCK IN", for: .normal)
////                labelClockedIn.isHidden = true
//            }
//        }
//    }
    
    //var dateClockIN: Date?
    //MARK: view controller cycle methods
    
    public class func instance() -> RMDashboardTableViewController {
        let dashboardStoryBoard = UIStoryboard(name: "RMDashboard", bundle: nil)
        let dashboardTableVC = dashboardStoryBoard.instantiateViewController(withIdentifier: "RMDashboardTableViewController") as! RMDashboardTableViewController
        return dashboardTableVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        isLocationAndNetworkWorking()
        labelTitleTotalProjectHours.textColor = RMGlobalManager.appBlueColor()
//        viewSeperatorInBetweenWorkedAndProjectTimes.backgroundColor = RMGlobalManager.appBlueColor()
//        labelClockedIn.adjustsFontSizeToFitWidth = true;
        
        self.title = RMGlobalManager.appName().uppercased()
        
        let headerView = UIView(frame: CGRect(x:0,y:0,width:tableView.frame.size.width,height:12))
        self.tableView.tableHeaderView = headerView
        self.tableView.tableFooterView = UIView()
        
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
        
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.showsHorizontalScrollIndicator = false
        
        let arrayEncloseButtons = [buttonEncloseClockIn,buttonEncloseProject,buttonEncloseTimecards,buttonEncloseLeave,buttonEncloseAnnouncements,buttonEncloseMessages,buttonEncloseCurrentLocation,buttonEncloseEvents,buttonEncloseTimeline,buttonEncloseNotes]
        
        arrayEncloseButtons.forEach{$0?.setTitle("", for: .normal)}
        
        let arrayEncloseViews = [viewEncloseClockIn,viewEncloseProject,viewEncloseTimecards,viewEncloseLeave,viewEncloseAnnouncements,viewEncloseMessages,viewEncloseCurrentLocation,viewEncloseEvents,viewEncloseTimeline,viewEncloseNotes]
        
        arrayEncloseViews.forEach{ $0?.isUserInteractionEnabled = false }
        
        arrayViewsWorkTime.forEach{ $0.layer.cornerRadius = 5
            $0.layer.borderWidth = 1
            $0.layer.borderColor = UIColor.red.cgColor
            $0.backgroundColor = UIColor.clear
        }
        
        arrayViewsProjectTime.forEach{ $0.layer.cornerRadius = 5
            $0.layer.borderWidth = 1
            $0.layer.borderColor = RMGlobalManager.appBlueColor().cgColor
            $0.textColor = RMGlobalManager.appBlueColor()
            $0.backgroundColor = UIColor.clear
        }
        
        labelProjectMinFullCoulmn.textColor = RMGlobalManager.appBlueColor()
        labelProjectHoursFullCoulmn.textColor = RMGlobalManager.appBlueColor()
        
        viewEncloseProjectHours.backgroundColor = UIColor.clear
        viewEncloseProjectHours.layer.cornerRadius = 5
        viewEncloseProjectHours.layer.borderColor = RMGlobalManager.appBlueColor().cgColor
        viewEncloseProjectHours.layer.borderWidth = 2
        viewEncloseProjectHours.clipsToBounds = true
        
        
        viewEncloseWorkHours.backgroundColor = UIColor.clear
        viewEncloseWorkHours.layer.cornerRadius = 5
        viewEncloseWorkHours.layer.borderColor = UIColor.red.cgColor
        viewEncloseWorkHours.layer.borderWidth = 2
        viewEncloseWorkHours.clipsToBounds = true
        
//        viewEncloseClockIn.backgroundColor = UIColor(red: 51, green: 68, blue: 91)
        //        viewEncloseProject.backgroundColor = UIColor(red: 52, green: 102, blue: 122)
//        viewEncloseTimecards.backgroundColor = UIColor(red: 210, green: 192, blue: 128)
//        viewEncloseLeave.backgroundColor = UIColor(red: 142, green: 106, blue: 44)
//        viewEncloseAnnouncements.backgroundColor = UIColor(red: 183, green: 173, blue: 167)
//        viewEncloseMessages.backgroundColor = UIColor(red: 147, green: 220, blue: 211)
//        viewEncloseCurrentLocation.backgroundColor = UIColor(red: 188, green: 69, blue: 74)
////        viewEncloseEvents.backgroundColor = UIColor(red: 245, green: 197, blue: 218)
//        
//        viewEncloseEvents.backgroundColor = UIColor(red: 74, green: 139, blue: 188)
//        
////        viewEncloseTimeline.backgroundColor =
//        
//        viewEncloseTimeline.backgroundColor = UIColor(red: 215, green: 160, blue: 142)
//        
//        viewEncloseNotes.backgroundColor = UIColor(red: 113, green: 102, blue: 93)
        
        
//        self.tableView.backgroundColor = UIColor(red: 216, green: 226, blue: 239).withAlphaComponent(0.2)
//
//        self.tableView.backgroundColor = UIColor(red: 97, green: 193, blue: 200).withAlphaComponent(1.2)
        
//        self.tableView.backgroundColor = UIColor(red: 174, green: 147, blue: 104).withAlphaComponent(1.2)
        
        self.tableView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        
        buttonClockInorOut.layer.cornerRadius = buttonClockInorOut.frame.size.height/2
        buttonClockInorOut.clipsToBounds = true
//       buttonClockInorOut.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        buttonClockInorOut.setTitleColor(viewEncloseClockIn.backgroundColor, for: .normal)
        
        viewEncloseTimings.backgroundColor = UIColor.clear
        
        fetchDashboardDetails()
        configureRefreshControl()
        
        
        
        
        viewEncloseClockIn.backgroundColor = UIColor.red
        viewEncloseProject.backgroundColor = UIColor(red: 145, green: 182, blue: 61)
        viewEncloseTimecards.backgroundColor = UIColor.blue
        viewEncloseLeave.backgroundColor = UIColor(red: 145, green: 182, blue: 61)
        viewEncloseAnnouncements.backgroundColor = UIColor.orange
        viewEncloseMessages.backgroundColor = UIColor(red: 145, green: 182, blue: 61)
        viewEncloseCurrentLocation.backgroundColor = UIColor.black
        //        viewEncloseEvents.backgroundColor = UIColor(red: 245, green: 197, blue: 218)
        
        viewEncloseEvents.backgroundColor = UIColor(red: 145, green: 182, blue: 61)
        
        //        viewEncloseTimeline.backgroundColor =
        
        viewEncloseTimeline.backgroundColor = UIColor.brown
        
        viewEncloseNotes.backgroundColor = UIColor(red: 145, green: 182, blue: 61)
        
        
        //FIXME: below line is temp
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(barButtonAddAction))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureRefreshControl() {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = RMGlobalManager.appBlueColor()
        refreshControl.tintColor = UIColor.white
        refreshControl.addTarget(self, action: #selector(refreshTableView), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    @objc func refreshTableView(_ refreshController:UIRefreshControl) {
        
//        refreshController.
//        DispatchQueue.main.async {
            refreshController.endRefreshing()
            self.perform(#selector(fetchDashboardDetails), with: nil, afterDelay: 0.8)
//        }
        
    }
    
    func clearAllUIData() {
        self.labelWorkHours.text = "00"
        self.labelWorkMinutes.text = "00"
        self.labelWorkSeconds.text = "00"
        self.labelProjectHours.text = "00"
        self.labelProjectMinutes.text = "00"
        self.labelProjectSeconds.text = "00"
    }
    
    @objc func fetchDashboardDetails() {
        clearAllUIData()
        
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController:self)
            return
        }
        
        RMWebServiceManager.dashboardDetails {[weak self] (result:Any?, isSuccess:Bool) in
            if isSuccess == true && result is Dictionary<String,Any?> {
                let dict = result as! Dictionary<String,Any?>
                let stringClockStatus = dict["clock_status"] as? String
                self?.buttonClockInorOut.setTitle(stringClockStatus, for: .normal)
                ////                labelClockedIn.isHidden = true
//                if stringClockStatus != nil {
//                    if stringClockStatus!.lowercased() == "in" {
//                        self?.isClockIN = true
//                    } else if stringClockStatus!.lowercased() == "out" {
//                        self?.isClockIN = false
//                    }
//                }
                
                
                let stringClockTime = dict["time"] as? String
//                if stringClockTime != nil {
                    self?.labelClockedIn.text = stringClockTime
//                    let dateClockIN = RMGlobalManager.dateFromWebServiceString(stringDate: stringClockTime!)
//                    if dateClockIN != nil {
//                       let stringDateClockIN = RMGlobalManager.shortDateAndTimeString(date: dateClockIN!)
//                        self?.labelClockedIn.text =  RMStrings.CLOCK_STARTED_AT_STRING(clockInDateTime: stringDateClockIN)
//                    }
//                }
                
                let stringTotalHoursWorked =  dict["total_hours_worked"] as? String
                let stringTotalProjectHours = dict["total_project_hours"] as? String
                
                let arrayTotalHoursWorked = stringTotalHoursWorked?.components(separatedBy: ":")
                let arrayTotalProjectHours = stringTotalProjectHours?.components(separatedBy: ":")
                if arrayTotalHoursWorked != nil {
                    if arrayTotalHoursWorked!.count > 0 {
                        let intValue = Int(arrayTotalHoursWorked![0]) ?? 0
                        self?.labelWorkHours.text =
                            String(format: "%d", intValue)
//                        self?.labelWorkHours.text =
//                            String(format: "%02d", intValue)
                    }
                    else {
                        self?.labelWorkHours.text = "00"
                    }
                    
                    if arrayTotalHoursWorked!.count > 1 {
                        let intValue = Int(arrayTotalHoursWorked![1]) ?? 0
                        self?.labelWorkMinutes.text =
                            String(format: "%d", intValue)
//                        self?.labelWorkMinutes.text =
//                            String(format: "%02d", intValue)
                    } else {
                        self?.labelWorkMinutes.text = "00"
                    }
                    
                    if arrayTotalHoursWorked!.count > 2 {
                        
                        let intValue = Int(arrayTotalHoursWorked![2]) ?? 0
                        self?.labelWorkSeconds.text =
                            String(format: "%d", intValue)
//                        self?.labelWorkSeconds.text =
//                            String(format: "%02d", intValue)
                    } else {
                        self?.labelWorkSeconds.text = "00"
                    }
                }
                
                if arrayTotalProjectHours != nil {
                    
                    if arrayTotalProjectHours!.count > 0 {
                        
                        
                        let intValue = Int(arrayTotalProjectHours![0]) ?? 0
                        self?.labelProjectHours.text =
                            String(format: "%d", intValue)
//                        self?.labelProjectHours.text =
//                            String(format: "%02d", intValue)
                    }else {
                        self?.labelProjectHours.text = "00"
                    }
                    
                    if arrayTotalProjectHours!.count > 1 {
                        
                        let intValue = Int(arrayTotalProjectHours![1]) ?? 0
                        self?.labelProjectMinutes.text =
                            String(format: "%d", intValue)
//                        self?.labelProjectMinutes.text =
//                            String(format: "%02d", intValue)
                    } else {
                        self?.labelProjectMinutes.text = "00"
                    }
                    
                    if arrayTotalProjectHours!.count > 2 {
                        
                        let intValue = Int(arrayTotalProjectHours![2]) ?? 0
                        self?.labelProjectSeconds.text =
                            String(format: "%d", intValue)
//                        self?.labelProjectSeconds.text =
//                            String(format: "%02d", intValue)
                    } else {
                        self?.labelProjectSeconds.text = "00"
                    }
                }
                
            }
        }
    }
    
    @objc func barButtonAddAction() {
        
        return;
        
        //FIXME: temp code
        self.navigationController?.pushViewController(RMPlaceListTempTableViewController(style: .plain), animated: true)
    }
    
    func isLocationAndNetworkWorking() -> Bool {
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController:self)
            return false;
        }
        RMLocationManager.sharedInstance.addDelegate(delegate: self)
        RMLocationManager.sharedInstance.fetchCurrentLocation()/**To show authorization*/
        //        if RMLocationManager.isLocationServicesEnabled() == false || RMLocationManager.islocationAuthorized() == false{
        
        if RMLocationManager.islocationAuthorized() == false {
            let defaultButtons = [RMStrings.PRIVACY_SETTINGS_STRING()]
            RMAlert.showAlert(title: nil, message: RMStrings.CHANGE_LOCATION_PRIVACY_SETTINGS_STRING(), showInViewController: self, defaultButtonNames: defaultButtons, destructiveButtonNames: nil, cancelButtonNames: [RMStrings.CANCEL_STRING()], completionAction: { (slectedButtonName) in
                
                if slectedButtonName == RMStrings.PRIVACY_SETTINGS_STRING() {
                    RMGlobalManager.openLocationPrivacySettings()
                }
            })
            return false;
        }
        
        //            return;
        //        }
        
        
        
        
        if currentLatString == nil && currentLongString == nil && lastLocationUpdatedDate == nil {
            
            RMAlert.showAlert(title: nil, message: "Location not updated", showInViewController: self, defaultButtonNames: ["OK"], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: nil)
            return false;
        }
            
            
            
            let interval = Date().timeIntervalSince(lastLocationUpdatedDate!)
            if interval > 15*60 {
                RMAlert.showAlert(title: nil, message: "Location not updated", showInViewController: self, defaultButtonNames: ["OK"], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: nil)
                
                return false;
            }
        return true;
    }

    @IBAction func buttonActionForClockIn(_ sender: Any) {
        print("buttonActionForClockIn")
        
        if isLocationAndNetworkWorking() == false {
            return;
        }
        
        if isClockInWebAPIStarted == true {
            return;
        }
                isClockInWebAPIStarted = true
                RMWebServiceManager.saveAttendance(latitude: currentLatString!, longitude: currentLongString!, note: "123QW", responseHandler: {[weak self] (result, isSuccess) in
                    self?.isClockInWebAPIStarted = false
                    if isSuccess == true {
                        let dict = result as! Dictionary<String,Any?>
                        let stringTimerStatus = dict["clock_status"] as? String
                        self?.labelClockedIn.text = dict["message"] as? String
                        self?.buttonClockInorOut.setTitle(stringTimerStatus, for: .normal)
//                        if stringTimerStatus?.lowercased() == "in" {
//
//                            self?.isClockIN = false
//
////                            completion(true,false)
//                        } else if stringTimerStatus?.lowercased() == "out"{
//                            self?.isClockIN = true
////                            self?.delegate?.timerUpdated(timerIsStarted: true)
////                            completion(true,true)
//                        } else {
////                            completion(false,nil)
//                        }
                    }
                })
                
                return;
            
        }
        
//        RMLocationManager.sharedInstance.addDelegate(delegate: self)
//        RMLocationManager.sharedInstance.fetchCurrentLocation()
    
    
    @IBAction func buttonActionForProject(_ sender: Any) {
        print("buttonActionForProject")
        RMSideMenuAndRoot.showBasicProject(navigationController:self.navigationController!)
    }
    
    @IBAction func buttonActionForTimecards(_ sender: Any) {
        print("buttonActionForTimecards")
        RMSideMenuAndRoot.showTimeCard(navigationController: self.navigationController!)
    }
    
    @IBAction func buttonActionForLeave(_ sender: Any) {
        print("buttonActionForLeave")
        RMSideMenuAndRoot.showLeave(navigationController: self.navigationController!)
    }
    
    @IBAction func buttonActionForAnnouncements(_ sender: Any) {
        print("buttonActionForAnnouncements")
        //Crashlytics.sharedInstance().crash()
        RMSideMenuAndRoot.showAnnouncements(navigationController: self.navigationController!)
    }
    
    @IBAction func buttonActionForMessages(_ sender: Any) {
        print("buttonActionForMessages")
        RMSideMenuAndRoot.showMessages(navigationController: self.navigationController!)
    }
    
    @IBAction func buttonActionForCurrentLocation(_ sender: Any) {
        print("buttonActionForCurrentLocation")
        
        if isLocationAndNetworkWorking() == false {
            return;
        }
        
        if isSaveLocationWebAPIStarted == true {
            
        }
        isSaveLocationWebAPIStarted = true
        RMWebServiceManager.saveLocation(userUniqueId: RMUserDefaultManager.getCurrentUserUniqueId()!, latitiude: currentLatString!, longitude: currentLongString!) { [weak self] (result,isSuccess) in
            self?.isSaveLocationWebAPIStarted = true
        }
    }
    
    @IBAction func buttonActionForEvents(_ sender: Any) {
        print("buttonActionForEvents")
        RMSideMenuAndRoot.showEvent(navigationController:self.navigationController!)
    }
    
    
    
    @IBAction func buttonActionForTimeline(_ sender: Any) {
        print("buttonActionForTimeline")
        RMSideMenuAndRoot.showTimeLine(navigationController:self.navigationController!)
    }
    
    @IBAction func buttonActionForNotes(_ sender: Any) {
        print("buttonActionForNotes")
        RMSideMenuAndRoot.showNote(navigationController:self.navigationController!)
    }
    
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
    }
    
    let timeDetailsCellHeight:CGFloat = 154
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1 || indexPath.row == 2 {
            return 0
        }
        
        if UIScreen.main.bounds.size.height < 568 {
            /**
             Small device below iPhone 5
             */
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
//        print(UIScreen.main.bounds.size.height)
        
        let h = (UIScreen.main.bounds.size.height - 68 - timeDetailsCellHeight)/3
        if indexPath.row == 5 {
            return timeDetailsCellHeight
        }
        return h;
        return super.tableView(tableView, heightForRowAt: indexPath)
    }

}

extension RMDashboardTableViewController: RMLocationDelegate {
    
    
    @objc  func RMLocationManagerDidUpdateLocations( locationManager:RMLocationManager, currentLatitude:Double, currentLongitude:Double) {
        if locationManager == nil {
            return
        }
        
        let locationManagerLocalStrong = locationManager.locationManager
        if locationManagerLocalStrong == nil {
            return;
        }
        
        
        print("dfd\(currentLatitude,currentLongitude)")
        lastLocationUpdatedDate = locationManagerLocalStrong!.location?.timestamp
        
        currentLatString = String(format:"%f",
                                      locationManagerLocalStrong!.location!.coordinate.latitude)
        currentLongString = String(format:"%f", locationManagerLocalStrong!.location!.coordinate.latitude)
        locationManager.stopLocationFetch()
        locationManager.stopLocationMonitoring()
//        buttonActionForClockIn(buttonClockInorOut)
    }
    
    @objc func RMLocationAuthorizationStateChanged( locationManager:RMLocationManager) {
        
        if RMLocationManager.islocationAuthorized() == true {
            locationManager.fetchCurrentLocation()
        }
    }
    
}


extension RMDashboardTableViewController: RMNavigationBarButtonItems {
    func neededNavigationBarButtonItems() -> RMNeededNavigationButtonItem {
//        return .none
        return .leftMenu
    }
}

extension RMDashboardTableViewController: RMNavigationBarButtonActions {
    func barButtonLeftAction() {
        RMSideMenuAndRoot.showLeftMenu()
    }
}
