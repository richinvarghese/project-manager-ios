//
//  RMSignupTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 14/05/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMSignupTableViewController: UITableViewController {
    
    public class func instance() -> RMSignupTableViewController {
        let rootStoryBoard = UIStoryboard(name: "RMSignup", bundle: nil)
        let vc = rootStoryBoard.instantiateViewController(withIdentifier: "RMSignupTableViewController") as! RMSignupTableViewController
        //        newTimeLineTableVC.selectedTimeLine = selectedTimeLine
        //        newTimeLineTableVC.replyToTimeLine = replyToTimeLine
        return vc
    }

    
    
//    email, first name, last name, password, company name, password base 64
    @IBOutlet weak var textFieldEmail: UITextField!
     @IBOutlet weak var textFieldFirstName: UITextField!
     @IBOutlet weak var textFieldLastName: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldConfirmPassword: UITextField!
     @IBOutlet weak var textFieldCompanyName: UITextField!
    @IBOutlet weak var buttonSignup: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.tableFooterView = UIView()
        self.tableView.allowsSelection = false
        self.tableView.separatorStyle = .none
        
        
        self.tableView.showsVerticalScrollIndicator = false
//        self.navigationController?.navigationBar.isHidden = true
        
        textFieldEmail.returnKeyType = .next
        textFieldPassword.returnKeyType = .done
        textFieldPassword.isSecureTextEntry = true
        textFieldEmail.keyboardType = .emailAddress
        textFieldEmail.autocorrectionType = .no
        //textFieldEmail.
        self.tableView.tableFooterView = UIView()
        self.tableView.allowsSelection = false
        setupTextFieldUI(textField: textFieldEmail)
        setupTextFieldUI(textField: textFieldPassword)
        setupTextFieldUI(textField: textFieldFirstName)
        setupTextFieldUI(textField: textFieldLastName)
        setupTextFieldUI(textField: textFieldConfirmPassword)
        setupTextFieldUI(textField: textFieldCompanyName)
        
        self.tableView.separatorStyle = .none
        //        self.tableView.allowsSelection = false
        let barButtonItem = UIBarButtonItem(title: RMStrings.DONE_STRING(), style: .done, target: self, action: #selector(barButtonDoneAction))
        let toolbarWithBarButtonDone = RMGlobalManager.toolbar(barButtonItem: barButtonItem)
        textFieldConfirmPassword.inputAccessoryView = toolbarWithBarButtonDone
        textFieldConfirmPassword.isSecureTextEntry = true
        
        
//        let barButtonItemNext = UIBarButtonItem(title: RMStrings.NEXT_STRING(), style: .done, target: self, action: #selector(barButtonNextAction))
//        let toolbarWithBarButtonNext = RMGlobalManager.toolbar(barButtonItem: barButtonItemNext)
//        textFieldEmail.inputAccessoryView = toolbarWithBarButtonNext
        textFieldEmail.placeholder = RMStrings.E_MAIL_STRING()
        textFieldPassword.placeholder = RMStrings.PASSWORD_STRING()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction))
        //        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        setupButton(button: buttonSignup)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Dismiss", style: .plain, target: self, action: #selector(dismissAction))
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupButton(button:UIButton) {
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 2
        button.clipsToBounds = true
        button.layer.borderColor =  UIColor.blue.cgColor
        button.backgroundColor = RMGlobalManager.appBlueColor()
    }
    
    func setupTextFieldUI(textField:UITextField) {
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.borderStyle = .none
        let leftPaddingView = UIView(frame: CGRect(x:0, y:0,width:10,height:textField.frame.size.height))
        textField.leftView = leftPaddingView
        textField.leftViewMode = .always
        
        let rightPaddingView = UIView(frame: CGRect(x:0, y:0,width:10,height:textField.frame.size.height))
        textField.rightView = rightPaddingView
        textField.rightViewMode = .always
        
        textField.delegate = self as UITextFieldDelegate
        
        let barButtonItemNext = UIBarButtonItem(title: RMStrings.NEXT_STRING(), style: .done, target: self, action: #selector(barButtonNextAction))
        let toolbarWithBarButtonNext = RMGlobalManager.toolbar(barButtonItem: barButtonItemNext)
        textField.inputAccessoryView = toolbarWithBarButtonNext
    }
    
    
    @objc func barButtonDoneAction() {
//        textFieldPassword.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    
    @objc func barButtonNextAction() {
        if textFieldEmail.isFirstResponder {
            textFieldEmail.resignFirstResponder()
            textFieldFirstName.becomeFirstResponder()
        } else if textFieldFirstName.isFirstResponder {
            textFieldFirstName.resignFirstResponder()
            textFieldLastName.becomeFirstResponder()
        }  else if textFieldLastName.isFirstResponder {
            textFieldLastName.resignFirstResponder()
            textFieldCompanyName.becomeFirstResponder()
        }  else if textFieldCompanyName.isFirstResponder {
            textFieldCompanyName.resignFirstResponder()
            textFieldPassword.becomeFirstResponder()
        }  else if textFieldPassword.isFirstResponder {
            textFieldPassword.resignFirstResponder()
            textFieldConfirmPassword.becomeFirstResponder()
        }
        
    }
    
    
    @objc func tapGestureAction() {
        self.view.endEditing(true)
    }
    
    
    
    @IBAction func buttonActionForSignup(_ sender: Any) {
        self.view.endEditing(true)
        textFieldEmail.text = textFieldEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController:self)
            return
        }
        
        if textFieldEmail.text?.count == 0 {
            RMAlert.showAlert(title: nil, message: RMStrings.ENTER_E_MAIL_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textFieldEmail.becomeFirstResponder()
            })
            return
        }
        
        if RMForgetPwdTableViewController.isValidEmail(testStr: textFieldEmail.text!) == false {
            RMAlert.showAlert(title: nil, message: RMStrings.ENTER_VALID_E_MAIL_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textFieldEmail.becomeFirstResponder()
            })
            return
        }
        
        if textFieldFirstName.text?.count == 0 {
            RMAlert.showAlert(title: nil, message: "Enter first name", showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textFieldFirstName.becomeFirstResponder()
            })
            return
        }
        
        if textFieldLastName.text?.count == 0 {
            RMAlert.showAlert(title: nil, message: "Enter last name", showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textFieldLastName.becomeFirstResponder()
            })
            return
        }
        
        if textFieldCompanyName.text?.count == 0 {
            RMAlert.showAlert(title: nil, message: "Enter company name", showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textFieldCompanyName.becomeFirstResponder()
            })
            return
        }
        
        if textFieldPassword.text?.count == 0 {
            RMAlert.showAlert(title: nil, message: "Enter password", showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textFieldPassword.becomeFirstResponder()
            })
            return
        }
        
        if textFieldConfirmPassword.text != textFieldPassword.text {
            RMAlert.showAlert(title: nil, message: "Password is not matching", showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textFieldPassword.becomeFirstResponder()
            })
            return
        }
        
        
        let dataPwd = (textFieldPassword.text!).data(using: String.Encoding.utf8)
        let base64Pwd = dataPwd!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
        RMWebServiceManager.signup(email: self.textFieldEmail.text!, firstName: self.textFieldFirstName.text!, lastName: self.textFieldLastName.text!, password: base64Pwd, companyName: self.textFieldCompanyName.text!) {[weak self] (result:Any?, isSuccess:Bool) in
            if isSuccess == true {
                self?.textFieldEmail.text = ""
                self?.textFieldLastName.text = ""
                self?.tableView.reloadData()
                DispatchQueue.main.async {
                    self?.dismissAction()
                }
            }
        }
    }
    
    
    @objc func dismissAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    

    // MARK: - Table view data source

    

}


extension RMSignupTableViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        if(newLength == 0)
        {
            //            labelPlaceholderDescription?.isHidden = false
        }
        else
        {
            //            labelPlaceholderDescription?.isHidden = true
        }
        return newLength <= RMLoginTableViewController.lengthLimitEmail
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldEmail {
            textFieldPassword.becomeFirstResponder()
            return false
        }
        return true
    }
}

