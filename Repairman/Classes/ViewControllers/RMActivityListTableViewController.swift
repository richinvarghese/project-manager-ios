//
//  RMActivityListTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 21/01/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMActivityListTableViewController: UITableViewController {

    var arrayToDisplay: Array<RMActivity>?//FIXME: temporary code
    
//    static let reuseIdentifierNoteCell = "RMNoteCell"
//
//    var selectedProjectUniqueId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.title = RMStrings.ACTIVITIES_STRING()
        //        self.tableView.register(UITableViewCell.self)
        //        self.tableView.register(RMNoteListTableViewCell.self)
//        let nib = UINib(nibName: "RMSubtitleTableViewCell", bundle: nil)
//        self.tableView.register(nib, forCellReuseIdentifier: RMNoteListTableViewController.reuseIdentifierNoteCell)
        self.tableView.register(RMActivityListTableViewCell.self)
        self.tableView.tableFooterView = UIView()
//        fetchNote()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
//    func fetchActivi() {
//
//        //        RMWebServiceManager.saveNote(title: "test123", description: "test4546")
//        //        return
//        RMNoteListTableViewController.arrayToDisplay = nil
//        self.tableView.reloadData()
//
//        RMWebServiceManager.fetchNote(projectUniqueId:selectedProjectUniqueId) {[weak self] (result:Any?, isSuccess:Bool) in
//            if isSuccess == false {
//                return
//            }
//            let notesArray = result as! [RMNote]
//            RMNoteListTableViewController.arrayToDisplay = notesArray
//            self?.tableView.reloadData()
//        }
//    }

    
//    @objc func buttonActionForAdd() {
//        let newNoteTableVC = RMAddNoteTableViewController.instance()
//        newNoteTableVC.delegate = self
//        newNoteTableVC.selectedProjectUniqueId = selectedProjectUniqueId
//        self.navigationController?.pushViewController(newNoteTableVC, animated: true)
//    }
    
//    func displayingArray() -> [RMNote] {
//        if RMNoteListTableViewController.arrayToDisplay == nil {
//            return []
//        }
//
//        return RMNoteListTableViewController.arrayToDisplay!
//
//    }
    
    func showingActivity(atIndexPath:IndexPath) -> RMActivity {
        //        let array = displayingArray()
        return arrayToDisplay![atIndexPath.row]
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayToDisplay == nil {
            return 0
        }
        return arrayToDisplay!.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let cell: RMActivityListTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        //        if cell == nil {
        //            cell = UITableViewCell(style: UITableViewCellStyle.value2, reuseIdentifier: "UITableViewCell")
        //        }
        
        let activity = showingActivity(atIndexPath: indexPath)
        cell.setActivity(activity:activity)
        //        cell!.textLabel?.text = note.noteTitle
        //        cell!.detailTextLabel?.text = RMGlobalManager.stringFromDate(date:note.notePostedDate!)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let activity = showingActivity(atIndexPath: indexPath)
//        let editNoteTableVC = RMAddNoteTableViewController.instance()
//        editNoteTableVC.selectedNote = note
//        editNoteTableVC.delegate = self
//        editNoteTableVC.selectedProjectUniqueId = selectedProjectUniqueId
//        self.navigationController?.pushViewController(editNoteTableVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let activity = showingActivity(atIndexPath: indexPath)
        if activity.isActionTypeUpdated() == true {
            return 132
        }
        return 72
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
        RMTheme.cellCornerRadius(cell: cell, borderColor: UIColor.red, backgroundColor: UIColor.clear)
    }
    
//    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//        let note = showingNote(atIndexPath: indexPath)
//        if editingStyle == .delete {
//
//            RMWebServiceManager.deleteNote(noteUniqueId: note.noteUniqueId!, responseHandler: {[weak self] (result:Any?, isSuccess:Bool) in
//                RMNoteListTableViewController.arrayToDisplay?.remove(at: indexPath.row)
//                self?.tableView.reloadData()
//            })
//
//
//        }
//    }
    
    deinit {
        print("dealloc=RMActivityListTableViewController")
    }
    
}
