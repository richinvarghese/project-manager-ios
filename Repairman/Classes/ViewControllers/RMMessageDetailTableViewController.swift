//
//  RMMessageDetailTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 20/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMMessageDetailTableViewController: UITableViewController {

    var arrayMessages: [RMMessage]?
    var selectedMessageUniqueId: String?
    
//    var delegate:RMMessageListTableViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(RMMessageBaseTableViewCell .self)
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.allowsSelection = false
        //        addDummyTask()//FIXME: Dummy data
                fetchMessageDetails()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
        func fetchMessageDetails() {
            if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
                RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController:self)
                return
            }
            RMWebServiceManager.fetchMessageDetails(messageUniqueId: selectedMessageUniqueId!){[weak self] (result, isSuccess) in
                if isSuccess == true {
                    self?.arrayMessages = result as! [RMMessage
                    ]
                    self?.tableView.reloadData()
                }
            }
        }
    

    func showingMessage(atIndexPath:IndexPath) -> RMMessage {
        //        let array = displayingArray()
        return arrayMessages![atIndexPath.row]
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayMessages == nil {
            return 0
        }
        return arrayMessages!.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:RMMessageBaseTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate = self as! RMMessageBaseTableViewCellDelegate
        let message = showingMessage(atIndexPath: indexPath)
        cell.setMessage(message: message)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 106;
                return UITableViewAutomaticDimension
    }
    
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let message = showingMessage(atIndexPath: indexPath)
//        let taskDetailTableVC = RMTaskDetailTableViewController(style: .plain)
//        //        taskDetailTableVC.selectedTask = message
//        self.navigationController?.pushViewController(taskDetailTableVC, animated: true)
//    }
}

extension RMMessageDetailTableViewController: RMMessageBaseTableViewCellDelegate {
    func buttonActionAttachment(cell: RMMessageBaseTableViewCell) {
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController: self)
            return
        }


        let indexPath =  tableView.indexPath(for: cell)
        if indexPath == nil {
            return
        }
        let message = showingMessage(atIndexPath: indexPath!)
        
        if message.attachment == nil {
            return;
        }
        
        let documentVC = RMDocumentViewController(url: message.attachment!, isGoingToPresent: true)
        let navController = UINavigationController(rootViewController: documentVC)
        self.present(navController, animated: true, completion: nil)
        
//        let documentVC = RMDocumentViewController(url:message.attachment!.absoluteString, isGoingToPresent:true)
//        let navController = UINavigationController(rootViewController: documentVC)
//        self.present(navController, animated: true, completion: nil)
        
    }


}

