//
//  RMMessageViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 18/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMMessageViewController: UIViewController {

    public class func instance() -> RMMessageViewController {
        let storyBoard = UIStoryboard(name: "RMMessage", bundle: nil)
        let messageVC = storyBoard.instantiateViewController(withIdentifier: "RMMessageViewController") as! RMMessageViewController
        //        newTimeLineTableVC.selectedTimeLine = selectedTimeLine
        //        newTimeLineTableVC.replyToTimeLine = replyToTimeLine
        return messageVC
    }
    
    //    public class func keyOverview -
    
//    var isFetchingProject:Bool?
    
    @IBOutlet weak var segment: UISegmentedControl!
    
    @IBOutlet weak var viewBelowSegment: UIView!
    let sentItemsVC = RMMessageListTableViewController(style:.plain)
    let inboxVC = RMMessageListTableViewController(style:.plain)

    let composeMessage = RMAddMessageTableViewController.instance(selectedMessage: nil, replyToMessage: nil)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        sendItem.delegate = self
//        inbox.delegate = self
        inboxVC.delegate = self
        composeMessage.delegate = self as! RMAddMessageTableViewControllerDelegate

        self.title = RMStrings.MESSAGES_STRING()
        
//        segment.tintColor = UIColor(red: 231, green: 242, blue: 236)
                segment.setTitle(RMStrings.INBOX_STRING(), forSegmentAt: 0)
        segment.setTitle("Compose", forSegmentAt: 1)
                segment.setTitle( "Sent", forSegmentAt: 2)
        
        segment.addTarget(self, action: #selector(changeSegment), for: .valueChanged)
        self.addChildViewController(sentItemsVC)
        self.addChildViewController(inboxVC)
        self.addChildViewController(composeMessage)
        fetchMessages(segmentSelectionOnCompletion: RMStrings.INBOX_STRING())
        
//        let barButtonItemCompose = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(buttonActionForCompose))
//        self.navigationItem.rightBarButtonItem = barButtonItemCompose
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
//        @objc func buttonActionForCompose() {
//    //        addOrEditTimeLine(timeLine: nil, replyToTimeLine: nil)
//
////            addOrReplyMessage(replyToMessage: nil)
//
//        }
    
    func addOrReplyMessage(replyToMessage:RMMessage?) {
        let addMessage = RMAddMessageTableViewController.instance(selectedMessage: nil, replyToMessage: replyToMessage)
        addMessage.delegate = self as! RMAddMessageTableViewControllerDelegate
//        addMessage.replyToMessageUniqueId = replyToMessage?.messageUniqueId
        self.navigationController?.pushViewController(addMessage, animated: true)
    }
    //
    //
    //    func addOrEditTaskList(timeLine:RMTimeLine?,replyToTimeLine:RMTimeLine?) {
    //        let addTimeLineTableVC = RMAddTimeLineTableViewController.instance(selectedTimeLine: timeLine, replyToTimeLine: replyToTimeLine)
    //        self.navigationController?.pushViewController(addTimeLineTableVC, animated: true)
    //    }
    

    
    func indexOfSegment(title:String) -> Int? {
        if self.segment.numberOfSegments <= 1 {
            return nil;
        }
        var indexNotes:Int?
        for i in 0...(self.segment.numberOfSegments - 1) {
            let title1 = self.segment.titleForSegment(at: i)
            if title1 == title {
                indexNotes = i
                break;
            }
        }
        return indexNotes
    }
    
    func clearView() {
        inboxVC.tableView.removeFromSuperview()
        sentItemsVC.tableView.removeFromSuperview()
    }
    
    @objc func changeSegment(_ sender: UISegmentedControl) {
        print("Change color handler is called.")
        print("Changing Color to ")
        clearView()
        let title = sender.titleForSegment(at: sender.selectedSegmentIndex)
        if title == nil {
            return
        }
        switch title! {
        case RMStrings.INBOX_STRING():
            //            let tableView =
            
            self.viewBelowSegment.addSubview(inboxVC.tableView)
//            projectOverview.barButtonItemTimer(navigationItem: self.navigationItem,isStarted: isTimerStarted)
            
            inboxVC.tableView.translatesAutoresizingMaskIntoConstraints = false
            RMProjectViewController.addConstraintWithSuperView(view:inboxVC.tableView)
            
        case "Compose":
            //            let tableView =
            
            self.viewBelowSegment.addSubview(composeMessage.tableView)
            //            projectOverview.barButtonItemTimer(navigationItem: self.navigationItem,isStarted: isTimerStarted)
            
            composeMessage.tableView.translatesAutoresizingMaskIntoConstraints = false
            RMProjectViewController.addConstraintWithSuperView(view:composeMessage.tableView)
            
        //            self.navigationItem.rightBarButtonItem = nil
        case "Sent":
            //            self.view.backgroundColor = UIColor.blue
            //            print("Blue")
            //            taskList.tableView.removeFromSuperview()
            //            projectOverview.tableView.removeFromSuperview()
            //            noteList.tableView.removeFromSuperview()
            self.viewBelowSegment.addSubview(sentItemsVC.tableView)
            RMProjectViewController.addConstraintWithSuperView(view:sentItemsVC.tableView)
            sentItemsVC.tableView.translatesAutoresizingMaskIntoConstraints = false
            //            self.navigationItem.rightBarButtonItem = nil
            
        default:
            self.viewBelowSegment.backgroundColor = UIColor.purple
            //            print("Purple")
            self.navigationItem.rightBarButtonItem = nil
        }
    }
    
    
    
    func fetchMessages(segmentSelectionOnCompletion:String?) {
        
//        if isFetchingProject == true {
//            return
//        }
        //FIXME: Dummy data
        //        dummyData()
        
        //        return;
//        if segment != nil {/**Otherwise crash will occure at first time*/
//            segment.removeAllSegments()
//        }
        inboxVC.arrayMessages = nil
        sentItemsVC.arrayMessages = nil
        inboxVC.tableView.reloadData()
        sentItemsVC.tableView.reloadData()
        
        
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController:self)
            return
        }
//        guard let projectUniqueId = selectedProjectUniqueId else {
//            return
//        }
        
//        isFetchingProject = true
        
        RMWebServiceManager.fetchMessages(currentUserUniqueId: RMUserDefaultManager.getCurrentUserUniqueId()!) {[weak self] (result:Any?, isSuccess:Bool) in
            
//            self?.isFetchingProject = nil
            
            if isSuccess == false {
                return
            }
            let messagesDict = result as? Dictionary<String,[RMMessage]>//as! Dictionary<String,Any>
            if messagesDict == nil {
                return
            }
            //            for key in projectsDict.keys {
//            if let _ = messagesDict[RMStrings.OVERVIEW_STRING()] {
//
//                let key = RMStrings.OVERVIEW_STRING()
            
//                self?.inboxVC.arrayToDisplay = projectsDict[key] as? Array<Any>
            self?.inboxVC.arrayMessages = messagesDict!["inbox"] as? [RMMessage]
            
//                let index = self?.segment.numberOfSegments ?? 0
//                self?.segment?.insertSegment(withTitle: RMStrings.OVERVIEW_STRING(), at: index, animated: false)
                //                    self?.segment.setTitle(RMStrings.OVERVIEW_STRING(), forSegmentAt: 0)
                
//            }
            
//            if let _ = messagesDict[RMStrings.SENT_ITEMS_STRING()] {
                let key = RMStrings.SENT_ITEMS_STRING()
//                self?.sentItemsVC.arrayMessages = messagesDict[key] as? [RMMessage]
            self?.sentItemsVC.arrayMessages = messagesDict!["sent_items"] as? [RMMessage]
//                let index = self?.segment.numberOfSegments ?? 0
//                self?.segment?.insertSegment(withTitle: RMStrings.TASKS_STRING(), at: index, animated: false)
//            }
            
        
            if self != nil {
                self!.segment.selectedSegmentIndex = 0
                self!.changeSegment(self!.segment)
            }
            
            self?.inboxVC.tableView.reloadData()
            self?.sentItemsVC.tableView.reloadData()
        
            
            if segmentSelectionOnCompletion != nil {
                let indexNeedToSelect = self?.indexOfSegment(title: segmentSelectionOnCompletion!)
                if indexNeedToSelect != nil {
                    self?.segment.selectedSegmentIndex = indexNeedToSelect!
                    self?.changeSegment((self?.segment)!)
                }
            }
            
        }
        
        
        
        
    }
    
    
    
    
}

//extension RMMessageViewController: RMNoteListTableViewControllerDelegate {
//    @objc func notesFetched(noteListTableVC: RMNoteListTableViewController) {
//
//        if inboxVC.arrayToDisplay == nil {
//            let selector = #selector(notesFetched(noteListTableVC:))
//            self.perform(selector, with: nil, afterDelay: 0.5)
//            return;
//        }
//
//
//        let indexNotes = indexOfNotesSegment()
//        if indexNotes != nil {
//            return
//        }
//        //        if projectOverview.arrayToDisplay?.count
//
//        if noteList.displayingArray().count > 0 {
//            let index = self.segment.numberOfSegments
//            self.segment.insertSegment(withTitle: RMStrings.NOTES_STRING(), at: index, animated: false)
//        }
//        //        else {
//        //            if self.segment.numberOfSegments <= 1 {
//        //                return
//        //            }
//        //
//        //            if indexNotes != nil {
//        //                self.segment.removeSegment(at: indexNotes!, animated: false)
//        //            }
//        //        }
//    }
//}

extension RMMessageViewController: RMAddMessageTableViewControllerDelegate {
    func modifiedMessage(addMessageTableViewController: RMAddMessageTableViewController) {
        let index = segment.selectedSegmentIndex
        let selectedTitle = segment.titleForSegment(at: index)
        fetchMessages(segmentSelectionOnCompletion: selectedTitle)
    }
}

extension RMMessageViewController: RMMessageListTableViewControllerDelegate {
    func buttonActionReply(messageListTableViewController: RMMessageListTableViewController,replyToMessage:RMMessage) {
        addOrReplyMessage(replyToMessage: replyToMessage)
    }
}

extension RMMessageViewController: RMNavigationBarButtonItems {
    func neededNavigationBarButtonItems() -> RMNeededNavigationButtonItem {
        
        if navigationController?.viewControllers.count == 1 {
            return .leftMenu
        }
        
        return .none
    }
}

extension RMMessageViewController: RMNavigationBarButtonActions {
    func barButtonLeftAction() {
        RMSideMenuAndRoot.showLeftMenu()
    }
}



