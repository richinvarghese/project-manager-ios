//
//  RMTimeLineListViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 16/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit

class RMTimeLineListTableViewController: UITableViewController {
    

    static var arrayToDisplay: Array<Array<RMTimeLine>>?//FIXME: temporary code
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = RMStrings.TIMELINE_STRING()
//        dummyArray()//FIXME: temporary code
        tableView.register(RMSubTimeLineListTableViewCell.self)
        tableView.register(RMTimeLineListTableViewCell.self)
        self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.tableFooterView = UIView()
        let barButtonItemAdd = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(buttonActionForAdd))
        self.navigationItem.rightBarButtonItem = barButtonItemAdd
//        self.tableView.allowsSelection = false
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(showLeftMenu))
        fetchTimeLine()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func showLeftMenu() {
//        RMSideMenuAndRoot.showLeftMenu()
//    }
    
//    func dummyArray() {
//        //FIXME: temporary code
//        RMTimeLineListTableViewController.arrayToDisplay = Array()
//        let timeLine = RMTimeLine()
//        timeLine.isAdminPost = true
//        timeLine.timeLineDescription = "The job should be completed"
//        timeLine.timeLineOwnerName = "Admin"
//        timeLine.timeLinePostedDate = RMGlobalManager.oldDate()
//        timeLine.timeLinePostedDate = Date()
//        timeLine.timeLineOwnerImageURL = "https://wordsmith.org/words/images/avatar2_large.png"
//        RMTimeLineListTableViewController.arrayToDisplay?.append(timeLine)
//
//        let timeLine1 = RMTimeLine()
//        timeLine1.isAdminPost = false
//        timeLine1.timeLineDescription = "Will try best"
//        timeLine1.timeLineOwnerName = "Emp1"
//        timeLine1.timeLinePostedDate = RMGlobalManager.yesterdayDate()
//        timeLine1.timeLineOwnerImageURL = "https://3.bp.blogspot.com/-WI_uJEpjAoI/VmyA2ktoz5I/AAAAAAAABGM/07yd8fvuGNo/s400/bill-gates-tweets%255B1%255D.png"
//        RMTimeLineListTableViewController.arrayToDisplay?.append(timeLine1)
//        let timeLine2 = RMTimeLine()
//        timeLine2.isAdminPost = false
//        timeLine2.timeLineDescription = "What about he field limit for this text... gdfgf hgdh ghdh dhgfhWhat about he field limit for this text... gdfgf hgdh ghdh dhgfhWhat about he field limit for this text... gdfgf hgdh ghdh dhgfhWhat about he field limit for this text... gdfgf hgdh ghdh dhgfhWhat about he field limit for this text... gdfgf hgdh ghdh dhgfhWhat about he field limit for this text... gdfgf hgdh ghdh dhgfhWhat about he field limit for this text... gdfgf hgdh ghdh dhgfhWhat about he field limit for this text... gdfgf hgdh ghdh dhgfhWhat about he field limit for this text... gdfgf hgdh ghdh dhgfhWhat about he field limit for this text... gdfgf hgdh ghdh dhgfhWhat about he field limit for this text... gdfgf hgdh ghdh dhgfhWhat about he field limit for this text... gdfgf hgdh ghdh dhgfhWhat about he field limit for this text... gdfgf hgdh ghdh dhgfhWhat about he field limit for this text... gdfgf hgdh ghdh dhgfhWhat about he field limit for this text... gdfgf hgdh ghdh dhgfhWhat about he field limit for this text... gdfgf hgdh ghdh dhgfhWhat about he field limit for this text... gdfgf hgdh ghdh dhgfhWhat about he field limit for this text... gdfgf hgdh ghdh dhgfh"
//        timeLine2.timeLineOwnerName = "Emp2"
//        timeLine2.timeLinePostedDate = RMGlobalManager.tomorrowDate()
//        timeLine2.timeLineOwnerImageURL = nil
//        RMTimeLineListTableViewController.arrayToDisplay?.append(timeLine2)
//    }
    
    func fetchTimeLine() {
        RMTimeLineListTableViewController.arrayToDisplay = nil
        self.tableView.reloadData()
        
        RMWebServiceManager.fetchTimeLine { (result:Any?,isSuccess:Bool ) in
            if isSuccess == true {
                let arrayTimeLine = result as! [[RMTimeLine]]
               RMTimeLineListTableViewController.arrayToDisplay = arrayTimeLine
//                RMTimeLineListTableViewController.arrayToDisplay?.append(contentsOf: arrayTimeLine)
                self.tableView.reloadData()
                
            }
        }
    }
    
    
    @objc func buttonActionForAdd() {
        addOrEditTimeLine(timeLine: nil, replyToTimeLine: nil)
    }
    
    func showingTimeLineAtIndexPath(indexPath:IndexPath) -> RMTimeLine? {
        if RMTimeLineListTableViewController.arrayToDisplay == nil {
            return nil
        }
        if RMTimeLineListTableViewController.arrayToDisplay?.count == 0 {
            return nil
        }
        
        let array = RMTimeLineListTableViewController.arrayToDisplay![indexPath.section]
        let timeLine = array[indexPath.row] as? RMTimeLine
        return timeLine
        
//        return RMTimeLineListTableViewController.arrayToDisplay![indexPath.row]
        
        
//        
//        let note = RMNote()
//        if indexPath.row%2 == 0 {
//            note.isAdminPost = true
//        } else {
//            note.isAdminPost = false
//        }
//        note.note = "fdasfasdfdsfdsFfsdg dfgdsgsg sgsfg fds"
//        note.noteOwnerName = "Sdfdsf"
//        note.notePostedDate = Date()
//        note.noteOwnerImageURL = "https://wordsmith.org/words/images/avatar2_large.png"
//        return note
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //FIXME: temporary code
        tableView.reloadData()
    }
    

// MARK: - TableView datasource and delegates
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        let count = RMTimeLineListTableViewController.arrayToDisplay?.count
        if count == nil {
            return 0
        }
        return count!
    }
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let subArray = RMTimeLineListTableViewController.arrayToDisplay![section]
        let timeLine = subArray.first
        let stringDate = RMGlobalManager.stringFromDate(date: timeLine!.timeLinePostedDate!,isWithTime: false)
        
        let view = UIView(frame: CGRect(x:0,y:0,width:tableView.frame.size.width,height:30))
//        view.backgroundColor = UIColor.lightGray
        let label = RMSectionLabel()
        label.layer.cornerRadius = 16/2
        label.clipsToBounds = true
//        label.frame = CGRect(x: 10, y: 5, width: tableView.frame.size.width - 20, height: 18)
        label.font = UIFont.boldSystemFont(ofSize: 13)
//        label.backgroundColor = 
        label.text = stringDate
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.backgroundColor = UIColor.darkGray
        label.numberOfLines = 1
        label.lineBreakMode = .byWordWrapping
//        label.con
        label.translatesAutoresizingMaskIntoConstraints = false
//        [self setNumberOfLines:0];
//        [self setLineBreakMode:UILineBreakModeWordWrap];
//        label
        
        view.addSubview(label)
        view.backgroundColor = UIColor.clear
        
        NSLayoutConstraint(item: label, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0).isActive = true
//        view.addConstraint(verticalCenter)
        NSLayoutConstraint(item: label, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1, constant: 0).isActive = true
//        view.addConstraint(horizontalCenter)
//        label.sizeToFit()
//        view.sizeToFit()
//        label.layoutSubviews()
//        view.layoutIfNeeded()
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 22
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = RMTimeLineListTableViewController.arrayToDisplay?.count
        if count == nil {
            return 0
        }
        
        let subArray = RMTimeLineListTableViewController.arrayToDisplay![section]
        
        return subArray.count
        
//        let count = RMTimeLineListTableViewController.arrayToDisplay?.count
//        if count == nil {
//            return 0
//        }
//        return count!
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let timeLine = showingTimeLineAtIndexPath(indexPath: indexPath)!
        var cell : UITableViewCell?
        if timeLine.isReply == false {
            let adminCell : RMTimeLineListTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
                adminCell.setTimeLine(timeLine: timeLine)
            adminCell.delegate = self
                cell = adminCell
        } else {
            let subTimeLineCell : RMSubTimeLineListTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            subTimeLineCell.setTimeLine(timeLine: timeLine)
            cell = subTimeLineCell
        }
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt
        indexPath: IndexPath) -> CGFloat {
        
//        if indexPath == selectedIndexPath {
            return UITableViewAutomaticDimension
//        }
//        return 204
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func addOrEditTimeLine(timeLine:RMTimeLine?,replyToTimeLine:RMTimeLine?) {
        let addTimeLineTableVC = RMAddTimeLineTableViewController.instance(selectedTimeLine: timeLine, replyToTimeLine: replyToTimeLine)
        addTimeLineTableVC.delegate = self
        self.navigationController?.pushViewController(addTimeLineTableVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let timeLine = showingTimeLineAtIndexPath(indexPath: indexPath)
        if timeLine?.isMyOwnPost == true {
            cell.selectionStyle = .default
        } else {
            cell.selectionStyle = .none
        }
        RMTheme.cellCornerRadius(cell: cell, borderColor: RMGlobalManager.appBlueColor(), backgroundColor: UIColor(red: 209, green: 223, blue: 230))
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let timeLine = showingTimeLineAtIndexPath(indexPath: indexPath)
        if timeLine?.isMyOwnPost == true {
            let addTimeLineTableVC = RMAddTimeLineTableViewController.instance(selectedTimeLine: timeLine, replyToTimeLine: nil)
            self.navigationController?.pushViewController(addTimeLineTableVC, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        let timeLine = showingTimeLineAtIndexPath(indexPath: indexPath)
        if timeLine?.isMyOwnPost == true {
            return .delete
        } else {
            return .none
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let timeLine = showingTimeLineAtIndexPath(indexPath: indexPath)
        if editingStyle == .delete {
            RMTimeLineListTableViewController.arrayToDisplay?.remove(at: indexPath.row)
            tableView.reloadData()
        }
    }

}

extension RMTimeLineListTableViewController: RMTimeLineListTableViewCellDelegate {

    func commonAction(cell:UITableViewCell) -> IndexPath? {
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController: self)
            return nil
        }
        
        
        let indexPath =  tableView.indexPath(for: cell)
        return indexPath
    }
    
    func buttonActionReply(cell:RMTimeLineListTableViewCell) {
        let indexPath = commonAction(cell: cell)
        if indexPath == nil {
            return
        }
        let timeLine = showingTimeLineAtIndexPath(indexPath: indexPath!)
        addOrEditTimeLine(timeLine: nil, replyToTimeLine: timeLine)
        
    }
    
    func buttonActionAttachment(cell:RMTimeLineListTableViewCell) {
        let indexPath = commonAction(cell: cell)
        if indexPath == nil {
            return
        }
        let timeLine = showingTimeLineAtIndexPath(indexPath: indexPath!)
        if timeLine == nil {
            return
        }
//        if timeLine!.attachmentType == RMTimeLine.TimeLineAttachmentType.Photo {
//
////            SDW
//
////            let image = UIImage(data:timeLine!.attachment!,scale:1.0)
////            if image == nil {
////                return
////            }
//            let imageViewer = RMImageViewerViewController.instance(imageURLToShow: timeLine!.attachment!,isGoingToPresent: true)
////            let imageViewer = RMImageViewerViewController.instance(imageToShow: image!,isGoingToPresent: true)
//            let nav = UINavigationController(rootViewController: imageViewer)
//            self.present(nav, animated: true, completion: nil)
////            self.navigationController?.pushViewController(imageViewer, animated: true)
//        } else if timeLine!.attachmentType == RMTimeLine.TimeLineAttachmentType.Document {
            let documentVC = RMDocumentViewController(url: timeLine!.attachment!, isGoingToPresent: true)
            let navController = UINavigationController(rootViewController: documentVC)
            self.present(navController, animated: true, completion: nil)
//        }
    }


}

extension RMTimeLineListTableViewController:RMAddTimeLineTableViewControllerDelegate{

    func modifiedTimeLine(addTimeLineTableVC:RMAddTimeLineTableViewController) {
        fetchTimeLine()
    }
    
}

class RMSectionLabel:UILabel {
//override func drawText(in rect: CGRect) {
//    let insets = UIEdgeInsets.init(top: 0, left: -150, bottom: 0, right: -15)
//    super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
//}
    
    var textInsets = UIEdgeInsets.zero {
        didSet { invalidateIntrinsicContentSize() }
    }
    
    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        let insetRect = UIEdgeInsetsInsetRect(bounds, textInsets)
        let textRect = super.textRect(forBounds: insetRect, limitedToNumberOfLines: numberOfLines)
        let invertedInsets = UIEdgeInsets(top: -0,
                                          left: -8,
                                          bottom: -0,
                                          right: -8)
        return UIEdgeInsetsInsetRect(textRect, invertedInsets)
    }
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, textInsets))
    }
}

extension RMTimeLineListTableViewController: RMNavigationBarButtonItems {
    func neededNavigationBarButtonItems() -> RMNeededNavigationButtonItem {
        if navigationController?.viewControllers.count == 1 {
            return .leftMenu
        }
        
        return .none
    }
}

extension RMTimeLineListTableViewController: RMNavigationBarButtonActions {
    func barButtonLeftAction() {
        RMSideMenuAndRoot.showLeftMenu()
    }
}

