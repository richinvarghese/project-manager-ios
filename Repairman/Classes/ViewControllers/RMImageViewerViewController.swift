//
//  RMImageViewerViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 17/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit
import SDWebImage
//import UIActivityIndicator_for_SDWebImage

class RMImageViewerViewController: UIViewController {

    
    @IBOutlet weak var imageView: UIImageView!
    private var imageToShow: UIImage?
    private var imageURLToShow: URL?
    private var isGoingToPresent:Bool?
    
    private class func instance() -> RMImageViewerViewController {
        let rootStoryBoard = UIStoryboard(name: "RMRoot", bundle: nil)
        let imageViewerVC = rootStoryBoard.instantiateViewController(withIdentifier: "RMImageViewerViewController") as! RMImageViewerViewController
        return imageViewerVC
    }
    
    public class func instance(imageToShow:UIImage, isGoingToPresent:Bool) -> RMImageViewerViewController {
        let imageViewerVC = instance()
        imageViewerVC.imageToShow = imageToShow
        imageViewerVC.isGoingToPresent = isGoingToPresent
        return imageViewerVC
    }
    
    public class func instance(imageURLToShow:URL, isGoingToPresent:Bool) -> RMImageViewerViewController {
        let imageViewerVC = instance()
        imageViewerVC.imageURLToShow = imageURLToShow
        imageViewerVC.isGoingToPresent = isGoingToPresent
        return imageViewerVC
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageView.contentMode = .scaleAspectFit
        if imageToShow != nil {
            imageView.image = imageToShow
        }
        
        if imageURLToShow != nil {
//            imageView.setImageWith(imageURLToShow,usingActivityIndicatorStyle:UIActivityIndicatorViewStyle.white)
            imageView.sd_setShowActivityIndicatorView(true)
            imageView.sd_setImage(with: imageURLToShow)
        }
        
        if isGoingToPresent == true {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: RMStrings.DISMISS_STRING(), style: .done, target: self, action: #selector(buttonActionForDismiss))
        }
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func buttonActionForDismiss() {
        self.dismiss(animated: true, completion: nil)
    }

}
