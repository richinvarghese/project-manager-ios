//
//  RMExpenseListTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 24/03/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

protocol RMExpenseListTableViewControllerDelegate: class {
    func expenseChanged(fileListTableVC:RMExpenseListTableViewController)
}

class RMExpenseListTableViewController: UITableViewController {

    
    
    var buttonSelectCategory: UIButton?
    var buttonAddExpense: UIButton?
    var arraySearch: [RMExpense]?
    var searchBar: UISearchBar?
    var arrayFilter: [RMExpense]?
    var arrayToDisplay: Array<RMExpense>?//FIXME: temporary code
    var arrayExpenseCategory: Array<RMExpenseCategory>?
    var projectUniqueId:String?
    weak var delegate:RMExpenseListTableViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(RMExpenseListTableViewCell.self)
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 122
        
        
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 32+50))
        buttonSelectCategory = UIButton(frame: CGRect(x: 0, y: 0, width: 140, height: 30))
        buttonAddExpense = UIButton(frame: CGRect(x: UIScreen.main.bounds.size.width - 170, y: 0, width: 140, height: 30))
        viewHeader.addSubview(buttonSelectCategory!)
        viewHeader.addSubview(buttonAddExpense!)
        buttonSelectCategory?.addTarget(self, action: #selector(buttonActionForSelectCategory), for: .touchUpInside)
        buttonAddExpense?.addTarget(self, action: #selector(buttonActionForAdd), for: .touchUpInside)
        buttonSelectCategory?.setTitle(selectCategoryString(), for: .normal)
        
        setupButton(button: buttonSelectCategory!)
        setupButton(button: buttonAddExpense!)
        buttonAddExpense!.backgroundColor = UIColor.colorWithHexString(hex: "#91b53d")
        //        let v = viewHeader.viewWithTag(347)
        tableView.tableHeaderView = viewHeader
        buttonAddExpense?.setTitle("Add Expense", for: .normal)
        //
        
        searchBar = UISearchBar(frame: CGRect(x: 0, y: 32, width: viewHeader.frame.size.width - 32, height: 50))
        searchBar!.delegate = self
        searchBar?.placeholder = "Search"
        viewHeader.addSubview(searchBar!)
        buttonSelectCategory!.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let e = RMExpense()
        e.expenseDescription = "Travelling"
        e.expenseDate = "2018-03-22"
        e.expenseCategoryTitle = "Miscellaneous expenses"
        e.expenseAmount = "$ 84"
        e.expenseFilesLink = "http://slideplayer.com/slide/7102443/"
        
        
        let e2 = RMExpense()
        e2.expenseDescription = "Accommodation"
        e2.expenseDate = "2017-12-24"
        e2.expenseCategoryTitle = "Miscellaneous expenses"
        e2.expenseAmount = "$ 38"
//        e2.expenseFilesLink = "http://slideplayer.com/slide/7102443/"
//        arrayToDisplay = [e,e2]
    }
    
    func selectCategoryString() -> String {
        return "Select Category"
    }
    
    func setupButton(button:UIButton) {
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 2
        button.clipsToBounds = true
        button.layer.borderColor =  UIColor.blue.cgColor
        button.backgroundColor = RMGlobalManager.appBlueColor()
    }
    
    func barButtonItemAdd(navigationItem:UINavigationItem) {
        let barButtonItemAdd = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(buttonActionForAdd))
        navigationItem.rightBarButtonItem = barButtonItemAdd
         navigationItem.rightBarButtonItem = nil
    }
    
    @objc func buttonActionForSelectCategory() {
        let ar = arrayExpenseCategory?.map {
            $0.expenseCategoryTitle
        }
        if ar == nil {
            return;
        }
        let cancelString = "Cancel"
        RMAlert.showActionSheet(title: nil, showInViewController: self, defaultButtonNames: (ar as! [String]), destructiveButtonNames: nil, cancelButtonNames: [cancelString]) { (selectedButtonTitle) in
            if selectedButtonTitle == cancelString {
                self.buttonSelectCategory?.setTitle(self.selectCategoryString(), for: .normal)
                self.arrayFilter = nil
            }else {
                self.buttonSelectCategory?.setTitle(selectedButtonTitle, for: .normal)
                self.arrayFilter = self.arrayToDisplay!.filter{
                    $0.expenseCategoryTitle == selectedButtonTitle
                }
            }
            
            
            
            self.tableView.reloadData()
        }
    }
    
    @objc func buttonActionForAdd() {
        let newExpenseTableVC = RMAddExpenseTableViewController.instance()
        newExpenseTableVC.delegate = self
        newExpenseTableVC.projectUniqueId = projectUniqueId
        newExpenseTableVC.arrayExpenseCategory = arrayExpenseCategory
        self.navigationController?.pushViewController(newExpenseTableVC, animated: true)
//        let newNoteTableVC = RMAddNoteTableViewController.instance()
//        newNoteTableVC.delegate = self
//        newNoteTableVC.selectedProjectUniqueId = selectedProjectUniqueId
//        self.navigationController?.pushViewController(newNoteTableVC, animated: true)
    }
    
    func showingObject(atIndexPath:IndexPath) -> RMExpense {
        if (searchBar?.text?.count ?? 0) > 0 {
            return arraySearch![atIndexPath.row]
        }
        if arrayFilter != nil {
            return arrayFilter![atIndexPath.row]
        }
        //        let array = displayingArray()
        return arrayToDisplay![atIndexPath.row]
    }
    
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (searchBar?.text?.count ?? 0) > 0 {
            return arraySearch?.count ?? 0
        }
        if arrayFilter != nil {
            return arrayFilter!.count
        }
        if arrayToDisplay == nil {
            return 0
        }
        return arrayToDisplay!.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: RMExpenseListTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        
        let expense = showingObject(atIndexPath: indexPath)
        cell.setExpense(expense: expense)
        cell.delegate = self
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let expense = showingObject(atIndexPath: indexPath)
        
        //        if projectFile.projectFileImageUrl == nil {
        //            return
        //        }
        //
        //        let documentVC = RMDocumentViewController(url:projectFile.projectFileImageUrl!, isGoingToPresent:true)
        //        //        documentVC.url
        //        //        let imageViewerVC = RMImageViewerViewController.instance(imageURLToShow: URL(string:projectFile.projectFileImageUrl!)!, isGoingToPresent: true)
        //        let navController = UINavigationController(rootViewController: documentVC)
        //        self.present(navController, animated: true, completion: nil)
        //        let editNoteTableVC = RMAddNoteTableViewController.instance()
        //        editNoteTableVC.selectedNote = note
        //        editNoteTableVC.delegate = self
        //        editNoteTableVC.selectedProjectUniqueId = selectedProjectUniqueId
        //        self.navigationController?.pushViewController(editNoteTableVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        let projectFile = showingProjectFile(atIndexPath: indexPath)
//        return 152
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
        
        RMTheme.cellCornerRadius(cell: cell, borderColor: UIColor.red, backgroundColor: UIColor.clear)
    }
    
    deinit {
        print("dealloc=RMExpenseListTableViewController")
    }
    
}

extension RMExpenseListTableViewController:RMExpenseListTableViewCellDelegate {
    func buttonActionAttachment(cell:RMExpenseListTableViewCell) {
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController: self)
            return;
        }
        
        
        let indexPath =  tableView.indexPath(for: cell)
        if indexPath == nil {
            return;
        }
        let expense = showingObject(atIndexPath: indexPath!)
        if expense.expenseFilesLink == nil {
            return;
        }
//        if expense!.attachmentType == RMTimeLine.TimeLineAttachmentType.Photo {
//        [
            //            SDW
            
            //            let image = UIImage(data:timeLine!.attachment!,scale:1.0)
            //            if image == nil {
            //                return
            //            }
            let attachmentURL = URL(string:expense.expenseFilesLink!)
        if attachmentURL == nil {
            return;
        }
        
        
        
        let documentVC = RMDocumentViewController(url: attachmentURL!, isGoingToPresent: true)
        let navController = UINavigationController(rootViewController: documentVC)
        self.present(navController, animated: true, completion: nil)
        
        
//            let imageViewer = RMImageViewerViewController.instance(imageURLToShow: attachmentURL!,isGoingToPresent: true)
//            //            let imageViewer = RMImageViewerViewController.instance(imageToShow: image!,isGoingToPresent: true)
//            let nav = UINavigationController(rootViewController: imageViewer)
//            self.present(nav, animated: true, completion: nil)
            //            self.navigationController?.pushViewController(imageViewer, animated: true)
//        }
    }
}

extension RMExpenseListTableViewController:RMAddExpenseTableViewControllerDelegate {
    func modifiedExpense(addExpenseTableViewController:RMAddExpenseTableViewController) {
        delegate?.expenseChanged(fileListTableVC: self)
    }
}


extension RMExpenseListTableViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) // called when text changes (including clear)
    {
        if arrayToDisplay == nil {
            return;
        }
        
        if arrayFilter != nil {
            arraySearch = arrayFilter?.filter {
                ($0.expenseDescription?.contains(searchText))!
            }
        } else {
            arraySearch = arrayToDisplay?.filter {
                ($0.expenseDescription?.contains(searchText))!
            }
        }
        tableView.reloadData()
    }
    
}




