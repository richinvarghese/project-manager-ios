//
//  RMMessageListTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 18/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

protocol RMMessageListTableViewControllerDelegate {
    func buttonActionReply(messageListTableViewController: RMMessageListTableViewController,replyToMessage:RMMessage)
}

class RMMessageListTableViewController: UITableViewController {

    var arrayMessages: [RMMessage]?
    
    var delegate:RMMessageListTableViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(RMMessageListTableViewCell.self)
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        //        addDummyTask()//FIXME: Dummy data
//        fetchMessage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    
//    func fetchMessage() {
//        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
//            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController:self)
//            return
//        }
//        RMWebServiceManager.fetchMessages {[weak self] (result, isSuccess) in
//            if isSuccess == true {
//                self?.arrayMessages = result as! [RMMessage
//                ]
//                self?.tableView.reloadData()
//            }
//        }
//    }
    
//    func addDummyTask() {//FIXME: Dummy data
//        arrayTask = []
//        let task = RMTask()
//        task.taskTitle = "Task Title1"
//        task.taskDescription = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
//        task.taskStatus = "Completed"
//        //        task.taskStatusType = RMTask.TaskStatusType.Completed
//        arrayTask?.append(task)
//
//        let taskTwo = RMTask()
//        taskTwo.taskTitle = "Second Task"
//        taskTwo.taskDescription = "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
//        taskTwo.taskStatus = "Not Started"
//        //        taskTwo.taskStatusType = RMTask.TaskStatusType.NotStarted
//        arrayTask?.append(taskTwo)
//
//
//        let task3 = RMTask()
//        task3.taskTitle = "Third Task Title"
//        task3.taskDescription = "Like Aldus PageMaker including versions of Lorem Ipsum"
//        task3.taskStatus = "In progress"
//        //        task3.taskStatusType = RMTask.TaskStatusType.InProgress
//        arrayTask?.append(task3)
//    }
    
    //    func addConstraintWithSuperView(){
    //        NSLayoutConstraint(item: tableView, attribute: .top, relatedBy: .equal, toItem: tableView.superview, attribute: .top, multiplier: 1, constant: 0).isActive = true
    //        NSLayoutConstraint(item: tableView, attribute: .bottom, relatedBy: .equal, toItem: tableView.superview, attribute: .bottom, multiplier: 1, constant: 10).isActive = true
    //        NSLayoutConstraint(item: tableView, attribute: .leading, relatedBy: .equal, toItem: tableView.superview, attribute: .leading, multiplier: 1, constant: 0).isActive = true
    //        NSLayoutConstraint(item: tableView, attribute: .trailing, relatedBy: .equal, toItem: tableView.superview, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
    //    }
    
    func showingMessage(atIndexPath:IndexPath) -> RMMessage {
        //        let array = displayingArray()
        return arrayMessages![atIndexPath.row]
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayMessages == nil {
            return 0
        }
        return arrayMessages!.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:RMMessageListTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate = self
        let message = showingMessage(atIndexPath: indexPath)
        cell.setMessage(message: message)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 106;
//        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let message = showingMessage(atIndexPath: indexPath)
        let taskDetailTableVC = RMMessageDetailTableViewController(style: .plain)
//        taskDetailTableVC.selectedTask = message
       taskDetailTableVC.selectedMessageUniqueId = message.messageUniqueId
        self.navigationController?.pushViewController(taskDetailTableVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
        RMTheme.cellCornerRadius(cell: cell, borderColor: UIColor.red, backgroundColor: UIColor.clear)
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//extension RMTaskListTableViewController : RMTaskListTableViewCellDelegate {
//    func buttonActionChangeStatus(cell:RMTaskListTableViewCell){
//        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
//            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController: self)
//            return
//        }
//
//
//        let indexPath =  tableView.indexPath(for: cell)
//        if indexPath == nil {
//            return
//        }
//        let task = showingTask(atIndexPath: indexPath!)
//        let taskStatusTableVC = RMTaskStatusTableViewController(style: .plain)
//        taskStatusTableVC.selectedTask = task
//        taskStatusTableVC.delegate = self
//        taskStatusTableVC.arrayTaskStatus = arrayTaskStatus
//        self.navigationController?.pushViewController(taskStatusTableVC, animated: true)
//    }
//}
//
//extension RMTaskListTableViewController : RMTaskStatusTableViewControllerDelegate {
//    func taskChanged(taskStatusTableVC:RMTaskStatusTableViewController, taskStatusChangedTo:RMTaskStatus, forTask:RMTask) {
//        //        let taskIndex = arrayTask!.index(of: forTask)
//        //        let indexPath = IndexPath(row: taskIndex!, section: 0)
//        //        tableView.reloadRows(at: [indexPath], with: .automatic)
//        self.delegate?.taskChanged(taskListTableVC: self)
//    }
//}

extension RMMessageListTableViewController: RMMessageListTableViewCellDelegate {
    func buttonActionReply(cell: RMMessageListTableViewCell) {
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController: self)
            return
        }
        
        
        let indexPath =  tableView.indexPath(for: cell)
        if indexPath == nil {
            return
        }
        let message = showingMessage(atIndexPath: indexPath!)
        delegate?.buttonActionReply(messageListTableViewController: self, replyToMessage: message)
    }
    
    
}


