//
//  RMForgetPwdTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 13/05/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMForgetPwdTableViewController: UITableViewController {
    
    class func instance() -> UITableViewController {
        let storyboard = UIStoryboard(name: "RMForgetPwd", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RMForgetPwdTableViewController") as! RMForgetPwdTableViewController
        return vc
    }
    
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var labelEmailTitle: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.tableView.allowsSelection = false
        self.tableView.separatorStyle = .none
        labelEmailTitle.textColor = RMGlobalManager.appBlueColor()
        buttonSubmit.setTitleColor(RMGlobalManager.appBlueColor(), for: .normal)
        
        textFieldEmail.layer.borderWidth = 1
        textFieldEmail.layer.borderColor = UIColor.lightGray.cgColor
        textFieldEmail.borderStyle = .none
        let leftPaddingView = UIView(frame: CGRect(x:0, y:0,width:10,height:textFieldEmail.frame.size.height))
        textFieldEmail.leftView = leftPaddingView
        textFieldEmail.leftViewMode = .always
        textFieldEmail.returnKeyType = .next
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Dismiss", style: .plain, target: self, action: #selector(dismissAction))
//        textFieldEmail.placeholder = RMStrings.E_MAIL_STRING()
//        textFieldEmail.delegate = self
        setupButton(button: buttonSubmit)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupButton(button:UIButton) {
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 2
        button.clipsToBounds = true
        button.layer.borderColor =  UIColor.blue.cgColor
        button.backgroundColor = RMGlobalManager.appBlueColor()
    }
    
    class func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    @IBAction func buttonActionForSubmit() {
        self.view.endEditing(true)
        textFieldEmail.text = textFieldEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController:self)
            return
        }
        
        if textFieldEmail.text?.count == 0 {
            RMAlert.showAlert(title: nil, message: RMStrings.ENTER_E_MAIL_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textFieldEmail.becomeFirstResponder()
            })
            return
        }
        
        if RMForgetPwdTableViewController.isValidEmail(testStr: textFieldEmail.text!) == false {
            RMAlert.showAlert(title: nil, message: RMStrings.ENTER_VALID_E_MAIL_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textFieldEmail.becomeFirstResponder()
            })
            return
        }
        RMWebServiceManager.sendEmail(email:self.textFieldEmail.text!)  {[weak self] (result:Any?, isSuccess:Bool) in
            if isSuccess == true {
                self?.textFieldEmail.text = ""
                self?.tableView.reloadData()
                DispatchQueue.main.async {
                    self?.dismissAction()
                }
            }
    }
    }
    
    @objc func dismissAction() {
        self.dismiss(animated: true, completion: nil)
    }

    // MARK: - Table view data source
    

}
