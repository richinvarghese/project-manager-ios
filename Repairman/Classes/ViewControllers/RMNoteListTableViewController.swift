//
//  RMNoteListTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 19/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit

protocol RMNoteListTableViewControllerDelegate: class {
    func notesFetched(noteListTableVC:RMNoteListTableViewController)
}

class RMNoteListTableViewController: UITableViewController {

    static var arrayToDisplay: Array<RMNote>?//FIXME: temporary code
    
    static let reuseIdentifierNoteCell = "RMNoteCell"
    
    var selectedProjectUniqueId: String?
    
    weak var delegate: RMNoteListTableViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = RMStrings.NOTE_STRING()
//        self.tableView.register(UITableViewCell.self)
//        self.tableView.register(RMNoteListTableViewCell.self)
        let nib = UINib(nibName: "RMSubtitleTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: RMNoteListTableViewController.reuseIdentifierNoteCell)
        self.tableView.tableFooterView = UIView()
        barButtonItemAdd(navigationItem:self.navigationItem)
        if RMNoteListTableViewController.arrayToDisplay == nil {
            RMNoteListTableViewController.arrayToDisplay = []
        }
        fetchNote()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //FIXME: temporary code
        tableView.reloadData()
    }
    
    
    func barButtonItemAdd(navigationItem:UINavigationItem) {
        let barButtonItemAdd = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(buttonActionForAdd))
        navigationItem.rightBarButtonItem = barButtonItemAdd
    }
    
    
    func fetchNote() {
        
//        RMWebServiceManager.saveNote(title: "test123", description: "test4546")
//        return
        RMNoteListTableViewController.arrayToDisplay = nil
        self.tableView.reloadData()

        RMWebServiceManager.fetchNote(projectUniqueId:selectedProjectUniqueId) {[weak self] (result:Any?, isSuccess:Bool) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.delegate?.notesFetched(noteListTableVC: strongSelf)
            if isSuccess == false {
                return
            }
            let notesArray = result as! [RMNote]
            RMNoteListTableViewController.arrayToDisplay = notesArray
            strongSelf.tableView.reloadData()
        }
    }
    
    
    @objc func buttonActionForAdd() {
        let newNoteTableVC = RMAddNoteTableViewController.instance()
       newNoteTableVC.delegate = self
       newNoteTableVC.selectedProjectUniqueId = selectedProjectUniqueId
        self.navigationController?.pushViewController(newNoteTableVC, animated: true)
    }
    
    func displayingArray() -> [RMNote] {
        if RMNoteListTableViewController.arrayToDisplay == nil {
            return []
        }
        
        return RMNoteListTableViewController.arrayToDisplay!
        
    }
    
    func showingNote(atIndexPath:IndexPath) -> RMNote {
//        let array = displayingArray()
        return displayingArray()[atIndexPath.row]
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayingArray().count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let cell: RMSubtitleTableViewCell = tableView.dequeueReusableCell(withIdentifier: RMNoteListTableViewController.reuseIdentifierNoteCell, for: indexPath) as! RMSubtitleTableViewCell
//        if cell == nil {
//            cell = UITableViewCell(style: UITableViewCellStyle.value2, reuseIdentifier: "UITableViewCell")
//        }

        let note = showingNote(atIndexPath: indexPath)
        cell.setNote(note: note)
//        cell!.textLabel?.text = note.noteTitle
//        cell!.detailTextLabel?.text = RMGlobalManager.stringFromDate(date:note.notePostedDate!)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let note = showingNote(atIndexPath: indexPath)
        let editNoteTableVC = RMAddNoteTableViewController.instance()
        editNoteTableVC.selectedNote = note
       editNoteTableVC.delegate = self
       editNoteTableVC.selectedProjectUniqueId = selectedProjectUniqueId
        self.navigationController?.pushViewController(editNoteTableVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let note = showingNote(atIndexPath: indexPath)
        if editingStyle == .delete {
            
            RMWebServiceManager.deleteNote(noteUniqueId: note.noteUniqueId!, responseHandler: {[weak self] (result:Any?, isSuccess:Bool) in
                RMNoteListTableViewController.arrayToDisplay?.remove(at: indexPath.row)
                self?.tableView.reloadData()
            })
            
            
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
        RMTheme.cellCornerRadius(cell: cell, borderColor: RMGlobalManager.appBlueColor(), backgroundColor: UIColor.clear)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    deinit {
        print("dealloc=RMNoteListTableVC")
    }

}

extension RMNoteListTableViewController: RMNavigationBarButtonItems {
    func neededNavigationBarButtonItems() -> RMNeededNavigationButtonItem {
        if navigationController?.viewControllers.count == 1 {
            return .leftMenu
        }
        
        return .none
    }
}

extension RMNoteListTableViewController: RMNavigationBarButtonActions {
    func barButtonLeftAction() {
        RMSideMenuAndRoot.showLeftMenu()
    }
}

extension RMNoteListTableViewController: RMAddNoteTableViewControllerDelegate {
    
    func modifiedNote(addNoteTableVC:RMAddNoteTableViewController) {
        fetchNote()
    }

}
