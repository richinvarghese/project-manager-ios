//
//  RMTimeCardListTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 05/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMTimeCardListViewController: UIViewController {

    var arrayToDisplay: Array<RMTimeCard>?//FIXME: temporary code
//    var arrayFilterResult: Array<RMTimeCard>?
    var datePickerStartDate: UIDatePicker?
    var datePickerEndDate: UIDatePicker?
    
//    var textFieldStartDate: RMNoPasteTextField?
//    var textFieldEndDate: RMNoPasteTextField?
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textFieldStartDate: RMNoPasteTextField!
    @IBOutlet weak var textFieldEndDate: RMNoPasteTextField!
    //    var delegate: RMFileListTableViewControllerDelegate?
    //    var projectUniqueId:String?
    //    static let reuseIdentifierNoteCell = "RMNoteCell"
    //
    //    var selectedProjectUniqueId: String?
    var labelTotalDuration: UILabel?
    
    
    public class func instance() -> RMTimeCardListViewController {
        let timecardStoryBoard = UIStoryboard(name: "RMTimecard", bundle: nil)
        let timecardTableVC = timecardStoryBoard.instantiateViewController(withIdentifier: "RMTimeCardListViewController") as! RMTimeCardListViewController
        return timecardTableVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = RMStrings.TIME_CARD_STRING()
        labelTotalDuration = UILabel(frame: CGRect(x:160,y:0,width:UIScreen.main.bounds.size.width,height:40))
//        labelTotalDuration?.text = "4:04:33"
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\(RMStrings.TOTAL_STRING()) 06:38:23")
        attributeString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.lightGray, range: NSMakeRange(0, RMStrings.TOTAL_STRING().count))
        labelTotalDuration?.textAlignment = .center
        labelTotalDuration?.attributedText =  attributeString
        
//        let viewHeader = Bundle.main.loadNibNamed("RMTimecardHeader", owner: self, options: nil)?.first as! UIView
//        textFieldStartDate = viewHeader.viewWithTag(12) as! RMNoPasteTextField
//        textFieldEndDate = viewHeader.viewWithTag(22) as! RMNoPasteTextField
        
        datePickerStartDate = UIDatePicker(frame: CGRect(x:0,y:0,width:UIScreen.main.bounds.size.width,height:200))
        datePickerStartDate?.minimumDate = nil;
        datePickerStartDate?.maximumDate = Date();
        datePickerStartDate?.translatesAutoresizingMaskIntoConstraints = false
        datePickerStartDate?.datePickerMode = .date
        
        datePickerStartDate?.addTarget(self, action: #selector(datePickerValueChanged(datePicker:)), for: .valueChanged)
        
        textFieldStartDate?.inputView = datePickerStartDate
        textFieldStartDate.delegate = self
        textFieldStartDate?.inputAccessoryView = RMGlobalManager.toolBarWithNext(target: self, action: #selector(barButtonNextAction))
        
        
        datePickerEndDate = UIDatePicker(frame: CGRect(x:0,y:0,width:UIScreen.main.bounds.size.width,height:200))
        datePickerEndDate?.minimumDate = nil;
        datePickerEndDate?.maximumDate = Date();
        datePickerEndDate?.translatesAutoresizingMaskIntoConstraints = false
        datePickerEndDate?.addTarget(self, action: #selector(datePickerValueChanged(datePicker:)), for: .valueChanged)
        datePickerEndDate?.datePickerMode = .date
        textFieldEndDate?.inputView = datePickerEndDate
        textFieldEndDate.delegate = self
        textFieldEndDate?.inputAccessoryView = RMGlobalManager.toolbar(barButtonItem: UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(barButtonDoneAction)))
        
//        self.tableView.tableHeaderView = viewHeader
        tableView.allowsSelection = false
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction))
        tableView.addGestureRecognizer(tapGesture)
//        self.tableView.tableHeaderView = viewHeader
//        viewHeader.setNeedsLayout()
//        viewHeader.layoutIfNeeded()
//        viewHeader.frame.size = viewHeader.systemLayoutSizeFitting(UILayoutFittingExpandedSize)
//        self.tableView.tableHeaderView = viewHeader
//        tableView.tableHeaderView?.frame = CGRect(x:0,y:0,width:UIScreen.main.bounds.size.width,height:50)
        
//       se self.tableView.tableHeaderView?.translatesAutoresizingMaskIntoConstraints = true
//        labelTotalDuration!.addSubview(viewHeader)

        //        self.tableView.register(UITableViewCell.self)
        //        self.tableView.register(RMNoteListTableViewCell.self)
        //        let nib = UINib(nibName: "RMSubtitleTableViewCell", bundle: nil)
        //        self.tableView.register(nib, forCellReuseIdentifier: RMNoteListTableViewController.reuseIdentifierNoteCell)
        self.tableView.register(RMTimeCardTableViewCell.self)
        self.tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        //        fetchNote()
        //        self.tableView.estimatedRowHeight = 200
        //        self.tableView.rowHeight = UITableViewAutomaticDimension
        let currentDateOnlyString = RMGlobalManager.webServiceOnlyStringDateFromDate(date:Date())
        fetchTimecard(stringStartDate: currentDateOnlyString, stringEndDate: currentDateOnlyString)
//        var timeCardInDate: String?
//        var timeCardInTime: String?
//        var timeCardOutDate: String?
//        var timeCardOutTime: String?
//        var timeCardDuration: String?
        
//        arrayToDisplay = [RMTimeCard]()
//        let timeCard = RMTimeCard()
//        timeCard.timeCardInDate = "2010 - 10 - 11"
//        timeCard.timeCardInTime = "11:40 AM"
//        timeCard.timeCardOutDate = "2010 - 10 - 12"
//        timeCard.timeCardOutTime = "10:10 AM"
//        timeCard.timeCardDuration = "00:04:23"
//
//        arrayToDisplay?.append(timeCard)//FIXME:temp code
//
//        let timeCard2 = RMTimeCard()
//        timeCard2.timeCardInDate = "2012 - 09 - 05"
//        timeCard2.timeCardInTime = "03:45 AM"
//        timeCard2.timeCardOutDate = "2014 - 12 - 14"
//        timeCard2.timeCardOutTime = "02:03 PM"
//        timeCard2.timeCardDuration = "02:04:23"
//
//        arrayToDisplay?.append(timeCard2)//FIXME:temp code
        datePickerValueChanged(datePicker: datePickerStartDate!)
        datePickerValueChanged(datePicker: datePickerEndDate!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    func fetchTimecard(stringStartDate:String, stringEndDate:String) {
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController:self)
            return
        }
        
        
//        let
        

        RMWebServiceManager.fetchAttendance(startDate: stringStartDate, endDate: stringEndDate) {[weak self] (result, isSuccess) in
            
            guard let strongSelf = self else {
                return;
            }
            
            if isSuccess == true {
                let ss = result as! Any
                let so = ss as? [RMTimeCard]
                strongSelf.arrayToDisplay = so //Array<RMTimeCard> //as? [RMTimeCard]
                strongSelf.tableView.reloadData()
            }
        }
    }
    
    //    func fetchActivi() {
    //
    //        //        RMWebServiceManager.saveNote(title: "test123", description: "test4546")
    //        //        return
    //        RMNoteListTableViewController.arrayToDisplay = nil
    //        self.tableView.reloadData()
    //
    //        RMWebServiceManager.fetchNote(projectUniqueId:selectedProjectUniqueId) {[weak self] (result:Any?, isSuccess:Bool) in
    //            if isSuccess == false {
    //                return
    //            }
    //            let notesArray = result as! [RMNote]
    //            RMNoteListTableViewController.arrayToDisplay = notesArray
    //            self?.tableView.reloadData()
    //        }
    //    }
    
    
    //    @objc func buttonActionForAdd() {
    //        let newNoteTableVC = RMAddNoteTableViewController.instance()
    //        newNoteTableVC.delegate = self
    //        newNoteTableVC.selectedProjectUniqueId = selectedProjectUniqueId
    //        self.navigationController?.pushViewController(newNoteTableVC, animated: true)
    //    }
    
    //    func displayingArray() -> [RMNote] {
    //        if RMNoteListTableViewController.arrayToDisplay == nil {
    //            return []
    //        }
    //
    //        return RMNoteListTableViewController.arrayToDisplay!
    //
    //    }
    
    @objc func tapGestureAction() {
        textFieldStartDate?.resignFirstResponder()
        textFieldEndDate?.resignFirstResponder()
    }
    
    @objc func barButtonNextAction() {
        textFieldStartDate?.resignFirstResponder()
        textFieldEndDate?.becomeFirstResponder()
    }
    
    @objc func barButtonDoneAction() {
        textFieldStartDate?.resignFirstResponder()
        textFieldEndDate?.resignFirstResponder()
    }
    
    
    @objc func datePickerValueChanged(datePicker:UIDatePicker) {
        if datePicker == datePickerStartDate {
            textFieldStartDate?.text = RMGlobalManager.webServiceOnlyStringDateFromDate(date: datePickerStartDate!.date)
        } else if datePicker == datePickerEndDate {
            textFieldEndDate?.text = RMGlobalManager.webServiceOnlyStringDateFromDate(date: datePickerEndDate!.date)
        }
    }
    
    
    @IBAction func buttonActionForFilter(_ sender: Any) {
        print("buttonActionForTimecards")
        self.view.endEditing(true)
        if textFieldStartDate.text?.count == 0 {
            RMAlert.showAlert(title: nil, message: "Enter valid start date", showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: { [weak self] (slectedButtonTitle) in
                self?.textFieldStartDate.becomeFirstResponder()
            })
            return;
        }
        
        if textFieldEndDate.text?.count == 0 || datePickerStartDate!.date > datePickerEndDate!.date {
            RMAlert.showAlert(title: nil, message: "Enter valid end date", showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: { [weak self] (slectedButtonTitle) in
                self?.textFieldEndDate.becomeFirstResponder()
            })
            return;
        }
        
        let startDateOnlyString = RMGlobalManager.webServiceOnlyStringDateFromDate(date:datePickerStartDate!.date)
        let endDateOnlyString = RMGlobalManager.webServiceOnlyStringDateFromDate(date:datePickerEndDate!.date)
        fetchTimecard(stringStartDate: startDateOnlyString, stringEndDate: endDateOnlyString)
    }
    
    func showingTimeCard(atIndexPath:IndexPath) -> RMTimeCard {
        //        let array = displayingArray()
        return arrayToDisplay![atIndexPath.row]
    }
    
    //    func barButtonItemAdd(navigationItem:UINavigationItem) {
    //        let barButtonItemAdd = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(buttonActionForAdd))
    //        navigationItem.rightBarButtonItem = barButtonItemAdd
    //    }
    //
    //    @objc func buttonActionForAdd() {
    //        let addTimeLineTableVC = RMAddTimeLineTableViewController.instance(selectedTimeLine: nil, replyToTimeLine: nil)
    //        addTimeLineTableVC.delegate = self
    //        addTimeLineTableVC.isForFileUpload = true
    //        self.navigationController?.pushViewController(addTimeLineTableVC, animated: true)
    //    }
    
    deinit {
        print("dealloc=RMTimeCardListTableViewController")
    }
    
}

extension RMTimeCardListViewController:UITableViewDelegate,UITableViewDataSource {
    // MARK: - Table view data source
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayToDisplay == nil {
            return 0
        }
        return arrayToDisplay!.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let cell: RMTimeCardTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        //        if cell == nil {
        //            cell = UITableViewCell(style: UITableViewCellStyle.value2, reuseIdentifier: "UITableViewCell")
        //        }
        
        let timerCard = showingTimeCard(atIndexPath: indexPath)
        cell.setTimeCard(timeCard: timerCard)
        //        cell!.textLabel?.text = note.noteTitle
        //        cell!.detailTextLabel?.text = RMGlobalManager.stringFromDate(date:note.notePostedDate!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let timeCard = showingTimeCard(atIndexPath: indexPath)
        
        //        if projectFile.projectFileImageUrl == nil {
        //            return
        //        }
        //
        //        let documentVC = RMDocumentViewController(url:projectFile.projectFileImageUrl!, isGoingToPresent:true)
        //        //        documentVC.url
        //        //        let imageViewerVC = RMImageViewerViewController.instance(imageURLToShow: URL(string:projectFile.projectFileImageUrl!)!, isGoingToPresent: true)
        //        let navController = UINavigationController(rootViewController: documentVC)
        //        self.present(navController, animated: true, completion: nil)
        //        let editNoteTableVC = RMAddNoteTableViewController.instance()
        //        editNoteTableVC.selectedNote = note
        //        editNoteTableVC.delegate = self
        //        editNoteTableVC.selectedProjectUniqueId = selectedProjectUniqueId
        //        self.navigationController?.pushViewController(editNoteTableVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        let projectFile = showingProjectFile(atIndexPath: indexPath)
        return 128
        //        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
        RMTheme.cellCornerRadius(cell: cell, borderColor: UIColor.red, backgroundColor: UIColor.clear)
    }
    
    //    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    //        let note = showingNote(atIndexPath: indexPath)
    //        if editingStyle == .delete {
    //
    //            RMWebServiceManager.deleteNote(noteUniqueId: note.noteUniqueId!, responseHandler: {[weak self] (result:Any?, isSuccess:Bool) in
    //                RMNoteListTableViewController.arrayToDisplay?.remove(at: indexPath.row)
    //                self?.tableView.reloadData()
    //            })
    //
    //
    //        }
    //    }
    
}

extension RMTimeCardListViewController:UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == textFieldStartDate || textField == textFieldEndDate {
            if (textField.text?.count == 0) {
                if textField == textFieldStartDate {
                    textField.text = RMGlobalManager.webServiceOnlyStringDateFromDate(date: datePickerStartDate!.date)
                } else if textField == textFieldEndDate {
                    textField.text = RMGlobalManager.webServiceOnlyStringDateFromDate(date: datePickerEndDate!.date)
                }
            }
        }
        return true
    }
}


extension RMTimeCardListViewController: RMNavigationBarButtonItems {
    func neededNavigationBarButtonItems() -> RMNeededNavigationButtonItem {
        
        if navigationController?.viewControllers.count == 1 {
            return .leftMenu
        }
        
        return .none
    }
}

extension RMTimeCardListViewController: RMNavigationBarButtonActions {
    func barButtonLeftAction() {
        RMSideMenuAndRoot.showLeftMenu()
    }
}
