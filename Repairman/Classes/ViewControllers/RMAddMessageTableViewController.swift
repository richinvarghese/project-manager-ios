//
//  RMAddMessageTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 18/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

//class RMAddMessageTableViewController: UITableViewController {

    @objc protocol RMAddMessageTableViewControllerDelegate: class {
        func modifiedMessage(addMessageTableViewController:RMAddMessageTableViewController)
        
        @objc optional func messagePostbuttonAction(addMessageTableViewController:RMAddMessageTableViewController,fileData:Data?,timeLineDescription:String, mimeType:String?,fileExtension:String?,completion:(Bool) -> Swift.Void)
    }
    
    class RMAddMessageTableViewController: UITableViewController {
        
//        var isForFileUpload:Bool?
        @IBOutlet weak var buttonSend : UIButton!
        @IBOutlet weak var buttonAttachment : UIButton!
        @IBOutlet weak var labelToTitle : UILabel!
        @IBOutlet weak var labelSubjectTitle : UILabel!
        @IBOutlet weak var labelMessageTitle : UILabel!
        @IBOutlet weak var textFieldRecipient: UITextField!
        @IBOutlet weak var textFieldTitle: UITextField!
        @IBOutlet weak var textViewDesciption: UITextView!
        var labelPlaceholderDescription : UILabel?
        let lengthLimitDescription = 250
//        var labelPlaceholderDescription : UILabel?
        var recipient:RMProjectMember?
        let lengthLimitTitle = 100
        internal var selectedMessage : RMMessage?
        private var replyToMessage : RMMessage?
        var currentAttachmentType: RMTimeLine.TimeLineAttachmentType?
        var currentAttachmentData: Data?
        var currentAttachmentURL: URL?
        weak var delegate:RMAddMessageTableViewControllerDelegate?
        var arrayMemebers: [RMProjectMember]?
//        var replyToMessageUniqueId:String?
        
        @IBOutlet weak var imageViewAttachment: UIImageView!
        @IBOutlet weak var attachementCell: UITableViewCell!
        @IBOutlet weak var recipientCell: UITableViewCell!
        @IBOutlet weak var titleCell: UITableViewCell!
        
//        @IBOutlet weak var submitButton: UIButton!
        
        public class func instance(selectedMessage:RMMessage?, replyToMessage:RMMessage?) -> RMAddMessageTableViewController {
            let rootStoryBoard = UIStoryboard(name: "RMMessage", bundle: nil)
            let newMessageTableVC = rootStoryBoard.instantiateViewController(withIdentifier: "RMAddMessageTableViewController") as! RMAddMessageTableViewController
            newMessageTableVC.selectedMessage = selectedMessage
            newMessageTableVC.replyToMessage = replyToMessage
            return newMessageTableVC
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            setupSendButton(sendButton: buttonSend)
            setupUploadButton(uploadButton: buttonAttachment)
            if selectedMessage != nil {
                textViewDesciption.text = selectedMessage?.messageDescription
                if selectedMessage!.attachment != nil {
                    setAttachment(attachment: selectedMessage!.attachment!, attachmentType: selectedMessage!.attachmentType!)
                }
            }
            
            textViewDesciption.layer.cornerRadius = 5
            textViewDesciption.clipsToBounds = true
            textViewDesciption.layer.borderWidth = 1
//            textViewDesciption.layer.borderColor = UIColor.lightGray.cgColor
            textViewDesciption.layer.borderColor = RMGlobalManager.appBlueColor().cgColor
            textViewDesciption.delegate = self
            //        let barButtonItemAttachment = UIBarButtonItem(title: RMStrings.ATTACHMENT_STRING(), style: .done, target: self, action: #selector(buttonActionForAttachment))
//            if replyToMessage == nil {
                let barButtonItemAttachment = UIBarButtonItem(image: UIImage(named:"AttachmentButton"), style: .done, target: self, action: #selector(buttonActionForAttachment))
                self.navigationItem.rightBarButtonItem = barButtonItemAttachment
//            }
            
//            if selectedMessage?.isAdminPost == false {
//                self.navigationItem.rightBarButtonItem = nil
//            }
            
            self.tableView.tableFooterView = UIView()
            labelPlaceholderDescription = RMGlobalManager.addPlaceholderForView(view: textViewDesciption, text: "", font: textViewDesciption.font!)
            if textViewDesciption.text.count > 0 {
                labelPlaceholderDescription?.isHidden = true
            }
            
            self.tableView.separatorStyle = .none
            //        self.tableView.allowsSelection = false
            let barButtonItem = UIBarButtonItem(title: RMStrings.DONE_STRING(), style: .done, target: self, action: #selector(barButtonDoneAction))
            let toolbarWithBarButton = RMGlobalManager.toolbar(barButtonItem: barButtonItem)
            textViewDesciption.inputAccessoryView = toolbarWithBarButton
            
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction))
            tapGesture.delegate = self
            self.view.addGestureRecognizer(tapGesture)
            
            
            imageViewAttachment.layer.borderColor = UIColor.lightGray.cgColor
            imageViewAttachment.layer.borderWidth = 1
            
            let _ = RMGlobalManager.pickerView(textField: textFieldRecipient, toolBarButtonTitle: RMStrings.NEXT_STRING(), pickerViewDelegateAndDataSource: self, barButtonTarget: self, barButtonAction: #selector(barButtonNextAction))
            
//            textFieldRecipient.placeholder = RMStrings.RECIPIENT_STRING()
            textFieldTitle.placeholder = ""//RMStrings.TITLE_STRING()
            setupTextField(textField: textFieldTitle)
            setupTextField(textField: textFieldRecipient)
            fetchRecipient()
            
            let barButtonItemNext = UIBarButtonItem(title: RMStrings.NEXT_STRING(), style: .done, target: self, action: #selector(barButtonNextAction))
            let toolbarWithBarButtonWithNext = RMGlobalManager.toolbar(barButtonItem: barButtonItemNext)
            textFieldTitle.inputAccessoryView = toolbarWithBarButtonWithNext
            
            labelToTitle.textColor = RMGlobalManager.appBlueColor()
            labelSubjectTitle.textColor = RMGlobalManager.appBlueColor()
            labelMessageTitle.textColor = RMGlobalManager.appBlueColor()
//            textViewDesciption.cor
//            setupButton(button: submitButton)
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
//        func setupButton(button:UIButton) {
//            button.setTitleColor(UIColor.white, for: .normal)
//            button.layer.cornerRadius = 2
//            button.clipsToBounds = true
//            button.layer.borderColor =  UIColor.blue.cgColor
//            button.backgroundColor = RMGlobalManager.appBlueColor()
//        }
        
        func setupUploadButton(uploadButton:UIButton) {
            uploadButton.layer.cornerRadius = 2.5
            uploadButton.layer.borderWidth = 1
            uploadButton.layer.borderColor = RMGlobalManager.appBlueColor().cgColor
        }
        
        func setupSendButton(sendButton:UIButton) {
            sendButton.backgroundColor = RMGlobalManager.appBlueColor()
            sendButton.setTitleColor(UIColor.white, for: .normal)
        }
        
        func setupTextField(textField:UITextField) {
            textField.leftView = UIView(frame: CGRect(x:0,y:0,width:8,height:30))
            textField.rightView = UIView(frame: CGRect(x:0,y:0,width:8,height:30))
            textField.rightViewMode = .always
            textField.leftViewMode = .always
            textField.delegate = self as! UITextFieldDelegate
            textField.layer.borderWidth = 1
//            textField.layer.borderColor = UIColor.lightGray.cgColor
            textField.layer.borderColor = RMGlobalManager.appBlueColor().cgColor
            textField.layer.cornerRadius = 5
            textField.clipsToBounds = true
            
        }
        
        func assignRecipientAtIndex(index:Int) {
            if arrayMemebers == nil {
                return;
            }
            if arrayMemebers!.count == 0 {
                return;
            }
            recipient = arrayMemebers![index]
            textFieldRecipient.text = recipient?.memberName
        }
        
        func fetchRecipient() {
            
            if replyToMessage != nil {
                return;
            }
            
            arrayMemebers = nil
            tableView.reloadData()
            
            if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
                RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController:self)
                return
            }
            
            RMWebServiceManager.fetchRecipient(currentUserUniqueId: RMUserDefaultManager.getCurrentUserUniqueId()!) { [weak self] (result, isSuccess) in
                if isSuccess == false || result == nil {
                    return;
                }
                
                self?.arrayMemebers = result as? [RMProjectMember]
                self?.tableView.reloadData()
                
            }
        }
        
        
        func isChangedCurrentAttachment(attachment:AnyObject?, attachmentType:RMTimeLine.TimeLineAttachmentType?) -> Bool {
            guard let attachment = attachment else {
                currentAttachmentURL = nil
                currentAttachmentData = nil
                currentAttachmentType = nil
                return false
            }
            
            guard let attachmentType = attachmentType else {
                currentAttachmentURL = nil
                currentAttachmentData = nil
                currentAttachmentType = nil
                return false
            }
            
            currentAttachmentType = attachmentType
            return true
        }
        
        func setAttachment(attachment:URL?, attachmentType:RMTimeLine.TimeLineAttachmentType?) {
            
            if isChangedCurrentAttachment(attachment: attachment as AnyObject, attachmentType: attachmentType) == false {
                return
            }
            currentAttachmentData = nil
            currentAttachmentURL = attachment
            
            if attachmentType == RMTimeLine.TimeLineAttachmentType.Photo {
                //            let image = UIImage(data:attachment,scale:1.0)
                //            imageViewAttachment.image = image
                imageViewAttachment.sd_setImage(with: attachment)
                
            } else if attachmentType == RMTimeLine.TimeLineAttachmentType.Document {
                imageViewAttachment.image = UIImage(named:"FileIcon")
            }
            tableView.reloadData()
        }
        
        
        func setAttachment(attachment:Data?, attachmentType:RMTimeLine.TimeLineAttachmentType?) {
            
            if isChangedCurrentAttachment(attachment: attachment as AnyObject, attachmentType: attachmentType) == false {
                return
            }
            currentAttachmentData = attachment
            currentAttachmentURL = nil
            
            if attachmentType == RMTimeLine.TimeLineAttachmentType.Photo {
                let image = UIImage(data:attachment!,scale:1.0)
                imageViewAttachment.image = image
                
            }
            tableView.reloadData()
        }
        
        //    public class func instance() -> RMAddNotesTableViewController {
        //        let rootStoryBoard = UIStoryboard(name: "RMRoot", bundle: nil)
        //        let newNoteTableVC = rootStoryBoard.instantiateViewController(withIdentifier: "RMAddNotesTableViewController") as! RMAddNotesTableViewController
        //        return newNoteTableVC
        //    }
        
        
        @IBAction func buttonActionForEditAttachment(_ sender: Any) {
            buttonActionForAttachment(sender)
        }
        
        
        @IBAction func buttonActionForDeleteAttachment(_ sender: Any) {
            RMAlert.showAlert(title: nil, message: RMStrings.CONFIRM_ATTACHMENT_DELETION_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.YES_STRING()], destructiveButtonNames: nil, cancelButtonNames: [RMStrings.NO_STRING()]) { [weak self](selectedButtonTitle:String) in
                guard let strongSelf = self else {
                    return
                }
                if selectedButtonTitle == RMStrings.YES_STRING() {
                    if strongSelf.currentAttachmentType == RMTimeLine.TimeLineAttachmentType.Photo {
                        strongSelf.imageViewAttachment.image = nil
                        strongSelf.tableView.reloadData()
                    }
                    strongSelf.currentAttachmentType = nil
                    strongSelf.currentAttachmentURL = nil
                }
            }
            
        }
        
        
        
        
        @objc func tapGestureAction() {
            
            self.view.endEditing(true)
            
        }
        
        @IBAction func buttonActionPost(_ sender: Any) {
            
            textFieldRecipient.resignFirstResponder()
            
            textFieldTitle.resignFirstResponder()
            
            textViewDesciption.resignFirstResponder()
            
            textFieldRecipient.text = textFieldRecipient.text?.trimmingCharacters(in: .whitespaces)
            
            textFieldTitle.text = textFieldTitle.text?.trimmingCharacters(in: .whitespaces)
            
            textViewDesciption.text = textViewDesciption.text.trimmingCharacters(in: .whitespacesAndNewlines)
            if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
                RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController:self)
                return
            }
            
            if textFieldRecipient.text?.count == 0 && replyToMessage == nil {
                RMAlert.showAlert(title: nil, message: RMStrings.ENTER_RECIPIENT_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                    self?.textFieldRecipient.becomeFirstResponder()
                })
                return
            }
            
            if textFieldTitle.text?.count == 0  && replyToMessage == nil  {
                RMAlert.showAlert(title: nil, message: RMStrings.ENTER_TITLE_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                    self?.textFieldTitle.becomeFirstResponder()
                })
                return
            }
            if textViewDesciption.text.count == 0 {
                RMAlert.showAlert(title: nil, message: RMStrings.ENTER_DESCRIPTION_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                    self?.textViewDesciption.becomeFirstResponder()
                })
                return
            }
            
            /***
             Success
             */
            
            if selectedMessage != nil {
                selectedMessage!.recipientUniqueId = recipient?.uniqueId
                selectedMessage!.messageSubject = textFieldTitle.text
                selectedMessage!.messageDescription = textViewDesciption.text
                selectedMessage!.attachment = currentAttachmentURL
                selectedMessage!.attachmentType = currentAttachmentType
                //            if currentAttachmentType == RMNote.NoteAttachmentType.Photo {
                //                var documentData : Data?
                ////                if currentAttachmentData != nil {
                ////                    documentData = UIImagePNGRepresentation(imageViewAttachment.image!)!
                ////                }
                //               selectedNote!.attachment = documentData
                //            }
                
            } else {
                
                //            //FIXME: Dummy data
//                            let message = RMMessage()
//                            message.isMyOwnPost = true
//                            message.messageDescription = textViewDesciption.text
//                            message.messageCreatedDate = Date()
//                            message.messageSubject = "My Name"
//                            message.messageOwnerAvatarImageUrl = "https://i0.wp.com/www.americanbazaaronline.com/wp-content/uploads/2014/02/SundarPichai-300x300.jpg"
                //            newTimeLine.attachment = currentAttachmentURL
                //            newTimeLine.attachmentType = currentAttachmentType
                //
                //            if replyToTimeLine == nil {
                //                //FIXME:dd
                ////                RMTimeLineListTableViewController.arrayToDisplay?.insert(newTimeLine, at: 0)
                //                newTimeLine.isAdminPost = true
                //            } else {
                //                //FIXME:dd
                //
                ////                let indexOfReply = RMTimeLineListTableViewController.arrayToDisplay!.index(of: replyToTimeLine!)!
                ////                RMTimeLineListTableViewController.arrayToDisplay?.insert(newTimeLine, at: indexOfReply+1)
                //                newTimeLine.isAdminPost = false
                //            }
            }
            
            var compressedData: Data?
            var mimeType:String = ""
            var fileExtensiom:String = ""
            if imageViewAttachment.image != nil {
                let imageCompressed = RMGlobalManager.compressImage(image: imageViewAttachment.image!)
                let image = UIImage(data:imageCompressed!)
                compressedData = UIImagePNGRepresentation(image!)
                mimeType = "image/jpeg"
                fileExtensiom = "jpeg"
                //            compressedData = imageCompressed//?.base64EncodedData()
                //            compressedData.
                //            let strBase64:String = (imageCompressed?.base64EncodedString())!
                //            compressedData = strBase64.data(using: .utf8)
                //            print(compressedData)
            }
            if currentAttachmentType == RMTimeLine.TimeLineAttachmentType.Document {
                compressedData = currentAttachmentData
                mimeType = "application/pdf"
                let url = currentAttachmentURL
                fileExtensiom = (url?.deletingPathExtension().lastPathComponent)!
                fileExtensiom = "pdf"
            }
            
//            let toId =  mess.recipient.uniqueId
            RMWebServiceManager.postMessage(toUserUniqueId: recipient?.uniqueId, fileData: compressedData, userUniqueId: RMUserDefaultManager.getCurrentUserUniqueId()!, messageSubject: textFieldTitle.text, messageDescription: textViewDesciption.text, replyMessageId: replyToMessage?.messageUniqueId, mimeType: mimeType, fileExtension: fileExtensiom )
            
//            { (<#Any?#>, <#Bool#>) in
//                <#code#>
//            }
            
            
//            RMWebServiceManager.postTimeLine(fileData: compressedData,userUniqueId:RMUserDefaultManager.getCurrentUserUniqueId()!,timeLineDescription:textViewDesciption.text!)
            {[weak self] (result: Any?,isSuccess:Bool) in
                
                guard let strongSelf = self else {
                    return
                }
                
                if isSuccess == true {
                    strongSelf.clearAllAndPopNavigationController()
                }
            }
            
            
            
        }
        
        func clearAllAndPopNavigationController() {
            textFieldRecipient.text = ""
            textFieldTitle.text = ""
            textViewDesciption.text = ""
            labelPlaceholderDescription?.isHidden = false
            imageViewAttachment.image = nil
            currentAttachmentType = nil
            currentAttachmentURL = nil
            selectedMessage = nil
            replyToMessage = nil
            tableView.reloadData()
            DispatchQueue.main.async {
//                self.navigationController?.popViewController(animated: true)
//                self.delegate?.modifiedMessage(addMessageTableViewController: self)
            }
        }
        
        
        @objc func barButtonNextAction() {
            if textFieldRecipient.isFirstResponder {
                textFieldRecipient.resignFirstResponder()
                textFieldTitle.becomeFirstResponder()
            } else if textFieldTitle.isFirstResponder {
                
                textFieldTitle.resignFirstResponder()
                textViewDesciption.becomeFirstResponder()
            }
            
        }
        
        @objc func barButtonDoneAction() {
            textViewDesciption.resignFirstResponder()
        }
        
        
        @IBAction func buttonActionForAttachment(_ sender: Any) {
            
            if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
                RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController:self)
                return
            }
            
            RMAttachment.sharedInstance.showPickerActionSheet(showInViewController: self) { [weak self] (attachmentType:RMAttachment.AttachmentType, url:URL?,data:Data?) in
                
                guard let strongSelf = self else {
                    return
                }
                
                if attachmentType == .Cancel {
                    return
                }
                
                //            strongSelf.setAttachment(attachment: object as! Data, attachmentType: attachmentType)
                if attachmentType == .Document {
                    strongSelf.setAttachment(attachment: url!, attachmentType: RMTimeLine.TimeLineAttachmentType.Document)
                }
                
                if attachmentType == .Photo {
                    strongSelf.setAttachment(attachment: data!, attachmentType: RMTimeLine.TimeLineAttachmentType.Photo)
                }
            }
        }
        
        // MARK: - Table view data source
        
        override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            let cell = super.tableView(tableView, cellForRowAt: indexPath)
            if imageViewAttachment.image == nil && cell == attachementCell {
                return 0
            }
            //        if selectedNote != nil && selectedNote!.attachment == nil && cell == attachementCell {
            //            return 0
            //        }
            if  replyToMessage != nil {
                if cell == recipientCell {
                    return 0;
                }
                if cell == titleCell {
                    return 0;
                }
            }
            return super.tableView(tableView, heightForRowAt: indexPath)
            
        }
        
        override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let cell = super.tableView(tableView, cellForRowAt: indexPath)
            if imageViewAttachment.image != nil && cell == attachementCell  && currentAttachmentType == RMTimeLine.TimeLineAttachmentType.Photo {
                let imageViewerVC = RMImageViewerViewController.instance(imageToShow: imageViewAttachment.image!,isGoingToPresent: true)
                let navController = UINavigationController(rootViewController: imageViewerVC)
                self.present(navController, animated: true, completion: nil)
            }
            
            if imageViewAttachment.image != nil && cell == attachementCell && currentAttachmentType == RMTimeLine.TimeLineAttachmentType.Document {
                //            let imageViewerVC = RMImageViewerViewController.instance(imageToShow: imageViewAttachment.image!,isGoingToPresent: true)
                let documentVC = RMDocumentViewController(url: currentAttachmentURL!, isGoingToPresent: true)
                //            let documentVC = RMDocumentViewController(dataToLoad: currentAttachmentData!, isGoingToPresent: true)
                let navController = UINavigationController(rootViewController: documentVC)
                self.present(navController, animated: true, completion: nil)
            }
        }
        
        override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            if imageViewAttachment.image != nil && cell == attachementCell {
                cell.selectionStyle = .default
            } else {
                cell.selectionStyle = .none
            }
        }
        
    }

extension RMAddMessageTableViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldTitle {
            textField.resignFirstResponder()
            textViewDesciption.becomeFirstResponder()
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textFieldRecipient == textField && textField.text?.count == 0 {
            
            assignRecipientAtIndex(index: 0)
        }
        return true
    }
}

    extension RMAddMessageTableViewController : UITextViewDelegate {
        // MARK: - Text view delegate
        
        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText string: String) -> Bool {
            guard let text = textView.text else { return true }
            let newLength = text.count + string.count - range.length
            if(newLength == 0)
            {
                labelPlaceholderDescription?.isHidden = false
            }
            else
            {
                labelPlaceholderDescription?.isHidden = true
            }
            return newLength <= lengthLimitDescription
        }
        
        
        
    }
    
    
    extension RMAddMessageTableViewController: UIGestureRecognizerDelegate {
        func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
            if gestureRecognizer is UITapGestureRecognizer {
                let location = touch.location(in: touch.view)
                if view.frame.contains(location) && imageViewAttachment.image != nil {
                    return false
                }
            }
            return true
        }
}

extension RMAddMessageTableViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
//    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
//
//    }
    
    
    // returns the # of rows in each component..
    @available(iOS 2.0, *)
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if arrayMemebers == nil {
            return 0
        }
        return arrayMemebers!.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let memeberName = arrayMemebers![row].memberName
        return memeberName
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        assignRecipientAtIndex(index: row)
    }
    
    
}
