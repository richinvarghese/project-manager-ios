//
//  RMMilestoneListTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 22/01/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMMilestoneListTableViewController: UITableViewController {

    var arrayToDisplay: Array<RMMilestone>?//FIXME: temporary code
    var searchBar: UISearchBar?
    var arraySearch: [RMMilestone]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(RMMilestoneListTableViewCell.self)
        self.tableView.tableFooterView = UIView()
//        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 130
        tableView.rowHeight = UITableViewAutomaticDimension
//        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 50))
        searchBar!.delegate = self
        searchBar?.placeholder = "Search"
        tableView.tableHeaderView = searchBar
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func showingMilestone(atIndexPath:IndexPath) -> RMMilestone {
        if searchBar?.text?.count == 0 {
            return arrayToDisplay![atIndexPath.row]
        }
        return arraySearch![atIndexPath.row]
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayToDisplay == nil {
            return 0
        }
        if searchBar?.text?.count == 0 {
            return arrayToDisplay!.count
        }
        if arraySearch == nil {
            return 0
        }
        return arraySearch!.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: RMMilestoneListTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let milestone = showingMilestone(atIndexPath: indexPath)
        cell.setMilestone(milestone: milestone)
        //        cell!.textLabel?.text = note.noteTitle
        //        cell!.detailTextLabel?.text = RMGlobalManager.stringFromDate(date:note.notePostedDate!)
//        if indexPath.row == 1 {
//            cell.labelDescription.text = RMDummy.lorem()
//        }
        
        
//        if cell.constraintDescriptionLabelBottomCopy != nil &&  cell.constraintDescriptionLabelBottomCopy!.isActive == true {
//            cell.constraintDescriptionLabelBottomCopy!.isActive = false
//        }
//
//        cell.assignConstraintDescriptionLabelBottom()
        
        //        if cell1.constraintDescriptionLabelBottomCopy!.isActive == false {
        //            cell1.constraintDescriptionLabelBottomCopy =
        //        }
        
//        if cell.viewDummyForAutomaticDimension.frame.size.height >= 115 {
//
//            cell.constraintDescriptionLabelBottomCopy!.isActive = false
//        }
//        else {
//            cell.constraintDescriptionLabelBottomCopy!.isActive = true
//        }
//        cell.layoutIfNeeded()
//        cell.layoutSubviews()
//        cell.labelDescription.layoutIfNeeded()
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let milestone = showingMilestone(atIndexPath: indexPath)
        //        let editNoteTableVC = RMAddNoteTableViewController.instance()
        //        editNoteTableVC.selectedNote = note
        //        editNoteTableVC.delegate = self
        //        editNoteTableVC.selectedProjectUniqueId = selectedProjectUniqueId
        //        self.navigationController?.pushViewController(editNoteTableVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let milestone = showingMilestone(atIndexPath: indexPath)
//        return 132
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 132
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell1: RMMilestoneListTableViewCell = cell as! RMMilestoneListTableViewCell
        RMTheme.cellCornerRadius(cell: cell, borderColor: UIColor.red, backgroundColor: UIColor.clear)//        if cell1.constraintDescriptionLabelBottomCopy != nil &&  cell1.constraintDescriptionLabelBottomCopy!.isActive == true {
//            cell1.constraintDescriptionLabelBottomCopy!.isActive = false
//        }
//
//        cell1.assignConstraintDescriptionLabelBottom()
//
////        if cell1.constraintDescriptionLabelBottomCopy!.isActive == false {
////            cell1.constraintDescriptionLabelBottomCopy =
////        }
//
//        if cell1.viewDummyForAutomaticDimension.frame.size.height >= 115 {
//
//            cell1.constraintDescriptionLabelBottomCopy!.isActive = false
//        }
//        else {
//            cell1.constraintDescriptionLabelBottomCopy!.isActive = true
//        }
    }
    
    //    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    //        let note = showingNote(atIndexPath: indexPath)
    //        if editingStyle == .delete {
    //
    //            RMWebServiceManager.deleteNote(noteUniqueId: note.noteUniqueId!, responseHandler: {[weak self] (result:Any?, isSuccess:Bool) in
    //                RMNoteListTableViewController.arrayToDisplay?.remove(at: indexPath.row)
    //                self?.tableView.reloadData()
    //            })
    //
    //
    //        }
    //    }
    
    deinit {
        print("dealloc=RMMilestoneListTableViewController")
    }
}

extension RMMilestoneListTableViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) // called when text changes (including clear)
    {
        if arrayToDisplay == nil {
            return;
        }
        arraySearch = arrayToDisplay?.filter {
            ($0.milestoneTitle?.contains(searchText))!
        }
        tableView.reloadData()
    }
    
}

