//
//  RMNavigationController.swift
//  Repairman
//
//  Created by Shebin Koshy on 20/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit

struct RMNeededNavigationButtonItem : OptionSet {
    let rawValue: Int
    static let leftMenu  = RMNeededNavigationButtonItem(rawValue: 1 << 0)
    static let rightMenu = RMNeededNavigationButtonItem(rawValue: 1 << 1)
    static let back  = RMNeededNavigationButtonItem(rawValue: 1 << 2)
    
    static let none  = RMNeededNavigationButtonItem(rawValue: 1 << 3)
}


class RMNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        for vc in self.viewControllers {
            setupBarButtonItems(viewController: vc)
        }
        
        self.navigationBar.barTintColor = RMGlobalManager.appBlueColor()
        self.navigationBar.isTranslucent = false
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
//        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.black]
        self.navigationBar.barStyle = UIBarStyle.black
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 20)]
//        self.navigationBar.tintColor = UIColor.black
        self.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension RMNavigationController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        setupBarButtonItems(viewController: viewController)
    }
    
    func setupBarButtonItems(viewController:UIViewController) {
        if viewController is RMNavigationBarButtonItems {
            let proto = viewController as! RMNavigationBarButtonItems
            let action = viewController as! RMNavigationBarButtonActions
            if  proto.neededNavigationBarButtonItems().contains(.leftMenu) {
                print("\(viewController)\(proto.neededNavigationBarButtonItems())")
                let barButtonSideMenu = UIBarButtonItem(image: UIImage(named:"SideMenuButton"), style: .plain, target: viewController, action: #selector(action.barButtonLeftAction))
                viewController.navigationItem.leftBarButtonItem = barButtonSideMenu
            }
            if proto.neededNavigationBarButtonItems().contains(.rightMenu) {
                print("\(viewController)\(proto.neededNavigationBarButtonItems())")
                viewController.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: viewController, action: #selector(action.barButtonRightAction))
            }
            if proto.neededNavigationBarButtonItems().contains(.back) {
                print("\(viewController)\(proto.neededNavigationBarButtonItems())")
                viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: viewController, action: #selector(action.barButtonBackAction))
            }
        }
        
    }
}

protocol RMNavigationBarButtonItems {
    func neededNavigationBarButtonItems() -> RMNeededNavigationButtonItem
}

@objc protocol RMNavigationBarButtonActions {
    @objc optional func barButtonLeftAction()
    @objc optional func barButtonRightAction()
    @objc optional func barButtonBackAction()
}

