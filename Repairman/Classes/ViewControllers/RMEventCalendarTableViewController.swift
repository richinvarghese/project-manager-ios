//
//  RMEventCalendarTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 23/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit
import JTCalendar

class RMEventCalendarTableViewController: UITableViewController {
    
    weak var eventListTableVC : RMEventListViewController?
    
    @IBOutlet weak var cellEncloseEventList: UITableViewCell!
    @IBOutlet weak var calendarContentView: JTHorizontalCalendarView!
    @IBOutlet weak var calendarMenuView: JTCalendarMenuView!
//    @IBOutlet weak var labelSelectedDate: UILabel!
    @IBOutlet weak var buttonAppointment: UIButton!
    @IBOutlet weak var labelTitleAppointmentSummary: UILabel!
    var calendarManager : JTCalendarManager? = JTCalendarManager()
    var dateSelected : Date? {
        didSet{
            if dateSelected == nil {
//                labelSelectedDate.text = nil
            } else {
//                labelSelectedDate.text = RMGlobalManager.onlyDateStringFromDate(date: dateSelected!)
            }
        }
    }
    
    static var arrayEvents : [RMEvent]? //FIXME: temporary code
//    var arrayStringEventDates : [String]?
    
    
    public class func instance() -> RMEventCalendarTableViewController {
        let storyBoard = UIStoryboard(name: "RMNoteAndEvent", bundle: nil)
        let eventCalendarTableVC = storyBoard.instantiateViewController(withIdentifier: "RMEventCalendarTableViewController") as! RMEventCalendarTableViewController
        return eventCalendarTableVC
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = RMStrings.EVENT_STRING()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        calendarManager?.settings.zeroPaddedDayFormat = false
        var calendar =  calendarManager?.dateHelper.calendar()
        calendar?.timeZone = TimeZone(abbreviation: "UTC")!
        calendarMenuView.backgroundColor = UIColor(red: 107, green: 154, blue: 164)
        calendarContentView.backgroundColor = UIColor(red: 147, green: 206, blue: 225)
        calendarMenuView.backgroundColor = RMGlobalManager.appBlueColor()
        calendarContentView.backgroundColor = RMGlobalManager.appBlueColor()
//        calendarMenuView.lab
        self.tableView.tableFooterView = UIView()
        calendarManager!.delegate = self as JTCalendarDelegate
        calendarManager!.menuView = calendarMenuView
        calendarManager!.contentView = calendarContentView
        calendarManager!.setDate(Date())
        if dateSelected == nil {
            let dateWithoutTime = RMGlobalManager.webServiceOnlyStringDateFromDate(date: Date())
            dateSelected = RMGlobalManager.dateFromWebServiceString(stringOnlyDate: dateWithoutTime)
        }
        self.tableView.allowsSelection = false
//        self.tableView.isScrollEnabled = false
        
//        let barButtonItemAdd = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(buttonActionForAdd))
//        self.navigationItem.rightBarButtonItem = barButtonItemAdd
//        stringDateFromEvent()
        updateEventList()
        buttonAppointment.backgroundColor = RMGlobalManager.appBlueColor()
        buttonAppointment.setTitleColor(UIColor.white, for: .normal)
        labelTitleAppointmentSummary.textColor = RMGlobalManager.appBlueColor()
        
        calendarMenuView.subviews.forEach{
            ($0 as? UILabel)?.textColor = UIColor.white
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadCalendarView() {
//        stringDateFromEvent()
        updateEventList()
        self.tableView.reloadData()
        calendarManager?.reload()
        self.tableView.reloadData()
//        calendarManager.
        
    }
    
//    func stringDateFromEvent() {
//        arrayStringEventDates = [String]()
//        var arrayEvents = eventsArray()
//
//        arrayEvents = arrayEvents.filter {
//            $0.eventStartDate != nil && $0.eventEndDate != nil
//        }
//
//        let result = arrayEvents.map({
//            $0.eventStartDate!//FIXME:No idear
//        })
//
//        for date in result {
//            let stringDate = RMGlobalManager.onlyDateStringFromDate(date: date)
//            arrayStringEventDates?.append(stringDate)
//        }
//        calendarManager?.reload()
//    }
    
    @objc func buttonActionForAdd() {
       showAddOrEditEventTableVC(event: nil)
    }
    
    @IBAction func buttonActionForAppointment() {
        showAddOrEditEventTableVC(event: nil)
    }
    
    func showAddOrEditEventTableVC(event:RMEvent?) {
        let newNoteTableVC = RMAddEventTableViewController.instance()
        newNoteTableVC.delegate = self
        newNoteTableVC.selectedEvent = event
        self.navigationController?.pushViewController(newNoteTableVC, animated: true)
    }
    
    func dateBeforeOrAfterFromToday(numberOfDays :Int?) -> Date {
        //FIXME: temporary code
        let resultDate = Calendar.current.date(byAdding: .day, value: numberOfDays!, to: Date())!
        return resultDate
    }
    
    func eventsArray() -> [RMEvent] {//FIXME: temporary code
//        var arraEvent = [RMEvent]()
        if RMEventCalendarTableViewController.arrayEvents != nil {
            return RMEventCalendarTableViewController.arrayEvents!
        }
        return []
//        RMEventCalendarTableViewController.arrayEvents = [RMEvent]()
//        let event = RMEvent()
//        event.eventTitle = "b'day"
//        event.eventDescription = "lets party"
//        eventStartDate!//FIXME:No idear
//        event.eventDate = dateBeforeOrAfterFromToday(numberOfDays: 7)
//        event.eventStringDate = RMGlobalManager.onlyDateStringFromDate(date: event.eventDate!)
//        RMEventCalendarTableViewController.arrayEvents!.append(event)
//        return RMEventCalendarTableViewController.arrayEvents!
    }
    
    
    func eventsForDay(date:Date) -> [RMEvent]? {
        
        var arOfEvents = Array<RMEvent>()
        let arrayEvents = eventsArray()
        
        let arResult = arrayEvents.filter{ $0.isNeedToConsider(calendarDate: date)}
        if arResult.count == 0 {
            return nil
        }
        return arResult
        
        
        let componenentCalendarDate = Calendar.current.dateComponents([.weekday,.month,.year,.day], from: date)
        
        let repeatingWeekDayEvents = arrayEvents.filter{ $0.eventRepeatEvery != 0 && $0.repeatType() == .week && $0.weekDay() == componenentCalendarDate.weekday}
        
        let repeatingMonthEvents = arrayEvents.filter{ $0.eventRepeatEvery != 0 && $0.repeatType() == .month && $0.month() == componenentCalendarDate.month && $0.day() == componenentCalendarDate.day}
        
        
        
        //let repeatingMonthEvents = arrayEvents.filter{ $0.eventRepeatEvery != 0 && $0.repeatType() == .month && $0.month() == componenentCalendarDate.month && $0.day() == componenentCalendarDate.day}
        
        let repeatingYearEvents = arrayEvents.filter{ $0.eventRepeatEvery != 0 && $0.repeatType() == .year && $0.year() == componenentCalendarDate.year && $0.month() == componenentCalendarDate.month && $0.day() == componenentCalendarDate.day}
        
        
        arOfEvents.append(contentsOf: repeatingWeekDayEvents)
        arOfEvents.append(contentsOf: repeatingMonthEvents)
        arOfEvents.append(contentsOf: repeatingYearEvents)
        
        
        let stringDate = RMGlobalManager.webServiceOnlyStringDateFromDate(date: date)
//        let arrayEvents = eventsArray()
        let filteredArray = arrayEvents.filter() { $0.eventStartingDateOnlyString() == stringDate }//FIXME: no idaer
//        if filteredArray.count == 0 {
//            return nil
//        }
        arOfEvents.append(contentsOf: filteredArray)
        let setOfEvents = Set(arOfEvents)
        
        
        if setOfEvents.count == 0 {
            return nil
        }
        
        return Array(setOfEvents)
        
//        if repeatingWeekDayEvents.count == 0 {
//            return nil
//        }
        
//        repeatingEvents.
//
//        return filteredArray
//
//
//        let componenentCurrentDate = Calendar.current.dateComponents([.weekday,.month,.year], from: Date())
//
//
//        let componenentEventDate = Calendar.current.dateComponents([.weekday,.month,.year], from: date)
//
//        if componenentCurrentDate.weekday == componenentEventDate.weekday {
//            return
//        }
        
        
        
        /*
        if arrayStringEventDates == nil {
            return nil
        }
        
        if arrayStringEventDates!.contains(stringDate) == true {
            let index = arrayStringEventDates!.index(of: stringDate)
            let arrayEvents = eventsArray()
            
            if arrayEvents.count <= index! {
                return nil
            }
            return arrayEvents[index!]
        } else {
            return nil
        }
        
        //below code performance is very poor
        let arrayEvents = eventsArray()
        
        let result = arrayEvents.map({
            $0.eventDate!
        })*/
        
        
        /*let calendarSys = Calendar.current.dateComponents([.day,.month], from: date)*/
        /*
        for dates in result {
            /*let calendarSys1 = Calendar.current.dateComponents([.day,.month], from: dates)*/
            if calendarManager!.dateHelper.date(date, isTheSameMonthThan: dates) == true && calendarManager!.dateHelper.date(date, isTheSameDayThan: dates) == true{
                let index = result.index(of: dates)
                return arrayEvents[index!]
            }
            
            /*if calendarSys1.day == calendarSys.day && calendarSys1.month == calendarSys.month {
                let index = result.index(of: dates)
                return arrayEvents[index!]
            }*/
        }
        return nil*/
    }
    
    
    func updateEventList(){
        if dateSelected == nil {
            return
        }
        let events = eventsForDay(date: dateSelected!)
        if events == nil {
            eventListTableVC!.showEvents(events: nil)
        } else {
            eventListTableVC?.showEvents(events: events)
        }
    }



    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
//        let cell = tableView.cellForRow(at: indexPath)
        if (cell == cellEncloseEventList && dateSelected == nil) || (cell == cellEncloseEventList && dateSelected != nil && eventsForDay(date: dateSelected!) == nil) {
            return 0
        }
        if dateSelected != nil {
            let events = eventsForDay(date: dateSelected!)
            if cell == cellEncloseEventList && events != nil {
                return CGFloat(events!.count * 44)
            }
        }
        return super.tableView(tableView, heightForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsetsMake(0, 16, 0, 16)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "EventListSequeId" {
            eventListTableVC = segue.destination as? RMEventListViewController
            eventListTableVC?.delegate = self
        }
    }
    
    deinit {
        print("deinit\(self)")
    }
   

}


extension RMEventCalendarTableViewController: JTCalendarDelegate{
    //    /*!
    //     * Provide a UIView, used as page for the menuView.
    //     * Return an instance of `UILabel` by default.
    //     */
    //    @available(iOS 2.0, *)
    //    public func calendarBuildMenuItemView(_ calendar: JTCalendarManager!) -> UIView!{
    //        return UILabel()
    ////        let v = UIView(frame:CGRect(x:0,y:0,width:10,height:10))
    ////        v.backgroundColor = UIColor.red
    ////        return v
    //    }
    //
    //
    //    /*!
    //     * Used to customize the menuItemView.
    //     * Set text attribute to the name of the month by default.
    //     */
    //    @available(iOS 2.0, *)
    public func calendar(_ calendar: JTCalendarManager!, prepareMenuItemView menuItemView: UIView!, date: Date!){
        //        print("prepareMenuItemView")
        let calendarSys = Calendar.current.dateComponents([.year,.month], from: date)
        var currentMonthIndex = calendarSys.month
        
        var dateFormatter : DateFormatter?
        if dateFormatter == nil {
            dateFormatter = DateFormatter()
            dateFormatter?.timeZone = calendarSys.timeZone
        }
        
        while currentMonthIndex! <= 0 {
            currentMonthIndex! += 12
        }
        
        let monthText = (dateFormatter!.standaloneMonthSymbols[currentMonthIndex! - 1] as String).capitalized
        let label = menuItemView as! UILabel
        //        label.frame = CGRect(x:0,y:0,width:150,height:70)
        label.text = String(format: "%@ %d",monthText,calendarSys.year!)
        label.textColor = UIColor.white
        //        NSCalendar *calendar = jt_calendar.calendarAppearance.calendar;
        //        NSDateComponents *comps = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:date];
        //        NSInteger currentMonthIndex = comps.month;
        //
        //        static NSDateFormatter *dateFormatter;
        //        if(!dateFormatter){
        //            dateFormatter = [NSDateFormatter new];
        //            dateFormatter.timeZone = jt_calendar.calendarAppearance.calendar.timeZone;
        //        }
        //
        //        while(currentMonthIndex <= 0){
        //            currentMonthIndex += 12;
        //        }
        //
        //        NSString *monthText = [[dateFormatter standaloneMonthSymbols][currentMonthIndex - 1] capitalizedString];
        //
        //        return [NSString stringWithFormat:@"%ld\n%@", comps.year, monthText];
        
        
    }
    
    
    func calendarBuildWeekDayView(_ calendar: JTCalendarManager!) -> (UIView & JTCalendarWeekDay)! {
        let v = JTCalendarWeekDayView()
        v.dayViews.forEach{($0 as? UILabel)?.textColor = UIColor.colorWithHexString(hex: "5a5d5e")}
        v.dayViews.forEach{($0 as? UILabel)?.textColor = UIColor.white}
        v.backgroundColor = UIColor(red: 231, green: 242, blue: 236)
        v.backgroundColor = UIColor(red: 173, green: 210, blue: 197)
        v.backgroundColor = RMGlobalManager.appBlueColor()
        return v
    }
    
    //
    //
    //    // Content view
    //
    //    /*!
    //     * Indicate if the calendar can go to this date.
    //     * Return `YES` by default.
    //     */
    //    public func calendar(_ calendar: JTCalendarManager!, canDisplayPageWith date: Date!) -> Bool{
    //        return true
    //    }
    //
    //
    //    /*!
    //     * Provide the date for the previous page.
    //     * Return 1 month before the current date by default.
    //     */
    ////    public func calendar(_ calendar: JTCalendarManager!, dateForPreviousPageWithCurrentDate currentDate: Date!) -> Date!{
    ////        return
    ////    }
    //
    //
    //    /*!
    //     * Provide the date for the next page.
    //     * Return 1 month after the current date by default.
    //     */
    ////    public func calendar(_ calendar: JTCalendarManager!, dateForNextPageWithCurrentDate currentDate: Date!) -> Date!{
    ////
    ////    }
    //
    //
    //    /*!
    //     * Indicate the previous page became the current page.
    //     */
    //    public func calendarDidLoadPreviousPage(_ calendar: JTCalendarManager!){
    //        print("calendarDidLoadPreviousPage")
    //    }
    //
    //
    //    /*!
    //     * Indicate the next page became the current page.
    //     */
    //    public func calendarDidLoadNextPage(_ calendar: JTCalendarManager!){
    //        print("calendarDidLoadNextPage")
    //    }
    //
    //
    //    /*!
    //     * Provide a view conforming to `JTCalendarPage` protocol, used as page for the contentView.
    //     * Return an instance of `JTCalendarPageView` by default.
    //     */
    //    @available(iOS 2.0, *)
    //    public func calendarBuildPageView(_ calendar: JTCalendarManager!) -> UIView!{
    //        let v = UIView(frame:CGRect(x:0,y:0,width:10,height:10))
    //        v.backgroundColor = UIColor.yellow
    //        return v
    //    }
    //
    //
    //    // Page view
    //
    //    /*!
    //     * Provide a view conforming to `JTCalendarWeekDay` protocol.
    //     * Return an instance of `JTCalendarWeekDayView` by default.
    //     */
    //    @available(iOS 2.0, *)
    //    public func calendarBuildWeekDayView(_ calendar: JTCalendarManager!) -> UIView!{
    //        let v = UIView(frame:CGRect(x:0,y:0,width:10,height:10))
    //        v.backgroundColor = UIColor.green
    //        return v
    //    }
    //
    //
    //    /*!
    //     * Provide a view conforming to `JTCalendarWeek` protocol.
    //     * Return an instance of `JTCalendarWeekView` by default.
    //     */
    //    @available(iOS 2.0, *)
    //    public func calendarBuildWeekView(_ calendar: JTCalendarManager!) -> UIView!{
    //        let v = UIView(frame:CGRect(x:0,y:0,width:10,height:10))
    //        v.backgroundColor = UIColor.brown
    //        return v
    //    }
    //
    //
    //    // Week view
    //
    //    /*!
    //     * Provide a view conforming to `JTCalendarDay` protocol.
    //     * Return an instance of `JTCalendarDayView` by default.
    //     */
    //    @available(iOS 2.0, *)
    //    public func calendarBuildDayView(_ calendar: JTCalendarManager!) -> UIView!{
    //        let v = UIView(frame:CGRect(x:0,y:0,width:10,height:10))
    //        v.backgroundColor = UIColor.blue
    //        return v
    //    }
    //
    //
    //    // Day view
    //
    //    /*!
    //     * Used to customize the dayView.
    //     */
    //    @available(iOS 2.0, *)
    //    func calendar(_ calendar: JTCalendarManager!, prepareDayView dayView: UIView!) {
    //        <#code#>
    //    }
    func calendar(_ calendar: JTCalendarManager!, prepareDayView dayView: (UIView & JTCalendarDay)!) {
//        <#code#>
//    }
//    public func calendar(_ calendar: JTCalendarManager!, prepareDayView dayView: UIView!){
        //        print("prepareDayView")
        let dayView = dayView as! JTCalendarDayView
        
        dayView.isHidden = false
        
        // Test if the dayView is from another month than the page
        // Use only in month mode for indicate the day of the previous or next month
        //        dayView.isFromAnotherMonth == true
        if dayView.isFromAnotherMonth == true {
            dayView.isHidden = true
        }
            // Today
            
        else if calendarManager!.dateHelper.date(Date(), isTheSameDayThan: dayView.date) == true {
            dayView.circleView.isHidden = false
            dayView.circleView.backgroundColor = UIColor(red: 44, green: 89, blue: 119)
            dayView.dotView.backgroundColor = UIColor.lightGray
            dayView.textLabel.textColor = UIColor.white
        }
            // Selected date
        else if dateSelected != nil && calendarManager!.dateHelper.date(dateSelected, isTheSameDayThan: dayView.date) == true {
            dayView.circleView.isHidden = false
            dayView.circleView.backgroundColor = UIColor(red: 209, green: 45, blue: 0)
            dayView.dotView.backgroundColor = UIColor.white
            dayView.textLabel.textColor = UIColor.white
        }
            // Another day of the current month
        else{
            dayView.circleView.isHidden = true;
            dayView.dotView.backgroundColor = UIColor(red: 209, green: 45, blue: 0)
//            dayView.textLabel.textColor = UIColor.brown
            dayView.textLabel.textColor = UIColor(red: 44, green: 89, blue: 119)
            dayView.textLabel.textColor = UIColor.white
        }
        
        // Your method to test if a date have an event for example
        if self.eventsForDay(date:dayView.date) != nil {
            dayView.dotView.isHidden = false;
        }
        else{
            dayView.dotView.isHidden = true;
        }
        
    }
    //
    //
    //    /*!
    //     * Indicate the dayView just get touched.
    //     */
    //    @available(iOS 2.0, *)
    func calendar(_ calendar: JTCalendarManager!, didTouchDayView dayView: (UIView & JTCalendarDay)!) {
//        <#code#>
//    }
//    public func calendar(_ calendar: JTCalendarManager!, didTouchDayView dayView: UIView!){
        //        print("didTouchDayView")
        let dayView = dayView as! JTCalendarDayView
        dateSelected = dayView.date
        
        // Animation for the circleView
        dayView.circleView.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
        //        dayView.circleView.transform = CGAffineTransformIdentity.scaledBy(x: 0.1, y: 0.1)
        UIView.transition(with: dayView, duration: 0.0, options: UIViewAnimationOptions(rawValue: 0), animations: {
            dayView.circleView.transform = .identity
        }) { (anim:Bool) in
            self.calendarManager!.reload()
        }
        //        [UIView transitionWithView:dayView
        //            duration:.3
        //            options:0
        //            animations:^{
        //            dayView.circleView.transform = CGAffineTransformIdentity;
        //            [_calendarManager reload];
        //            } completion:nil];
        
        // Load the previous or next page if touch a day from another month
        if calendarManager!.dateHelper.date(calendarContentView.date, isTheSameMonthThan: dayView.date) == false {
            if calendarContentView.date.compare(dayView.date) == .orderedAscending {
                calendarContentView.loadNextPageWithAnimation()
                //            } else if calendarContentView.date.compare(dayView.date) == .orderedSame {
                //                print("sameeee")
            } else {
                calendarContentView.loadPreviousPageWithAnimation()
            }
        }
        
        //        if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        //            if([_calendarContentView.date compare:dayView.date] == NSOrderedAscending){
        //                [_calendarContentView loadNextPageWithAnimation];
        //            }
        //            else{
        //                [_calendarContentView loadPreviousPageWithAnimation];
        //            }
        //        }
        
        updateEventList()
//        self.tableView.reloadRows(at: IndexPath(), with: <#T##UITableViewRowAnimation#>)
        self.tableView.reloadData()
    }
    //
}


extension RMEventCalendarTableViewController:RMEventListDelegate {
    func didSelected(eventListVC: RMEventListViewController, event: RMEvent) {
        showAddOrEditEventTableVC(event:event)
    }
    
    func didDeleted(eventListVC:RMEventListViewController, event:RMEvent) {
        let index = eventsArray().index(of: event)
////        eventsArray().remove(at: index)
//        print("removedEvn\(index!)")
        RMEventCalendarTableViewController.arrayEvents?.remove(at: index!)
//        arrayStringEventDates?.remove(at: index!)
        calendarManager!.reload()
        self.tableView.reloadData()
        
    }
}

extension RMEventCalendarTableViewController: RMAddEventDelegate {
    func addedOrEditedEvent(addEventTableVC:RMAddEventTableViewController){
        eventListTableVC!.fetchEvents()
//        stringDateFromEvent()
//        self.tableView.reloadData()
//        updateEventList()
    }
}


extension RMEventCalendarTableViewController: RMNavigationBarButtonItems {
    func neededNavigationBarButtonItems() -> RMNeededNavigationButtonItem {
        
        if navigationController?.viewControllers.count == 1 {
            return .leftMenu
        }
        
        return .none
    }
}

extension RMEventCalendarTableViewController: RMNavigationBarButtonActions {
    func barButtonLeftAction() {
        RMSideMenuAndRoot.showLeftMenu()
    }
}
