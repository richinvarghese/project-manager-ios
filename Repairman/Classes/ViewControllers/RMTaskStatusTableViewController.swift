//
//  RMTaskStatusTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 06/01/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

protocol RMTaskStatusTableViewControllerDelegate: class {
    func taskChanged(taskStatusTableVC:RMTaskStatusTableViewController, taskStatusChangedTo:RMTaskStatus, forTask:RMTask)
}

class RMTaskStatusTableViewController: UITableViewController {
    
    var selectedTask:RMTask?
    var arrayTaskStatus: [RMTaskStatus]?
    weak var delegate:RMTaskStatusTableViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        title = RMStrings.STATUS_STRING()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        tableView.register(UITableViewCell.self)
        tableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func showingTaskStatus(atIndexPath:IndexPath) -> RMTaskStatus {
        //        let array = displayingArray()
        return arrayTaskStatus![atIndexPath.row]
    }
    
//    func enumToArray() {
////        var taskElements = String[]()
//        for index in 0...RMTask.TaskStatusType.co {
//           print(index)
////            taskElements.append(self.getValueFromSuitAtIndex(indexOfElement: index))
//        }
//        // suitElements printed is: [Spades, Hearts, Diamonds, Clubs]
////        println(suitElements)
//    }
    

    // MARK: - Table view data source

//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 0
//    }
    


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if arrayTaskStatus == nil {
            return 0
        }
        return arrayTaskStatus!.count
        
//        return RMTask.arrayTaskStatusTypes().count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
//        let statusString = RMTask.arrayTaskStatusTypes()[indexPath.row]
        let taskStatus = showingTaskStatus(atIndexPath: indexPath)
        cell.textLabel?.text = taskStatus.taskStatusTitle
        if selectedTask != nil && selectedTask!.taskStatus?.lowercased() == taskStatus.taskStatusTitle?.lowercased() {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let statusString = RMTask.arrayTaskStatusTypes()[indexPath.row]
//        let statusType = RMTask.TaskStatusType(rawValue:indexPath.row)
        
        let taskStatus = showingTaskStatus(atIndexPath: indexPath)
        
        selectedTask!.taskStatus = taskStatus.taskStatusTitle
//        selectedTask!.taskStatusType = statusType
        

        RMWebServiceManager.taskStatusUpdate(taskUniqueId: selectedTask!.taskUniqueId!, statusUniqueId: taskStatus.taskStatusUniqueId!) {[weak self] (result:Any?, isSuccess:Bool) in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.delegate?.taskChanged(taskStatusTableVC: strongSelf, taskStatusChangedTo: taskStatus,
                forTask: strongSelf.selectedTask!)
            strongSelf.tableView.reloadData()
            strongSelf.navigationController?.popViewController(animated: true)
        }
        
        
        
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
