//
//  RMLeaveListTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 24/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMLeaveListTableViewController: UITableViewController {

    var arrayToDisplay: Array<RMLeave>?//FIXME: temporary code
    //    var delegate: RMFileListTableViewControllerDelegate?
    //    var projectUniqueId:String?
    //    static let reuseIdentifierNoteCell = "RMNoteCell"
    //
    //    var selectedProjectUniqueId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
                self.title = RMStrings.LEAVE_STRING()
        //        self.tableView.register(UITableViewCell.self)
        //        self.tableView.register(RMNoteListTableViewCell.self)
        //        let nib = UINib(nibName: "RMSubtitleTableViewCell", bundle: nil)
        //        self.tableView.register(nib, forCellReuseIdentifier: RMNoteListTableViewController.reuseIdentifierNoteCell)
        self.tableView.register(RMLeaveListTableViewCell.self)
        self.tableView.tableFooterView = UIView()
        self.tableView.allowsSelection = false
        self.tableView.estimatedRowHeight = 180
        //        fetchNote()
        //        self.tableView.estimatedRowHeight = 200
        //        self.tableView.rowHeight = UITableViewAutomaticDimension
        let barButtonItemAdd = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(buttonActionForAdd))
        navigationItem.rightBarButtonItem = barButtonItemAdd
        fetchLeave()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//    }
    
    
        func fetchLeave() {
    
            //        RMWebServiceManager.saveNote(title: "test123", description: "test4546")
            //        return
            arrayToDisplay = nil
            self.tableView.reloadData()
    
//            let currentUserUniqueId = "8"//FIXME: hardcoded
           let currentUserUniqueId = RMUserDefaultManager.getCurrentUserUniqueId()!
            RMWebServiceManager.fetchLeaves(currentUserUniqueId:currentUserUniqueId) {[weak self] (result:Any?, isSuccess:Bool) in
                if isSuccess == false {
                    return
                }
                let leaveArray = result as! [RMLeave]
                
                self?.arrayToDisplay = leaveArray
                self?.tableView.reloadData()
            }
        }
    
    
        @objc func buttonActionForAdd() {
            let newLeaveTableVC = RMAddLeaveTableViewController.instance()
            newLeaveTableVC.delegate = self
//            newLeaveTableVC.selectedProjectUniqueId = selectedProjectUniqueId
            self.navigationController?.pushViewController(newLeaveTableVC, animated: true)
        }
    
    //    func displayingArray() -> [RMNote] {
    //        if RMNoteListTableViewController.arrayToDisplay == nil {
    //            return []
    //        }
    //
    //        return RMNoteListTableViewController.arrayToDisplay!
    //
    //    }
    
    func showingLeave(atIndexPath:IndexPath) -> RMLeave {
        //        let array = displayingArray()
        return arrayToDisplay![atIndexPath.row]
    }
    
    //    func barButtonItemAdd(navigationItem:UINavigationItem) {
    //        let barButtonItemAdd = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(buttonActionForAdd))
    //        navigationItem.rightBarButtonItem = barButtonItemAdd
    //    }
    //
    //    @objc func buttonActionForAdd() {
    //        let addTimeLineTableVC = RMAddTimeLineTableViewController.instance(selectedTimeLine: nil, replyToTimeLine: nil)
    //        addTimeLineTableVC.delegate = self
    //        addTimeLineTableVC.isForFileUpload = true
    //        self.navigationController?.pushViewController(addTimeLineTableVC, animated: true)
    //    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayToDisplay == nil {
            return 0
        }
        return arrayToDisplay!.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let cell: RMLeaveListTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        //        if cell == nil {
        //            cell = UITableViewCell(style: UITableViewCellStyle.value2, reuseIdentifier: "UITableViewCell")
        //        }
        cell.delegate = self
        let leave = showingLeave(atIndexPath: indexPath)
        cell.setleave(leave: leave)
        
        //        cell!.textLabel?.text = note.noteTitle
        //        cell!.detailTextLabel?.text = RMGlobalManager.stringFromDate(date:note.notePostedDate!)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let leave = showingLeave(atIndexPath: indexPath)
        
        //        if projectFile.projectFileImageUrl == nil {
        //            return
        //        }
        //
        //        let documentVC = RMDocumentViewController(url:projectFile.projectFileImageUrl!, isGoingToPresent:true)
        //        //        documentVC.url
        //        //        let imageViewerVC = RMImageViewerViewController.instance(imageURLToShow: URL(string:projectFile.projectFileImageUrl!)!, isGoingToPresent: true)
        //        let navController = UINavigationController(rootViewController: documentVC)
        //        self.present(navController, animated: true, completion: nil)
        //        let editNoteTableVC = RMAddNoteTableViewController.instance()
        //        editNoteTableVC.selectedNote = note
        //        editNoteTableVC.delegate = self
        //        editNoteTableVC.selectedProjectUniqueId = selectedProjectUniqueId
        //        self.navigationController?.pushViewController(editNoteTableVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        let projectFile = showingProjectFile(atIndexPath: indexPath)
//        return 192
                return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
        RMTheme.cellCornerRadius(cell: cell, borderColor: RMGlobalManager.appBlueColor(), backgroundColor: UIColor.clear)
    }
    
    deinit {
        print("dealloc=RMLeaveListTableViewController")
    }
    
}

extension RMLeaveListTableViewController : RMLeaveListTableViewCellDelegate {
    func buttonActionCancel(cell: RMLeaveListTableViewCell) {
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController: self)
            return
        }
        
        
        let indexPath =  tableView.indexPath(for: cell)
        if indexPath == nil {
            return
        }
        let leave = showingLeave(atIndexPath: indexPath!)
        RMWebServiceManager.cancelLeave(currentUserUniqueId: RMUserDefaultManager.getCurrentUserUniqueId()!, leaveUniqueId: leave.uniqueId!, isCanceled: true) { [weak self]  (result, isSuccess) in
            if isSuccess == false {
                return;
            }
            let dict = result as! [String:Any]
            let stringStatus = dict["status"] as? String
            if stringStatus  == "error" {
                return;
            }
            self?.fetchLeave()
//            leave.leaveIsCanceled = true
        }
//        let taskStatusTableVC = RMTaskStatusTableViewController(style: .plain)
//        taskStatusTableVC.selectedTask = task
//        taskStatusTableVC.delegate = self
//        taskStatusTableVC.arrayTaskStatus = arrayTaskStatus
//        self.navigationController?.pushViewController(taskStatusTableVC, animated: true)
    }
}

extension RMLeaveListTableViewController : RMAddLeaveTableViewControllerDelegate {
    
    func modifiedLeave(addLeaveTableViewController:RMAddLeaveTableViewController) {
        fetchLeave()
    }
    
}

extension RMLeaveListTableViewController: RMNavigationBarButtonItems {
    func neededNavigationBarButtonItems() -> RMNeededNavigationButtonItem {
        
        if navigationController?.viewControllers.count == 1 {
            return .leftMenu
        }
        
        return .none
    }
}

extension RMLeaveListTableViewController: RMNavigationBarButtonActions {
    func barButtonLeftAction() {
        RMSideMenuAndRoot.showLeftMenu()
    }
}
