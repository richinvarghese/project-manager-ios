//
//  RMDocumentViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 16/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit
import WebKit

var myContext = 0

class RMDocumentViewController: UIViewController {
    
    var progressView: UIProgressView!
    private var isGoingToPresent:Bool?
    private var stringUrl : String?
    private var url : URL?
    private var dataToLoad : Data?
    convenience init(url:String, isGoingToPresent:Bool?) {
      self.init()
        self.stringUrl = url
        self.isGoingToPresent = isGoingToPresent
    }
    convenience init(url:URL, isGoingToPresent:Bool?) {
        self.init()
        self.url = url
        self.isGoingToPresent = isGoingToPresent
    }
    convenience init(dataToLoad:Data, isGoingToPresent:Bool?) {
        self.init()
        self.dataToLoad = dataToLoad
        self.isGoingToPresent = isGoingToPresent
    }
    
//    init() {
//        super.init()
//    }
    
//    init() {
//        self.init()
//    }
    
    private override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    internal required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        let webView = WKWebView()
//        webView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
//        self.view.addSubview(webView)
//        let urlRequest = URLRequest(url: self.url!)
//        webView.load(urlRequest)
//
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
    
    
    
    var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: view.bounds, configuration: webConfiguration)
//        webView.uiDelegate = self
        //        webView.navigationDelegate = self
        
        view.addSubview(webView)
//        view.translatesAutoresizingMaskIntoConstraints = false
//        webView.translatesAutoresizingMaskIntoConstraints = false
//        addConstraintWithSuperView()
        
        
        if isGoingToPresent == true {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: RMStrings.DISMISS_STRING(), style: .done, target: self, action: #selector(buttonActionForDismiss))
        }
        
        
        // // add observer for key path
        webView.addObserver(self, forKeyPath: "title", options: .new, context: &myContext)
        webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: &myContext)
        
        
        //add progresbar to navigation bar
        progressView = UIProgressView(progressViewStyle: .default)
        progressView.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
//        progressView.tintColor = #colorLiteral(red: 0.6576176882, green: 0.7789518833, blue: 0.2271372974, alpha: 1)
        navigationController?.navigationBar.addSubview(progressView)
        let navigationBarBounds = self.navigationController?.navigationBar.bounds
        progressView.frame = CGRect(x: 0, y: navigationBarBounds!.size.height - 2, width: navigationBarBounds!.size.width, height: 2)
        
        if dataToLoad != nil {
            if #available(iOS 9.0, *) {
//                UTF8.en
                webView.load(dataToLoad!, mimeType: "application/vnd.openxmlformats-officedocument.wordprocessingml.document", characterEncodingName: "utf-8", baseURL: URL(string: "http://localhost")!)
            } else {
                // Fallback on earlier versions
            }
            return;
        }
        
        
        if url != nil {
            let urlReq = URLRequest(url: url!)
            webView.load(urlReq)
            return;
        }
        
        let myURL = URL(string: stringUrl!)!
//        let myURL = URL(string: stringUrl!)
        if myURL != nil {
            
//            if #available(iOS 9.0, *) {
//                webView.loadFileURL(myURL, allowingReadAccessTo: myURL)
//            } else {
                // Fallback on earlier versions
                let urlReq = URLRequest(url: myURL)
                webView.load(urlReq)
//            }
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func addConstraintWithSuperView(){
        var horizontalpadding = 0
        var verticalPadding = 20
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436 {
            print("iPhone X")
            horizontalpadding = 20
            verticalPadding = 40
        } else {
            print("not-iX")
        }
        
        NSLayoutConstraint(item: webView, attribute: .top, relatedBy: .equal, toItem: webView.superview, attribute: .top, multiplier: 1, constant: CGFloat(verticalPadding)).isActive = true
        NSLayoutConstraint(item: webView, attribute: .bottom, relatedBy: .equal, toItem: webView.superview, attribute: .bottom, multiplier: 1, constant: CGFloat(-verticalPadding)).isActive = true
        NSLayoutConstraint(item: webView, attribute: .leading, relatedBy: .equal, toItem: webView.superview, attribute: .leading, multiplier: 1, constant: CGFloat(horizontalpadding)).isActive = true
        NSLayoutConstraint(item: webView, attribute: .trailing, relatedBy: .equal, toItem: webView.superview, attribute: .trailing, multiplier: 1, constant: CGFloat(-horizontalpadding)).isActive = true
    }


    
    
    @objc func buttonActionForDismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    //observer
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        guard let change = change else { return }
        if context != &myContext {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        
        if keyPath == "title" {
            if let title = change[NSKeyValueChangeKey.newKey] as? String {
                self.navigationItem.title = title
            }
            return
        }
        if keyPath == "estimatedProgress" {
            if let progress = (change[NSKeyValueChangeKey.newKey] as AnyObject).floatValue {
                progressView.progress = progress;
            }
            return
        }
    }
    
    
    //deinit
    deinit {
        //remove all observers
        webView.removeObserver(self, forKeyPath: "title")
        webView.removeObserver(self, forKeyPath: "estimatedProgress")
        //remove progress bar from navigation bar
        progressView.removeFromSuperview()
    }
    
}


extension RMDocumentViewController: WKNavigationDelegate {
    
    //WKNavigationDelegate
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        if let url = navigationAction.request.url {
            if url.absoluteString.contains("/something") {
                // if url contains something; take user to native view controller
//                Routing.showAnotherVC(fromVC: self)
                decisionHandler(.cancel)
            } else if url.absoluteString.contains("done") {
                //in case you want to stop user going back
//                hideBackButton()
//                addDoneButton()
                decisionHandler(.allow)
            } else if url.absoluteString.contains("AuthError") {
                //in case of erros, show native allerts
            }
            else{
                decisionHandler(.allow)
            }
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        progressView.isHidden = true
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        progressView.isHidden = false
    }
}
