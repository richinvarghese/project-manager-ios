//
//  RMLoginTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 13/01/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMLoginTableViewController: UITableViewController {
    
    public class func instance() -> RMLoginTableViewController {
        let rootStoryBoard = UIStoryboard(name: "RMLogin", bundle: nil)
        let loginTableVC = rootStoryBoard.instantiateViewController(withIdentifier: "RMLoginTableViewController") as! RMLoginTableViewController
        //        newTimeLineTableVC.selectedTimeLine = selectedTimeLine
        //        newTimeLineTableVC.replyToTimeLine = replyToTimeLine
        return loginTableVC
    }

    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var buttonLogin: UIButton!
    @IBOutlet weak var buttonForgetPassword: UIButton!
    @IBOutlet weak var buttonSignup: UIButton!
    static let lengthLimitEmail = 30
//    let lengthLimitPassword = 30
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.showsVerticalScrollIndicator = false
        self.navigationController?.navigationBar.isHidden = true
        
        textFieldEmail.returnKeyType = .next
        textFieldPassword.returnKeyType = .done
        textFieldPassword.isSecureTextEntry = true
        textFieldEmail.keyboardType = .emailAddress
        textFieldEmail.autocorrectionType = .no
        //textFieldEmail.
        self.tableView.tableFooterView = UIView()
        self.tableView.allowsSelection = false
        setupTextFieldUI(textField: textFieldEmail)
        setupTextFieldUI(textField: textFieldPassword)
        
        
        self.tableView.separatorStyle = .none
        //        self.tableView.allowsSelection = false
        let barButtonItem = UIBarButtonItem(title: RMStrings.DONE_STRING(), style: .done, target: self, action: #selector(barButtonDoneAction))
        let toolbarWithBarButtonDone = RMGlobalManager.toolbar(barButtonItem: barButtonItem)
        textFieldPassword.inputAccessoryView = toolbarWithBarButtonDone
        
        
        let barButtonItemNext = UIBarButtonItem(title: RMStrings.NEXT_STRING(), style: .done, target: self, action: #selector(barButtonNextAction))
        let toolbarWithBarButtonNext = RMGlobalManager.toolbar(barButtonItem: barButtonItemNext)
        textFieldEmail.inputAccessoryView = toolbarWithBarButtonNext
        textFieldEmail.placeholder = RMStrings.E_MAIL_STRING()
        textFieldPassword.placeholder = RMStrings.PASSWORD_STRING()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction))
        //        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        
        
        textFieldEmail.text = "64.rahulraj@gmail.com"
        textFieldPassword.text = "123456789"
        setupButton(button: buttonLogin)
        setupButton(button: buttonForgetPassword)
        setupButton(button: buttonSignup)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupButton(button:UIButton) {
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 2
        button.clipsToBounds = true
        button.layer.borderColor =  UIColor.blue.cgColor
        button.backgroundColor = RMGlobalManager.appBlueColor()
    }
    
    func setupTextFieldUI(textField:UITextField) {
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.borderStyle = .none
        let leftPaddingView = UIView(frame: CGRect(x:0, y:0,width:10,height:textField.frame.size.height))
        textField.leftView = leftPaddingView
        textField.leftViewMode = .always
        
        let rightPaddingView = UIView(frame: CGRect(x:0, y:0,width:10,height:textField.frame.size.height))
        textField.rightView = rightPaddingView
        textField.rightViewMode = .always
        
        textField.delegate = self as UITextFieldDelegate
    }
    
    
    @objc func barButtonDoneAction() {
        textFieldPassword.resignFirstResponder()
    }
    
    
    @objc func barButtonNextAction() {
        textFieldEmail.resignFirstResponder()
        textFieldPassword.becomeFirstResponder()
    }
    
    
    @objc func tapGestureAction() {
        self.view.endEditing(true)
    }
    
    @IBAction func buttonActionForSignIn(_ sender: Any) {
        textFieldEmail.resignFirstResponder()
        textFieldPassword.resignFirstResponder()
        
        textFieldEmail.text = textFieldEmail.text ?? ""
        textFieldPassword.text = textFieldPassword.text ?? ""
        
        textFieldEmail.text = textFieldEmail.text!.trimmingCharacters(in: .whitespaces)
        textFieldPassword.text = textFieldPassword.text!.trimmingCharacters(in: .whitespaces)
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController:self)
            return
        }
        if textFieldEmail.text!.count == 0 {
            RMAlert.showAlert(title: nil, message: RMStrings.ENTER_E_MAIL_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textFieldEmail.becomeFirstResponder()
            })
            return
        }
        
        if RMGlobalManager.validateEmail(enteredEmail:textFieldEmail.text!) == false {
            RMAlert.showAlert(title: nil, message: RMStrings.ENTER_VALID_E_MAIL_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textFieldEmail.becomeFirstResponder()
            })
            return
        }
        
        if textFieldPassword.text!.count == 0 {
            RMAlert.showAlert(title: nil, message: RMStrings.ENTER_PASSWORD_STRING(), showInViewController: self, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: {[weak self] (selectedButtonName) in
                self?.textFieldPassword.becomeFirstResponder()
            })
            return
        }
        
        
        RMWebServiceManager.login(username: textFieldEmail.text!, password: textFieldPassword.text!) {[weak self] (result:Any?, isSuccess:Bool) in
            guard let strongSelf = self else {
                return
            }
            if isSuccess == true {
                /***
                 Success
                 */
                let dict = result as! Dictionary<String,Any>
                if dict["user_id"] == nil {
                    return;
                }
                let userId = dict["user_id"] as! String
              RMAppDelegate.firebaseSubscribe(topic: userId)
                RMUserDefaultManager.setCurrentUserUniqueId(appUserUniqueId: userId)
//                RMUserDefaultManager.setCurrentUserUniqueId(uniqueId: userId)
                strongSelf.textFieldEmail.text = ""
                strongSelf.textFieldPassword.text = ""
                strongSelf.tableView.reloadData()
                DispatchQueue.main.async {
                   RMSideMenuAndRoot.showDashboard()
                }
            }
        }
        
        
    }
    
    
    @IBAction func buttonActionForForgetPwd(_ sender: Any) {
        let vc = RMForgetPwdTableViewController.instance()
        let nav = UINavigationController(rootViewController: vc)
        self.present(nav, animated: true, completion: nil)
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonActionForSignup(_ sender: Any) {
        let vc = RMSignupTableViewController.instance()
        let nav = UINavigationController(rootViewController: vc)
        self.present(nav, animated: true, completion: nil)
    }

    // MARK: - Table view data source
    
    
    
    

    
    

}


extension RMLoginTableViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        if(newLength == 0)
        {
            //            labelPlaceholderDescription?.isHidden = false
        }
        else
        {
            //            labelPlaceholderDescription?.isHidden = true
        }
        return newLength <= RMLoginTableViewController.lengthLimitEmail
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldEmail {
            textFieldPassword.becomeFirstResponder()
            return false
        }
        return true
    }
}
