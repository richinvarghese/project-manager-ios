//
//  RMEventListTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 23/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit

protocol RMEventListDelegate: class {
    func didSelected(eventListVC:RMEventListViewController, event:RMEvent)
    func didDeleted(eventListVC:RMEventListViewController, event:RMEvent)
}

class RMEventListViewController: UIViewController {
    
    
    weak var delegate : RMEventListDelegate?
    
    var arrayToDisplay:[RMEvent]?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.tableView.tableFooterView = UIView()
//        self.tableView.allowsSelection = false
        self.tableView.register(RMEventListTableViewCell.self)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.isScrollEnabled = false
        fetchEvents()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchEvents(){
        RMWebServiceManager.fetchEvent(currentUserUniqueId: RMUserDefaultManager.getCurrentUserUniqueId()!) {[weak self] (result, isSuccess) in
            if isSuccess == false || (result is [RMEvent]) == false {
                return
            }
            self?.arrayToDisplay = result as? [RMEvent]
            RMEventCalendarTableViewController.arrayEvents = self?.arrayToDisplay
            let calendarTableVC = self?.delegate as! RMEventCalendarTableViewController
            calendarTableVC.reloadCalendarView()
        }
    }
    
    func showEvents(events:[RMEvent]?) {
        arrayToDisplay = events
        tableView.reloadData()
    }
    
    func showingEvent(atIndexPath:IndexPath) -> RMEvent {
        //        let array = displayingArray()
        return arrayToDisplay![atIndexPath.row]
    }
    
    
    deinit {
        print("deinit\(self)")
    }
    
}
    // MARK: - Table view data source
extension RMEventListViewController: UITableViewDelegate,UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if arrayToDisplay != nil {
            return arrayToDisplay!.count
        }
        return 0
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:RMEventListTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath) //as! RMEventListTableViewCell
        cell.backgroundColor = UIColor.purple
        cell.contentView.backgroundColor = UIColor.purple
        let event = showingEvent(atIndexPath: indexPath)
//        cell.textLabel!.text = event.eventTitle
//        cell.backgroundColor = UIColor.purple
//        cell.contentView.backgroundColor = UIColor.purple
        let calendar = self.delegate as! RMEventCalendarTableViewController
//        let stringDate = RMGlobalManager.stringFromDate(date: calendar.dateSelected!, isWithTime: false)
        let stringDate = RMGlobalManager.webServiceOnlyStringDateFromDate(date: calendar.dateSelected!)
        cell.set(event: event, selectedDate: stringDate)
        return cell
    }
 

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let event = showingEvent(atIndexPath: indexPath)
        delegate?.didSelected(eventListVC: self, event: event)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let event = showingEvent(atIndexPath: indexPath)
            let index = arrayToDisplay?.index(of: event)
            arrayToDisplay?.remove(at:index!)
            self.tableView.reloadData()
            delegate?.didDeleted(eventListVC: self, event: event)
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
         cell.separatorInset = UIEdgeInsetsMake(0, 16, 0, 16)
        cell.contentView.backgroundColor = UIColor(red: 231, green: 242, blue: 236)
        cell.contentView.backgroundColor = UIColor.white
//        tableView.backgroundColor = cell.contentView.backgroundColor
//        (delegate as! RMEventCalendarTableViewController).tableView.backgroundColor = tableView.backgroundColor
        cell.textLabel?.backgroundColor = cell.contentView.backgroundColor
        cell.backgroundColor = cell.contentView.backgroundColor
    }
    
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
