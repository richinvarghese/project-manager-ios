//
//  RMTaskListTableViewController.swift
//  Repairman
//
//  Created by Shebin Koshy on 25/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit

protocol RMTaskListTableViewControllerDelegate: class {
    func taskChanged(taskListTableVC:RMTaskListTableViewController)
}

class RMTaskListTableViewController: UITableViewController {
    
    var arrayTask: [RMTask]?
    var arraySearch: [RMTask]?
    
    var arrayTaskStatus: [RMTaskStatus]? {
        didSet{
            print("dasdfdsfd\(arrayTaskStatus)")
        }
    }
    
    var delegate:RMTaskListTableViewControllerDelegate?
    var searchBar: UISearchBar?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        self.tableView.register(RMTaskListTableViewCell.self)
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 138
        tableView.rowHeight = UITableViewAutomaticDimension
//        addDummyTask()//FIXME: Dummy data
        searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 50))
        searchBar!.delegate = self
        searchBar?.placeholder = "Search"
        tableView.tableHeaderView = searchBar
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addDummyTask() {//FIXME: Dummy data
        arrayTask = []
        let task = RMTask()
        task.taskTitle = "Task Title1"
        task.taskDescription = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
        task.taskStatus = "Completed"
//        task.taskStatusType = RMTask.TaskStatusType.Completed
        arrayTask?.append(task)
        
        let taskTwo = RMTask()
        taskTwo.taskTitle = "Second Task"
        taskTwo.taskDescription = "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
        taskTwo.taskStatus = "Not Started"
//        taskTwo.taskStatusType = RMTask.TaskStatusType.NotStarted
        arrayTask?.append(taskTwo)
        
        
        let task3 = RMTask()
        task3.taskTitle = "Third Task Title"
        task3.taskDescription = "Like Aldus PageMaker including versions of Lorem Ipsum"
        task3.taskStatus = "In progress"
//        task3.taskStatusType = RMTask.TaskStatusType.InProgress
        arrayTask?.append(task3)
    }
    
//    func addConstraintWithSuperView(){
//        NSLayoutConstraint(item: tableView, attribute: .top, relatedBy: .equal, toItem: tableView.superview, attribute: .top, multiplier: 1, constant: 0).isActive = true
//        NSLayoutConstraint(item: tableView, attribute: .bottom, relatedBy: .equal, toItem: tableView.superview, attribute: .bottom, multiplier: 1, constant: 10).isActive = true
//        NSLayoutConstraint(item: tableView, attribute: .leading, relatedBy: .equal, toItem: tableView.superview, attribute: .leading, multiplier: 1, constant: 0).isActive = true
//        NSLayoutConstraint(item: tableView, attribute: .trailing, relatedBy: .equal, toItem: tableView.superview, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
//    }
    
    func showingTask(atIndexPath:IndexPath) -> RMTask {
        //        let array = displayingArray()
        if searchBar?.text?.count == 0 {
            return arrayTask![atIndexPath.row]
        }
        return arraySearch![atIndexPath.row]
    }

    // MARK: - Table view data source

//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 0
//    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayTask == nil {
            return 0
        }
        if searchBar?.text?.count == 0 {
            return arrayTask!.count
        }
        if arraySearch == nil {
            return 0
        }
        return arraySearch!.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:RMTaskListTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate = self
        let task = showingTask(atIndexPath: indexPath)
        cell.setTask(task: task)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let task = showingTask(atIndexPath: indexPath)
        let taskDetailTableVC = RMTaskDetailTableViewController(style: .plain)
        taskDetailTableVC.selectedTask = task
        self.navigationController?.pushViewController(taskDetailTableVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        RMTheme.cellCornerRadius(cell: cell, borderColor: UIColor.red, backgroundColor: UIColor.clear)
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RMTaskListTableViewController : RMTaskListTableViewCellDelegate {
    func buttonActionChangeStatus(cell:RMTaskListTableViewCell){
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {
            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController: self)
            return
        }
        
        
        let indexPath =  tableView.indexPath(for: cell)
        if indexPath == nil {
            return
        }
        let task = showingTask(atIndexPath: indexPath!)
        let taskStatusTableVC = RMTaskStatusTableViewController(style: .plain)
        taskStatusTableVC.selectedTask = task
        taskStatusTableVC.delegate = self
       taskStatusTableVC.arrayTaskStatus = arrayTaskStatus
        self.navigationController?.pushViewController(taskStatusTableVC, animated: true)
    }
}

extension RMTaskListTableViewController : RMTaskStatusTableViewControllerDelegate {
    func taskChanged(taskStatusTableVC:RMTaskStatusTableViewController, taskStatusChangedTo:RMTaskStatus, forTask:RMTask) {
//        let taskIndex = arrayTask!.index(of: forTask)
//        let indexPath = IndexPath(row: taskIndex!, section: 0)
//        tableView.reloadRows(at: [indexPath], with: .automatic)
        self.delegate?.taskChanged(taskListTableVC: self)
    }
}

extension RMTaskListTableViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) // called when text changes (including clear)
    {
        if arrayTask == nil {
            return;
        }
        arraySearch = arrayTask?.filter {
            ($0.taskTitle?.contains(searchText))!
        }
        tableView.reloadData()
    }
    
}


