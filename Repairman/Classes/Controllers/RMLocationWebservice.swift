//
//  RMLocationWebservice.swift
//  Repairman
//
//  Created by Shebin Koshy on 10/03/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit
import Alamofire

class RMLocationWebservice: NSObject {

    
    class func webAPI(url:String,parameters:Dictionary<String,Any>?, completion:@escaping (_ result:Any?,_ isSuccess:Bool) -> Void) {
        //        let url = "http://testurl.com"
        //
        //        let parameters = [
        //            "email": "asd@fgh.hjk",
        //            "password": "55555"
        //        ]
        //
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    print(value)
                    completion(response.result,true);
                    return;
                }
            case .failure(let error):
                print(error)
            }
            completion(nil,false);
            return;
        }
    }
    
    class func so(currentLatitude:Double,currentLongitude:Double) {
        let strLat = "\(currentLatitude)"
        let strLong = "\(currentLongitude)"
        if RMUserDefaultManager.getCurrentUserUniqueId() == nil {
            return;
        }
        RMWebServiceManager.saveLocation(userUniqueId: RMUserDefaultManager.getCurrentUserUniqueId()!, latitiude: strLat, longitude: strLong) { (result:Any?, isSuccess:Bool) in
            if isSuccess == true {
//                RMLocalNotification.showLocalNotification(text: "Hug")
                RMLocationWebservice.saveAddressName(lat: currentLatitude, long: currentLongitude)
            } else {
//                RMLocalNotification.showLocalNotification(text: "MAd")
            }
        }
//        webAPI(url: "http://dev-project.apstrix.com/index.php/api/location/save", parameters: nil) {[currentLatitude,currentLongitude] (result:Any?, isSuccess:Bool) in
//            print("fds")
//            if isSuccess == true {
//                RMLocalNotification.sss(text: "Hug")
//                RMLocationWebservice.saveAddressName(lat: currentLatitude, long: currentLongitude)
//            } else {
//                RMLocalNotification.sss(text: "MAd")
//            }
//
//        }
    }
    
    class func saveAddressName(lat:Double,long:Double) {
        
        RMBackgroundLocationManager.sharedInstance.addressFrom(latitude: lat, longitude: long) { (str:String, er:Error?) in
            
            let pObj = RMPlace(pDate:Date(),pName:str,pLat:lat,pLong:long)
            //            pObj.name = str //?? ""
            
            RMUserDefaultManager.addPlace(place: pObj)
        }
    }

    
}
