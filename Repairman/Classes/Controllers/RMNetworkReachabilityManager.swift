//
//  RMNetworkReachabilityManager.swift
//  StreetWall
//
//  Created by Shebin Koshy on 16/07/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import Foundation
import UIKit

@objc protocol RMNetworkReachabilityDelegate {
     @objc optional func RMNetworkReachabilityManagerDidStateChanged(networkReachabilityManager:RMNetworkReachabilityManager)
}

class RMNetworkReachabilityManager:NSObject{
    
    public static let sharedInstance = RMNetworkReachabilityManager()
    private var delegates : NSHashTable<RMNetworkReachabilityManager>?
    
    override init(){
        super.init()

        NotificationCenter.default.addObserver(self, selector: #selector(networkReachabilityStateChanged), name:.reachabilityChanged, object: nil)
        
        self.reachability().startNotifier()
        
    }
    
    private func reachability() -> Reachability {
        return Reachability.forInternetConnection()
    }
    
    @objc func networkReachabilityStateChanged() {

        let enumerator = delegates?.objectEnumerator()
        for delgate in enumerator! {
            (delgate as! RMNetworkReachabilityDelegate).RMNetworkReachabilityManagerDidStateChanged?(networkReachabilityManager: self)
        }
    }
    
    func isNetworkReachable() -> Bool {

        if(self.reachability().currentReachabilityStatus() == NotReachable){
            return false
        }
        
        return true
    }
    
    func addDelegate(delegate: RMNetworkReachabilityDelegate!){
        weak var weakDelegate = delegate
        if weakDelegate == nil {
            return
        }
        if delegates == nil {
            delegates = NSHashTable.weakObjects()
        }

        delegates?.add(weakDelegate! as! RMNetworkReachabilityManager)
    }
    
    class func showNetworkNotReachableAlert(showInViewController:UIViewController) {
        RMAlert.showAlert(title: nil, message: RMStrings.NO_INTERNET_CONNECTION_STRING(), showInViewController: showInViewController, defaultButtonNames: [RMStrings.OK_STRING()], destructiveButtonNames: nil, cancelButtonNames: nil, completionAction: nil)
    }
    
    
    
    deinit {
        print("RMNetworkReachabilityManager deallocated")
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: nil)
    }
}
