//
//  RMCollectionView.swift
//  Repairman
//
//  Created by Shebin Koshy on 25/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit

//class RMCollectionView: UICollectionView {
//
//    /*
//    // Only override draw() if you perform custom drawing.
//    // An empty implementation adversely affects performance during animation.
//    override func draw(_ rect: CGRect) {
//        // Drawing code
//    }
//    */
//
//}

extension UICollectionView {
    
    func register<T: UICollectionViewCell>(_: T.Type) where T: ReusableView {
        register(T.self, forCellWithReuseIdentifier: T.defaultReuseIdentifier)
//        register(T.self, forCellReusxeIdentifier: T.defaultReuseIdentifier)
        //        registerClass(T.self, forCellWithReuseIdentifier: T.defaultReuseIdentifier)
    }
    
    func register<T: UICollectionViewCell>(_: T.Type) where T: ReusableView, T: NibLoadableView {
        let bundle = Bundle(for: T.self)
        let nib = UINib(nibName: T.nibName, bundle: bundle)
        //            let nib = UINib(T.nibName, bundle: bundle)
//        register(nib, forCellReuseIdentifier: T.defaultReuseIdentifier)
        register(nib, forCellWithReuseIdentifier: T.defaultReuseIdentifier)
        //            registerNib(nib, forCellWithReuseIdentifier: T.defaultReuseIdentifier)
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(forIndexPath indexPath: IndexPath) -> T where T: ReusableView {
//        dequeueReusableCell(withReuseIdentifier: <#T##String#>, for: <#T##IndexPath#>)
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.defaultReuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.defaultReuseIdentifier)")
        }
        
        return cell
    }
}

