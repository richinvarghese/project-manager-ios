//
//  RMAlert.swift
//  StreetWall
//
//  Created by Shebin Koshy on 18/07/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
// gfg

import Foundation
import UIKit

class RMAlert {
    public typealias AlertActionHandler = (String) -> Swift.Void

    public class func showAlert(title:String?, message:String, showInViewController:UIViewController,defaultButtonNames:[String]?,destructiveButtonNames:[String]?,cancelButtonNames:[String]?,completionAction:AlertActionHandler?){
        
        showAlertActionController(title: title, message: message, showInViewController: showInViewController, defaultButtonNames: defaultButtonNames, destructiveButtonNames: destructiveButtonNames, cancelButtonNames: cancelButtonNames, completionAction: completionAction, preferredStyle: .alert)
    }
    private class func showAlertActionController(title:String?, message:String?, showInViewController:UIViewController,defaultButtonNames:[String]?,destructiveButtonNames:[String]?,cancelButtonNames:[String]?,completionAction:AlertActionHandler?, preferredStyle: UIAlertControllerStyle){
        let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        
        if defaultButtonNames != nil {
            for btnName in (defaultButtonNames)! {
                let action = UIAlertAction(title: btnName, style: .default, handler: { (act) in
                    if completionAction == nil {
                        return
                    }
                    completionAction!(act.title!)
                })
                alert.addAction(action)
            }

        }
        
        
        if destructiveButtonNames != nil {
            for btnName in (destructiveButtonNames)! {
                let action = UIAlertAction(title: btnName, style: .destructive, handler: { (act) in
                    if completionAction == nil {
                        return
                    }
                    completionAction!(act.title!)
                })
                alert.addAction(action)
            }
        }
        
        
        if cancelButtonNames != nil {
            for btnName in (cancelButtonNames)! {
                let action = UIAlertAction(title: btnName, style: .cancel, handler: { (act) in
                    if completionAction == nil {
                        return
                    }
                    completionAction!(act.title!)
                })
                alert.addAction(action)
            }
        }
        
        showInViewController.present(alert, animated: true, completion: nil)
    }
    

    public class func showActionSheet(title:String?,showInViewController:UIViewController,defaultButtonNames:[String]?,destructiveButtonNames:[String]?,cancelButtonNames:[String]?,completionAction:AlertActionHandler?){
        
        showAlertActionController(title: title, message: nil, showInViewController: showInViewController, defaultButtonNames: defaultButtonNames, destructiveButtonNames: destructiveButtonNames, cancelButtonNames: cancelButtonNames, completionAction: completionAction, preferredStyle: .actionSheet)
    }
    
    deinit {
        print("SWAlert deallocated")
    }
}
