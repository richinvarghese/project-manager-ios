//
//  RMUserDefaultManager.swift
//  StreetWall
//
//  Created by Shebin Koshy on 05/08/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit

class RMUserDefaultManager: NSObject {
    
    
    
//    public class func setCurrentUserUniqueId(uniqueId:String) {
//        UserDefaults.standard.set(uniqueId, forKey: "kCurrentUserUniqueId")
//        UserDefaults.standard.synchronize()
//    }
//    
//    public class func getLastUpdatedLocation() -> String? {
//        let uniqueId = UserDefaults.standard.value(forKey: "kCurrentUserUniqueId") as? String
//        return uniqueId
//    }
    
    
    public class func setLastUpdatedLocation(latitude:Double,longitude:Double) {
        var dict = Dictionary<String,Double>()
        dict["latitude"] = latitude
        dict["longitude"] = longitude
        UserDefaults.standard.set(dict, forKey: "kLastUpdatedLocation")
        UserDefaults.standard.synchronize()
    }
    
    public class func getLastUpdatedLocation(handler:(Double,Double) -> Swift.Void) {
        let dict = UserDefaults.standard.value(forKey: "kLastUpdatedLocation") as? Dictionary<String,Double>
        handler(Double(dict!["latitude"]!),Double(dict!["longitude"]!))
    }
    
    
    public class func setMapViewRegion(dictMapViewRegion:Dictionary<String,Double>){
        UserDefaults.standard.set(dictMapViewRegion, forKey: "kMapViewRegion")
        UserDefaults.standard.synchronize()
    }
    
    public class func getMapViewRegion() -> Dictionary<String,Double>? {
        let dictMapViewRegion  = UserDefaults.standard.object(forKey: "kMapViewRegion")
        return dictMapViewRegion as? Dictionary<String, Double>
    }
    
    
    
    public class func setIsPortraitWhenMapPageDisppearLastTime(isPortraitWhenMapPageDisppearLastTime:Bool){
        UserDefaults.standard.set(isPortraitWhenMapPageDisppearLastTime, forKey: "kIsPortraitWhenMapPageDisppearLastTime")
        UserDefaults.standard.synchronize()
    }
    
    public class func getIsPortraitWhenMapPageDisppearLastTime() -> Bool? {
        let isPortraitWhenMapPageDisppearLastTime  = UserDefaults.standard.object(forKey: "kIsPortraitWhenMapPageDisppearLastTime")
        return isPortraitWhenMapPageDisppearLastTime as? Bool
    }
    
    
    
    public class func setDeviceEventPostLimit(dictDeviceEventPostLimit:Dictionary<String,Any>?){
        UserDefaults.standard.set(dictDeviceEventPostLimit, forKey: "kDeviceEventPostLimit")
        UserDefaults.standard.synchronize()
    }
    
    public class func getDeviceEventPostLimit() -> Dictionary<String,Any>? {
        let dictDeviceEventPostLimit  = UserDefaults.standard.object(forKey: "kDeviceEventPostLimit")
        return dictDeviceEventPostLimit as? Dictionary<String, Any>
    }
    
    
    public class func setDictNewEventComposed(dictNewEventComposed:Dictionary<String,String>?){
        UserDefaults.standard.set(dictNewEventComposed, forKey: "kDictNewEventComposed")
        UserDefaults.standard.synchronize()
    }
    
    public class func getDictNewEventComposed() -> Dictionary<String,String>? {
        let dictNewEventComposed  = UserDefaults.standard.object(forKey: "kDictNewEventComposed")
        return dictNewEventComposed as? Dictionary<String, String>
    }
    
    
    public class func setTabBarControllerSelectedIndex(tabBarControllerSelectedIndex:Int){
        UserDefaults.standard.set(tabBarControllerSelectedIndex, forKey: "kTabBarControllerSelectedIndex")
        UserDefaults.standard.synchronize()
    }
    
    public class func getTabBarControllerSelectedIndex() -> Int? {
        let tabBarControllerSelectedIndex  = UserDefaults.standard.object(forKey: "kTabBarControllerSelectedIndex")
        return tabBarControllerSelectedIndex as? Int
    }
    
    
    // MARK: settings attributes
    
    public class func setCurrentUserUniqueId(appUserUniqueId:String?) {
        if appUserUniqueId == nil {
            UserDefaults.standard.removeObject(forKey: "kCurrentUserUniqueId")
            UserDefaults.standard.synchronize()
            return
        }
        UserDefaults.standard.set(appUserUniqueId, forKey: "kCurrentUserUniqueId")
        UserDefaults.standard.synchronize()
    }
    
    public class func getCurrentUserUniqueId() -> String? {
//        return "3"
        let appUserUniqueId  = UserDefaults.standard.object(forKey: "kCurrentUserUniqueId")
        return appUserUniqueId as? String
    }
    

    public class func setIsNeedToShowOnlyNearByEvents(isNeedToShowOnlyNearByEvents: Bool) {
        UserDefaults.standard.set(isNeedToShowOnlyNearByEvents, forKey: "kIsNeedToShowOnlyNearByEvents")
        UserDefaults.standard.synchronize()
    }
    
    public class func getIsNeedToShowOnlyNearByEvents() -> Bool? {
        return UserDefaults.standard.value(forKey: "kIsNeedToShowOnlyNearByEvents") as? Bool
    }
    
    
    public class func setRadiusForObserveEvents(radiusForObserveEvents: Int) {
        UserDefaults.standard.set(radiusForObserveEvents, forKey: "kRadiusForObserveEvents")
        UserDefaults.standard.synchronize()
    }
    
    public class func getRadiusForObserveEvents() -> Int {
        let radius = UserDefaults.standard.value(forKey: "kRadiusForObserveEvents") as? Int
        if radius == nil {
            return RMGlobalManager.defaultRadiusForFetchNearbyEvents()
        }
        return radius!
    }
    
    // MARK: - Guest User
    
    public class func setGuestUserUUID(guestUserUUID:String) {
        UserDefaults.standard.set(guestUserUUID, forKey: "kGuestUserUUID")
        UserDefaults.standard.synchronize()
    }
    
    public class func getGuestUserUUID() -> String? {
        let guestUserUUID = UserDefaults.standard.value(forKey: "kGuestUserUUID") as? String
        return guestUserUUID
    }
    
    public class func addGuestUserEvent(eventUniqueId:String) {
        var guestUserEvents = getGuestUserEvents()
        if guestUserEvents == nil {
            guestUserEvents = []
        }
        guestUserEvents!.append(eventUniqueId)
        UserDefaults.standard.set(guestUserEvents, forKey: "kGuestUserEvents")
        UserDefaults.standard.synchronize()
    }
    
    /*public class func removeGuestUserEvent(eventUniqueId:String) {
        guard var guestUserEvents = getGuestUserEvents() else {
            return
        }
        if guestUserEvents.contains(eventUniqueId) {
            guestUserEvents = guestUserEvents.filter{
                $0 != eventUniqueId
            }
        }
        UserDefaults.standard.set(guestUserEvents, forKey: "kGuestUserEvents")
        UserDefaults.standard.synchronize()
    }*/
    
    public class func removeGuestUserEvents(eventUniqueIds:[String]) {
        guard var guestUserEvents = getGuestUserEvents() else {
            return
        }
        if eventUniqueIds.count == 0 {
            return
        }
            guestUserEvents = guestUserEvents.filter{
                eventUniqueIds.contains($0) == false
            }
        UserDefaults.standard.set(guestUserEvents, forKey: "kGuestUserEvents")
        UserDefaults.standard.synchronize()
    }
    
    public class func getGuestUserEvents() -> [String]? {
        let guestUserEvents = UserDefaults.standard.value(forKey: "kGuestUserEvents") as? [String]
        return guestUserEvents
    }
    
    //MARK - location background temp
    

    
    
    private static func archivePeople(people : [RMPlace]) -> NSData {
        
        return NSKeyedArchiver.archivedData(withRootObject: people as NSArray) as NSData
    }
    
    static func loadPeople() -> [RMPlace]? {
        
        if let unarchivedObject = UserDefaults.standard.object(forKey: "arrayPlace") as? Data {
            
            return NSKeyedUnarchiver.unarchiveObject(with: unarchivedObject as Data) as? [RMPlace]
        }
        
        return nil
    }
    
    
    
    class func addPlace(place:RMPlace) {
        
        var arPlace = loadPeople()
        if arPlace == nil {
            arPlace = [RMPlace]()
        }
        arPlace!.append(place)
        UserDefaults.standard.set(archivePeople(people: arPlace!), forKey: "arrayPlace")
        UserDefaults.standard.synchronize()
    }
    
    class func removeAllPlaces() {
        UserDefaults.standard.set(nil, forKey: "arrayPlace")
        UserDefaults.standard.synchronize()
    }
    
    class func getAllPlace() -> [RMPlace]? {
        //        let p = Place(pDate: Date(), pName: "dsf", pLat: 5.0, pLong: 4.0)
        //        return [p]
        
        let some = loadPeople()
        return some
    }
    
    deinit {
        print("RMUserDefaultManager deallocated")
    }
}
