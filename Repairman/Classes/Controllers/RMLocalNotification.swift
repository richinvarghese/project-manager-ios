//
//  RMLocalNotification.swift
//  Repairman
//
//  Created by Shebin Koshy on 10/03/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

class RMLocalNotification: NSObject {
    class func requestAuthorization() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: {didAllow, error in
                if didAllow == true {
//                    ssse(text: text)
                }
            })
        } else {
            // Fallback on earlier versions
            let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound]
            let setting = UIUserNotificationSettings(types: type, categories: nil)
            UIApplication.shared.registerUserNotificationSettings(setting)
        }
    }
    
    class func showLocalNotification(text:String) {
        
        requestAuthorization()
        
        if #available(iOS 10.0, *) {
            let content = UNMutableNotificationContent()
            
            //adding title, subtitle, body and badge
            content.title = text
            content.subtitle = "iOsdfment is fun"
            content.body = "Wedsfcation"
            
            //getting the notification request
            let request = UNNotificationRequest(identifier: "SimplifiedIOSNotification", content: content, trigger: nil)
            
            
            //adding the notification to notification center
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        } else {
            // Fallback on earlier versions
//            let localNUILocalNotification()
            let notification = UILocalNotification()
            notification.fireDate = Date()
            notification.alertBody = text
//            notification.alertAction = "Due : \(alertDate)"
            notification.soundName = UILocalNotificationDefaultSoundName
//            notification.userInfo = ["taskObjectId": taskTypeId]
            UIApplication.shared.scheduleLocalNotification(notification)
        }
        
       
        //        content.badge = 1
        
        //getting the notification trigger
        //it will be called after 5 seconds
        //        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        
    }
    
    
    
}
