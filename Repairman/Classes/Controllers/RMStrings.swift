//
//  RMStrings.swift
//  StreetWall
//
//  Created by Shebin Koshy on 05/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit

class RMStrings: NSObject {
    
    class func SETTINGS_STRING() -> String {
        return NSLocalizedString("Settings", comment: "")
    }
    
    class func SHARE_STRING(appName:String) -> String {
        let myString = String(format: NSLocalizedString("Share", comment: ""), appName)
        return myString
    }
    
    class func DONE_STRING() -> String {
        return NSLocalizedString("Done", comment: "")
    }
    
    class func LOCATION_SERVICE_ENTIRELY_DISABLED_FOR_DEVICE_WARNING_STRING() -> String {
        return NSLocalizedString("Location service entirely disabled for device warning", comment: "")
    }
    
    class func LOCATION_SERVICE_IS_DISABLED_FOR_APP_WARNING_STRING(appName:String) -> String {
        return String(format: NSLocalizedString("Location service is disabled for app warning", comment:""),appName)
    }
    
    class func LOCATION_SERVICE_IS_DISABLED_WHEN_APP_NOT_USING_WARNING_STRING(appName:String) -> String {
        return String(format: NSLocalizedString("Location service disabled when app is not using warning", comment:""),appName)
    }
    
    class func SHARING_TEXT_CONTENT_STRING(appName:String) -> String {
        return String(format: NSLocalizedString("Sharing text content", comment:""),appName)
    }
    
    class func SUPPORT_STRING() -> String {
        return NSLocalizedString("Support", comment: "")
    }

    class func DISPLAY_NAME_STRING() -> String {
        return NSLocalizedString("Display name", comment: "")
    }
    
    class func E_MAIL_STRING() -> String {
        return NSLocalizedString("E-mail", comment: "")
    }
    
    class func RANK_STRING() -> String {
        return NSLocalizedString("Rank", comment: "")
    }
    
    class func LIST_STRING() -> String {
        return NSLocalizedString("List", comment: "")
    }
    
    class func ANONYMOUS_STRING() -> String {
        return NSLocalizedString("Anonymous", comment: "")
    }
    
    class func YES_STRING() -> String {
        return NSLocalizedString("Yes", comment: "")
    }
    
    
    class func NO_STRING() -> String {
        return NSLocalizedString("No", comment: "")
    }

    
    class func CONFIRM_EVENT_DELETION_STRING(eventTitle:String) -> String {
        return String(format: NSLocalizedString("Confirm event deletion", comment:""),eventTitle)
    }
    

    class func MAP_STRING() -> String {
        return NSLocalizedString("Map", comment: "")
    }
    
    
    class func ENTER_E_MAIL_STRING() -> String {
        return NSLocalizedString("Enter E-mail", comment: "")
    }
    
    
    class func OK_STRING() -> String {
        return NSLocalizedString("OK", comment: "")
    }
    
    
    class func ENTER_PASSWORD_STRING() -> String {
        return NSLocalizedString("Enter Password", comment: "")
    }
    
    class func ENTER_VALID_E_MAIL_STRING() -> String {
        return NSLocalizedString("Enter valid E-mail", comment: "")
    }
    
    class func NEW_STRING() -> String {
        return NSLocalizedString("New", comment: "")
    }
    
    class func ONE_HOUR_STRING() -> String {
        return NSLocalizedString("1 hour", comment: "")
    }
    
    
    class func TWO_HOURS_STRING() -> String {
        return NSLocalizedString("2 hours", comment: "")
    }
    
    
    class func FOUR_HOURS_STRING() -> String {
        return NSLocalizedString("4 hours", comment: "")
    }
    
    class func EIGHT_HOURS_STRING() -> String {
        return NSLocalizedString("8 hours", comment: "")
    }
    
    class func ONE_DAY_STRING() -> String {
        return NSLocalizedString("1 day", comment: "")
    }
    
    class func TWO_DAYS_STRING() -> String {
        return NSLocalizedString("2 days", comment: "")
    }
    
    class func DESCRIPTION_STRING() -> String {
        return NSLocalizedString("Description", comment:"")
    }
    
    class func TITLE_STRING() -> String {
        return NSLocalizedString("Title", comment:"")
    }
    
    class func EXPIRY_STRING() -> String {
        return NSLocalizedString("Expiry", comment:"")
    }
    
    class func POST_STRING() -> String {
        return NSLocalizedString("Post", comment: "")
    }
    
        
    
    class func NEXT_STRING() -> String {
        return NSLocalizedString("Next", comment: "")
    }
    
    class func CHANGE_LOCATION_PRIVACY_SETTINGS_STRING() -> String {
        return NSLocalizedString("Change location privacy settings", comment: "")
    }
    
    
    class func CANCEL_STRING() -> String {
        return NSLocalizedString("Cancel", comment: "")
    }
    
    class func PRIVACY_SETTINGS_STRING() -> String {
        return NSLocalizedString("Privacy Settings", comment: "")
    }
    
    class func UNABLE_TO_GET_LOCATION_STRING() -> String {
        return NSLocalizedString("Unable to get location", comment: "")
    }
    
    
    class func ENTER_TITLE_STRING() -> String {
        return NSLocalizedString("Enter title", comment: "")
    }
    
    
    
    class func ENTER_DESCRIPTION_STRING() -> String {
        return NSLocalizedString("Enter description", comment: "")
    }
    
    
    class func ENTER_EXPIRY_STRING() -> String {
        return NSLocalizedString("Enter expiry", comment: "")
    }
    
    class func UPDATE_LOCATION_DETAILS_STRING() -> String {
        return NSLocalizedString("Update location details", comment: "")
    }
    
    
    class func YOUR_LIMIT_REACHED_FOR_TODAY_STRING() -> String {
        return NSLocalizedString("Your limit reached for today", comment: "")
    }
    
    class func LOGIN_STRING() -> String {
        return NSLocalizedString("Login", comment: "")
    }
    
    
    class func POST_AS_GUEST_STRING() -> String {
        return NSLocalizedString("Post as Guest", comment: "")
    }
    
    class func SUCCESSFULLY_POSTED_STRING() -> String {
        return NSLocalizedString("Successfully posted", comment: "")
    }
    
    class func LOCATION_STRING() -> String {
        return NSLocalizedString("Location", comment: "")
    }
    
    
    class func DISMISS_STRING() -> String {
        return NSLocalizedString("Dismiss", comment: "")
    }
    
    
    class func ENABLE_LOCATION_AND_NETWORK_STRING(appName:String) -> String {
        return String(format: NSLocalizedString("Enable location and network", comment:""),appName)
    }
    
    
    
    class func ENABLE_LOCATION_STRING(appName:String) -> String {
        return String(format: NSLocalizedString("Enable location", comment:""),appName)
    }
    
    class func ENABLE_NETWORK_STRING(appName:String) -> String {
        return String(format: NSLocalizedString("Enable network", comment:""),appName)
    }
    
    class func NO_INTERNET_CONNECTION_STRING() -> String {
        return NSLocalizedString("No internet connection", comment: "")
    }
    
    
    class func E_MAIL_ID_ALREADY_EXIST_STRING() -> String {
        return NSLocalizedString("e-mail id already exist", comment: "")
    }
    
    class func USER_NOT_FOUND_STRING() -> String {
        return NSLocalizedString("User not found", comment: "")
    }
    
    class func RANK_NOT_FOUND_STRING() -> String {
        return NSLocalizedString("Rank not found", comment: "")
    }
    
    class func SOMETHING_WENT_WRONG_STRING() -> String {
        return NSLocalizedString("Something went wrong", comment: "")
    }

    class func VERIFY_YOUR_E_MAIL_STRING() -> String {
        return NSLocalizedString("Verify your e-mail", comment: "")
    }
    
    class func WILL_SEND_CONFIRMATION_E_MAIL_STRING(appName:String) -> String {
        return String(format: NSLocalizedString("Will send confirmation e-mail", comment:""),appName)
    }

    
    class func CHECK_E_MAIL_AND_CONFIRM_STRING() -> String {
        return NSLocalizedString("Check e-mail and confirm", comment: "")
    }
    
    
    
    class func INCORRECT_USERNAME_OR_PASSWORD_STRING() -> String {
        return NSLocalizedString("Incorrect username or password", comment: "")
    }
    
    
    class func COULD_NOT_SEND_EMAIL_STRING() -> String {
        return NSLocalizedString("Could Not Send Email", comment: "")
    }
    
    
    class func COULD_NOT_SEND_EMAIL_CHECK_CONFIGURATION_STRING() -> String {
        return NSLocalizedString("Could Not Send Email. check configuration", comment: "")
    }
    
    
    class func DOCUMENT_STRING() -> String {
        return NSLocalizedString("Document", comment: "")
    }
    
    class func PHOTO_AND_VIDEO_LIBRARY_STRING() -> String {
        return NSLocalizedString("Photo & video library", comment: "")
    }
    
    class func CAMERA_STRING() -> String {
        return NSLocalizedString("Camera", comment: "")
    }
    
    class func ATTACHMENT_STRING() -> String {
        return NSLocalizedString("Attachment", comment: "")
    }
    
    class func ENTER_NOTE_STRING() -> String {
        return NSLocalizedString("Enter note", comment: "")
    }
    
    
    class func NOTE_STRING() -> String {
        return NSLocalizedString("Note", comment: "")
    }
    
    class func TODAY_STRING() -> String {
        return NSLocalizedString("Today", comment: "")
    }
    
    
    class func YESTERDAY_STRING() -> String {
        return NSLocalizedString("Yesterday", comment: "")
    }
    
    
    class func TOMORROW_STRING() -> String {
        return NSLocalizedString("Tomorrow", comment: "")
    }
    
    class func CONFIRM_ATTACHMENT_DELETION_STRING() -> String {
        return NSLocalizedString("Confirm attachment deletion", comment:"")    }
    
    class func TIMELINE_STRING() -> String {
        return NSLocalizedString("Timeline", comment: "")
    }
    
    class func EVENT_STRING() -> String {
        return NSLocalizedString("Event", comment: "")
    }
    
    class func EVENT_DATE_STRING() -> String {
        return NSLocalizedString("Event Date", comment: "")
    }
    
    
    class func ENTER_EVENT_DATE_STRING() -> String {
        return NSLocalizedString("Enter event date", comment: "")
    }
    
    class func OVERVIEW_STRING() -> String {
        return NSLocalizedString("Overview", comment: "")
    }
    
    
    class func TASKS_STRING() -> String {
        return NSLocalizedString("Tasks", comment: "")
    }
    
    class func PROJECT_STRING() -> String {
        return NSLocalizedString("Project", comment: "")
    }

    
    class func STATUS_STRING() -> String {
        return NSLocalizedString("Status", comment: "")
    }
    
    
    class func PASSWORD_STRING() -> String {
        return NSLocalizedString("Password", comment: "")
    }
    
    class func MILESTONES_STRING() -> String {
        return NSLocalizedString("Milestones", comment: "")
    }
    
    class func NOTES_STRING() -> String {
        return NSLocalizedString("Notes", comment: "")
    }
    
    class func FILES_STRING() -> String {
        return NSLocalizedString("Files", comment: "")
    }
    
    class func ACTIVITIES_STRING() -> String {
        return NSLocalizedString("Activities", comment: "")
    }
    
    class func ATTACH_FILE_STRING() -> String {
        return NSLocalizedString("Attach file", comment: "")
    }
    
    class func TIMESHEET_STRING() -> String {
        return NSLocalizedString("Timesheet", comment: "")
    }
    
    class func TIMER_STRING() -> String {
        return NSLocalizedString("Timer", comment: "")
    }
    
    class func STOP_TIMER_STRING() -> String {
        return NSLocalizedString("Stop timer", comment: "")
    }
    
    class func STOP_STRING() -> String {
        return NSLocalizedString("Stop", comment: "")
    }
    
    class func START_STRING() -> String {
        return NSLocalizedString("Start", comment: "")
    }
    
    class func CLOCK_STARTED_AT_STRING(clockInDateTime:String) -> String {
        let myString = String(format: NSLocalizedString("Clock started at %@", comment: ""), clockInDateTime)
        return myString
    }
    
    class func TIME_CARD_STRING() -> String {
        return NSLocalizedString("Time card", comment: "")
    }
    
    
    class func TOTAL_STRING() -> String {
        return NSLocalizedString("Total", comment: "")
    }
    
    
    class func ANNOUNCEMENT_STRING() -> String {
        return NSLocalizedString("Announcement", comment: "")
    }
    
    class func INBOX_STRING() -> String {
        return NSLocalizedString("Inbox", comment: "")
    }
    
    class func SENT_ITEMS_STRING() -> String {
        return NSLocalizedString("Sent Items", comment: "")
    }
    
    
    class func MESSAGES_STRING() -> String {
        return NSLocalizedString("Messages", comment: "")
    }
    
    class func REPLY_STRING() -> String {
        return NSLocalizedString("Reply", comment: "")
    }
    
//    class func recipient_STRING() -> String {
//        return NSLocalizedString("Reply", comment: "")
//    }
    
    class func RECIPIENT_STRING() -> String {
        return NSLocalizedString("Recipient", comment: "")
    }
    
    class func ENTER_RECIPIENT_STRING() -> String {
        return NSLocalizedString("Enter recipient", comment: "")
    }
    
    class func ME_STRING() -> String {
        return NSLocalizedString("Me", comment: "")
    }
    
    class func LEAVE_STRING() -> String {
        return NSLocalizedString("Leave", comment: "")
    }
    
    class func REASON_STRING() -> String {
        return NSLocalizedString("Reason", comment: "")
    }
    
    class func ENTER_LEAVE_TYPE_STRING() -> String {
        return NSLocalizedString("Enter leave type", comment: "")
    }
    
    class func ENTER_REASON_STRING() -> String {
        return NSLocalizedString("Enter reason", comment: "")
    }
    
    
    class func ENTER_DATE_STRING() -> String {
        return NSLocalizedString("Enter date", comment: "")
    }
    
    
    class func ENTER_HOUR_STRING() -> String {
        return NSLocalizedString("Enter hour", comment: "")
    }
    
    class func ENTER_START_DATE_STRING() -> String {
        return NSLocalizedString("Enter start date", comment: "")
    }
    
    class func ENTER_END_DATE_STRING() -> String {
        return NSLocalizedString("Enter end date", comment: "")
    }
    
    
    class func START_DATE_STRING() -> String {
        return NSLocalizedString("Start date", comment: "")
    }
    
    
    class func END_DATE_STRING() -> String {
        return NSLocalizedString("End date", comment: "")
    }
    
    class func EXPENSE_CATEGORY_STRING() -> String {
        return NSLocalizedString("Expense Category", comment: "")
    }
    
    class func EXPENSE_STRING() -> String {
        return NSLocalizedString("Expense", comment: "")
    }
    
//    class func CHANGE_LOCATION_PRIVACY_SETTINGS_STRING() -> String {
//        return NSLocalizedString("Change location privacy settings", comment: "")
//    }
    
    
}
