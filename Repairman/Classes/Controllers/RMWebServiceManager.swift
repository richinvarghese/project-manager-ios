//
//  SWWebServiceManager.swift
//  StreetWall
//
//  Created by Shebin Koshy on 05/09/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit
import Alamofire

//public


class RMWebServiceManager: NSObject {
    
    static var sharedOverlayView : UIView?
    static var overlayCount: Int = 0
    
    public class func baseURL() -> String {
        return "http://demo-projects.akstech.com.sg"
//        return "http://testproject.apstrix.com.sg"
        return Bundle.main.object(forInfoDictionaryKey: "RMWEB_SERVICE_BASE_URL") as! String
    }
    
    public typealias ResponseHandler = (Any?,Bool) -> Swift.Void
    
    
    public class func showHUD(isUserInteractionEnabled:Bool) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        if sharedOverlayView != nil {
            overlayCount = overlayCount + 1
            return;
        }
        let window = UIApplication.shared.delegate!.window!
        overlayCount = 1
        sharedOverlayView = UIView(frame: UIScreen.main.bounds)
        sharedOverlayView!.backgroundColor = UIColor(white: 0, alpha: 0.4)
        window!.addSubview(sharedOverlayView!)
        
        MBProgressHUD.showAdded(to: sharedOverlayView!, animated: true)
        if isUserInteractionEnabled == false {
            UIApplication.shared.beginIgnoringInteractionEvents()
        }
//        MBProgressHUD.colo
    }
    
    public class func removeHUD() {
        if sharedOverlayView == nil {
            return;
        }
        overlayCount = overlayCount - 1
        if overlayCount != 0 {
            return;
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        MBProgressHUD.hide(for: sharedOverlayView!, animated: true)
        sharedOverlayView?.removeFromSuperview()
        sharedOverlayView = nil
        if UIApplication.shared.isIgnoringInteractionEvents == true {
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
    
    public class func webservice(urlString:String, httpMethod:String, headerFields:Dictionary<String,String>, body:Dictionary<String, Any>?, responseHandler:@escaping ResponseHandler){
        let session = URLSession.shared
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        request.httpMethod = httpMethod
//        for header in headerFields {
            for key in headerFields.keys {
                request.addValue(headerFields[key]!, forHTTPHeaderField: key)
            }
            
//        }
        if body != nil {
            do {
                let httpBody = try JSONSerialization.data(withJSONObject: body!, options: JSONSerialization.WritingOptions.init(rawValue: 1))
                let some = try JSONSerialization.jsonObject(with: httpBody, options: .mutableContainers)
                request.httpBody = httpBody
            }
            catch let error as NSError {
                print(error.localizedDescription)
                responseHandler(nil, false)
            }
           
        }
        
//        let hud = MBProgressHUD()
//        let window = UIApplication.shared.delegate!.window!
//        MBProgressHUD.showAdded(to: window!, animated: true)
        showHUD(isUserInteractionEnabled: false)
        
        
        let dataTask = session.dataTask(with: request) { (data:Data?, response:URLResponse?, error:Error?) in
            
            DispatchQueue.main.async {
//                let window = UIApplication.shared.delegate!.window!
//                MBProgressHUD.hide(for: window!, animated: true)
                removeHUD()
                if error != nil {
                    responseHandler(nil, false)
                    return
                }
                if data != nil {
                    do{
                        let jsonDict = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                        responseHandler(jsonDict,true)
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        responseHandler(nil, false)
                    }
                }
            }
                       
            
        }
        dataTask.resume()
    }
    
    deinit {
        print("RMWebServiceManager deallocated")
    }
}


extension RMWebServiceManager {
    
    class func login(username:String,password:String,responseHandler:@escaping ResponseHandler) {
        let config = URLSessionConfiguration.default
        let userPasswordString = "\(username):\(password)"
        let userPasswordData = userPasswordString.data(using: .utf8, allowLossyConversion: true)
        let base64EncodedCredential = userPasswordData?.base64EncodedString(options: .lineLength64Characters)
        let authString = "Basic \(base64EncodedCredential!)"
        config.httpAdditionalHeaders = ["Authorization" : authString]
        let session = URLSession(configuration: config)
        
        let url = URL(string: "\(RMWebServiceManager.baseURL())/index.php/api/signin/login")
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
//        let window = UIApplication.shared.delegate!.window!
//        MBProgressHUD.showAdded(to: window!, animated: true)
        
        showHUD(isUserInteractionEnabled: false)
        
        let task = session.dataTask(with: request as URLRequest) {
            (
            data, response, error) in
            DispatchQueue.main.async {
//            let window = UIApplication.shared.delegate!.window!
//            MBProgressHUD.hide(for: window!, animated: true)
                removeHUD()
            
            guard let _:Data = (data as! Data), let _:URLResponse = response, error == nil else {
                print("error")
                responseHandler(nil,false)
                return
            }
            
                do {
                    let result = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String,Any>
                    print(result)
                    responseHandler(result,true)
                    
                } catch {
                    print("JSON Err")
                    
                    responseHandler(nil,false)
                }
                responseHandler(nil,false)
            }
        }
        task.resume()
    }
    
}

extension RMWebServiceManager {
    
    class func jsonHeaderField() -> [String:String] {
        return ["Accept":"Application/json","Content-Type":"Application/json"]
    }
    
    
    class func fetchTimeLine(responseHandler:@escaping ResponseHandler) {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/timeline/gettimeline"
        let header = jsonHeaderField()
        
        
        webservice(urlString: urlString, httpMethod: "POST", headerFields: header, body: nil) { (result: Any?,isSuccess:Bool) in
            if isSuccess == true && result is Array<Any> {
                var resultArray = result as! Array<Dictionary<String,Any?>>
                
                 resultArray = resultArray.sorted(by: { (dict1:Dictionary<String,Any?>,dict2:Dictionary<String,Any?>) -> Bool in
                    let stringDate1 = dict1["created_at"] as! String
                    let date1 = RMGlobalManager.dateFromWebServiceString(stringDate: stringDate1)
                    
                    let stringDate2 = dict2["created_at"] as! String
                    let date2 = RMGlobalManager.dateFromWebServiceString(stringDate: stringDate2)
                    
                    if date1 == nil || date2 == nil {
                        return true
                    }
                    
                    return date1! > date2!
                })
                
                var arraySection = Array<Array<RMTimeLine>>()
                var setDateStrings = Set<String>()
                
                var arrayTimeLine = Array<RMTimeLine>()
                for dict in resultArray {
                    let timeLine = RMTimeLine.timeLineForDict(dictTimeLine: dict)
                    
                    
                    let count = setDateStrings.count
                    let dateString = RMGlobalManager.onlyDateStringFromDate(date: timeLine.timeLinePostedDate!)
                    setDateStrings.insert(dateString)
                    if setDateStrings.count != count && count != 0 {
                        arraySection.append(arrayTimeLine)
                        arrayTimeLine = Array<RMTimeLine>()
                    }
                    
                    
                    
                    timeLine.isReply = false
                    arrayTimeLine.append(timeLine)
                   
                    
                    let arrayReply = dict["reply"] as? Array<Dictionary<String,Any>>
                    if arrayReply != nil {
                        for dictReply in arrayReply! {
                            let timeLineReply = RMTimeLine.timeLineForDict(dictTimeLine: dictReply)
                            timeLineReply.isReply = true
                            arrayTimeLine.append(timeLineReply)
                        }
                    }
                    
                    
                    
                    
                }
                
                arraySection.append(arrayTimeLine)
                
                responseHandler(arraySection,isSuccess)
            } else {
                responseHandler(nil,false)
            }
        }
    }
    
    
    
    
    
    
    class func postTimeLine(fileData:Data?,userUniqueId:String,timeLineDescription:String,mimeType:String,fileExtension: String, responseHandler:@escaping ResponseHandler) {
//        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/timeline/savetimeline"
//        let header = ["Accept":"Application/json","Content-Type":"Application/json"]

        let postTimeLineURL = URLPostTimeLine()

        let parameters = ["user_id": userUniqueId,"description":timeLineDescription] as [String : Any]
        
        

//        let window = UIApplication.shared.delegate!.window!
//        let view = UIView(frame:CGRect(x:0,y:0,width:UIScreen.main.bounds.size.width,height:UIScreen.main.bounds.size.height))
//        view.backgroundColor = UIColor.black
//        view.alpha = 0.7
////        window?.addSubview(view)
//        MBProgressHUD.showAdded(to: window!, animated: true)
        showHUD(isUserInteractionEnabled: false)
        print("cuuuuuuu\(Thread.current)")
        Alamofire.upload(multipartFormData: { (multipartFormData) in
//            if fileData != nil {
//                multipartFormData.append(fileData!, withName: "image", fileName: "myimage.jpg", mimeType: "image/jpg")
//            }
            


            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }

            if fileData != nil {
                multipartFormData.append(fileData!, withName: "manualFiles", fileName: "user.\(fileExtension)", mimeType: mimeType)
            }

            print("multipartFormData")

        }, to: postTimeLineURL, encodingCompletion: { (result) in
            DispatchQueue.main.async {
                
                
                switch result {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        
//                        let window = UIApplication.shared.delegate!.window!
//                        MBProgressHUD.hide(for: window!, animated: true)
                        removeHUD()
                        //                    self.delegate?.showSuccessAlert()
                        print(response.request)  // original URL request
                        print(response.response) // URL response
                        print(response.data)     // server data
                        print(response.result)   // result of response serialization
                        //                        self.showSuccesAlert()
                        //                    self.removeImage("frame", fileExtension: "txt")
                        if let JSON = response.result.value {
                            print("JSON: \(JSON)")
                            responseHandler(JSON as! Dictionary<String,Any?>,true)
                            return;
                        }
                        responseHandler(nil,false)
                    }
                    
                case .failure(let encodingError):
                    //                self.delegate?.showFailAlert()
                    print(encodingError)
                    responseHandler(nil,false)
                }
            }
            

        })

    }
    
    
//    class func postTimeLine(fileData:Data?,userUniqueId:String,timeLineDescription:String, responseHandler:@escaping ResponseHandler) {
//
//        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/timeline/savetimeline"
//        var r  = URLRequest(url: URL(string: urlString)!)
//        r.httpMethod = "POST"
//        let boundary = "Boundary-\(UUID().uuidString)"
//        r.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
//
//        let params = ["user_id": userUniqueId,"description":timeLineDescription] as [String : String]
//
//
//        r.httpBody = createBody(parameters: params,
//                                boundary: boundary,
//                                data: fileData!,
//                                mimeType: "image/jpg",
//                                filename: "hello.jpg")
//
//        let session = URLSession.shared
////        let url = URL(string: urlString)
//        let dataTask = session.dataTask(with: r) { (data:Data?, response:URLResponse?, error:Error?) in
//
//            DispatchQueue.main.async {
//                if error != nil {
//                    responseHandler(nil, false)
//                    return
//                }
//                if data != nil {
//                    do{
//                        let jsonDict = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
//                        responseHandler(jsonDict,true)
//
//                    }
//                    catch let error as NSError {
//                        print(error.localizedDescription)
//                        responseHandler(nil, false)
//                    }
//                }
//            }
//
//
//        }
//        dataTask.resume()
//    }
//
//
//
//    class func createBody(parameters: [String: String],
//                    boundary: String,
//                    data: Data,
//                    mimeType: String,
//                    filename: String) -> Data {
//        var body = NSMutableData()
//
//        let boundaryPrefix = "--\(boundary)\r\n"
//
//        for (key, value) in parameters {
//            body = appendString(body,boundaryPrefix)
//            body = appendString(body,"Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
//            body = appendString(body,"\(value)\r\n")
//        }
//
//        body = appendString(body,boundaryPrefix)
//        body = appendString(body,"Content-Disposition: form-data; name=\"file\"; filename=\"\(filename)\"\r\n")
//        body = appendString(body,"Content-Type: \(mimeType)\r\n\r\n")
//        body.append(data)
//        body = appendString(body,"\r\n")
//        body = appendString(body,"--".appending(boundary.appending("--")))
//
//        return body as Data
//    }
//
//
//    class func appendString(_ body:NSMutableData,_ string: String) -> NSMutableData {
//        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
//        body.append(data!)
//        return body
//    }
    
}


class URLPostTimeLine:URLConvertible {

    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/timeline/savetimeline"
        return URL(string:urlString)!
    }
    
}

class URLFetchNote:URLConvertible {
    
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/note/getnote"
        return URL(string:urlString)!
    }
    
}


class URLPostNote:URLConvertible {
    
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/note/save"
        return URL(string:urlString)!
    }
    
}


//class URLPostNote:URLConvertible {
//
//    func asURL() throws -> URL {
//        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/note/save"
//        return URL(string:urlString)!
//    }
//
//}


class URLDeleteNote:URLConvertible {
    
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/note/delete"
        return URL(string:urlString)!
    }
    
}


class URLFetchProject:URLConvertible {
    
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/project/getproject"
        return URL(string:urlString)!
    }
    
}


class URLFetchBasicProject:URLConvertible {
    
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/project/getproject"
        return URL(string:urlString)!
    }
    
}

class URLFetchProjectDetails:URLConvertible {
    
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/project/project_details"
        return URL(string:urlString)!
    }
    
}

class URLTimerStatus:URLConvertible {
    
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/project/timer"
        return URL(string:urlString)!
    }
    
}


class URLSaveTaskStatus:URLConvertible {
    
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/project/taskstatus"
        return URL(string:urlString)!
    }
    
}

class URLFileUpload:URLConvertible {
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/project/file"
        return URL(string:urlString)!
    }
}

class URLDashboardDetails:URLConvertible {
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/dashboard/details"
        return URL(string:urlString)!
    }
}


class URLSaveAttendance:URLConvertible {
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/attendance/save"
        return URL(string:urlString)!
    }
}

class URLFetchAttendance:URLConvertible {
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/attendance/list"
        return URL(string:urlString)!
    }
}


class URLFetchAnnouncements:URLConvertible {
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/announcements/list"
        return URL(string:urlString)!
    }
}

class URLFetchMessages:URLConvertible {
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/messages/list"
        return URL(string:urlString)!
    }
}

class URLPostMessage:URLConvertible {
    
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/messages/send"
        return URL(string:urlString)!
    }
    
}

class URLReplyMessage:URLConvertible {
    
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/messages/reply"
        return URL(string:urlString)!
    }
    
}


class URLMessageRecipient:URLConvertible {
    
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/messages/members"
        return URL(string:urlString)!
    }
    
}


class URLMessageDetail:URLConvertible {
    
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/messages/thread"
        return URL(string:urlString)!
    }
    
}

class URLFetchLeaves:URLConvertible {
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/leave/list"
        return URL(string:urlString)!
    }
}

class URLLeaveType:URLConvertible {
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/leave/type"
        return URL(string:urlString)!
    }
}

class URLLeavePost:URLConvertible {
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/leave/apply"
        return URL(string:urlString)!
    }
}


class URLLeaveUpdate:URLConvertible {
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/leave/update"
        return URL(string:urlString)!
    }
}


class URLEventList:URLConvertible {
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/events/list"
        return URL(string:urlString)!
    }
}


class URLEventSave:URLConvertible {
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/events/save"
        return URL(string:urlString)!
    }
}

class URLLocationSave:URLConvertible {
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/location/save"
        return URL(string:urlString)!
    }
}


class URLProjectMemberLeave:URLConvertible {
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/project/reject"
        return URL(string:urlString)!
    }
}

class URLAddExpense:URLConvertible {
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/project/expense"
        return URL(string:urlString)!
    }
}


class URLEmailSend:URLConvertible {
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/signin/email"
        return URL(string:urlString)!
    }
}

class URLSignup:URLConvertible {
    func asURL() throws -> URL {
        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/signup/create"
        return URL(string:urlString)!
    }
}




//extension RMWebServiceManager {
//    class func fetchNote(responseHandler:@escaping ResponseHandler) {
//        let urlString = "http://dev-project.apstrix.com/index.php/api/note/getnote"
//        let body = ["created_by":RMUserDefaultManager.getCurrentUserUniqueId()!] as! Dictionary<String,AnyObject>
//
//
//        webserviceFormData(urlConvertable:URLFetchNote(), parameters: body) {(result:Any?, isSuccess:Bool) in
////            <#code#>
////        }
//
//            if isSuccess == false {
//                responseHandler(nil,false)
//                return
//            }
////        webservice(urlString: urlString, httpMethod: "POST", headerFields: jsonHeaderField(), body: body) { (result:Any?, isSuccess:Bool) in
//            let array = result as! [Dictionary<String,String>]
//
//
//
//
//            let resultArray = array.sorted(by: { (dict1:Dictionary<String,String>,dict2:Dictionary<String,String>) -> Bool in
//                let stringDate1 = dict1["created_at"] as! String
//                let date1 = RMGlobalManager.dateFromWebServiceString(stringDate: stringDate1)
//
//                let stringDate2 = dict2["created_at"] as! String
//                let date2 = RMGlobalManager.dateFromWebServiceString(stringDate: stringDate2)
//
//                return date1 > date2
//            })
//
//
//
//            var arrayOfNotes = [RMNote]()
//            for dict in resultArray {
//
////                "id": "5",
////                "created_by": "2",
////                "created_at": "2018-01-16 17:20:26",
////                "title": "from rest1",
////                "description": "",
////                "project_id": "0",
////                "client_id": "0",
////                "user_id": "0",
////                "labels": "",
////                "deleted": "0"
//                if dict["deleted"] == "0" {
//                    let note = RMNote()
//                    note.notePostedDate = RMGlobalManager.dateFromWebServiceString(stringDate: dict["created_at"]!)
//                    note.noteTitle = dict["title"]
//                    note.noteDescription = dict["description"]
//                    note.noteUniqueId = dict[
//                    "id"]
//                    arrayOfNotes.append(note)
//                }
//            }
//
//            responseHandler(arrayOfNotes, true)
//        }
//    }
//
//
//
//    class func saveNote(title:String,description:String,responseHandler:@escaping ResponseHandler) {
//        let body = ["created_by":RMUserDefaultManager.getCurrentUserUniqueId()!,"title":title,"description":description] as! [String:AnyObject]
//
//        webserviceFormData(urlConvertable: URLPostNote(),parameters: body) { (result:Any?, isSuccess:Bool) in
//
//            responseHandler(result,isSuccess)
//              print("ssss")
//            }
//
//    }
//
//
//    class func updateNote(noteUniqueId:String,title:String,description:String,responseHandler:@escaping ResponseHandler) {
//        let body = ["created_by":RMUserDefaultManager.getCurrentUserUniqueId()!,"title":title,"description":description,"id":noteUniqueId] as! [String:AnyObject]
//
//        webserviceFormData(urlConvertable: URLPostNote(),parameters: body) { (result:Any?, isSuccess:Bool) in
//
//            responseHandler(result,isSuccess)
//            print("ssss")
//        }
//
//    }
//
//
//
//    class func deleteNote(noteUniqueId:String,responseHandler:@escaping ResponseHandler) {
//        let body = ["id":noteUniqueId] as! [String:AnyObject]
//
//        webserviceFormData(urlConvertable: URLDeleteNote(),parameters: body) { (result:Any?, isSuccess:Bool) in
//
//            responseHandler(result,isSuccess)
//            print("ssss")
//        }
//
//    }
//
//}

extension RMWebServiceManager {
    class func webserviceFormData(urlConvertable:URLConvertible,parameters:Dictionary<String,AnyObject> ,responseHandler:@escaping ResponseHandler) {
//        let postTimeLineURL = URLPostNote()
        
//        let parameters = ["created_by": RMUserDefaultManager.getCurrentUserUniqueId()]
        
        
        
        //        let window = UIApplication.shared.delegate!.window!
        //        let view = UIView(frame:CGRect(x:0,y:0,width:UIScreen.main.bounds.size.width,height:UIScreen.main.bounds.size.height))
        //        view.backgroundColor = UIColor.black
        //        view.alpha = 0.7
        ////        window?.addSubview(view)
        //        MBProgressHUD.showAdded(to: window!, animated: true)
        showHUD(isUserInteractionEnabled: false)
        print("cuuuuuuu\(Thread.current)")
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            //            if fileData != nil {
            //                multipartFormData.append(fileData!, withName: "image", fileName: "myimage.jpg", mimeType: "image/jpg")
            //            }
            
            
            
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
//            if fileData != nil {
//                multipartFormData.append(fileData!, withName: "manualFiles", fileName: "user.jpeg", mimeType: "image/jpeg")
//            }
            
            print("multipartFormData")
            
        }, to: urlConvertable, encodingCompletion: { (result) in
            DispatchQueue.main.async {
                
                
                switch result {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        
                        //                        let window = UIApplication.shared.delegate!.window!
                        //                        MBProgressHUD.hide(for: window!, animated: true)
                        removeHUD()
                        //                    self.delegate?.showSuccessAlert()
                        print(response.request)  // original URL request
                        print(response.response) // URL response
                        print(response.data)     // server data
                        print(response.result)   // result of response serialization
                        //                        self.showSuccesAlert()
                        //                    self.removeImage("frame", fileExtension: "txt")
                        if let JSON = response.result.value {
                            print("JSON: \(JSON)")
                            responseHandler(JSON,true)
                            return
                        }
                        responseHandler(nil,false)
                    }
                    
                case .failure(let encodingError):
                    //                self.delegate?.showFailAlert()
                    print(encodingError)
                    responseHandler(nil,false)
                }
            }
            
            
        })
    }
}


extension RMWebServiceManager {
    
    class func fetchProject(responseHandler:@escaping ResponseHandler) {
        let body = ["user_id":RMUserDefaultManager.getCurrentUserUniqueId()!] as! Dictionary<String,AnyObject>
        
        
        webserviceFormData(urlConvertable:URLFetchProject(), parameters: body) {(result:Any?, isSuccess:Bool) in
            //            <#code#>
            //        }
            
            if isSuccess == false {
                responseHandler(nil,false)
                return;
            }
            //        webservice(urlString: urlString, httpMethod: "POST", headerFields: jsonHeaderField(), body: body) { (result:Any?, isSuccess:Bool) in
            if result is [Dictionary<String,Any?>] == false {
                responseHandler(nil,false)
                return;
            }
            
            let array = result as! [Dictionary<String,Any?>]
            
            
            
            
            let resultArray = array.sorted(by: { (dict1:Dictionary<String,Any?>,dict2:Dictionary<String,Any?>) -> Bool in
                let stringDate1 = dict1["created_at"] as! String
                let date1 = RMGlobalManager.dateFromWebServiceString(stringDate: stringDate1)
                
                let stringDate2 = dict2["created_at"] as! String
                let date2 = RMGlobalManager.dateFromWebServiceString(stringDate: stringDate2)
                if date1 == nil || date2 == nil {
                    return true
                }
                
                return date1! > date2!
            })
            
            
            
            var arrayOfProjects = [RMProject]()
            for dict in resultArray {
                
                //                "id": "5",
                //                "created_by": "2",
                //                "created_at": "2018-01-16 17:20:26",
                //                "title": "from rest1",
                //                "description": "",
                //                "project_id": "0",
                //                "client_id": "0",
                //                "user_id": "0",
                //                "labels": "",
                //                "deleted": "0"
                if (dict["deleted"] as? String) == "0" {
                    let project = RMProject()
//                    project.notePostedDate = RMGlobalManager.dateFromWebServiceString(stringDate: dict["created_at"]!)
                    project.projectTitle = dict["title"] as? String
                    project.projectDescription = dict["description"] as? String
                    project.projectCompanyName = dict[
                        "company_name"] as? String
                    project.projectCurrencySymbol = dict["currency_symbol"] as? String
                    project.projectPrice = dict["price"] as? String
                    project.projectStatus = dict["status"] as? String
                    project.projectSartDate = dict["start_date"] as? String
                    project.projectEndDate = dict["deadline"] as? String
//                    project.projectTotalPoints = dict["total_points"] as? String
//                    project.projectCompletedPoints = dict["completed_points"] as? String
//                    project.pr
                    arrayOfProjects.append(project)
                }
            }
            
            responseHandler(arrayOfProjects, true)
        }
    }
    
    
    
    
    
    
    class func fetchBasicProject(responseHandler:@escaping ResponseHandler) {
        let body = ["user_id":RMUserDefaultManager.getCurrentUserUniqueId()!] as! Dictionary<String,AnyObject>
        
        
        webserviceFormData(urlConvertable:URLFetchProject(), parameters: body) {(result:Any?, isSuccess:Bool) in
            //            <#code#>
            //        }
            
            if isSuccess == false {
                responseHandler(nil,false)
                return;
            }
            
            if result is [Dictionary<String,Any?>] == false {
                responseHandler(nil,false)
                return;
            }
            //        webservice(urlString: urlString, httpMethod: "POST", headerFields: jsonHeaderField(), body: body) { (result:Any?, isSuccess:Bool) in
            let array = result as! [Dictionary<String,Any?>]
            
            
            
            
//            let resultArray = array.sorted(by: { (dict1:Dictionary<String,Any?>,dict2:Dictionary<String,Any?>) -> Bool in
//                let stringDate1 = dict1["created_at"] as! String
//                let date1 = RMGlobalManager.dateFromWebServiceString(stringDate: stringDate1)
//
//                let stringDate2 = dict2["created_at"] as! String
//                let date2 = RMGlobalManager.dateFromWebServiceString(stringDate: stringDate2)
//
//                return date1 > date2
//            })
            
            
            
            var arrayOfProjects = [RMProject]()
            for dict in array {
                
                //                "id": "5",
                //                "created_by": "2",
                //                "created_at": "2018-01-16 17:20:26",
                //                "title": "from rest1",
                //                "description": "",
                //                "project_id": "0",
                //                "client_id": "0",
                //                "user_id": "0",
                //                "labels": "",
                //                "deleted": "0"
//                if (dict["deleted"] as? String) == "0" {
                    let project = RMProject()
                    //                    project.notePostedDate = RMGlobalManager.dateFromWebServiceString(stringDate: dict["created_at"]!)
                    project.projectTitle = dict["title"] as? String
                    project.projectDescription = dict["description"] as? String
                    project.projectCompanyName = dict[
                        "company_name"] as? String
//                    project.projectCurrencySymbol = dict["currency_symbol"] as? String
//                    project.projectPrice = dict["price"] as? String
                    project.projectStatus = dict["status"] as? String
                project.projectProgress = dict["progress"] as? String
                project.projectUniqueId = dict["id"] as? String
//                    project.projectSartDate = dict["start_date"] as? String
//                    project.projectEndDate = dict["deadline"] as? String
                    //                    project.projectTotalPoints = dict["total_points"] as? String
                    //                    project.projectCompletedPoints = dict["completed_points"] as? String
                    //                    project.pr
                    arrayOfProjects.append(project)
//                }
            }
            
            responseHandler(arrayOfProjects, true)
        }
    }
    
    
    
    class func fetchProjectDetails(projectUniqueId:String,currentUserUniqueId:String, responseHandler:@escaping ResponseHandler) {
        let body = ["user_id":currentUserUniqueId, "project_id":projectUniqueId] as! Dictionary<String,AnyObject>
        
        webserviceFormData(urlConvertable:URLFetchProjectDetails(), parameters: body) {(result:Any?, isSuccess:Bool) in
            
            if isSuccess == false {
                responseHandler(nil,false)
                return;
            }
            
            if result is Dictionary<String,Any?> {
                let dict  = result as! Dictionary<String,Any?>
                
                if dict["status"] is String == true && dict["status"] as! String == "error" {
                    responseHandler(nil,false)
                    return;
                }
            } else {
                responseHandler(nil,false)
                return;
            }
            
            let dict = result as! Dictionary<String,Any?>
            
//            let resultArray = array.sorted(by: { (dict1:Dictionary<String,Any?>,dict2:Dictionary<String,Any?>) -> Bool in
//                let stringDate1 = dict1["created_at"] as! String
//                let date1 = RMGlobalManager.dateFromWebServiceString(stringDate: stringDate1)
//
//                let stringDate2 = dict2["created_at"] as! String
//                let date2 = RMGlobalManager.dateFromWebServiceString(stringDate: stringDate2)
//
//                return date1 > date2
//            })
            let arrayOverView = dict["overview"] as? [Dictionary<String,Any?>]
            var arrayTask = dict["task"] as? [Dictionary<String,Any?>]
            let arrayMember = dict["member"] as? [Dictionary<String,Any?>]
            var arrayActivity = dict["activity"] as? [Dictionary<String,Any?>]
            var arrayMilestone = dict["milestone"] as? [Dictionary<String,Any?>]
            
            var arrayStatusList = dict["task_status"] as? [Dictionary<String,Any?>]
            
            var arrayFiles = dict["files"] as? [Dictionary<String,Any?>]
            var arrayExpense = dict["expense"] as? [Dictionary<String,Any?>]
            var arrayExpenseCategory = dict["expense_category"] as? [Dictionary<String,Any?>]
            var stringTimerStatus = dict["timer_status"] as? String
            
            
            var arrayTimesheet = dict["timesheet"] as? [Dictionary<String,Any?>]
            
            
            if arrayMilestone == nil {
                arrayMilestone = [Dictionary<String,Any?>]()
            }
            
            if arrayFiles == nil {
                arrayFiles = [Dictionary<String,Any?>]()
            }
            
            if arrayStatusList == nil {
                arrayStatusList = [Dictionary<String,Any?>]()
            }
            
            if arrayActivity == nil {
                arrayActivity = [Dictionary<String,Any?>]()
            }
            
            if arrayTask == nil{
                arrayTask = [Dictionary<String,Any?>]()
            }
            
            if arrayTimesheet == nil {
                arrayTimesheet = [Dictionary<String,Any?>]()
            }
            
            if arrayExpense == nil {
                arrayExpense = [Dictionary<String,Any?>]()
            }
            
            if arrayExpenseCategory == nil {
                arrayExpenseCategory = [Dictionary<String,Any?>]()
            }
            
            var arrayOfProjects = [RMProject]()
            for dict in arrayOverView! {
                
                    let project = RMProject()
                    project.projectTitle = dict["title"] as? String
                    project.projectDescription = dict["description"] as? String
                    project.projectCompanyName = dict[
                        "company_name"] as? String
                    project.projectCurrencySymbol = dict["currency_symbol"] as? String
                    project.projectPrice = dict["price"] as? String
                    project.projectStatus = dict["status"] as? String
                    project.projectSartDate = dict["start_date"] as? String
                    project.projectEndDate = dict["deadline"] as? String
                project.projectProgress = dict["progress"] as? String
                    //                    project.projectTotalPoints = dict["total_points"] as? String
                    //                    project.projectCompletedPoints = dict["completed_points"] as? String
                    //                    project.pr
                    arrayOfProjects.append(project)
            }
            
            
            
            var arrayOfTasks = [RMTask]()
            for dict in arrayTask! {
                
                let task = RMTask()
                task.taskTitle = dict["title"] as? String
                task.taskDescription = dict["description"] as? String
                task.taskStatusKeyName = dict["status_key_name"] as? String
                task.taskStatus = dict["status_title"] as? String
                task.taskStatusHexColor = dict["status_color"] as? String
                task.taskAssignedUser = dict["assigned_to_user"] as? String
                task.taskAssignedUserAvatarImageUrl = dict["assigned_to_avatar"] as? String
                task.taskUniqueId = dict["id"] as? String
//                project.projectCompanyName = dict[
//                    "company_name"] as? String
//                project.projectCurrencySymbol = dict["currency_symbol"] as? String
//                project.projectPrice = dict["price"] as? String
//                project.projectStatus = dict["status"] as? String
//                project.projectSartDate = dict["start_date"] as? String
//                project.projectEndDate = dict["deadline"] as? String
                //                    project.projectTotalPoints = dict["total_points"] as? String
                //                    project.projectCompletedPoints = dict["completed_points"] as? String
                //                    project.pr
                arrayOfTasks.append(task)
            }
            
            
            var arrayOfProjectMembers = [RMProjectMember]()
            for dict in arrayMember! {
                let projectMember = RMProjectMember()
                projectMember.memberName = dict["member_name"] as? String
                projectMember.memberJobTitle = dict["job_title"] as? String
                projectMember.memberAvatarImageUrl = dict["member_image"] as? String
                arrayOfProjectMembers.append(projectMember)
            }
            
            
            var arrayOfActivities = [RMActivity]()
            
            for dict in arrayActivity! {
                
                let activity = RMActivity()
                activity.activityTitle = dict["title"] as? String
                activity.activityUserName = dict["name"] as? String
                if dict["action"] is String {
                    activity.setActivityAction(action: dict["action"] as! String)
                }
                activity.activityPreviousStatus = dict["previous"] as? String
                activity.activityCurrentStatus = dict["current"] as? String
                activity.activityPriority = dict["priority"] as? String
                activity.activityCreatedDate = dict["created_at"] as? String
                activity.activityUserAvatarImageUrl = dict["imageurl"] as? String
                arrayOfActivities.append(activity)
            }
            
            
            
            var arrayOfMilestones = [RMMilestone]()
            
            for dict in arrayMilestone! {
                
                let milestone = RMMilestone()
                milestone.milestoneTitle = dict["title"] as? String
                milestone.milestoneDueDate = dict["due_date"] as? String
                
                milestone.milestoneProgress = dict["progress"] as? String
                milestone.milestoneUniqueId = dict["id"] as? String
                milestone.milestoneDescription = dict["description"] as? String
                milestone.projectUniqueId = dict["project_id"] as? String
                arrayOfMilestones.append(milestone)
            }
            
            
            var arrayOfFiles = [RMProjectFile]()
            
            for dict in arrayFiles! {
                
                let projectFile = RMProjectFile()
                projectFile.fileUploadedById = dict["id"] as? String
                projectFile.projectFileImageUrl = dict["file_name"] as? String
                projectFile.projectFileDescription = dict["description"] as? String
                projectFile.projectFileCreatedAt = dict["created_at"] as? String
                projectFile.projectUniqueId = dict["project_id"] as? String
                projectFile.fileUploadedById = dict["uploaded_by"] as? String
                projectFile.fileUploadedByUsername = dict["uploaded_by_user_name"] as? String
                
                projectFile.fileUploadedByUserImageUrl = dict["uploaded_by_user_image"] as? String
                projectFile.fileUploadedByUserType = dict["uploaded_by_user_type"] as? String
                arrayOfFiles.append(projectFile)
            }
            
            var arrayOfTaskStatus = [RMTaskStatus]()
            for dict in arrayStatusList! {
                let isDeleted = dict["deleted"] as? String
                if  isDeleted == "1" {
                    continue;
                }
                
                let taskStatus = RMTaskStatus()
                taskStatus.taskStatusUniqueId = dict["id"] as? String
                taskStatus.taskStatusTitle = dict["title"] as? String
                taskStatus.taskStatusKeyName = dict["key_name"] as? String
                taskStatus.taskStatusColor = dict["color"] as? String
                taskStatus.taskStatusSort = dict["sort"] as? String
                arrayOfTaskStatus.append(taskStatus)
            }
            
            arrayOfTaskStatus.sort(by: { (task1, task2) -> Bool in
                    let sort1 = Int(task1.taskStatusSort ?? "0") ?? 0
                    let sort2 = Int(task2.taskStatusSort ?? "0") ?? 0
                    return sort1 >= sort2
                })
            
            
            
            var arrayOfTimesheet = [RMTimesheet]()
            for dict in arrayTimesheet! {
                let isDeleted = dict["deleted"] as? String
                if  isDeleted == "1" {
                    continue;
                }
                
                let timesheet = RMTimesheet()
                timesheet.timesheetUniqueId = dict["id"] as? String
                timesheet.timesheetStatus = dict["status"] as? String
                timesheet.timesheetTaskTitle = dict["task_title"] as? String
                timesheet.timesheetStartTime = dict["start_time"] as? String
                timesheet.timesheetEndTime = dict["end_time"] as? String
                timesheet.timesheetUsername = dict["logged_by_user"] as? String
                timesheet.timesheetTotalTime = dict["total"] as? String
                timesheet.timesheetUserAvatarUrl = dict["logged_by_avatar"] as? String
                arrayOfTimesheet.append(timesheet)
            }
            
            var arrayOfExpense = [RMExpense]()
            for dict in arrayExpense! {
                let expense = RMExpense.objectForDict(dictExpense:dict)
                arrayOfExpense.append(expense)
            }
            
            var arrayOfExpenseCategory = [RMExpenseCategory]()
            for dict in arrayExpenseCategory! {
                let expenseCategory = RMExpenseCategory.objectForDict(dictExpenseCategory:dict)
                arrayOfExpenseCategory.append(expenseCategory)
            }
            
            
            
//            var arrayResult = Array<Any>()
            var dictResult = Dictionary<String,[Any]>()
            
           var combinedProjectsAndMembers = Array<Any>()
            
            combinedProjectsAndMembers = combinedProjectsAndMembers + arrayOfProjects;
            
            combinedProjectsAndMembers.append(arrayOfProjectMembers);
//            arrayResult.append(combinedProjectsAndMembers)
            dictResult[RMStrings.OVERVIEW_STRING()] = combinedProjectsAndMembers
            if arrayOfTasks.count > 0 {
                dictResult[RMStrings.TASKS_STRING()] = arrayOfTasks
            }
            
            if arrayOfActivities.count > 0 {
                dictResult[RMStrings.ACTIVITIES_STRING()] = arrayOfActivities
            }
            
            if arrayOfMilestones.count > 0 {
                dictResult[RMStrings.MILESTONES_STRING()] = arrayOfMilestones
            }
            
//            if arrayOfFiles.count > 0 {
                dictResult[RMStrings.FILES_STRING()] = arrayOfFiles
//            }
            
            if arrayOfTaskStatus.count > 0 {
                dictResult["taskStatus"] = arrayOfTaskStatus
            }
            
            if arrayOfTimesheet.count > 0 {
                dictResult[RMStrings.TIMER_STRING()] = arrayOfTimesheet
            }
            
//            if arrayOfExpense.count > 0 {
                dictResult[RMStrings.EXPENSE_STRING()] = arrayOfExpense
//            }
            
            if arrayOfExpenseCategory.count > 0 {
                dictResult[RMStrings.EXPENSE_CATEGORY_STRING()] = arrayOfExpenseCategory
            }
            
            
            if stringTimerStatus != nil {
                dictResult["timerStatus"] = [stringTimerStatus]
            }
            
            responseHandler(dictResult, true)
        }
    }
    
    
    
    class func timerStatus(projectUniqueId:String,currentUserUniqueId:String,note:String?,taskId:String?, responseHandler:@escaping ResponseHandler) {
        
//        var stringTimerStatus = "start";
//        if isTimerStarted == true {
//            stringTimerStatus = "stop";
//        }
        
        var body = ["user_id":currentUserUniqueId, "project_id":projectUniqueId] as Dictionary<String,AnyObject>

        if note != nil {
            body["note"] = note! as AnyObject
        }
        
        if taskId != nil {
            body["task_id"] = taskId! as AnyObject
        }
        
        webserviceFormData(urlConvertable:URLTimerStatus(), parameters: body) {(result:Any?, isSuccess:Bool) in
            
            if isSuccess == false {
                responseHandler(nil,false)
                return;
            }
            
            if result is Dictionary<String,Any?> {
                let dict  = result as! Dictionary<String,Any?>
                
                if dict["status"] is String == true && dict["status"] as! String == "error" {
                    responseHandler(nil,false)
                    return;
                }
            } else {
                responseHandler(nil,false)
                return;
            }

            responseHandler(result,true)
//            let dict = result as! Dictionary<String,Any?>
//            dict
        }
    }
    
}


extension RMWebServiceManager {
    class func fetchNote(projectUniqueId:String?, responseHandler:@escaping ResponseHandler) {
//        let urlString = "http://dev-project.apstrix.com/index.php/api/note/getnote"
        var body: Dictionary<String,AnyObject>?
        if projectUniqueId != nil {
             body = ["created_by":RMUserDefaultManager.getCurrentUserUniqueId()!,"project_id":projectUniqueId] as! Dictionary<String,AnyObject>
        } else {
            body = ["created_by":RMUserDefaultManager.getCurrentUserUniqueId()!] as! Dictionary<String,AnyObject>
        }
        
        
        webserviceFormData(urlConvertable:URLFetchNote(), parameters: body!) {(result:Any?, isSuccess:Bool) in
            //            <#code#>
            //        }
            
            if isSuccess == false {
                responseHandler(nil,false)
                return;
            }
            
            if result is Dictionary<String,Any> {
                let dict = result as! Dictionary<String,Any>
                responseHandler(nil,false)
                return;
            }
            //        webservice(urlString: urlString, httpMethod: "POST", headerFields: jsonHeaderField(), body: body) { (result:Any?, isSuccess:Bool) in
            
            if result is [Dictionary<String,String>] == false {
                responseHandler(nil,false)
                return;
            }
            
            let array = result as! [Dictionary<String,String>]
            
            
            
            
            let resultArray = array.sorted(by: { (dict1:Dictionary<String,String>,dict2:Dictionary<String,String>) -> Bool in
                let stringDate1 = dict1["created_at"] as! String
                let date1 = RMGlobalManager.dateFromWebServiceString(stringDate: stringDate1)
                
                let stringDate2 = dict2["created_at"] as! String
                let date2 = RMGlobalManager.dateFromWebServiceString(stringDate: stringDate2)
                
                if date1 == nil || date2 == nil {
                    return true
                }
                
                return date1! > date2!
            })
            
            
            
            var arrayOfNotes = [RMNote]()
            for dict in resultArray {
                
                //                "id": "5",
                //                "created_by": "2",
                //                "created_at": "2018-01-16 17:20:26",
                //                "title": "from rest1",
                //                "description": "",
                //                "project_id": "0",
                //                "client_id": "0",
                //                "user_id": "0",
                //                "labels": "",
                //                "deleted": "0"
                if dict["deleted"] == "0" {
                    let note = RMNote()
                    note.notePostedDate = RMGlobalManager.dateFromWebServiceString(stringDate: dict["created_at"]!)
                    note.noteTitle = dict["title"]
                    note.noteDescription = dict["description"]
                    note.noteUniqueId = dict[
                        "id"]
                    arrayOfNotes.append(note)
                }
            }
            
            responseHandler(arrayOfNotes, true)
        }
    }
    
    
    
    class func saveNote(projectUniqueId:String?,title:String,description:String,responseHandler:@escaping ResponseHandler) {
        var body: Dictionary<String,AnyObject>?
        if projectUniqueId != nil {
            body = ["created_by":RMUserDefaultManager.getCurrentUserUniqueId()!,"title":title,"description":description,"project_id":projectUniqueId] as! [String:AnyObject]
        } else{
            body = ["created_by":RMUserDefaultManager.getCurrentUserUniqueId()!,"title":title,"description":description] as! [String:AnyObject]
        }
        
        webserviceFormData(urlConvertable: URLPostNote(),parameters: body!) { (result:Any?, isSuccess:Bool) in
            
            responseHandler(result,isSuccess)
            print("ssss")
        }
        
    }
    
    
    class func updateNote(projectUniqueId:String?,noteUniqueId:String,title:String,description:String,responseHandler:@escaping ResponseHandler) {
        var body: Dictionary<String,AnyObject>?
        if projectUniqueId != nil {
            body = ["created_by":RMUserDefaultManager.getCurrentUserUniqueId()!,"title":title,"description":description,"id":noteUniqueId,"project_id":projectUniqueId] as! [String:AnyObject]
        } else {
            body = ["created_by":RMUserDefaultManager.getCurrentUserUniqueId()!,"title":title,"description":description,"id":noteUniqueId] as! [String:AnyObject]
        }
        
        webserviceFormData(urlConvertable: URLPostNote(),parameters: body!) { (result:Any?, isSuccess:Bool) in
            
            responseHandler(result,isSuccess)
            print("ssss")
        }
        
    }
    
    class func deleteNote(noteUniqueId:String,responseHandler:@escaping ResponseHandler) {
        let body = ["id":noteUniqueId] as! [String:AnyObject]
        
        webserviceFormData(urlConvertable: URLDeleteNote(),parameters: body) { (result:Any?, isSuccess:Bool) in
            
            responseHandler(result,isSuccess)
            print("ssss")
        }
        
    }
}

extension RMWebServiceManager {
    
    class func taskStatusUpdate(taskUniqueId:String, statusUniqueId:String ,responseHandler:@escaping ResponseHandler) {
        let body = ["task_id":taskUniqueId,"status_id":statusUniqueId] as [String:AnyObject]
        
        webserviceFormData(urlConvertable: URLSaveTaskStatus(),parameters: body) { (result:Any?, isSuccess:Bool) in
            
            responseHandler(result,isSuccess)
            print("ssss")
        }
    }
    
}

extension RMWebServiceManager {
    
    class func fileUpload(projectUniqueId:String, fileDescription:String, currentUserUniqueId:String, fileData:Data, mimeType:String, fileExtension:String, responseHandler:@escaping ResponseHandler) {
        
        
        let postTimeLineURL = URLFileUpload()
        
        let parameters = ["user_id": currentUserUniqueId,"description":fileDescription,"project_id":projectUniqueId] as [String : Any]
        
        
        showHUD(isUserInteractionEnabled: false)
        print("cuuuuuuu\(Thread.current)")
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
//            if fileData != nil {
                multipartFormData.append(fileData, withName: "manualFiles", fileName: "user.\(fileExtension)", mimeType: mimeType)
//            }
            
            print("multipartFormData")
            
        }, to: postTimeLineURL, encodingCompletion: { (result) in
            DispatchQueue.main.async {
                
                
                switch result {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        removeHUD()
                        //                    self.delegate?.showSuccessAlert()
                        print(response.request)  // original URL request
                        print(response.response) // URL response
                        print(response.data)     // server data
                        print(response.result)   // result of
                        if let JSON = response.result.value {
                            print("JSON: \(JSON)")
                            responseHandler(JSON as! Dictionary<String,Any?>,true)
                            return;
                        }
                        responseHandler(nil,false)
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    responseHandler(nil,false)
                }
            }
            
            
        })
        
    }
    
}

extension RMWebServiceManager {
    
    class func dashboardDetails(responseHandler:@escaping ResponseHandler) {
        let body = ["user_id":RMUserDefaultManager.getCurrentUserUniqueId()] as [String:AnyObject]
        
        webserviceFormData(urlConvertable: URLDashboardDetails(),parameters: body) { (result:Any?, isSuccess:Bool) in
            responseHandler(result,isSuccess)
        }
    }
}


extension RMWebServiceManager {
    
    class func saveAttendance(latitude:String,longitude:String,note:String?,responseHandler:@escaping ResponseHandler) {
        var body = ["user_id":RMUserDefaultManager.getCurrentUserUniqueId(),"latitude":latitude,"longitude":longitude] as [String:AnyObject]
        
        if note != nil {
            body["note"] = note! as AnyObject
        }
        
        webserviceFormData(urlConvertable: URLSaveAttendance(),parameters: body) { (result:Any?, isSuccess:Bool) in
            responseHandler(result,isSuccess)
        }
    }
    
    
    class func fetchAttendance(startDate:String,endDate:String,responseHandler:@escaping ResponseHandler) {
        var body = ["user_id":RMUserDefaultManager.getCurrentUserUniqueId(),"start_date":startDate,"end_date":endDate] as [String:AnyObject]
        
//        if note != nil {
//            body["note"] = note! as AnyObject
//        }
        
        webserviceFormData(urlConvertable: URLFetchAttendance(),parameters: body) { (result:Any?, isSuccess:Bool) in
            
            if isSuccess == false {
                return;
            }
            
//            "user_id": "2",
//            "date_time_in": "2018-02-08 14:44:12",
//            "date_in": "2018-02-08",
//            "time_in": "08:14 pm",
//            "date_time_out": "2018-02-08 14:48:30",
//            "date_out": "2018-02-08",
//            "time_out": "08:18 pm",
//            "working_time": "00:04:18"
            
            let array = result as! [Dictionary<String,Any?>]
            var arrayOfTimeCards = [RMTimeCard]()
            for dict in array {
                let timecard = RMTimeCard()
                timecard.timeCardInDate = dict["date_in"] as? String
                timecard.timeCardInTime = dict["time_in"] as? String
                timecard.timeCardOutTime = dict["time_out"] as? String
                timecard.timeCardOutDate = dict["date_out"] as? String
                timecard.timeCardDuration = dict["working_time"] as? String
                timecard.timeCardDateAndTimeIn = dict["date_time_in"] as? String
                timecard.timeCardDateAndTimeOut = dict["date_time_out"] as? String
                arrayOfTimeCards.append(timecard)
            }
            
            responseHandler(arrayOfTimeCards,isSuccess)
        }
    }
    
    
}
extension RMWebServiceManager {


    class func fetchAnnouncements(responseHandler:@escaping  ResponseHandler) {
    var body = ["user_id":RMUserDefaultManager.getCurrentUserUniqueId()] as [String:AnyObject]
    
    //        if note != nil {
    //            body["note"] = note! as AnyObject
    //        }
    
    webserviceFormData(urlConvertable: URLFetchAnnouncements(),parameters: body) { (result:Any?, isSuccess:Bool) in
        
        if isSuccess == false {
            return;
        }
        
//        //            "image_url": "http://dev-project.apstrix.com/files/profile_images/_file5a4b7393d9425-avatar.png",
//        "created_by_id": "3",
//        "created_by_name": "Rahul Raj",
//        "start_date": "2018-02-27",
//        "end_date": "2018-02-28",
//        "title": "Announcement Title 3",
//        "description": "Desc 3"
        
        let array = result as! [Dictionary<String,Any?>]
        var arrayOfAnnouncement = [RMAnnouncement]()
        for dict in array {
            let announcement = RMAnnouncement()
            announcement.announcementCreatedByUsername = dict["created_by_name"] as? String
            announcement.announcementStartDate = dict["start_date"] as? String
            announcement.announcementEndDate = dict["end_date"] as? String
            announcement.announcementTitle = dict["title"] as? String
            announcement.announcementDescription = dict["description"] as? String
            announcement.accouncementAvatarImageURL = dict["image_url"] as? String
            arrayOfAnnouncement.append(announcement)
        }
        
        responseHandler(arrayOfAnnouncement,isSuccess)
    }
}


}



extension RMWebServiceManager {
    
    
    class func fetchMessages(currentUserUniqueId:String,responseHandler:@escaping  ResponseHandler) {
//        var body = ["user_id":RMUserDefaultManager.getCurrentUserUniqueId()] as [String:AnyObject]
        
        var body = ["user_id":currentUserUniqueId] as [String:AnyObject]
        
//        body["mode"] = "sent_items" as AnyObject
        //        if note != nil {
        //            body["note"] = note! as AnyObject
        //        }
        
        webserviceFormData(urlConvertable: URLFetchMessages(),parameters: body) { (result:Any?, isSuccess:Bool) in
            
            if isSuccess == false {
                return;
            }
            
            //        //            "image_url": "http://dev-project.apstrix.com/files/profile_images/_file5a4b7393d9425-avatar.png",
            //        "created_by_id": "3",
            //        "created_by_name": "Rahul Raj",
            //        "start_date": "2018-02-27",
            //        "end_date": "2018-02-28",
            //        "title": "Announcement Title 3",
            //        "description": "Desc 3"
            
            let dict = result as? Dictionary<String,Any?>
            if dict == nil {
                responseHandler(nil,false)
                return;
            }
            let arrayInbox =  dict!["inbox"] as? [Dictionary<String,Any?>]
            let arraySentItems = dict!["sent_items"] as? [Dictionary<String,Any?>]
            if arrayInbox == nil || arraySentItems == nil {
                responseHandler(nil,false)
                return;
            }
            var arrayOfMessagesInbox = [RMMessage]()
            for dict in arrayInbox! {
                let message = RMMessage.messageForDict(dictMessage: dict)
                message.isMyOwnPost = false
                arrayOfMessagesInbox.append(message)
            }
            var arrayOfMessagesSentItem = [RMMessage]()
            for dict in arraySentItems! {
                let message = RMMessage.messageForDict(dictMessage: dict)
                message.isMyOwnPost = true
                arrayOfMessagesSentItem.append(message)
            }
            let dictResultObj = ["inbox":arrayOfMessagesInbox,"sent_items":arrayOfMessagesSentItem]
            responseHandler(dictResultObj,isSuccess)
        }
    }
    
    
//}


    class func postMessage(toUserUniqueId:String?,fileData:Data?,userUniqueId:String,messageSubject:String?,messageDescription:String,replyMessageId:String?,mimeType:String, fileExtension:String, responseHandler:@escaping ResponseHandler) {
    //        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/timeline/savetimeline"
    //        let header = ["Accept":"Application/json","Content-Type":"Application/json"]
    
        var postMessageURL:URLConvertible = URLPostMessage()
    
    var parameters = ["user_id": userUniqueId] as [String : Any]
    
        if replyMessageId != nil {
            postMessageURL = URLReplyMessage()
            parameters["reply_message"] = messageDescription
            parameters["message_id"] = replyMessageId
        } else {
            parameters["message"] = messageDescription
            parameters["to_user_id"] = toUserUniqueId
            parameters["subject"] = messageSubject
        }
    
    //        let window = UIApplication.shared.delegate!.window!
    //        let view = UIView(frame:CGRect(x:0,y:0,width:UIScreen.main.bounds.size.width,height:UIScreen.main.bounds.size.height))
    //        view.backgroundColor = UIColor.black
    //        view.alpha = 0.7
    ////        window?.addSubview(view)
    //        MBProgressHUD.showAdded(to: window!, animated: true)
    showHUD(isUserInteractionEnabled: false)
    print("cuuuuuuu\(Thread.current)")
    Alamofire.upload(multipartFormData: { (multipartFormData) in
        //            if fileData != nil {
        //                multipartFormData.append(fileData!, withName: "image", fileName: "myimage.jpg", mimeType: "image/jpg")
        //            }
        
        
        
        for (key, value) in parameters {
            multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
        }
        
        if fileData != nil {
            multipartFormData.append(fileData!, withName: "manualFiles", fileName: "user\(fileExtension)", mimeType: mimeType)
        }
        
        print("multipartFormData")
        
    }, to: postMessageURL, encodingCompletion: { (result) in
        DispatchQueue.main.async {
            
            
            switch result {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    
                    
                    //                        let window = UIApplication.shared.delegate!.window!
                    //                        MBProgressHUD.hide(for: window!, animated: true)
                    removeHUD()
                    //                    self.delegate?.showSuccessAlert()
                    print(response.request)  // original URL request
                    print(response.response) // URL response
                    print(response.data)     // server data
                    print(response.result)   // result of response serialization
                    //                        self.showSuccesAlert()
                    //                    self.removeImage("frame", fileExtension: "txt")
                    if let JSON = response.result.value {
                        print("JSON: \(JSON)")
                        responseHandler(JSON as! Dictionary<String,Any?>,true)
                        return;
                    }
                    responseHandler(nil,false)
                }
                
            case .failure(let encodingError):
                //                self.delegate?.showFailAlert()
                print(encodingError)
                responseHandler(nil,false)
            }
        }
        
        
    })
    
}
    
    
    
    
    class func fetchRecipient(currentUserUniqueId:String, responseHandler:@escaping  ResponseHandler) {
        
        var body = ["user_id":currentUserUniqueId] as [String:AnyObject]
        
        
        webserviceFormData(urlConvertable: URLMessageRecipient(),parameters: body) { (result:Any?, isSuccess:Bool) in
            
            if isSuccess == false {
                responseHandler(nil,false)
                return;
            }
            
            let array = result as? [Dictionary<String,Any?>]
            if array == nil {
                responseHandler(nil,false)
                return;
            }
            
            var arrayOfMessageRecipient = [RMProjectMember]()
            for dict in array! {
                let projectMember = RMProjectMember()
                let firstName = dict["first_name"] as? String ?? ""
                let lastName = dict["last_name"] as? String ?? ""
                projectMember.memberName = "\(firstName) \(lastName)"
                projectMember.uniqueId = dict["id"] as? String
                arrayOfMessageRecipient.append(projectMember)
            }
            
            responseHandler(arrayOfMessageRecipient,isSuccess)
        }
    }
    
    
    
    
    class func fetchMessageDetails(messageUniqueId:String, responseHandler:@escaping  ResponseHandler) {
        
        var body = ["message_id":messageUniqueId] as [String:AnyObject]
        
        
        webserviceFormData(urlConvertable: URLMessageDetail(),parameters: body) { (result:Any?, isSuccess:Bool) in
            
            if isSuccess == false {
                responseHandler(nil,false)
                return;
            }
            
            let dict = result as? Dictionary<String,Any?>
            if dict == nil {
                responseHandler(nil,false)
                return;
            }
            
            let baseMessage = RMMessage.messageForDict(dictMessage: dict!)
            
            let arrayReply = dict!["reply"] as? [Dictionary<String,Any?>]
            
            var arrayOfMessages = [RMMessage]()
            arrayOfMessages.append(baseMessage)
            for dictReply in arrayReply! {
                
                let message = RMMessage.messageForDict(dictMessage: dictReply)
                arrayOfMessages.append(message)
            }
            responseHandler(arrayOfMessages,isSuccess)
        }
    }
    
}






extension RMWebServiceManager {
    
    
    class func fetchLeaves(currentUserUniqueId:String,responseHandler:@escaping  ResponseHandler) {
        //        var body = ["user_id":RMUserDefaultManager.getCurrentUserUniqueId()] as [String:AnyObject]
        
        let body = ["user_id":currentUserUniqueId] as [String:AnyObject]
        
        //        body["mode"] = "sent_items" as AnyObject
        //        if note != nil {
        //            body["note"] = note! as AnyObject
        //        }
        
        webserviceFormData(urlConvertable: URLFetchLeaves(),parameters: body) { (result:Any?, isSuccess:Bool) in
            
            if isSuccess == false {
                return;
            }
            
            //        //            "image_url": "http://dev-project.apstrix.com/files/profile_images/_file5a4b7393d9425-avatar.png",
            //        "created_by_id": "3",
            //        "created_by_name": "Rahul Raj",
            //        "start_date": "2018-02-27",
            //        "end_date": "2018-02-28",
            //        "title": "Announcement Title 3",
            //        "description": "Desc 3"
            
            let dict = result as? Dictionary<String,Any?>
            if dict == nil {
                responseHandler(nil,false)
                return;
            }
            let arrayLeaves =  dict!["leaves"] as? [Dictionary<String,Any?>]
//            let arraySentItems = dict!["sent_items"] as? [Dictionary<String,Any?>]
            if arrayLeaves == nil{
                responseHandler(nil,false)
                return;
            }
            var arrayOfLeaves = [RMLeave]()
            for dict in arrayLeaves! {
                let leave = RMLeave.leaveForDict(dictLeave: dict)
                arrayOfLeaves.append(leave)
            }
            responseHandler(arrayOfLeaves,isSuccess)
        }
    }
    
    
    //}
    
    
    class func postLeave(leaveTypeUniqueId:String,leaveReason:String,userUniqueId:String,leaveDuration:String?,leaveStartDate:String?,leaveEndDate:String?,leaveHourDate:String?,leaveHours:String?,leaveSingleDate:String?,responseHandler:@escaping ResponseHandler) {
        //        let urlString = "\(RMWebServiceManager.baseURL())/index.php/api/timeline/savetimeline"
        //        let header = ["Accept":"Application/json","Content-Type":"Application/json"]
        
//        var postLeaveURL:URLConvertible = URLPostLeave()
        
        var body = ["user_id": userUniqueId] as Dictionary<String, AnyObject>
        
        
        body["leave_type_id"] = leaveTypeUniqueId as AnyObject
            body["reason"] = leaveReason  as AnyObject
            body["duration"] = leaveDuration  as AnyObject
        if (leaveDuration == "single_date") {
            body["single_date"] = leaveSingleDate  as AnyObject
        } else if (leaveDuration == "hours") {
            body["hour_date"] = leaveHourDate  as AnyObject
            body["hours"] = leaveHours  as AnyObject
        } else if (leaveDuration == "multiple_days") {
            body["start_date"] = leaveStartDate  as AnyObject
            body["end_date"] = leaveEndDate  as AnyObject
        }
        
        
        
        //        let window = UIApplication.shared.delegate!.window!
        //        let view = UIView(frame:CGRect(x:0,y:0,width:UIScreen.main.bounds.size.width,height:UIScreen.main.bounds.size.height))
        //        view.backgroundColor = UIColor.black
        //        view.alpha = 0.7
        ////        window?.addSubview(view)
        //        MBProgressHUD.showAdded(to: window!, animated: true)
        webserviceFormData(urlConvertable: URLLeavePost(),parameters: body) { (result:Any?, isSuccess:Bool) in
            
            if isSuccess == false {
                responseHandler(nil,false)
                return;
            }
            
            let dict = result as? Dictionary<String,Any?>
            if dict == nil {
                responseHandler(nil,false)
                return;
            }
            let status =  dict!["status"] as? String
            
            if status?.lowercased() != "success" {
                responseHandler(nil,false)
                return;
            }
            
            responseHandler(result,isSuccess)
        }
        
    }
    
    
    
    
    class func fetchLeaveType(currentUserUniqueId:String, responseHandler:@escaping  ResponseHandler) {
        
        var body = ["user_id":currentUserUniqueId] as [String:AnyObject]
        
        
        webserviceFormData(urlConvertable: URLLeaveType(),parameters: body) { (result:Any?, isSuccess:Bool) in
            
            if isSuccess == false {
                return;
            }
            
            let array = result as? [Dictionary<String,Any?>]
            if array == nil {
                responseHandler(nil,false)
                return;
            }
            
            var arrayOfLeaveType = [RMLeaveType]()
            for dict in array! {
                
            
                
                let leaveType = RMLeaveType()
                leaveType.leaveTypeTitle = dict["title"] as? String
                leaveType.uniqueId = dict["id"] as? String
                leaveType.leaveTypeStatus = dict["status"] as? String
                leaveType.leaveTypeColor = dict["color"] as? String
                leaveType.leaveTypeDescription = dict["description"] as? String
                arrayOfLeaveType.append(leaveType)
            }
            
            responseHandler(arrayOfLeaveType,isSuccess)
        }
    }
    
    
    
    class func cancelLeave(currentUserUniqueId:String,leaveUniqueId:String,isCanceled:Bool,
        responseHandler:@escaping  ResponseHandler) {
        
        var body = ["user_id":currentUserUniqueId] as [String:AnyObject]
        
        if isCanceled {
            body["status"] = "canceled" as AnyObject
        }
        body["id"] = leaveUniqueId as AnyObject
        
        
        webserviceFormData(urlConvertable: URLLeaveUpdate(),parameters: body) { (result:Any?, isSuccess:Bool) in
            
            if isSuccess == false {
                return;
            }
            
            let dict = result as? Dictionary<String,Any?>
            if dict == nil {
                responseHandler(nil,false)
                return;
            }
            
            responseHandler(dict,isSuccess)
        }
    }
    
    
    
    
}

extension RMWebServiceManager {
    
    class func fetchEvent(currentUserUniqueId:String,
                           responseHandler:@escaping  ResponseHandler) {
        
        var body = ["user_id":currentUserUniqueId] as [String:AnyObject]
        
        webserviceFormData(urlConvertable: URLEventList(),parameters: body) { (result:Any?, isSuccess:Bool) in
            
            if isSuccess == false {
                return;
            }
            
            let array = result as? [Dictionary<String,Any?>]
            if array == nil {
                responseHandler(nil,false)
                return;
            }
            
            var arrayOfEvents = [RMEvent]()
            for dictEvent in array! {
                let event = RMEvent.eventFromDict(dict: dictEvent)
                arrayOfEvents.append(event)
            }
            
            responseHandler(arrayOfEvents,isSuccess)
        }
    }
    
    
    
    class func saveEvent(eventUniqueId:String?,userUniqueId:String,eventStartDate:String?,eventEndDate:String?,eventTitle:String?,eventDescription:String?,eventLocation:String?,isOnlyMe:Bool?,responseHandler:@escaping ResponseHandler) {
        var body = ["user_id": userUniqueId] as Dictionary<String, AnyObject>
        
        body["title"] = eventTitle as AnyObject
        body["description"] = eventDescription  as AnyObject
        let startDateOnly = eventStartDate?.components(separatedBy: " ")[0]
        let startTimeOnly = eventStartDate?.components(separatedBy: " ")[1]
        let endDateOnly = eventEndDate?.components(separatedBy: " ")[0]
        let endTimeOnly = eventEndDate?.components(separatedBy: " ")[1]
        body["start_date"] = startDateOnly  as AnyObject
        body["end_date"] = endDateOnly  as AnyObject
        body["start_time"] = startTimeOnly  as AnyObject
        body["end_time"] = endTimeOnly  as AnyObject
        if (eventUniqueId != nil) {
            body["id"] = eventUniqueId  as AnyObject
        }
        if isOnlyMe == true {
            body["visibility"] = "0" as AnyObject
        } else {
            body["visibility"] = "1" as AnyObject
        }
        if eventLocation != nil {
            body["location"] = eventLocation as AnyObject
        }
        webserviceFormData(urlConvertable: URLEventSave(),parameters: body) { (result:Any?, isSuccess:Bool) in
            
            if isSuccess == false {
                responseHandler(nil,false)
                return;
            }
            
            let dict = result as? Dictionary<String,Any?>
            if dict == nil {
                responseHandler(nil,false)
                return;
            }
            let status =  dict!["status"] as? String
            
            if status?.lowercased() != "success" {
                responseHandler(nil,false)
                return;
            }
            
            responseHandler(result,isSuccess)
        }
        
    }
    
}

extension RMWebServiceManager {
    
    
    class func saveLocation(userUniqueId:String,latitiude:String,longitude:String,responseHandler:@escaping ResponseHandler) {
        var body = ["user_id": userUniqueId] as Dictionary<String, AnyObject>
        
        body["latitude"] = latitiude as AnyObject
        body["logitude"] = longitude  as AnyObject
        
        
        webserviceFormData(urlConvertable: URLLocationSave(),parameters: body) { (result:Any?, isSuccess:Bool) in
            
            if isSuccess == false {
                responseHandler(nil,false)
                return;
            }
            
            let dict = result as? Dictionary<String,Any?>
            if dict == nil {
                responseHandler(nil,false)
                return;
            }
            let status =  dict!["status"] as? String
            
            if status?.lowercased() != "success" {
                responseHandler(nil,false)
                return;
            }
            
            responseHandler(result,isSuccess)
        }
        
    }
    
}

extension RMWebServiceManager {
    
    
    class func projectMemeberLeave(userUniqueId:String,projectUniqueId:String,responseHandler:@escaping ResponseHandler) {
        var body = ["user_id": userUniqueId] as Dictionary<String, AnyObject>
        
        body["project_id"] = projectUniqueId as AnyObject
        
        
        webserviceFormData(urlConvertable: URLProjectMemberLeave(),parameters: body) { (result:Any?, isSuccess:Bool) in
            
            if isSuccess == false {
                responseHandler(nil,false)
                return;
            }
            
            let dict = result as? Dictionary<String,Any?>
            if dict == nil {
                responseHandler(nil,false)
                return;
            }
            let status =  dict!["status"] as? String
            
            if status?.lowercased() != "success" {
                responseHandler(nil,false)
                return;
            }
            
            responseHandler(result,isSuccess)
        }
        
    }
}




extension RMWebServiceManager {
    
    
    class func projectExpense(userUniqueId:String,projectUniqueId:String,expenseDate:String,expenseAmount:String,expenseCategoryUniqueId:String,expenseTitle:String,expenseDescription:String,fileData:Data?,mimeType:String, fileExtension:String,responseHandler:@escaping ResponseHandler) {
        
        let addExpenseURL:URLConvertible = URLAddExpense()
        
        var parameters = ["user_id": userUniqueId] as [String : Any]
        parameters["project_id"] = projectUniqueId as AnyObject
        parameters["expense_date"] = expenseDate as AnyObject
        parameters["category_id"] = expenseCategoryUniqueId as AnyObject
        parameters["amount"] = expenseAmount as AnyObject
        parameters["title"] = expenseTitle as AnyObject
        
        showHUD(isUserInteractionEnabled: false)
        print("cuuuuuuu\(Thread.current)")
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
            if fileData != nil {
                multipartFormData.append(fileData!, withName: "manualFiles", fileName: "user.\(fileExtension)", mimeType: mimeType)
            }
            
            print("multipartFormData")
            
        }, to: addExpenseURL, encodingCompletion: { (result) in
            DispatchQueue.main.async {
                
                
                switch result {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        
                        //                        let window = UIApplication.shared.delegate!.window!
                        //                        MBProgressHUD.hide(for: window!, animated: true)
                        removeHUD()
                        //                    self.delegate?.showSuccessAlert()
                        print(response.request)  // original URL request
                        print(response.response) // URL response
                        print(response.data)     // server data
                        print(response.result)   // result of response serialization
                        //                        self.showSuccesAlert()
                        //                    self.removeImage("frame", fileExtension: "txt")
                        if let JSON = response.result.value {
                            print("JSON: \(JSON)")
                            responseHandler(JSON as! Dictionary<String,Any?>,true)
                            return;
                        }
                        responseHandler(nil,false)
                    }
                    
                case .failure(let encodingError):
                    //                self.delegate?.showFailAlert()
                    print(encodingError)
                    responseHandler(nil,false)
                }
            }
            
            
        })
        
    }
}



extension RMWebServiceManager {
class func sendEmail(email:String,responseHandler:@escaping ResponseHandler) {
    let body = ["email": email] as Dictionary<String, AnyObject>
    
    
    webserviceFormData(urlConvertable: URLEmailSend(),parameters: body) { (result:Any?, isSuccess:Bool) in
        
        if isSuccess == false {
            responseHandler(nil,false)
            return;
        }
        
        let dict = result as? Dictionary<String,Any?>
        if dict == nil {
            responseHandler(nil,false)
            return;
        }
        let status =  dict!["status"] as? String
        
        if status?.lowercased() != "success" {
            responseHandler(nil,false)
            return;
        }
        
        responseHandler(result,isSuccess)
    }
    
}
}



extension RMWebServiceManager {
    
    
    
    class func signup(email:String,firstName:String, lastName:String, password:String, companyName:String, responseHandler:@escaping ResponseHandler) {
        var body = ["email": email] as Dictionary<String, AnyObject>
        body["first_name"] = firstName as AnyObject
        body["last_name"] = lastName as AnyObject
        body["password"] = password as AnyObject
        body["company_name"] = companyName as AnyObject
        
        
        webserviceFormData(urlConvertable: URLSignup(),parameters: body) { (result:Any?, isSuccess:Bool) in
            
            if isSuccess == false {
                responseHandler(nil,false)
                return;
            }
            
            let dict = result as? Dictionary<String,Any?>
            if dict == nil {
                responseHandler(nil,false)
                return;
            }
            let status =  dict!["status"] as? String
            
            if status?.lowercased() != "success" {
                responseHandler(nil,false)
                return;
            }
            
            responseHandler(result,isSuccess)
        }
        
    }
}

