//
//  SWTableViewCell.swift
//  StreetWall
//
//  Created by Shebin Koshy on 10/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit


protocol ReusableView: class {
    static var defaultReuseIdentifier: String { get }
}

extension ReusableView where Self: UIView {
    static var defaultReuseIdentifier: String {
        return NSStringFromClass(self)
    }
}

protocol NibLoadableView: class {
    static var nibName: String { get }
}

extension NibLoadableView where Self: UIView {
    static var nibName: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
}

extension UICollectionViewCell: ReusableView {
}

extension UITableViewCell : ReusableView {
    
}

extension RMTimeLineListTableViewCell: NibLoadableView {
    
}

extension RMSubTimeLineListTableViewCell: NibLoadableView {
    
}

extension RMNoteListTableViewCell: NibLoadableView {
    
}


extension RMMembersCollectionViewCell : NibLoadableView {
    
}

extension RMTaskListTableViewCell : NibLoadableView {
    
}

extension RMProjectTableViewCell : NibLoadableView {
    
}

extension RMProjectBasicListTableViewCell : NibLoadableView {
    
}

extension RMProjectMemebersTableViewCell: NibLoadableView {
    
}

extension RMActivityListTableViewCell: NibLoadableView {
    
}

extension RMMilestoneListTableViewCell: NibLoadableView {
    
}

extension RMFileListTableViewCell: NibLoadableView {
    
}

extension RMTimeSheetTableViewCell: NibLoadableView {
    
}

extension RMTimeCardTableViewCell: NibLoadableView {
    
}


extension RMAnnouncementListTableViewCell: NibLoadableView {
    
}

extension RMMessageListTableViewCell: NibLoadableView {
    
}

extension RMMessageBaseTableViewCell: NibLoadableView {
    
}

extension RMLeaveListTableViewCell: NibLoadableView {
    
}

extension RMExpenseListTableViewCell: NibLoadableView {
    
}

extension RMEventListTableViewCell: NibLoadableView {
    
}
//
//extension GrowingLabelTableViewCell: ReusableView {
//    
//}
//
//extension UITableViewCell:  NibLoadableView {
//    
////    class func reuseIdentifier1() -> String {
////        let identifier1:String = String(describing: self)
////        print("MAnga#r:\(identifier1)")
////        return identifier1
////    }
//    
////    static var reuseIdentifier1: String {
////        var identifier1:String = String(describing: self)
////        print("MAnga#r:\(identifier1)")
////        
////        return identifier1
//////        return NSStringFromClass(self).componentsSeparatedByString(".").last!
////    }
//    
//    
//    class func printSome() {
//        var identifier1 = self.defaultReuseIdentifier
//        withUnsafePointer(to: &identifier1) {
//            print(" str value \(identifier1) has address: \($0)")
//        }
//    }
//    
//    class func registerNib1(tableView:UITableView) {
////        let cellClass = type(of: self)
//        printSome()
//        printSome()
//        let nib = UINib(nibName: self.nibName, bundle: nil)
//        tableView.register(nib, forCellReuseIdentifier: self.defaultReuseIdentifier)
//    }
//    
//    class func registerClass1(tableView:UITableView) {
////        let cellClass = type(of: self)
//        tableView.register(self, forCellReuseIdentifier: self.defaultReuseIdentifier)
//    }
//    
//    class func dequeReusableCell1(tableView:UITableView){
////        let cellClass = type(of: self)
//        tableView.dequeueReusableCell(withIdentifier: self.defaultReuseIdentifier)
//    }
//    
//    class func dequeReusableCell1(tableView:UITableView, indexPath:IndexPath) -> UITableViewCell {
//        //        let cellClass = type(of: self)
//        printSome()
//        printSome()
//        return tableView.dequeueReusableCell(withIdentifier: self.defaultReuseIdentifier, for:indexPath)
//    }

//}
