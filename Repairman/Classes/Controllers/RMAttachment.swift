//
//  RMAttachment.swift
//  Repairman
//
//  Created by Shebin Koshy on 16/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit
import MobileCoreServices

class RMAttachment: NSObject {
    
    public enum AttachmentType: Int {
        case Document = 1
        case Photo = 2
        case Video = 3
        case Cancel = 4
    }
    
    public typealias PickerActionHandler = (AttachmentType, URL?, Data?) -> Swift.Void
    
    public static let sharedInstance = RMAttachment()
    
    var pickerActionCompletionHandler : PickerActionHandler?
    
    func showPickerActionSheet(showInViewController:UIViewController, pickerActionHandler:@escaping PickerActionHandler) {
        pickerActionCompletionHandler = pickerActionHandler
        RMAlert.showActionSheet(title: nil, showInViewController: showInViewController, defaultButtonNames: [RMStrings.CAMERA_STRING(),RMStrings.PHOTO_AND_VIDEO_LIBRARY_STRING(), RMStrings.DOCUMENT_STRING()], destructiveButtonNames: nil, cancelButtonNames: [RMStrings.CANCEL_STRING()]) { [weak self] (selectedButton:String) in
            guard let strongSelf = self else {
                return
            }
            if selectedButton == RMStrings.CANCEL_STRING() {
                return
            }
            
            if selectedButton == RMStrings.DOCUMENT_STRING() {
                strongSelf.showDocumentPicker(showInViewController: showInViewController)
            }
            
            if selectedButton == RMStrings.CAMERA_STRING() {
                
//                return;//FIXME: only work in real device
                
                strongSelf.showImagePicker(showInViewController: showInViewController, isCamera: true)
            }
            
            if selectedButton == RMStrings.PHOTO_AND_VIDEO_LIBRARY_STRING() {
                strongSelf.showImagePicker(showInViewController: showInViewController, isCamera: false)
            }
        }
    }
    
    func showDocumentPicker(showInViewController:UIViewController) {
//        return
        
        
//        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.item"], in: .import)
        let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypePDF as String], in: .import)
        documentPicker.delegate = self
        if #available(iOS 11.0, *) {
            documentPicker.allowsMultipleSelection = false
        } else {
            // Fallback on earlier versions
        }
        showInViewController.present(documentPicker, animated: false, completion: nil)
    }

    
    func showImagePicker(showInViewController:UIViewController,isCamera:Bool) {
        let picker = UIImagePickerController()
        picker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        picker.allowsEditing = true
        if isCamera == true {
            picker.sourceType = .camera
        } else {
            picker.sourceType = .photoLibrary
        }
        showInViewController.present(picker, animated: true, completion: nil)
    }
    
    func dataFromURL(url:URL) -> Data? {
        var documentData : Data?
        do {
        documentData = try Data(contentsOf: url as URL)
        } catch {
        print("Unable to load data: \(error)")
        }
        //FIXME: Compress data
        return documentData
    }
    
}

extension RMAttachment: UIDocumentPickerDelegate, UINavigationControllerDelegate {
    @available(iOS 8.0, *)
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let documentData = dataFromURL(url: url)
        pickerActionCompletionHandler?(.Document, url, documentData)
    }
    
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        if urls.count == 0 {
            return;
        }
        let url = urls[0]
        let documentData = dataFromURL(url: url)
        pickerActionCompletionHandler?(.Document, url, documentData)
    }

    
    
    // called if the user dismisses the document picker without selecting a document (using the Cancel button)
    @available(iOS 8.0, *)
     public func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        pickerActionCompletionHandler?(.Cancel,nil, nil)
    }
}


extension RMAttachment: UIImagePickerControllerDelegate{
    @available(iOS 2.0, *)
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        DispatchQueue.main.async {
            let type = info[UIImagePickerControllerMediaType] as? String
            let mediaURL = info[UIImagePickerControllerMediaURL] as? URL
            var image: UIImage? = info[UIImagePickerControllerEditedImage] as? UIImage
            if image == nil {
                image = info[UIImagePickerControllerOriginalImage] as? UIImage
            }
            
            
            if type == kUTTypeImage as String {
                
                
                let documentData = UIImagePNGRepresentation(image!)!
                
    //            let documentData = dataFromURL(url: mediaURL as! URL)
                self.pickerActionCompletionHandler?(.Photo,mediaURL, documentData)
            }
            else {
                let documentData = self.dataFromURL(url: mediaURL!)
                self.pickerActionCompletionHandler?(.Video,mediaURL, documentData)
            }
        }
    }
    
    @available(iOS 2.0, *)
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        pickerActionCompletionHandler?(.Cancel,nil, nil)
    }
}
