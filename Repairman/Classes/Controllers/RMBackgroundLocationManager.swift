//
//  RMBackgroundLocationManager.swift
//  Repairman
//
//  Created by Shebin Koshy on 10/03/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation

@objc  protocol RMBackgroundLocationDelegate : NSObjectProtocol {
    @objc optional func backgroundLocationDidUpdateLocations(backgroundLocation:RMBackgroundLocationManager, currentLatitude:Double, currentLongitude:Double)
    @objc optional func backgroundLocationAuthorizationStateChanged(backgroundLocation:RMBackgroundLocationManager)
}

class RMBackgroundLocationManager:NSObject, CLLocationManagerDelegate{
    
    public static let sharedInstance = RMBackgroundLocationManager()
    private var delegates : NSHashTable<RMBackgroundLocationDelegate>?
    public typealias AddressCompletionHandler = (String, Error?) -> Swift.Void
    //        var prevLat : Double?
    //        var prevLong : Double?
    //        var prevAddress : String?
    var locationManager : CLLocationManager?
    //        var lastLocationUpdatedDate : Date?
    
    //        func clearLocationDetails()  {
    //            prevLat = nil
    //            prevLong = nil
    //            prevAddress = nil
    //            lastLocationUpdatedDate = nil
    //        }
    
    public func addDelegate(delegate:RMBackgroundLocationDelegate!){
        if(delegates == nil){
            delegates = NSHashTable.weakObjects()
        }
        
        
        //            //NEED TO FIX
        //            if prevLong != nil && prevLat != nil /*&& isRespond == true*/ {
        //                delegate!.backgroundLocationDidUpdateLocations!(backgroundLocation: self, currentLatitude: prevLat!, currentLongitude: prevLong!)
        //            }
        weak var weakDelegate = delegate
        delegates?.add(weakDelegate)
    }
    
    public func fetchCurrentLocation(){
        
        // For use in foreground
        if self.locationManager != nil && CLLocationManager.locationServicesEnabled() == true {
            
            if RMBackgroundLocationManager.isBackgroundLocationFetchPossible() == true && UIApplication.shared.applicationState == .background {
                //                    self.locationManager?.stopUpdatingLocation()
                self.locationManager?.startMonitoringSignificantLocationChanges()
                self.locationManager?.startUpdatingLocation()
                //                self.locationManager?.startUpdatingLocation()
            }
            else {
                self.locationManager?.stopMonitoringSignificantLocationChanges()
                self.locationManager?.startUpdatingLocation()
                //                    self.locationManager?.startUpdatingLocation()
            }
            return
        }
        self.locationManager = CLLocationManager()
        if #available(iOS 11.0, *) {
            /**
             In iOS 11 or above, app should first ask requestWhenInUseAuthorization. after accepting that only we can ask for requestAlwaysAuthorization
             */
            self.locationManager?.requestWhenInUseAuthorization()
        }
        self.locationManager?.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager?.delegate = self //as CLLocationManagerDelegate
            
            
            //FIXME: Need to uncomment below two
            //                locationManager?.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
//            locationManager?.distanceFilter = 3000
            locationManager?.distanceFilter = 10
            locationManager?.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            self.locationManager?.startMonitoringSignificantLocationChanges()
                self.locationManager?.startUpdatingLocation()
            
            
        }
    }
    
    public class func isLocationServicesEnabled() -> Bool {
        return CLLocationManager.locationServicesEnabled()
    }
    
    public class func isBackgroundLocationFetchPossible() -> Bool {
        if isLocationServicesEnabled() == false {
            return false
        }
        if isAuthorizedLocationAlways() == false {
            return false
        }
        if CLLocationManager.significantLocationChangeMonitoringAvailable() == false {
            // The service is not available.
            return false
        }
        return true
    }
    
    public func invokeAllDidUpdateLocationsDelegate(latitude:Double, longitude:Double) {
        let enumerator = delegates?.objectEnumerator()
        while let delgt: RMBackgroundLocationDelegate = enumerator?.nextObject() as? RMBackgroundLocationDelegate {
            
            delgt.backgroundLocationDidUpdateLocations?(backgroundLocation: self, currentLatitude: latitude, currentLongitude: longitude)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        RMLocalNotification.showLocalNotification(text: "BackJai")
        
        if UIApplication.shared.applicationState == .background {
            
//            manager.stopUpdatingLocation()
//            manager.startMonitoringSignificantLocationChanges()
        }
        if manager.location == nil {
            return
        }
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        //            if prevLat == locValue.latitude && prevLong == locValue.longitude {
        //                return
        //            }
        invokeAllDidUpdateLocationsDelegate(latitude: locValue.latitude, longitude: locValue.longitude)
        //            prevLat = locValue.latitude
        //            prevLong = locValue.longitude
        //            lastLocationUpdatedDate = manager.location?.timestamp
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        // Notify the user of any errors.
    }
    
    //        func stopLocationFetch() {
    //            locationManager?.stopUpdatingLocation()
    //            locationManager = nil
    //        }
    
    func stopLocationMonitoring(){
        locationManager?.stopMonitoringSignificantLocationChanges()
    }
    
    public class func islocationAuthorized() -> Bool {
        let status = CLLocationManager.authorizationStatus()
        if status == CLAuthorizationStatus.authorizedWhenInUse || status == CLAuthorizationStatus.authorizedAlways {
            return true
        }
        else
        {
            return false
        }
    }
    
    public class func isAuthorizedLocationWhenInUse() -> Bool {
        let status = CLLocationManager.authorizationStatus()
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            return true
        }
        return false
    }
    
    public class func isAuthorizedLocationAlways() -> Bool {
        let status = CLLocationManager.authorizationStatus()
        if status == CLAuthorizationStatus.authorizedAlways {
            return true
        }
        return false
    }
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus){
        
        DispatchQueue.main.async {
            if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
                self.locationManager?.requestAlwaysAuthorization()
            }
        }
        let enumerator = delegates?.objectEnumerator()
        for delgt in enumerator! {
            (delgt as! RMBackgroundLocationDelegate).backgroundLocationAuthorizationStateChanged!(backgroundLocation: self)
        }
        //        SWErrorBanner.invokeErrorBannerProtocol()
        
    }
    
    public func addressFrom(latitude: Double, longitude: Double, completionHandler: @escaping AddressCompletionHandler) {
        
        //            if prevLat == latitude && prevLong == longitude {
        //
        //                if prevAddress != nil {
        //                    completionHandler(prevAddress!, nil)
        //                }
        //
        //            }
        
        let geocoder = CLGeocoder.init()
        let location = CLLocation(latitude: latitude,longitude: longitude)
        
        var strAdd : String?
        geocoder.reverseGeocodeLocation(location, completionHandler: { [weak self] placemarks,error in
            if (placemarks == nil)
            {
                return
            }
            if(error != nil)
            {
                return
            }
            
            if (error == nil && (placemarks?.count)! > 0)
            {
                let placemark = placemarks?.last;
                
                
                if (placemark?.subThoroughfare?.characters.count != 0){
                    strAdd = placemark?.subThoroughfare;
                }
                
                if (placemark?.thoroughfare?.characters.count != 0)
                {
                    if (strAdd != nil && strAdd?.characters.count != 0)
                    {
                        
                        strAdd = String(format:"\(strAdd!), \(placemark!.thoroughfare!)")
                    }
                    else
                    {
                        strAdd = placemark?.thoroughfare;
                    }
                    
                }
                
                if (placemark?.postalCode != nil && placemark?.postalCode?.characters.count != 0)
                {
                    if (strAdd != nil && strAdd?.characters.count != 0)
                    {
                        strAdd = String(format:"\(strAdd!), \(placemark!.postalCode!)")
                    }
                    else
                    {
                        strAdd = placemark?.postalCode;
                    }
                }
                
                if (placemark?.locality?.characters.count != 0)
                {
                    if (strAdd != nil && strAdd?.characters.count != 0)
                    {
                        strAdd = String(format:"\(strAdd!), \(placemark!.locality!)")
                    }
                    else
                    {
                        strAdd = placemark?.locality;
                    }
                }
                
                
                if (placemark?.administrativeArea?.characters.count != 0)
                {
                    if (strAdd != nil && strAdd?.characters.count != 0)
                    {
                        strAdd = String(format:"\(strAdd!), \(placemark!.administrativeArea!)")
                    }
                    else
                    {
                        strAdd = placemark?.administrativeArea;
                    }
                }
                
                if (placemark?.country?.characters.count != 0)
                {
                    if (strAdd != nil && strAdd?.characters.count != 0)
                    {
                        strAdd = String(format:"\(strAdd!), \(placemark!.country!)")
                    }
                    else
                    {
                        strAdd = placemark?.country;
                    }
                }
            }
            //                self?.prevAddress = strAdd
            completionHandler(strAdd!, nil)
        })
        
    }
    
    deinit {
        print("RMBackgrounLocationManager deallocated")
    }
}

