//
//  SWTableView.swift
//  StreetWall
//
//  Created by Shebin Koshy on 13/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit

extension UITableView {
    
    func register<T: UITableViewCell>(_: T.Type) where T: ReusableView {
        register(T.self, forCellReuseIdentifier: T.defaultReuseIdentifier)
//        registerClass(T.self, forCellWithReuseIdentifier: T.defaultReuseIdentifier)
        }
        
        func register<T: UITableViewCell>(_: T.Type) where T: ReusableView, T: NibLoadableView {
            let bundle = Bundle(for: T.self)
            let nib = UINib(nibName: T.nibName, bundle: bundle)
//            let nib = UINib(T.nibName, bundle: bundle)
            register(nib, forCellReuseIdentifier: T.defaultReuseIdentifier)
//            registerNib(nib, forCellWithReuseIdentifier: T.defaultReuseIdentifier)
        }
        
        func dequeueReusableCell<T: UITableViewCell>(forIndexPath indexPath: IndexPath) -> T where T: ReusableView {
            guard let cell = dequeueReusableCell(withIdentifier: T.defaultReuseIdentifier, for: indexPath) as? T else {
                fatalError("Could not dequeue cell with identifier: \(T.defaultReuseIdentifier)")
            }
            
            return cell
        }    
}
