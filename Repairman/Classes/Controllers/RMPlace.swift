//
//  RMPlace.swift
//  Repairman
//
//  Created by Shebin Koshy on 10/03/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMPlace: NSObject,NSCoding {
    
    var date:Date?
    var name:String?
    var lat: Double?
    var longi: Double?
    var stateOfApp: Int?
    
    
    required init(pDate:Date,pName:String,pLat:Double,pLong:Double) {
        super.init()
        date = pDate
        name = pName
        lat = pLat
        longi = pLong
        stateOfApp = UIApplication.shared.applicationState.rawValue
    }
    
    
    func latLongString() -> String {
        return "lat = \(lat!) long = \(longi!)";
    }
    
    func dateString() -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .full
        let stringDate = formatter.string(from: date!)
        return stringDate;
    }
    
    func stateApp() -> String {
        let appState:UIApplicationState = UIApplicationState(rawValue: stateOfApp!)!
        if appState == .active {
            return "act";
        }
        if appState == .background {
            return "back";
        }
        if appState == .inactive {
            return "inactive";
        }
        return "Unkwn"
        
//        case active
//
//        case inactive
//
//        case background
    }
    
    
    //    required init(n:String, a:Int) {
    //
    //        name = n
    //        age = a
    //    }
    //
    
    required init(coder aDecoder: NSCoder) {
        date = aDecoder.decodeObject(forKey: "d") as? Date
        name = aDecoder.decodeObject(forKey: "name") as? String
        lat = aDecoder.decodeObject(forKey: "lat") as? Double
        longi = aDecoder.decodeObject(forKey: "long") as? Double
        stateOfApp = aDecoder.decodeObject(forKey: "stateOfApp") as? Int
    }
    
    
    public func encode(with aCoder: NSCoder) {
        
        aCoder.encode(name, forKey: "name")
        aCoder.encode(date, forKey: "d")
        aCoder.encode(lat, forKey: "lat")
        aCoder.encode(longi, forKey: "long")
        aCoder.encode(stateOfApp, forKey: "stateOfApp")
    }
    
    
    
}
