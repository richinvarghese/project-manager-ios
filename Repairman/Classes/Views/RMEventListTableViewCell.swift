//
//  RMEventListTableViewCell.swift
//  Repairman
//
//  Created by Shebin Koshy on 02/05/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMEventListTableViewCell: UITableViewCell {

    @IBOutlet weak var labelEventName: UILabel!
    @IBOutlet weak var labelEventDate: UILabel!
    @IBOutlet weak var viewEncloseAll: UIView!
//    @IBOutlet weak var labelEventTime: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backgroundColor = UIColor.brown
        labelEventName.textColor = RMGlobalManager.appBlueColor()
        labelEventDate.textColor = UIColor.white
        labelEventDate.backgroundColor = RMGlobalManager.appBlueColor()
        labelEventDate.layer.borderColor = UIColor.white.cgColor
        labelEventDate.layer.borderWidth = 2
        viewEncloseAll.backgroundColor = UIColor(red: 233, green: 163, blue: 166)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func set(event:RMEvent, selectedDate:String?){
        labelEventName.text = event.eventTitle
        labelEventDate.text = selectedDate
//        labelEventTime.text = "Time"
    }
    
}
