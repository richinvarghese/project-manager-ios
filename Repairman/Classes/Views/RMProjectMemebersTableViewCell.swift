//
//  RMProjectMemebersTableViewCell.swift
//  Repairman
//
//  Created by Shebin Koshy on 18/01/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMProjectMemebersTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionViewMembers:UICollectionView!
    
    private var arrayProjectMembers: [RMProjectMember]?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionViewMembers.register(RMMembersCollectionViewCell.self)
        //        collectionView.dataSource = self
        //        collectionView.delegate = self
        collectionViewMembers.delegate = self
        collectionViewMembers.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setProjectMembers(projectMembers:[RMProjectMember]) {
        arrayProjectMembers = projectMembers
        collectionViewMembers.reloadData()
    }
    
    func showingProjectMember(atIndexPath:IndexPath) -> RMProjectMember {
        //        let array = displayingArray()
        return arrayProjectMembers![atIndexPath.row]
    }
    
    
}


extension RMProjectMemebersTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if arrayProjectMembers == nil {
            return 0
        }
        return arrayProjectMembers!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:RMMembersCollectionViewCell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as RMMembersCollectionViewCell
        let projectMember = showingProjectMember(atIndexPath: indexPath)
        cell.labelMemeberName.text = projectMember.memberName
        if projectMember.memberAvatarImageUrl != nil {
            cell.memberAvatarImageView.sd_setImage(with: URL(string: projectMember.memberAvatarImageUrl!), placeholderImage: UIImage(named: "AvatarPlaceholder"))
        } else {
            cell.memberAvatarImageView.image = UIImage(named: "AvatarPlaceholder")
        }
        cell.labelJobTitle.text = projectMember.memberJobTitle
//        if collectionView == collectionView2 {
//            cell.managa = 2
//        }
        //        cell.contentView.backgroundColor = UIColor.red
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cell = cell as! RMMembersCollectionViewCell
        print("\(cell.memberAvatarImageView.frame)")
        //        cell.memberAvatarImageView.layer.cornerRadius = CGFloat(roundf(Float(cell.memberAvatarImageView
        //            .frame.size.width/2.0)))
        //        cell.memberAvatarImageView.layer.masksToBounds = true
        //        cell.memberAvatarImageView.clipsToBounds = true
    }
    //    size
}

extension RMProjectMemebersTableViewCell:UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let width = UIScreen.main.bounds.size.width - 16 - 16 - 16 - 16 //16 for viewEncloseTableView, 16 for collectionView
        let width = RMProjectMemebersTableViewCell.collectionViewCellHieght() - 20
        return CGSize(width: width, height: width)
    }
    
    class func collectionViewCellHieght() -> CGFloat {
        let width = UIScreen.main.bounds.size.width - 16 - 16 - 16 - 16 //16 for viewEncloseTableView, 16 for collectionView
        return width/2
    }
    
    //    @available(iOS 6.0, *)
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    //
    //    @available(iOS 6.0, *)
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    //
    //    @available(iOS 6.0, *)
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    //
    //    @available(iOS 6.0, *)
    //    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
    //
    //    }
    //
    //    @available(iOS 6.0, *)
    //     public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize
}

