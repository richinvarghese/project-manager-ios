//
//  RMProjectBasicListTableViewCell.swift
//  Repairman
//
//  Created by Shebin Koshy on 20/01/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

protocol RMProjectBasicListTableViewCellDelegate: class {
    func buttonActionLeaveProject(cell:RMProjectBasicListTableViewCell)
}

class RMProjectBasicListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelProjectName: UILabel!
    @IBOutlet weak var viewEncloseProgress: UIView!
    @IBOutlet weak var labelProgress: UILabel!
    @IBOutlet weak var labelCompanyName: UILabel!
    @IBOutlet weak var labelSatus: UILabel!
    var circleProgress : CircleProgressBar?
    weak var delegate: RMProjectBasicListTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        circleProgress = CircleProgressBar(frame: CGRect(x:0, y:0, width:viewEncloseProgress.frame.size.width,height:viewEncloseProgress.frame.size.height))
        viewEncloseProgress.addSubview(circleProgress!)
        circleProgress!.progressBarWidth = 5
        circleProgress!.progressBarProgressColor = UIColor.green
        circleProgress!.progressBarTrackColor = UIColor.lightGray
        //        circleProgress.layer.borderColor = UIColor.red.cgColor
        //        circleProgress.layer.borderWidth = 2
        circleProgress!.setProgress(0.0, animated: true)
        circleProgress!.hintHidden = true
        circleProgress!.hintTextFont = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
        circleProgress!.startAngle = 180
        viewEncloseProgress.backgroundColor = UIColor.clear
        circleProgress!.backgroundColor = UIColor.clear
        labelProgress.text = "0%"
        labelProgress.textColor = UIColor.green
        labelSatus.layer.borderWidth = 1
        labelSatus.layer.cornerRadius = labelSatus.frame.size.height/2
        labelSatus.clipsToBounds = true
        labelSatus.textAlignment = .center
//        labelProgress.font = UIFont.systemFont(ofSize: 25)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class func progressString(progress:String?) -> String{
        if progress == nil {
            return "0"
        }
        let array = progress!.components(separatedBy: ".")
        var stringProgress : String?
        if array.count > 1 {
            let double = Double(progress!)
            stringProgress = String(format: "%.1f", double!)
        } else {
            stringProgress = progress!
        }
        if stringProgress == nil {
            stringProgress = "0"
        }
        return stringProgress!
    }
    
    class func floatPogress(stringProgress:String?) -> CGFloat {
        if stringProgress == nil {
            return 0
        }
        if let doubleValue = Double(stringProgress!) {
            let cgFloat = CGFloat(doubleValue)
            return cgFloat/100
        }
        return 0
    }
    
    @IBAction func buttonActionForLeaveProject(_ sender: Any) {
        self.delegate?.buttonActionLeaveProject(cell: self)
    }
    
    
    
    func setBasicProject(project:RMProject) {
        if project.projectTitle == nil {
            labelProjectName.text = ""
        } else {
            labelProjectName.text = "Project: \(project.projectTitle!)"
        }
        
        if project.projectCompanyName == nil {
            labelCompanyName.text = ""
        } else {
            labelCompanyName.text = "Client: \(project.projectCompanyName!)"
        }
        
        labelSatus.text = project.projectStatus
        
        
//        let array = project.projectProgress?.components(separatedBy: ".")
        let stringProgress = RMProjectBasicListTableViewCell.progressString(progress: project.projectProgress)
//        if array != nil && array!.count > 1 {
//            let double = Double(project.projectProgress!)
//            stringProgress = String(format: "%.1f", double!)
//        } else {
//            stringProgress = project.projectProgress
//        }
        labelProgress.text = ("\(stringProgress)%")
//        if let doubleValue = Double(project.projectProgress!) {
            let cgFloat = RMProjectBasicListTableViewCell.floatPogress(stringProgress: project.projectProgress)
        
            circleProgress!.setProgress(cgFloat, animated: true)
//        }
        labelSatus.layer.borderColor = project.projectStatusColor().cgColor
        labelSatus.textColor = project.projectStatusColor()
    }
    
}
