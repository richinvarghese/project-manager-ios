//
//  RMMessageListTableViewCell.swift
//  Repairman
//
//  Created by Shebin Koshy on 18/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit


protocol RMMessageListTableViewCellDelegate: class {
    func buttonActionReply(cell:RMMessageListTableViewCell)
}

class RMMessageListTableViewCell: UITableViewCell {

    @IBOutlet weak var labelReplyStatus: UILabel!
    @IBOutlet weak var labelMessageOwnerName: UILabel!
    @IBOutlet weak var labelMessageSubject: UILabel!
//    @IBOutlet weak var buttonStatus: UIButton!
    @IBOutlet weak var constraintBetweenReplyStatusAndSubject: NSLayoutConstraint!
    
    @IBOutlet weak var constraintReplyButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintReplyLabelWidth: NSLayoutConstraint!
    @IBOutlet weak var labelMessageCreated: UILabel!
    @IBOutlet weak var imageViewMessageOwnerAvatar: UIImageView!
    
    weak var delegate : RMMessageListTableViewCellDelegate?
    
//    weak var delegate:RMTaskListTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageViewMessageOwnerAvatar.layer.cornerRadius = imageViewMessageOwnerAvatar.frame.size.width/2
        imageViewMessageOwnerAvatar.clipsToBounds = true
//        buttonStatus.layer.cornerRadius = buttonStatus.frame.size.height/2
//        buttonStatus.clipsToBounds = true
//        buttonStatus.setTitleColor(UIColor.white, for: .normal)
        //        buttonStatus.titleEdgeInsets = UIEdgeInsets(top: -0,
        //                                                    left: -18,
        //                                                    bottom: -0,
        ////                                                    right: -18)
        labelMessageCreated.textColor = UIColor.lightGray
        
        if #available(iOS 8.2, *) {
            labelMessageOwnerName.font = UIFont.systemFont(ofSize: labelMessageOwnerName.font.pointSize, weight: .medium)
        } else {
            // Fallback on earlier versions
            labelMessageOwnerName.font = UIFont.boldSystemFont(ofSize: labelMessageOwnerName.font.pointSize)
        }
        //        labelTaskTitle.font = UIFont(name: "Helvetica-Semibold", size: labelTaskTitle.font.pointSize)
        labelReplyStatus.backgroundColor = UIColor(red: 76/255, green: 176/255, blue: 147/255, alpha: 1)
        labelReplyStatus.layer.cornerRadius = 2.5
        labelReplyStatus.textAlignment = .center
        labelReplyStatus.clipsToBounds = true
        labelReplyStatus.textColor = UIColor.white
//        labelReplyStatus.set
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
//    @IBAction func buttonActionChangeStatus(_ sender: Any) {
//        delegate?.buttonActionChangeStatus(cell: self)
//    }
    @IBAction func buttonActionForReply(_ sender: Any) {
        delegate?.buttonActionReply(cell: self)
    }
    
    func setMessage(message:RMMessage){
        labelMessageSubject.text = message.messageSubject
//        labelTaskDescription.text = task.taskDescription
        labelMessageOwnerName.text = message.messageOwnerName
//        labelMessageCreated.text = message.messageCreated
//        buttonStatus.setTitle(task.taskStatus, for: .normal)
        //        if #available(iOS 9.0, *) {
        //            buttonStatus.widthAnchor.constraint(equalToConstant: buttonStatus.titleLabel!.intrinsicContentSize.width + 5 * 2.0).isActive = true
        //        } else {
        //            // Fallback on earlier versions
        //        }
        
        
//        buttonStatus.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        
        //        buttonStatus.setTitleColor(task.taskStatusColor(), for: .normal)
        //        buttonStatus.setba
//        buttonStatus.backgroundColor = task.taskStatusColor()
        if message.messageOwnerAvatarImageUrl != nil {
            imageViewMessageOwnerAvatar.sd_setImage(with: URL(string: message.messageOwnerAvatarImageUrl!), placeholderImage: UIImage(named: "AvatarPlaceholder"))
        } else {
            imageViewMessageOwnerAvatar.image = UIImage(named: "AvatarPlaceholder")
        }
        
        if message.messageCreatedDate != nil {
            labelMessageCreated.text = RMGlobalManager.stringFromDate(date: message.messageCreatedDate!, isWithTime: true)
        } else {
            labelMessageCreated.text = ""
        }
        
        if message.isReply ==  true {
            labelReplyStatus.text = RMStrings.REPLY_STRING()
            labelReplyStatus.layer.cornerRadius = 2.5
            constraintBetweenReplyStatusAndSubject.constant = 8
            constraintReplyLabelWidth.constant = 50
        } else {
            labelReplyStatus.text = nil
            labelReplyStatus.layer.cornerRadius = 0
            constraintBetweenReplyStatusAndSubject.constant = 0
            constraintReplyLabelWidth.constant = 0
        }
        
        if message.isMyOwnPost == true {
            constraintReplyButtonWidth.constant = 0
        } else {
            constraintReplyButtonWidth.constant = 60
        }
//        labelMessageCreated.text = message.taskAssignedUser
        
        
        //labelTaskDescription.text = RMDummy.lorem()
        
    }
    
}
