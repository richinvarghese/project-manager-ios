//
//  RMMessageBaseTableViewCell.swift
//  Repairman
//
//  Created by Shebin Koshy on 20/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

protocol RMMessageBaseTableViewCellDelegate: class {
    func buttonActionAttachment(cell:RMMessageBaseTableViewCell)
}


class RMMessageBaseTableViewCell: UITableViewCell {
    
    @IBOutlet weak var contraintAttachmentWidth: NSLayoutConstraint!
    @IBOutlet weak var buttonAttachment: UIButton!
    @IBOutlet weak var labelMessageOwnerName: UILabel!
    @IBOutlet weak var labelMessageSubject: UILabel!
    @IBOutlet weak var labelMessageCreated: UILabel!
    @IBOutlet weak var labelMessageDescription: UILabel!
    @IBOutlet weak var imageViewMessageOwnerAvatar: UIImageView!
    weak var delegate:RMMessageBaseTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageViewMessageOwnerAvatar.layer.cornerRadius = imageViewMessageOwnerAvatar.frame.size.width/2
        imageViewMessageOwnerAvatar.clipsToBounds = true
        
        
        labelMessageCreated.textColor = UIColor.lightGray
        
        if #available(iOS 8.2, *) {
            labelMessageOwnerName.font = UIFont.systemFont(ofSize: labelMessageOwnerName.font.pointSize, weight: .medium)
        } else {
            // Fallback on earlier versions
            labelMessageOwnerName.font = UIFont.boldSystemFont(ofSize: labelMessageOwnerName.font.pointSize)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func buttonActionForAttachment(_ sender: Any) {
        delegate?.buttonActionAttachment(cell: self)
    }
    
    func setMessage(message:RMMessage){
        labelMessageSubject.text = message.messageSubject
        labelMessageDescription.text = message.messageDescription
        if message.messageOwnerAvatarImageUrl != nil {
            imageViewMessageOwnerAvatar.sd_setImage(with: URL(string: message.messageOwnerAvatarImageUrl!), placeholderImage: UIImage(named: "AvatarPlaceholder"))
        } else {
            imageViewMessageOwnerAvatar.image = UIImage(named: "AvatarPlaceholder")
        }
        
        if message.messageCreatedDate != nil {
            labelMessageCreated.text = RMGlobalManager.stringFromDate(date: message.messageCreatedDate!, isWithTime: true)
        } else {
            labelMessageCreated.text = ""
        }
        
        if message.isReply ==  true {
            labelMessageSubject.isHidden = true
        } else {
            labelMessageSubject.isHidden = false
        }
        
        if message.isMyOwnPost == true {
            labelMessageOwnerName.text = RMStrings.ME_STRING()
        } else {
            labelMessageOwnerName.text = message.messageOwnerName
        }
        
        if message.attachment == nil {
            contraintAttachmentWidth.constant = 0
        } else {
            contraintAttachmentWidth.constant = 30
        }
        
    }
    
}
