//
//  RMSubTimeLineListTableViewCell.swift
//  Repairman
//
//  Created by Shebin Koshy on 16/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit
import SDWebImage

class RMSubTimeLineListTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewAvatar: UIImageView!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelTimeLinePostedTime: UILabel!
    @IBOutlet weak var labelTimeLineDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelTimeLineDescription.numberOfLines = 0
        imageViewAvatar.layer.cornerRadius = imageViewAvatar.frame.size.width/2
        imageViewAvatar.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTimeLine(timeLine:RMTimeLine) {
        labelTimeLineDescription.text = timeLine.timeLineDescription
        labelUserName.text = timeLine.timeLineOwnerName
        //        imageViewAvatar.image =
        var imageURL:URL?
        if timeLine.timeLineOwnerImageURL == nil {
            imageViewAvatar.image = UIImage(named:"AvatarPlaceholder")
        } else {
            imageURL = URL(string: timeLine.timeLineOwnerImageURL!)
            
        }
        
        if imageURL != nil {
            imageViewAvatar.sd_setImage(with: imageURL!, placeholderImage: UIImage(named: "AvatarPlaceholder"))
        } else {
            imageViewAvatar.image = UIImage(named:"AvatarPlaceholder")
        }
        
        if timeLine.timeLinePostedDate != nil {
            labelTimeLinePostedTime.text = RMGlobalManager.timeStringFromDate(date: timeLine.timeLinePostedDate!)
        } else {
            labelTimeLinePostedTime.text = ""
        }
        
        
    }
    
}
