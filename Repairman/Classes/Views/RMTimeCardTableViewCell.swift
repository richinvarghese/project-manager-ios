//
//  RMTimeCardTableViewCell.swift
//  Repairman
//
//  Created by Shebin Koshy on 05/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMTimeCardTableViewCell: UITableViewCell {

    @IBOutlet weak var labelInDate: UILabel!
    @IBOutlet weak var labelInTime: UILabel!
    @IBOutlet weak var labelOutTime: UILabel!
    @IBOutlet weak var labelOutDate: UILabel!
    @IBOutlet weak var labelDuration: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTimeCard(timeCard: RMTimeCard) {
        labelInDate.text = timeCard.timeCardInDate
        labelInTime.text = timeCard.timeCardInTime
        labelOutDate.text = timeCard.timeCardOutDate
        labelOutTime.text = timeCard.timeCardOutTime
        labelDuration.text = timeCard.timeCardDuration
    }
    
}
