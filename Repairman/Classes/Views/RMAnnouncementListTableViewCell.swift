//
//  RMAnnouncementListTableViewCell.swift
//  Repairman
//
//  Created by Shebin Koshy on 05/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMAnnouncementListTableViewCell: UITableViewCell {

//    announcementTitle: String?
//    var announcementCreatedByUsername:String?
//    var announcementStartDate: String?
//    var announcementEndDate: String?
    
    @IBOutlet weak var labelAnnouncementTitle: UILabel!
    @IBOutlet weak var labelAnnouncementCreatedByUsername: UILabel!
    @IBOutlet weak var labelAnnouncementStartDate: UILabel!
    @IBOutlet weak var labelAnnouncementEndDate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setAnnouncement(announcement: RMAnnouncement) {
        labelAnnouncementTitle.text = announcement.announcementTitle
        labelAnnouncementCreatedByUsername.text = announcement.announcementCreatedByUsername
        labelAnnouncementStartDate.text = announcement.announcementStartDate
        labelAnnouncementEndDate.text = announcement.announcementEndDate
    }
    
}
