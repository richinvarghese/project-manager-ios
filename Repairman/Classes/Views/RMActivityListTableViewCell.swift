//
//  RMActivityListTableViewCell.swift
//  Repairman
//
//  Created by Shebin Koshy on 21/01/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMActivityListTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewUserAvatarImage: UIImageView!
    @IBOutlet weak var labelAction: UILabel!
    @IBOutlet weak var labelCreatedDate: UILabel!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelPriority: UILabel!
    @IBOutlet weak var labelPreviousStatus: UILabel!
    @IBOutlet weak var labelCurrentStatus: UILabel!
    @IBOutlet weak var labelPlaceholderPriority: UILabel!
    @IBOutlet weak var labelPlaceholderStatus: UILabel!
    @IBOutlet weak var viewHoldingStatus: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelPlaceholderStatus.text = "\u{2022} Status:"
        labelPlaceholderPriority.text = "\u{2022} Priority:"
        labelAction.layer.cornerRadius = 2.5
        labelAction.textAlignment = .center
        labelAction.clipsToBounds = true
        labelAction.textColor = UIColor.white
        imageViewUserAvatarImage.layer.cornerRadius = imageViewUserAvatarImage.frame.size.width/2
        imageViewUserAvatarImage.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setActivity(activity:RMActivity) {
        labelTitle.text = activity.activityTitle
        labelCreatedDate.text = ""
        if activity.activityCreatedDate != nil {
            let date1 = RMGlobalManager.dateFromWebServiceString(stringDate: activity.activityCreatedDate!)
            if date1 != nil {
                labelCreatedDate.text = RMGlobalManager.shortDateAndTimeString(date: date1!)
            }
        }
        
        if activity.activityUserAvatarImageUrl != nil {
            imageViewUserAvatarImage.sd_setImage(with: URL(string: activity.activityUserAvatarImageUrl!), placeholderImage: UIImage(named: "AvatarPlaceholder"))
        } else {
            imageViewUserAvatarImage.image = UIImage(named: "AvatarPlaceholder")
        }
        
        labelAction.text = activity.getActivityAction()
        labelAction.backgroundColor = activity.activityActionColor()
        labelAction.layer.cornerRadius = 0.5
        labelAction.clipsToBounds = true
        labelUserName.text = activity.activityUserName
        if activity.isActionTypeUpdated() == true {
            labelPriority.text = activity.activityPriority
            viewHoldingStatus.isHidden = false
            labelCurrentStatus.text = activity.activityCurrentStatus
//            labelPreviousStatus.text = activity.activityPreviousStatus
            labelCurrentStatus.textColor = activity.currentStatusColor()
            if activity.activityPreviousStatus != nil {
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: activity.activityPreviousStatus!)
                attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
                labelPreviousStatus.attributedText = attributeString
            } else {
                labelPreviousStatus.text = ""
            }
            
        } else {
            viewHoldingStatus.isHidden = true
        }
        
    }
    
}
