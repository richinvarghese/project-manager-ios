//
//  RMLeaveListTableViewCell.swift
//  Repairman
//
//  Created by Shebin Koshy on 24/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

protocol RMLeaveListTableViewCellDelegate: class {
    func buttonActionCancel(cell:RMLeaveListTableViewCell)
}

class RMLeaveListTableViewCell: UITableViewCell {
    
//    @IBOutlet weak var contraintAttachmentWidth: NSLayoutConstraint!
    @IBOutlet weak var buttonCancel: UIButton!
    @IBOutlet weak var labelLeaveType: UILabel!
    @IBOutlet weak var labelLeaveDate: UILabel!
    @IBOutlet weak var labelLeaveDuration: UILabel!
    @IBOutlet weak var labelLeaveReason: UILabel!
    @IBOutlet weak var labelLeaveStatus: UILabel!
    weak var delegate:RMLeaveListTableViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        imageViewMessageOwnerAvatar.layer.cornerRadius = imageViewMessageOwnerAvatar.frame.size.width/2
//        imageViewMessageOwnerAvatar.clipsToBounds = true
        labelLeaveStatus.layer.cornerRadius = labelLeaveStatus.frame.size.height/2
        labelLeaveStatus.textAlignment = .center
        labelLeaveStatus.clipsToBounds = true
        labelLeaveStatus.textColor = UIColor.white
//        labelLeaveType.textColor = UIColor.white
//        labelLeaveType.layer.cornerRadius = labelLeaveType.frame.size.height/2
        
//        labelMessageCreated.textColor = UIColor.lightGray
//
//        if #available(iOS 8.2, *) {
//            labelMessageOwnerName.font = UIFont.systemFont(ofSize: labelMessageOwnerName.font.pointSize, weight: .medium)
//        } else {
//            // Fallback on earlier versions
//            labelMessageOwnerName.font = UIFont.boldSystemFont(ofSize: labelMessageOwnerName.font.pointSize)
//        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func buttonActionForCancel(_ sender: Any) {
        delegate?.buttonActionCancel(cell: self)
    }
    
    func setleave(leave:RMLeave){
        
//        labelLeaveType: UIL
//        labelLeaveDate: UIL
//        labelLeaveDuration:
//        labelLeaveReason: U
//        labelLeaveStatus: U

        
        labelLeaveType.text = leave.leaveTypeTitle
        if leave.leaveStartDate != nil && leave.leaveStartDate == leave.leaveEndDate {
            labelLeaveDate.text = leave.leaveStartDate
        } else if leave.leaveStartDate != nil && leave.leaveEndDate != nil {
            labelLeaveDate.text = "\(leave.leaveStartDate!) to \(leave.leaveEndDate!)"
        } else {
            labelLeaveDate.text = ""
        }
        
        labelLeaveDuration.text = "\(leave.leaveTotalDays!) Day (\(leave.leaveTotalHours!) Hour(s))"
        
        labelLeaveStatus.text = leave.leaveStatus
        labelLeaveReason.text = leave.leaveReason
        
        if leave.leaveStatus?.lowercased().contains("cancel") == true {
            buttonCancel.isHidden = true
        } else {
            buttonCancel.isHidden = false
        }
        
        labelLeaveStatus.backgroundColor = leave.colorLeaveStatus()
        labelLeaveType.textColor = leave.colorLeaveType()
        labelLeaveType.shadowColor = UIColor.lightGray
    }
    
}
