//
//  RMProjectTableViewCell.swift
//  Repairman
//
//  Created by Shebin Koshy on 18/01/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMProjectTableViewCell: UITableViewCell {
    
    @IBOutlet var arrayLines: [UIView]!
    @IBOutlet weak internal var labelProgressDoneTitle: UILabel!
    
    @IBOutlet weak internal var viewSquareProgressDone: UIView!

    @IBOutlet weak var projectProgress: UIProgressView!
    
    @IBOutlet weak var labelProjectName: UILabel!
    
    
//    @IBOutlet weak var viewEncloseProgress: UIView!
    @IBOutlet weak var labelProgress: UILabel!
    @IBOutlet weak var labelStartDate: UILabel!
    @IBOutlet weak var labelEndDate: UILabel!
    
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelCompanyName: UILabel!

    @IBOutlet weak var labelDescription: UILabel!

    @IBOutlet weak var labelSatus: UILabel!
    
    var circleProgress: CircleProgressBar?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelProgressDoneTitle.textColor = RMGlobalManager.appBlueColor()
        viewSquareProgressDone.backgroundColor = RMGlobalManager.appBlueColor()
        projectProgress.progressViewStyle = .bar
        projectProgress.progressTintColor = RMGlobalManager.appBlueColor()
        projectProgress.trackTintColor = UIColor.red
//         circleProgress = CircleProgressBar(frame: CGRect(x:0, y:0, width:viewEncloseProgress.frame.size.width,height:viewEncloseProgress.frame.size.height))
//        viewEncloseProgress.addSubview(circleProgress!)
//        circleProgress!.progressBarWidth = 15
//        circleProgress!.progressBarProgressColor = UIColor.green
//        circleProgress!.progressBarTrackColor = UIColor.lightGray
//        //        circleProgress.layer.borderColor = UIColor.red.cgColor
//        //        circleProgress.layer.borderWidth = 2
//        circleProgress!.setProgress(0.0, animated: true)
//        circleProgress!.hintHidden = true
//        circleProgress!.hintTextFont = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
//        circleProgress!.startAngle = 180
//        viewEncloseProgress.backgroundColor = UIColor.clear
//        circleProgress!.backgroundColor = UIColor.clear
        labelProgress.text = "0%"
        labelProgress.textColor = UIColor.green
        labelProgress.font = UIFont.systemFont(ofSize: 25)
        
        
        labelSatus.layer.borderWidth = 1
        labelSatus.layer.cornerRadius = labelSatus.frame.size.height/2
        labelSatus.clipsToBounds = true
        labelSatus.textAlignment = .center
        
        labelPrice.font = UIFont.boldSystemFont(ofSize: labelPrice.font.pointSize)
        
        for view in arrayLines {
            view.backgroundColor = UIColor.colorWithHexString(hex: "#162f3d")// UIColor(red: 22, green: 47, blue: 61)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setProject(project:RMProject) {
//        var projectTitle: String?
//        var projectDescription: String?
//        var projectSartDate: String?
//        var projectEndDate: String?
//        var projectStatus: String?
//        var projectPrice: String?
//        var projectCompanyName: String?
//        var projectCurrencySymbol: String?
//        var projectTotalPoints: String?
//        var projectCompletedPoints: String?
//        var projectUniqueId: String?
        labelProjectName.text = project.projectTitle
        labelDescription.text = project.projectDescription
        labelStartDate.text = project.projectSartDate
        labelEndDate.text = project.projectEndDate
        labelSatus.text = project.projectStatus
        if project.projectPrice != nil {
            labelPrice.text = "Price:\(project.projectPrice!)\(project.projectCurrencySymbol!)"
        } else {
            labelPrice.text = "Price:"
        }
        if project.projectCompanyName != nil {
            labelCompanyName.text = "Client:\(project.projectCompanyName!)"
        } else {
            labelCompanyName.text = "Client:"
        }
        
//        labelProgress.
        labelSatus.layer.borderColor = project.projectStatusColor().cgColor
        labelSatus.textColor = project.projectStatusColor()
        
//        project.projectProgress = "10"
        let stringProgress = RMProjectBasicListTableViewCell.progressString(progress: project.projectProgress)
        
        labelProgress.text = ("\(stringProgress)%")
        let cgFloat = RMProjectBasicListTableViewCell.floatPogress(stringProgress: project.projectProgress)
//        let cgFloat = RMProjectBasicListTableViewCell.floatPogress(stringProgress: "30")
        
//        circleProgress!.setProgress(cgFloat, animated: true)
        projectProgress.setProgress(Float(cgFloat), animated: true)
        /*
        labelProjectName.text = "Security Test and Analysis - Norton"
        labelPrice.text = "200$"
        labelCompanyName.text = "Robert Bosch GmbH private limited, India"
        labelDescription.text = RMDummy.lorem()*/
        self.contentView.layoutIfNeeded()
        self.contentView.layoutSubviews()
    }
    
}
