//
//  RMMilestoneListTableViewCell.swift
//  Repairman
//
//  Created by Shebin Koshy on 22/01/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMMilestoneListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewDummyForAutomaticDimension: UIView!
    @IBOutlet weak var constraintDescriptionLabelBottom: NSLayoutConstraint!
    var constraintDescriptionLabelBottomCopy: NSLayoutConstraint?
    //    milestoneUniqueId:String?
//    var milestoneTitle:String?
//    var projectUniqueId:String?
//    var milestoneDueDate:String?
//    var milestoneDescription:String?
//    var milestoneProgress:String?

//    @IBOutlet weak var imageViewUserAvatarImage: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDueDay: UILabel!
    @IBOutlet weak var labelDueMonth: UILabel!
    @IBOutlet weak var labelDueWeekDay: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelProgress: UILabel!
    @IBOutlet weak var viewEncloseProgress: UIView!
    @IBOutlet weak var viewEncloseDueDateLabels: UIView!
    
    var circleProgress : CircleProgressBar?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//assignConstraintDescriptionLabelBottom()
//        constraintDescriptionLabelBottom.isActive = false
//        constraintDescriptionLabelBottomCopy!.isActive = true
        circleProgress = CircleProgressBar(frame: CGRect(x:0, y:0, width:viewEncloseProgress.frame.size.width,height:viewEncloseProgress.frame.size.height))
        viewEncloseProgress.addSubview(circleProgress!)
        circleProgress!.progressBarWidth = 5
        circleProgress!.progressBarProgressColor = UIColor.green
        circleProgress!.progressBarTrackColor = UIColor.lightGray
        //        circleProgress.layer.borderColor = UIColor.red.cgColor
        //        circleProgress.layer.borderWidth = 2
        circleProgress!.setProgress(0.0, animated: true)
        circleProgress!.hintHidden = true
        circleProgress!.hintTextFont = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
        circleProgress!.startAngle = 180
        viewEncloseProgress.backgroundColor = UIColor.clear
        circleProgress!.backgroundColor = UIColor.clear
        labelProgress.text = "0%"
        labelProgress.textColor = UIColor.green
        
        
        viewEncloseDueDateLabels.layer.borderWidth = 1
        viewEncloseDueDateLabels.layer.borderColor = UIColor.lightGray.cgColor
        labelDueMonth.backgroundColor = UIColor.red
        labelDueMonth.textColor = UIColor.white
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func assignConstraintDescriptionLabelBottom() {
        if constraintDescriptionLabelBottomCopy != nil {
            return
        }
        constraintDescriptionLabelBottomCopy = NSLayoutConstraint(item: labelDescription, attribute: .bottom, relatedBy: .equal, toItem: labelDescription.superview, attribute: .bottom, multiplier: 1, constant: 8)
    }
    
    func setMilestone(milestone:RMMilestone) {
        let stringProgress = RMProjectBasicListTableViewCell.progressString(progress: milestone.milestoneProgress)
        
        labelProgress.text = ("\(stringProgress)%")
        
        let cgFloat = RMProjectBasicListTableViewCell.floatPogress(stringProgress: milestone.milestoneProgress)
        circleProgress!.setProgress(cgFloat, animated: true)
        
        labelTitle.text = milestone.milestoneTitle
        labelDescription.text = milestone.milestoneDescription
        labelDueMonth.text = ""
        labelDueWeekDay.text = ""
        labelDueDay.text = ""
        if milestone.milestoneDueDate != nil {
            
            let dueDate = RMGlobalManager.dateFromWebServiceString(stringOnlyDate: milestone.milestoneDueDate!)
            if dueDate == nil {
                return
            }
            let cal = Calendar(identifier: .gregorian)
            let arrayComponents = Set([Calendar.Component.weekday,Calendar.Component.day,Calendar.Component.month])
            let components = cal.dateComponents(arrayComponents, from: dueDate!)
            let month = components.month
            let week = components.weekday
            let day = components.day
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .full
            dateFormatter.timeStyle = .none
//            let dateString = formatter.stringFromDate(morningOfChristmas)
//            print("dateString : \(dateString)")
            
            
            //Current month - complete name
//            let dateFormatter: NSDateFormatter = NSDateFormatter()
            let months = dateFormatter.monthSymbols
            let monthSymbol = months![month!-1] as! String
            let weeks = dateFormatter.weekdaySymbols
            let weekSymbol = weeks![week!-1] as! String
            
            labelDueMonth.text = monthSymbol
            labelDueWeekDay.text = weekSymbol
            labelDueDay.text = "\(day!)"
        }
        
        
    }
}
