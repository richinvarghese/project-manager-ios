//
//  RMSubtitleTableViewCell.swift
//  Repairman
//
//  Created by Shebin Koshy on 21/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit

class RMSubtitleTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTop: UILabel!
    @IBOutlet weak var labelBottom: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setNote(note:RMNote) {
        self.labelBottom.textColor = UIColor.gray
        self.labelBottom.numberOfLines = 1
        self.labelBottom.textAlignment = .right
        self.labelBottom.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
        labelTop.text = note.noteTitle
        labelBottom.text = RMGlobalManager.stringFromDate(date:note.notePostedDate!,isWithTime: true)
    }

    
}
