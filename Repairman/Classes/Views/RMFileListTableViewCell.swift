//
//  RMFileListTableViewCell.swift
//  Repairman
//
//  Created by Shebin Koshy on 27/01/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMFileListTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewFile: UIImageView!
    @IBOutlet weak var labelFileDescription: UILabel!
    @IBOutlet weak var imageViewUploadedUserAvatar: UIImageView!
    @IBOutlet weak var labelUploadedUsername: UILabel!
    @IBOutlet weak var labelUploadedUserType: UILabel!
    @IBOutlet weak var labelCreatedDate: UILabel!
    @IBOutlet weak var buttonOpen: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageViewUploadedUserAvatar.layer.cornerRadius = imageViewUploadedUserAvatar.frame.size.width/2
        imageViewUploadedUserAvatar.clipsToBounds = true
        buttonOpen.backgroundColor = RMGlobalManager.appBlueColor()
        buttonOpen.setTitleColor(UIColor.white, for: .normal)
        buttonOpen.layer.cornerRadius = 2
        buttonOpen.clipsToBounds = true
        buttonOpen.layer.borderColor =  UIColor.blue.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setProjectFile(projectFile:RMProjectFile) {
        
        if projectFile.fileUploadedByUserImageUrl != nil {
            imageViewUploadedUserAvatar.sd_setImage(with: URL(string: projectFile.fileUploadedByUserImageUrl!), placeholderImage: UIImage(named: "AvatarPlaceholder"))
        } else {
            imageViewUploadedUserAvatar.image = UIImage(named: "AvatarPlaceholder")
        }
        
//        if projectFile.projectFileImageUrl != nil {
//            imageViewFile.sd_setImage(with: URL(string: projectFile.projectFileImageUrl!), placeholderImage: UIImage(named: "AvatarPlaceholder"))
//        } else {
//            imageViewFile.image = UIImage(named: "AvatarPlaceholder")
//        }
        
        labelFileDescription.text = projectFile.projectFileDescription
        labelUploadedUsername.text = projectFile.fileUploadedByUsername
        labelUploadedUserType.text = projectFile.fileUploadedByUserType
        
        labelCreatedDate.text = ""
        if projectFile.projectFileCreatedAt != nil {
            let date1 = RMGlobalManager.dateFromWebServiceString(stringDate: projectFile.projectFileCreatedAt!)
            if date1 != nil {
            labelCreatedDate.text = RMGlobalManager.shortDateAndTimeString(date: date1!)
            }
        }
        
//        labelFileDescription.text = RMDummy.lorem()
        
    }
    
}
