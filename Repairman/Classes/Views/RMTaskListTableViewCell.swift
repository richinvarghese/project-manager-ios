//
//  RMTaskListTableViewCell.swift
//  Repairman
//
//  Created by Shebin Koshy on 06/01/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

protocol RMTaskListTableViewCellDelegate: class {
    func buttonActionChangeStatus(cell:RMTaskListTableViewCell)
}

class RMTaskListTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTaskTitle: UILabel!
    @IBOutlet weak var labelTaskDescription: UILabel!
    @IBOutlet weak var buttonStatus: UIButton!
    
    @IBOutlet weak var labelAssignedUserName: UILabel!
    @IBOutlet weak var imageViewAssignedUserAvatar: UIImageView!
    @IBOutlet weak var buttonView: UIButton!
    @IBOutlet weak var buttonUpload: UIButton!
    weak var delegate:RMTaskListTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageViewAssignedUserAvatar.layer.cornerRadius = imageViewAssignedUserAvatar.frame.size.width/2
        imageViewAssignedUserAvatar.clipsToBounds = true
        buttonStatus.layer.cornerRadius = buttonStatus.frame.size.height/2
        buttonStatus.clipsToBounds = true
        buttonStatus.setTitleColor(UIColor.white, for: .normal)
//        buttonStatus.titleEdgeInsets = UIEdgeInsets(top: -0,
//                                                    left: -18,
//                                                    bottom: -0,
////                                                    right: -18)
        
//        if #available(iOS 8.2, *) {
//            labelTaskTitle.font = UIFont.systemFont(ofSize: labelTaskTitle.font.pointSize, weight: .medium)
//        } else {
//            // Fallback on earlier versions
//            labelTaskTitle.font = UIFont.boldSystemFont(ofSize: labelTaskTitle.font.pointSize)
//        }
//        labelTaskTitle.font = UIFont(name: "Helvetica-Semibold", size: labelTaskTitle.font.pointSize)
        setupButton(button: buttonView)
        setupButton(button: buttonUpload)
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupButton(button:UIButton) {
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 2
        button.clipsToBounds = true
        button.layer.borderColor =  UIColor.blue.cgColor
        button.backgroundColor = RMGlobalManager.appBlueColor()
    }
    
    @IBAction func buttonActionChangeStatus(_ sender: Any) {
        delegate?.buttonActionChangeStatus(cell: self)
    }
    
    func setTask(task:RMTask){
        labelTaskTitle.text = task.taskTitle
        labelTaskDescription.text = task.taskDescription
        
        buttonStatus.setTitle(task.taskStatus, for: .normal)
//        if #available(iOS 9.0, *) {
//            buttonStatus.widthAnchor.constraint(equalToConstant: buttonStatus.titleLabel!.intrinsicContentSize.width + 5 * 2.0).isActive = true
//        } else {
//            // Fallback on earlier versions
//        }
        
        
        buttonStatus.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        
//        buttonStatus.setTitleColor(task.taskStatusColor(), for: .normal)
//        buttonStatus.setba
        buttonStatus.backgroundColor = task.taskStatusColor()
        if task.taskAssignedUserAvatarImageUrl != nil {
            imageViewAssignedUserAvatar.sd_setImage(with: URL(string: task.taskAssignedUserAvatarImageUrl!), placeholderImage: UIImage(named: "AvatarPlaceholder"))
        } else {
            imageViewAssignedUserAvatar.image = UIImage(named: "AvatarPlaceholder")
        }
        labelAssignedUserName.text = task.taskAssignedUser
        
        
        //labelTaskDescription.text = RMDummy.lorem()
        
    }
    
}

class RMTaskStatusButton:UIButton {
    //override func drawText(in rect: CGRect) {
    //    let insets = UIEdgeInsets.init(top: 0, left: -150, bottom: 0, right: -15)
    //    super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    //}
    
    var textInsets = UIEdgeInsets.zero {
        didSet { invalidateIntrinsicContentSize() }
    }
    
    override func titleRect(forContentRect contentRect: CGRect) -> CGRect {
        let insetRect = UIEdgeInsetsInsetRect(bounds, textInsets)
        let textRect = super.titleRect(forContentRect: insetRect)
        let invertedInsets = UIEdgeInsets(top: -0,
                                          left: -8,
                                          bottom: -0,
                                          right: -8)
        return UIEdgeInsetsInsetRect(textRect, invertedInsets)
    }
    
//    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
//        let insetRect = UIEdgeInsetsInsetRect(bounds, textInsets)
//        let textRect = super.textRect(forBounds: insetRect, limitedToNumberOfLines: numberOfLines)
//        let invertedInsets = UIEdgeInsets(top: -0,
//                                          left: -8,
//                                          bottom: -0,
//                                          right: -8)
//        return UIEdgeInsetsInsetRect(textRect, invertedInsets)
//    }
    
    override func draw(_ rect: CGRect) {
        super.draw(UIEdgeInsetsInsetRect(rect, textInsets))
    }
    
//    override func drawText(in rect: CGRect) {
//        super.drawText(in: UIEdgeInsetsInsetRect(rect, textInsets))
//    }
}
