//
//  RMExpenseListTableViewCell.swift
//  Repairman
//
//  Created by Shebin Koshy on 24/03/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

protocol RMExpenseListTableViewCellDelegate: class {
    func buttonActionAttachment(cell:RMExpenseListTableViewCell)
}


class RMExpenseListTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonAttachment: UIButton!
    @IBOutlet weak var labelExpenseDescription: UILabel!
    @IBOutlet weak var labelExpenseAmount: UILabel!
    @IBOutlet weak var labelExpenseCategory: UILabel!
    @IBOutlet weak var labelExpenseCreatedDate: UILabel!
    weak var delegate: RMExpenseListTableViewCellDelegate?
    @IBOutlet weak var constraintAttachmentButtonHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelExpenseAmount.adjustsFontSizeToFitWidth = true
        
        labelExpenseCategory.backgroundColor = UIColor(red: 76/255, green: 176/255, blue: 147/255, alpha: 1)
//        labelExpenseCategory.backgroundColor = UIColor.orange
        labelExpenseCategory.layer.cornerRadius = 2
        labelExpenseCategory.clipsToBounds = true
        labelExpenseCategory.textColor = UIColor.white
        labelExpenseCreatedDate.backgroundColor = UIColor(red: 233/255, green: 176/255, blue: 96/255, alpha: 1)
        
//        UIColor(red: 233/255, green: 176/255, blue: 96/255, alpha: 1)
//        labelExpenseCreatedDate.backgroundColor = UIColor.orange
        labelExpenseCreatedDate.layer.cornerRadius = 2
        labelExpenseCreatedDate.clipsToBounds = true
        labelExpenseCreatedDate.textColor = UIColor.white
        
        setupButton(button: buttonAttachment)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupButton(button:UIButton) {
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 2
        button.clipsToBounds = true
        button.layer.borderColor =  UIColor.blue.cgColor
        button.backgroundColor = RMGlobalManager.appBlueColor()
    }
    
    @IBAction func buttonActionAttachment(_ sender: Any) {
        delegate?.buttonActionAttachment(cell: self)
    }
    
    func setExpense(expense:RMExpense){
        labelExpenseCreatedDate.text = ""
        labelExpenseCategory.text = ""
        labelExpenseDescription.text = ""
        if expense.expenseDate != nil {
            labelExpenseCreatedDate.text = "\(expense.expenseDate!)"
        } else {
            labelExpenseCreatedDate.text = ""
        }
        labelExpenseCategory.text = expense.expenseCategoryTitle
        labelExpenseDescription.text = expense.expenseDescription
        if expense.expenseAmount != nil {
            labelExpenseAmount.text = "Amount: \(expense.expenseAmount!)"
        }
        
        if expense.expenseDescription != nil {
            labelExpenseDescription.text = "Description: \(expense.expenseDescription!)"
        }
        
        if expense.expenseFilesLink == nil {
            buttonAttachment.isHidden = true
            constraintAttachmentButtonHeight.constant = 0;
        } else {
            buttonAttachment.isHidden = false
            constraintAttachmentButtonHeight.constant = 24;
        }
//        "expense_date": "2018-03-24",
//        "category_title": "Miscellaneous expense",
//        "title": "",
//        "description": "Project: OLWC\n Team member: Rahul Raj",
//        "files_link": "",
//        "amount": "₹89.00"
    }
    
}

