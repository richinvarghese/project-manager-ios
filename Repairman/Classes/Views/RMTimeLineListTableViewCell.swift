//
//  RMTimeLineListTableViewCell.swift
//  Repairman
//
//  Created by Shebin Koshy on 16/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit
import SDWebImage


protocol RMTimeLineListTableViewCellDelegate: class {
    func buttonActionReply(cell:RMTimeLineListTableViewCell)
    func buttonActionAttachment(cell:RMTimeLineListTableViewCell)
}

class RMTimeLineListTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonAttachment: UIButton!
    @IBOutlet weak var imageViewAvatar: UIImageView!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelTimeLinePostedTime: UILabel!
    @IBOutlet weak var labelTimeLineDescription: UILabel!
    @IBOutlet weak var constraintAttachmentButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintAttachmentButtonBottom: NSLayoutConstraint!
    var initialAttachmentButtonHeight : CGFloat?
    var initialAttachmentButtonBottom : CGFloat?
    weak var delegate: RMTimeLineListTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelTimeLineDescription.numberOfLines = 0
        imageViewAvatar.layer.cornerRadius = imageViewAvatar.frame.size.width/2
        imageViewAvatar.clipsToBounds = true
        initialAttachmentButtonBottom = constraintAttachmentButtonBottom.constant
        initialAttachmentButtonHeight = constraintAttachmentButtonHeight.constant
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func buttonActionReply(_ sender: Any) {
        delegate?.buttonActionReply(cell: self)
    }
    
    @IBAction func buttonActionAttachment(_ sender: Any) {
        delegate?.buttonActionAttachment(cell: self)
    }
    
    func setTimeLine(timeLine:RMTimeLine) {
        labelTimeLineDescription.text = timeLine.timeLineDescription
        labelUserName.text = timeLine.timeLineOwnerName
//        imageViewAvatar.sd_setImage(with: <#T##URL?#>) =
        if timeLine.timeLineOwnerImageURL != nil {
            imageViewAvatar.sd_setImage(with: URL(string: timeLine.timeLineOwnerImageURL!), placeholderImage: UIImage(named: "AvatarPlaceholder"))
        } else {
            imageViewAvatar.image = UIImage(named: "AvatarPlaceholder")
        }
//        let stringDate = RMGlobalManager.stringFromDate(date: note.notePostedDate!)
//        if stringDate.contains(RMStrings.TODAY_STRING()) == true || stringDate.contains(RMStrings.YESTERDAY_STRING()) == true || stringDate.contains(RMStrings.TOMORROW_STRING()) == true {
        if timeLine.timeLinePostedDate != nil {
            labelTimeLinePostedTime.text = RMGlobalManager.timeStringFromDate(date: timeLine.timeLinePostedDate!)
        } else {
            labelTimeLinePostedTime.text = ""
        }
//        } else {
//            labelNotePostedTime.text = stringDate
//        }
        
        if timeLine.attachmentType == nil {
            buttonAttachment.isHidden = true
            constraintAttachmentButtonBottom.constant = 0
            constraintAttachmentButtonHeight.constant = 0
            buttonAttachment.superview?.updateConstraints()
        } else {
            buttonAttachment.isHidden = false
            constraintAttachmentButtonBottom.constant = initialAttachmentButtonBottom!
            constraintAttachmentButtonHeight.constant = initialAttachmentButtonHeight!
            buttonAttachment.superview?.updateConstraints()
        }
        
    }
    
}
