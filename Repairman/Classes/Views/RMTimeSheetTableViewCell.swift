//
//  RMTimeTableViewCell.swift
//  Repairman
//
//  Created by Shebin Koshy on 04/02/18.
//  Copyright © 2018 Shebin Koshy. All rights reserved.
//

import UIKit

class RMTimeSheetTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelTotalTime: UILabel!
    @IBOutlet weak var labelEndTime: UILabel!
    @IBOutlet weak var labelStartTime: UILabel!
    @IBOutlet weak var labelTaskTitle: UILabel!
//    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
//    @IBOutlet weak var imageViewAvatar: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        imageViewAvatar.layer.cornerRadius = imageViewAvatar.frame.size.width/2
//        imageViewAvatar.clipsToBounds = true
        
        labelStatus.layer.cornerRadius = labelStatus.frame.size.height/2
        labelStatus.textAlignment = .center
        labelStatus.clipsToBounds = true
        labelStatus.textColor = UIColor.white
        labelStartTime.backgroundColor = UIColor.colorWithHexString(hex: "#91b53d")
        labelEndTime.backgroundColor = UIColor.red
        labelTotalTime.backgroundColor = UIColor.colorWithHexString(hex: "#ebb13d")
        labelStartTime.textColor = UIColor.white
        labelEndTime.textColor = UIColor.white
        labelTotalTime.textColor = UIColor.white
        labelTaskTitle.textColor = UIColor.red
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTimesheet(timesheet:RMTimesheet){
//        "id": "5",
//        "project_id": "1",
//        "user_id": "2",
//        "start_time": "2017-12-14 16:53:38",
//        "end_time": "2017-12-14 16:53:46",
//        "status": "logged",
//        "note": "",
//        "task_id": "2",
//        "deleted": "0",
//        "logged_by_user": "Rahul Raj",
//        "logged_by_avatar": "_file5a642d7a4d8bb-avatar.png",
//        "task_title": "Sample task",
//        "project_title": "Test project"
//        labelUsername.text = timesheet.timesheetUsername
        labelStatus.text = timesheet.timesheetStatusString()
        
        labelStatus.backgroundColor = timesheet.timesheetStatusColor()
        
        if timesheet.timesheetTaskTitle != nil {
            labelTaskTitle.text = "Task:\(timesheet.timesheetTaskTitle!)"
        } else {
            labelTaskTitle.text = "Task:"
        }
        
//        labelStartTime.text = ""
//        if timesheet.timesheetStartTime != nil {
//            let date1 = RMGlobalManager.dateFromWebServiceString(stringDate: timesheet.timesheetStartTime!)
//            if date1 != nil {
//            labelStartTime.text = RMGlobalManager.shortDateAndTimeString(date: date1!)
//            }
//        }
        
        labelStartTime.text = timesheet.timesheetStartTime
        
//        labelEndTime.text = ""
//        if timesheet.timesheetEndTime != nil {
//            let date1 = RMGlobalManager.dateFromWebServiceString(stringDate: timesheet.timesheetEndTime!)
//            if date1 != nil {
//            labelEndTime.text = RMGlobalManager.shortDateAndTimeString(date: date1!)
//            }
//        }
        labelEndTime.text = timesheet.timesheetEndTime
        
        
//        if timesheet.timesheetUserAvatarUrl != nil {
//            imageViewAvatar.sd_setImage(with: URL(string: timesheet.timesheetUserAvatarUrl!), placeholderImage: UIImage(named: "AvatarPlaceholder"))
//        } else {
//            imageViewAvatar.image = UIImage(named: "AvatarPlaceholder")
//        }
        
        labelTotalTime.text = ""
        if timesheet.timesheetTotalTime != nil {
            labelTotalTime.text = "Summary:\(timesheet.timesheetTotalTime!)"
        } else {
            labelTotalTime.text = "Summary:"
        }
        
    }
    
}
