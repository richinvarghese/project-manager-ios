//
//  RMSideMenuAndRoot.swift
//  Repairman
//
//  Created by Shebin Koshy on 20/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit
import SideMenu

class RMSideMenuAndRoot: NSObject {

//    static var eventVC : RMEventCalendarTableViewController?
    
    class func initialSetup() {
        SideMenuManager.menuFadeStatusBar = false
    }
    
    class func showLogin() {
        let loginVC = RMLoginTableViewController.instance()
        let nav = RMNavigationController(rootViewController: loginVC)
        RMAppDelegate.appDelegate().window?.rootViewController = nav
    }
    
//    class func showProject() {
//        let projectVC = RMProjectViewController.instance()
//        let nav = RMNavigationController(rootViewController: projectVC)
//        RMAppDelegate.appDelegate().window?.rootViewController = nav
//    }
    
    class func showDashboard() {
        var dashboardTableVC = RMAppDelegate.appDelegate().dashboard
        if dashboardTableVC == nil {
            dashboardTableVC = RMDashboardTableViewController.instance()
        }
        let nav = RMNavigationController(rootViewController: dashboardTableVC!)
        RMAppDelegate.appDelegate().window?.rootViewController = nav
        RMAppDelegate.appDelegate().dashboard = dashboardTableVC
    }
    
    class func showBasicProject() {
        let projectVC = RMProjectBasicListTableViewController(style:.plain)
        let nav = RMNavigationController(rootViewController: projectVC)
        RMAppDelegate.appDelegate().window?.rootViewController = nav
    }
    
    class func showBasicProject(navigationController:UINavigationController) {
        let projectVC = RMProjectBasicListTableViewController(style:.plain)
        navigationController.pushViewController(projectVC, animated: true)
//        let nav = RMNavigationController(rootViewController: projectVC)
//        RMAppDelegate.appDelegate().window?.rootViewController = nav
    }
    
    
    class func showTimeLine() {
        let nav = timeLineListTableVC()
        RMAppDelegate.appDelegate().window?.rootViewController = nav
    }
    
    
    class func showTimeLine(navigationController:UINavigationController) {
        let timeLineListTableVC = RMTimeLineListTableViewController()
        navigationController.pushViewController(timeLineListTableVC, animated: true)
//        RMAppDelegate.appDelegate().window?.rootViewController = nav
    }
    
    
    class func showNote() {
        let nav = RMNavigationController(rootViewController: RMNoteListTableViewController())
        RMAppDelegate.appDelegate().window?.rootViewController = nav
    }
    
    
    class func showNote(navigationController:UINavigationController) {
        let note = RMNoteListTableViewController()
        navigationController.pushViewController(note, animated: true)
    }
    
    
    
    class func showTimeCard() {
        let timecardVC = RMTimeCardListViewController.instance()
        let nav = RMNavigationController(rootViewController: timecardVC)
        RMAppDelegate.appDelegate().window?.rootViewController = nav
    }
    
    class func showTimeCard(navigationController:UINavigationController) {
        
//       RMTimeCardListViewController.instance()
        navigationController.pushViewController(RMTimeCardListViewController.instance(), animated: true)
        
//        let nav = RMNavigationController(rootViewController: RMTimeCardListTableViewController())
//        RMAppDelegate.appDelegate().window?.rootViewController = nav
    }
    
    class func showAnnouncements() {
        let announcements = RMAnnouncementListTableViewController()
        let nav = RMNavigationController(rootViewController: announcements)
        RMAppDelegate.appDelegate().window?.rootViewController = nav
    }
    
    class func showAnnouncements(navigationController:UINavigationController) {
        
        navigationController.pushViewController(RMAnnouncementListTableViewController(), animated: true)
        
//        let nav = RMNavigationController(rootViewController: RMAnnouncementListTableViewController())
//        RMAppDelegate.appDelegate().window?.rootViewController = nav
    }
    
    
    class func showMessages() {
        let messageVC = RMMessageViewController.instance()
        let nav = RMNavigationController(rootViewController: messageVC)
        RMAppDelegate.appDelegate().window?.rootViewController = nav
    }
    
    class func showMessages(navigationController:UINavigationController) {
        
        navigationController.pushViewController(RMMessageViewController.instance(), animated: true)
        
        //        let nav = RMNavigationController(rootViewController: RMAnnouncementListTableViewController())
        //        RMAppDelegate.appDelegate().window?.rootViewController = nav
    }
    
    
    class func showLeave() {
        let leaveVC = RMLeaveListTableViewController(style: .plain)
        let nav = RMNavigationController(rootViewController: leaveVC)
        RMAppDelegate.appDelegate().window?.rootViewController = nav
    }
    
    class func showLeave(navigationController:UINavigationController) {
        
        navigationController.pushViewController(RMLeaveListTableViewController(style: .plain), animated: true)
        
        //        let nav = RMNavigationController(rootViewController: RMAnnouncementListTableViewController())
        //        RMAppDelegate.appDelegate().window?.rootViewController = nav
    }
    
    
    class func showEvent(navigationController:UINavigationController) {
        let eventVC = RMEventCalendarTableViewController.instance()
        
        navigationController.pushViewController(eventVC, animated: true)
        
//        let nav = RMNavigationController(rootViewController: eventVC)
//        RMAppDelegate.appDelegate().window?.rootViewController = nav
    }
    
    class func showEvent() {
        let eventVC = RMEventCalendarTableViewController.instance()
        let nav = RMNavigationController(rootViewController: eventVC)
        RMAppDelegate.appDelegate().window?.rootViewController = nav
    }
    
    class func showLeftMenu() {
        if SideMenuManager.menuLeftNavigationController == nil {
            let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: RMLeftSideTableViewController.instance())
//            menuLeftNavigationController.
//            menuLeftNavigationController.menuWidth = 270
//            RMLeftSideTableViewController.me
            SideMenuManager.menuLeftNavigationController = menuLeftNavigationController
        }
        RMAppDelegate.appDelegate().window!.rootViewController!.present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    
    /*class func showRightMenu() {
        if SideMenuManager.default.menuRightNavigationController == nil {
            let menuRightNavigationController = UISideMenuNavigationController(rootViewController: RightTableViewController())
            menuRightNavigationController.menuWidth = 100
            SideMenuManager.default.menuRightNavigationController = menuRightNavigationController
        }
        appDelegate().window!.rootViewController!.present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }*/

    
//    func showLeftMenu(showInViewController:UIViewController) {
//        
//        showInViewController.present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
//    }
    
    class func timeLineListTableVC() -> UINavigationController {
        let addTimeLineTableVC = RMAddTimeLineTableViewController.instance(selectedTimeLine: nil, replyToTimeLine: nil)
        let timeLineListTableVC = RMTimeLineListTableViewController()
        let navController = RMNavigationController(rootViewController: timeLineListTableVC)
        return navController
    }
    
    fileprivate class func deallocLeftMenu() {
        SideMenuManager.menuLeftNavigationController = nil
    }
    
    fileprivate class func deallocRightMenu() {
        SideMenuManager.menuRightNavigationController = nil
    }
    
    deinit {
        print("\(self) deinit")
    }
    
}

extension UIViewController {
    
    public func sideMenuWillAppear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Appearing! (animated: \(animated))")
    }
    
    public func sideMenuDidAppear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Appeared! (animated: \(animated))")
    }
    
    public func sideMenuWillDisappear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappearing! (animated: \(animated))")
    }
    
    public func sideMenuDidDisappear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappeared! (animated: \(animated))")
        if menu == SideMenuManager.menuLeftNavigationController {
            RMSideMenuAndRoot.deallocLeftMenu()
        } else if menu == SideMenuManager.menuRightNavigationController {
            RMSideMenuAndRoot.deallocRightMenu()
        }
    }
    
    
    
}

