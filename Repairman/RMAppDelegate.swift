//
//  AppDelegate.swift
//  Repairman
//
//  Created by Shebin Koshy on 15/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import Firebase
import UserNotifications
//import Messages
//import CoreData
//import SideMenu

@UIApplicationMain
class RMAppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var lastBackLocationUpdateDate:Date?
    var dashboard: RMDashboardTableViewController?

    class func locationBackgroundCall(){
        let loc = RMBackgroundLocationManager.sharedInstance
        
        loc.addDelegate(delegate: UIApplication.shared.delegate as! RMBackgroundLocationDelegate)
        loc.fetchCurrentLocation()
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        Fabric.sharedSDK().debug = true
        registerPush(application: application)
        Messaging.messaging().delegate = self
        //        Fabric.sharedSDK().debug = true
//        Fabric.with([Crashlytics.self])
        
//        window!.makeKey()
        // Override point ofor customization after application launch.
//        UIViewController.swizzleViewDidLoad()
        RMLocalNotification.requestAuthorization()
        
        let locationNotif = launchOptions?[UIApplicationLaunchOptionsKey.location] as? NSDictionary
        if locationNotif != nil {
//            RMLocalNotification.showLocalNotification(text: "ssssere")
        }
        RMAppDelegate.locationBackgroundCall()

        window = UIWindow.init(frame: UIScreen.main.bounds)
        window?.backgroundColor = UIColor.white
        window?.tintColor = UIColor.magenta
        window?.tintColor = UIColor(red: 76, green: 134, blue: 169)
        window?.tintColor = RMGlobalManager.appBlueColor()
//        
////        let tabBarController = SWTabBarController.instance()
////        let selectedIndex = SWUserDefaultManager.getTabBarControllerSelectedIndex() ?? 0
////        tabBarController.selectedIndex = selectedIndex
////        window?.rootViewController = tabBarController
////        UISideMenuNa
//        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: RMLeftSideTableViewController.instance())
//        // UISideMenuNavigationController is a subclass of UINavigationController, so do any additional configuration
//        // of it here like setting its viewControllers. If you're using storyboards, you'll want to do something like:
//        // let menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as! UISideMenuNavigationController
//        SideMenuManager.menuLeftNavigationController = menuLeftNavigationController
////        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
//        
////        let menuRightNavigationController = UISideMenuNavigationController(rootViewController: YourViewController)
//        // UISideMenuNavigationController is a subclass of UINavigationController, so do any additional configuration
//        // of it here like setting its viewControllers. If you're using storyboards, you'll want to do something like:
//        // let menuRightNavigationController = storyboard!.instantiateViewController(withIdentifier: "RightMenuNavigationController") as! UISideMenuNavigationController
////        SideMenuManager.default.menuRightNavigationController = menuRightNavigationController
//        
//        // Enable gestures. The left and/or right menus must be set up above for these to work.
//        // Note that these continue to work on the Navigation Controller independent of the view controller it displays!
////        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
////        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
//        
//        
//        let addTimeLineTableVC = RMAddTimeLineTableViewController.instance(selectedTimeLine: nil, replyToTimeLine: nil)
//        let timeLineListTableVC = RMTimeLineListTableViewController()
//        let navController = UINavigationController(rootViewController: timeLineListTableVC)
//        window?.rootViewController = UISideMenuNavigationController(rootViewController: timeLineListTableVC)
//        window!.rootViewController =
        RMSideMenuAndRoot.initialSetup()
        
        RMSideMenuAndRoot.showNote()
        RMSideMenuAndRoot.showEvent()
        
        if RMUserDefaultManager.getCurrentUserUniqueId() == nil {
            RMSideMenuAndRoot.showLogin()
        } else {
            RMSideMenuAndRoot.showDashboard()
//            RMSideMenuAndRoot.showBasicProject()
        }
        
//        RMSideMenuAndRoot.showTimeLine()
        window?.makeKeyAndVisible()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
//        RMLocalNotification.showLocalNotification(text: "poda")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        if window == nil || window?.rootViewController == nil {
            return
        }
        if RMNetworkReachabilityManager.sharedInstance.isNetworkReachable() == false {

            RMNetworkReachabilityManager.showNetworkNotReachableAlert(showInViewController: window!.rootViewController!)
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
//        self.saveContext()
    }
    
    class func dashboardInstance() -> RMDashboardTableViewController {
//        let appObj = UIApplication.shared.delegate as! RMAppDelegate
//        let nav = appObj.window!.rootViewController! as! UINavigationController
//        let dash = nav.viewControllers.first as! RMDashboardTableViewController
//        return dash
        return RMAppDelegate.appDelegate().dashboard!
    }
    
    class func firebaseSubscribe(topic:String) {
        Messaging.messaging().subscribe(toTopic: topic)
//        [[FIRMessaging messaging] subscribeToTopic:@"news"];

    }
    
    class func firebaseUnSubscribe(topic:String) {
        Messaging.messaging().unsubscribe(fromTopic: topic)
        //        [[FIRMessaging messaging] subscribeToTopic:@"news"];
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
//        if let messageID = userInfo[gcmMessageIDKey] {
//            print("Message ID: \(messageID)")
//        }
        gotPush(userInfo: userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//        if let messageID = userInfo[gcmMessageIDKey] {
//            print("Message ID: \(messageID)")
//        }
        gotPush(userInfo: userInfo)
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func gotPush(userInfo: [AnyHashable : Any]) {
//        print(userInfo)
        
//        {
//            "aps" : {
//                "alert" : {
//                    "body" : "great match!",
//                    "title" : "Portugal vs. Denmark",
//                },
//                "badge" : 1,
//            },
//            "customKey" : "customValue"
//        }
        guard let aps = userInfo["aps"] as? [String: Any] else {
            return;
        }
        
        guard let alert = aps["alert"] as? String else {
            return;
        }
//        guard let body = alert["body"] else {
//            return;
//        }
//        guard let title = alert["title"] else {
//            return;
//        }
        let banner = Banner(title: alert, subtitle: nil, image: nil, backgroundColor: UIColor.black)
        banner.show(duration: 3.0)
    }
    
    func registerPush(application:UIApplication) {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }


//    // MARK: - Core Data stack
//
//    lazy var persistentContainer: NSPersistentContainer = {
//        /*
//         The persistent container for the application. This implementation
//         creates and returns a container, having loaded the store for the
//         application to it. This property is optional since there are legitimate
//         error conditions that could cause the creation of the store to fail.
//        */
//        let container = NSPersistentContainer(name: "Repairman")
//        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
//            if let error = error as NSError? {
//                // Replace this implementation with code to handle the error appropriately.
//                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//                 
//                /*
//                 Typical reasons for an error here include:
//                 * The parent directory does not exist, cannot be created, or disallows writing.
//                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
//                 * The device is out of space.
//                 * The store could not be migrated to the current model version.
//                 Check the error message to determine what the actual problem was.
//                 */
//                fatalError("Unresolved error \(error), \(error.userInfo)")
//            }
//        })
//        return container
//    }()
//
//    // MARK: - Core Data Saving support
//
//    func saveContext () {
//        let context = persistentContainer.viewContext
//        if context.hasChanges {
//            do {
//                try context.save()
//            } catch {
//                // Replace this implementation with code to handle the error appropriately.
//                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//                let nserror = error as NSError
//                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
//            }
//        }
//    }
    
    
    

    class func appDelegate() -> RMAppDelegate {
        return UIApplication.shared.delegate! as! RMAppDelegate
    }
}
extension RMAppDelegate : RMBackgroundLocationDelegate{
    
    @objc func backgroundLocationDidUpdateLocations(backgroundLocation:RMBackgroundLocationManager, currentLatitude:Double, currentLongitude:Double){
        
        var elapsed : Double!
        if lastBackLocationUpdateDate != nil {
            elapsed = Date().timeIntervalSince(lastBackLocationUpdateDate!)
        } else {
            elapsed = 0
        }

        if UIApplication.shared.applicationState != .active {
            //            saveAddressName(lat: currentLatitude, long: currentLongitude)
//            RMLocalNotification.showLocalNotification(text: "appOpenWithLocationCallBack")
            RMLocationWebservice.so(currentLatitude: currentLatitude, currentLongitude: currentLongitude)
            lastBackLocationUpdateDate = backgroundLocation.locationManager?.location?.timestamp
        } else if elapsed > (60*5) || elapsed == 0 {
            RMLocationWebservice.so(currentLatitude: currentLatitude, currentLongitude: currentLongitude)
            lastBackLocationUpdateDate = backgroundLocation.locationManager?.location?.timestamp
        }
    }
    
    @objc func backgroundLocationAuthorizationStateChanged(backgroundLocation:RMBackgroundLocationManager){
        
    }
    
}

extension RMAppDelegate: UNUserNotificationCenterDelegate {
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
}

extension RMAppDelegate: MessagingDelegate {
    
}

