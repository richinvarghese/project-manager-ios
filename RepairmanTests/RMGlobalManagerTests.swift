//
//  RMGlobalManagerTests.swift
//  Repairman
//
//  Created by Shebin Koshy on 16/12/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import XCTest
@testable import Repairman

class RMGlobalManagerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testStringFromDate() {
        let date = Date()
        let stringDate = RMGlobalManager.stringFromDate(date: date, isWithTime: true)
        
        print(stringDate)
    }
    
    func testTimeStringFromDate() {
        let date = Date()
        let stringDate = RMGlobalManager.timeStringFromDate(date:date)
        print("\(stringDate)fdsf")
    }
    
    
    func testValidateEmail() {
        let email = "a@a"
        let result = RMGlobalManager.validateEmail(enteredEmail: email)
        XCTAssertEqual(result, false, "\(email)=WWWWwrong")
        
        let email2 = "a@a.a"
        let result2 = RMGlobalManager.validateEmail(enteredEmail: email2)
        XCTAssertEqual(result2, false, "\(email2)=WWWWwrong")
        
        
        let email3 = "a@a"
        let result3 = RMGlobalManager.validateEmail(enteredEmail: email3)
        XCTAssertEqual(result3, false, "\(email3)=WWWWwrong")
        
        
        let email4 = "a@a@w.f"
        let result4 = RMGlobalManager.validateEmail(enteredEmail: email4)
        XCTAssertEqual(result4, false, "\(email4)=WWWWwrong")
        
        
        let email5 = "aa@aa.aa."
        let result5 = RMGlobalManager.validateEmail(enteredEmail: email5)
        XCTAssertEqual(result5, false, "\(email5)=WWWWwrong")
        
        let email6 = "aa@aa.aa.tt"
        let result6 = RMGlobalManager.validateEmail(enteredEmail: email6)
        XCTAssertEqual(result6, true, "\(email6)=WWWWwrong")
    }
    
    
}
